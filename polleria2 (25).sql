-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-12-2018 a las 16:16:13
-- Versión del servidor: 10.1.33-MariaDB
-- Versión de PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `polleria2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acceso`
--

CREATE TABLE `acceso` (
  `IdAcceso` int(5) NOT NULL,
  `AcceNombre` varchar(40) NOT NULL,
  `accEstReg` int(1) NOT NULL,
  `AccSerializacion` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `acceso`
--

INSERT INTO `acceso` (`IdAcceso`, `AcceNombre`, `accEstReg`, `AccSerializacion`) VALUES
(1, 'GestionarUsuario', 1, ''),
(2, 'UnidadMedida', 1, ''),
(3, 'Articulo', 1, ''),
(4, 'Tipo Articulo', 1, ''),
(5, 'Lista', 1, ''),
(6, 'EmpresaGrupo', 1, ''),
(7, 'Documentos', 1, ''),
(8, 'Comprobantes', 1, ''),
(9, 'Formulas', 1, ''),
(10, 'Compras', 1, ''),
(11, 'Ventas', 1, ''),
(12, 'Produccion', 1, ''),
(13, 'Movimiento', 1, ''),
(14, 'Nivelacion Produccion', 1, ''),
(15, 'Modificar Comprobante', 1, ''),
(16, 'kardex', 1, ''),
(17, 'ventas/compras', 1, ''),
(18, 'arqueo caja', 1, ''),
(19, 'Reporte Diario', 1, ''),
(20, 'Enviar SUNAT', 1, ''),
(21, 'Enviar CENTRAL', 1, ''),
(22, 'Enviar Lista', 1, ''),
(23, 'Recibir CENTRAL', 1, ''),
(24, 'Recibir Lista', 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivo`
--

CREATE TABLE `archivo` (
  `IdArchivo` int(5) NOT NULL,
  `IdSede` int(5) NOT NULL,
  `NombreArchivo` varchar(100) NOT NULL,
  `CodigoArchivo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `archivo`
--

INSERT INTO `archivo` (`IdArchivo`, `IdSede`, `NombreArchivo`, `CodigoArchivo`) VALUES
(1, 1, 'DATA_ENVIO (34).gce', 'e3010a82-786d-4d3a-91cb-ea2fe80d7e0f'),
(1, 1, 'DATA_ENVIO (34).gce', 'e3010a82-786d-4d3a-91cb-ea2fe80d7e0f');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE `articulo` (
  `IdArticulo` int(11) NOT NULL,
  `IdUnidadMedida` int(11) DEFAULT NULL,
  `ArtNom` char(60) DEFAULT NULL,
  `ArtCos` double DEFAULT NULL,
  `ArtIgv` double DEFAULT NULL,
  `ArtCat` int(5) NOT NULL,
  `ArtSubCat` int(2) NOT NULL DEFAULT '0',
  `ArtEstReg` int(1) DEFAULT '1',
  `ArtSerializacion` varchar(50) NOT NULL DEFAULT '',
  `ArtCol` int(7) NOT NULL DEFAULT '6750054'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`IdArticulo`, `IdUnidadMedida`, `ArtNom`, `ArtCos`, `ArtIgv`, `ArtCat`, `ArtSubCat`, `ArtEstReg`, `ArtSerializacion`, `ArtCol`) VALUES
(0, NULL, '0', 1, 1, 0, 0, 0, '1', 6750054),
(1, 7, 'Aceite Fritura Intensa x 20 L', 0, 0.18, 1, 0, 1, '', 6750054),
(2, 7, 'Aceite Multiusos x 20 L', 0, 0.18, 1, 0, 1, '', 6750054),
(3, 8, 'Aceite Multiusos x 1 Litro', 0, 0.18, 1, 0, 1, '', 6750054),
(4, 1, '07 cereales', 0, 0.18, 1, 0, 1, '', 6750054),
(5, 1, 'Api a granel', 0, 0.18, 1, 0, 1, '', 6750054),
(6, 1, 'Arroz a granel', 0, 0, 1, 0, 1, '', 6750054),
(7, 7, 'Arroz embolsado SUREÑO x 750 gr', 0, 0.18, 1, 0, 1, '', 6750054),
(8, 1, 'Avena', 0, 0.18, 1, 0, 1, '', 6750054),
(9, 1, 'Azucar Blanca', 0, 0.18, 1, 0, 1, '', 6750054),
(10, 1, 'Azucar Rubia', 0, 0.18, 1, 0, 1, '', 6750054),
(11, 7, 'Café Instantáneo', 0, 0.18, 1, 0, 1, '', 6750054),
(12, 1, 'Cebada Tostada', 0, 0.18, 1, 0, 1, '', 6750054),
(13, 1, 'Cemola a granel', 0, 0.18, 1, 0, 1, '', 6750054),
(14, 1, 'Chalona', 0, 0.18, 1, 0, 1, '', 6750054),
(15, 1, 'Cochayuyo', 0, 0.18, 1, 0, 1, '', 6750054),
(16, 7, 'Cocoa', 0, 0.18, 1, 0, 1, '', 6750054),
(17, 7, 'Fideos Cabello de Ángel x 250 gr', 0, 0.18, 1, 0, 1, '', 6750054),
(18, 7, 'Fideos Spagueti x 500gr', 0, 0.18, 1, 0, 1, '', 6750054),
(19, 1, 'Frejol', 0, 0.18, 1, 0, 1, '', 6750054),
(20, 1, 'Gelatina a granel', 0, 0.18, 1, 0, 1, '', 6750054),
(21, 1, 'Harina a granel sin preparar', 0, 0.18, 1, 0, 1, '', 6750054),
(22, 7, 'Leche BONLE (Caja x 500gr)', 0, 0.18, 1, 0, 1, '', 6750054),
(23, 7, 'Leche PURA VIDA (Lata x 400gr)', 0, 0.18, 1, 0, 1, '', 6750054),
(24, 1, 'Linaza a granel', 0, 0.18, 1, 0, 1, '', 6750054),
(25, 1, 'Maicena a granel', 0, 0.18, 1, 0, 1, '', 6750054),
(26, 1, 'Mandioca a granel', 0, 0.18, 1, 0, 1, '', 6750054),
(27, 1, 'Mani a granel', 0, 0.18, 1, 0, 1, '', 6750054),
(28, 7, 'Mayonesa base MACBEL (Bolsa x 2L)', 0, 0.18, 1, 0, 1, '', 6750054),
(29, 7, 'Mazamorra NEGRITA x 160gr', 0, 0.18, 1, 0, 1, '', 6750054),
(30, 7, 'Mermelada x 900 gramos', 0, 0.18, 1, 0, 1, '', 6750054),
(31, 1, 'Quinua a granel', 0, 0.18, 1, 0, 1, '', 6750054),
(32, 7, 'Sal de mesa MARINA x 1.0 Kg', 0, 0.18, 1, 0, 1, '', 6750054),
(33, 7, 'Sal de cocina MARINA x 1.0 Kg', 0, 0.18, 1, 0, 1, '', 6750054),
(34, 7, 'Sillao Salsa Soya AJINOSILLAO x 5L', 0, 0.18, 1, 0, 1, '', 6750054),
(35, 1, 'Soya molida a granel', 0, 0.18, 1, 0, 1, '', 6750054),
(36, 7, 'Té Filtrante (Caja x 25 und)', 0, 0.18, 1, 0, 1, '', 6750054),
(37, 7, 'Trozos de atun x 165 gr', 0, 0.18, 1, 0, 1, '', 6750054),
(38, 7, 'Vinagre blanco DEL FIRME x 1.1L', 0, 0.18, 1, 0, 1, '', 6750054),
(39, 7, 'Vinagre tinto DEL FIRME x 1.1L', 0, 0.18, 1, 0, 1, '', 6750054),
(40, 7, 'Sillao Salsa Soya AJINOSILLAO x 1L', 0, 0.18, 1, 0, 1, '', 6750054),
(41, 1, 'Sillao x 1 L Mantequilla Barra de 1 Kg', 0, 0.18, 1, 0, 1, '', 6750054),
(42, 7, 'Manjar x 1 Kg', 0, 0.18, 1, 0, 1, '', 6750054),
(43, 1, 'Lenteja', 0, 0.18, 1, 0, 1, '', 6750054),
(44, 7, 'Ambientador sapolio aereosol x 360ml', 0, 0.18, 2, 0, 1, '', 6750054),
(45, 7, 'Botador de agua VIRUTEX 40cm', 0, 0.18, 2, 0, 1, '', 6750054),
(46, 7, 'Crema para cuero y vinilo', 0, 0.18, 2, 0, 1, '', 6750054),
(47, 7, 'Destapador de baños (sin marca)', 0, 0.18, 2, 0, 1, '', 6750054),
(48, 1, 'Detergente a granel', 0, 0.18, 2, 0, 1, '', 6750054),
(49, 7, 'Escobas', 0, 0.18, 2, 0, 1, '', 6750054),
(50, 7, 'Escobas', 0, 0.18, 2, 0, 1, '', 6750054),
(51, 7, 'Escobas', 0, 0.18, 2, 0, 1, '', 6750054),
(52, 7, 'Escobilla para piso pequeños sin marca', 0, 0.18, 2, 0, 1, '', 6750054),
(53, 7, 'Esponja para limpieza CARMELITA', 0, 0.18, 2, 0, 1, '', 6750054),
(54, 7, 'Estropajo para limpieza CARMELITA', 0, 0.18, 2, 0, 1, '', 6750054),
(55, 7, 'Fosforos (Paq x 10 Und)', 0, 0.18, 2, 0, 1, '', 6750054),
(56, 7, 'Guantes de jebe para horno', 0, 0.18, 2, 0, 1, '', 6750054),
(57, 7, 'Guantes de Jebe para limpieza', 0, 0.18, 2, 0, 1, '', 6750054),
(58, 7, 'Guantes descartables de latex (Caja x100und)', 0, 0.18, 2, 0, 1, '', 6750054),
(59, 7, 'Jabon Liquido MYC x 01 galón', 0, 0.18, 2, 0, 1, '', 6750054),
(60, 7, 'Jabon Liquido x aprox 500ml (Envasado)', 0, 0.18, 2, 0, 1, '', 6750054),
(61, 7, 'Lava vajillas sapolio x 900gr', 0, 0.18, 2, 0, 1, '', 6750054),
(62, 7, 'Lejia CLOROX x 639ml', 0, 0.18, 2, 0, 1, '', 6750054),
(63, 7, 'Lejia cojin SHIROI x 127gramos', 0, 0.18, 2, 0, 1, '', 6750054),
(64, 7, 'Limpia vidrios MYC x 01 galón', 0, 0.18, 2, 0, 1, '', 6750054),
(65, 7, 'Mandiles de produccion', 0, 0.18, 2, 0, 1, '', 6750054),
(66, 7, 'Mascarilla descartable (Caja x 50und)', 0, 0.18, 2, 0, 1, '', 6750054),
(67, 7, 'Matamoscas sapolio aereosol x 360ml', 0, 0.18, 2, 0, 1, '', 6750054),
(68, 7, 'Mondadientes (Paq x 100 und)', 0, 0.18, 2, 0, 1, '', 6750054),
(69, 7, 'Pabillo DEL FIRME (En cono)', 0, 0.18, 2, 0, 1, '', 6750054),
(70, 7, 'Papel Higienico IDEAL (Rollo x 500m)', 0, 0.18, 2, 0, 1, '', 6750054),
(71, 7, 'Papel toalla de una hoja ELITE (Rollo x 200m)', 0, 0.18, 2, 0, 1, '', 6750054),
(72, 7, 'Recogedores domesticos de basura (Sin marca)', 0, 0.18, 2, 0, 1, '', 6750054),
(73, 7, 'Red de derrames y manchas TELAGRARD', 0, 0.18, 2, 0, 1, '', 6750054),
(74, 7, 'Secadores sin marca', 0, 0.18, 2, 0, 1, '', 6750054),
(75, 7, 'Servilletas cortadas LIDER (Paq x 100 Und)', 0, 0.18, 2, 0, 1, '', 6750054),
(76, 7, 'Tenedores descartables salchipaperas  Paq. X100und', 0, 0.18, 2, 0, 1, '', 6750054),
(77, 7, 'Tocas (Gorros) descartable (Caja x100und)', 0, 0.18, 2, 0, 1, '', 6750054),
(78, 7, 'Trapeador de paño para piso Virutex 50x85cm', 0, 0.18, 2, 0, 1, '', 6750054),
(79, 7, 'Gel limpiamanos x 1 Galón', 0, 0.18, 2, 0, 1, '', 6750054),
(80, 1, 'Soda Caústica', 0, 0.18, 2, 0, 1, '', 6750054),
(81, 7, 'Franela Amarilla de 1m x 0.60m aprox', 0, 0.18, 2, 0, 1, '', 6750054),
(82, 7, 'Cerveza Negra CUSQUEÑA x 330ml', 0, 0.18, 1, 0, 1, '', 6750054),
(83, 7, 'Agua San Luis 600 ml', 0, 0.18, 3, 0, 1, '', 6750054),
(84, 7, 'Coca Cola x 296ml (Retornable) Personal', 0, 0.18, 3, 0, 1, '', 6750054),
(85, 7, 'Coca Cola x 500ml (No retornable)', 0, 0.18, 3, 0, 1, '', 6750054),
(86, 7, 'Coca Cola Zero x 500ml (No retornable)', 0, 0.18, 3, 0, 1, '', 6750054),
(87, 7, 'Coca Cola x 1.0L (Retornable)', 0, 0.18, 3, 0, 1, '', 6750054),
(88, 7, 'Coca Cola x 2.25L (No retornable)', 0, 0.18, 3, 0, 1, '', 6750054),
(89, 7, 'Escocesa x 296ml (Retornable) Personal', 0, 0.18, 3, 0, 1, '', 6750054),
(90, 7, 'Escocesa x 600ml (No retornable)', 0, 0.18, 3, 0, 1, '', 6750054),
(91, 7, 'Escocesa x 1.0L (Retornable)', 0, 0.18, 3, 0, 1, '', 6750054),
(92, 7, 'Escocesa x 1.5L (No retornable)', 0, 0.18, 3, 0, 1, '', 6750054),
(93, 7, 'Inca Kola x 296ml (Retornable) Personal', 0, 0.18, 3, 0, 1, '', 6750054),
(94, 7, 'Inca Kola x 500ml (No retornable)', 0, 0.18, 3, 0, 1, '', 6750054),
(95, 7, 'Inca Kola x 1.0L (Retornable)', 0, 0.18, 3, 0, 1, '', 6750054),
(96, 7, 'Inca Kola x 2.25L (No retornable)', 0, 0.18, 3, 0, 1, '', 6750054),
(97, 7, 'Sprite x 296ml (Retornable) Personal', 0, 0.18, 3, 0, 1, '', 6750054),
(98, 7, 'Fanta x 3.0 L (No retornable)', 0, 0.18, 3, 0, 1, '', 6750054),
(99, 1, 'Carbon Vegetal', 0, 0.18, 4, 0, 1, '', 6750054),
(100, 1, 'Caparina', 0, 0, 5, 0, 1, '', 6750054),
(101, 1, 'Chuleta de cerdo', 0, 0.18, 5, 0, 1, '', 6750054),
(102, 1, 'Chuleta de cordero', 0, 0.18, 5, 0, 1, '', 6750054),
(103, 1, 'Chuleta de res', 0, 0.18, 5, 0, 1, '', 6750054),
(104, 1, 'Huevo fresco', 0, 0.18, 1, 0, 1, '', 6750054),
(105, 1, 'Menudo (Patas y cuellos, corazon, higado)', 0, 0.18, 5, 0, 1, '', 6750054),
(106, 1, 'Mollejitas (Paq. x 10 Und) p/parrilla', 0, 0.18, 5, 0, 1, '', 6750054),
(107, 1, 'Mollejitas (Paq. x 8 Und) p/mollejitas', 0, 0.18, 5, 0, 1, '', 6750054),
(108, 1, 'Mollejitas', 0, 0.18, 5, 0, 1, '', 6750054),
(109, 1, 'Panza (Paq x 5 und)', 0, 0, 5, 0, 1, '', 6750054),
(110, 7, 'Pollo fresco', 0, 0.18, 5, 0, 1, '', 6750054),
(111, 7, 'Pollo aderezado', 0, 0.18, 5, 0, 1, '', 6750054),
(112, 7, 'Pollo cocinado', 0, 0.18, 5, 0, 1, '', 6750054),
(113, 1, 'Ubre (Paq x 5 und)', 0, 0, 5, 0, 1, '', 6750054),
(114, 1, 'Lomo Fino', 0, 0.18, 5, 0, 1, '', 6750054),
(115, 1, 'Ajinomoto', 0, 0.18, 6, 0, 1, '', 6750054),
(116, 1, 'Canela entera', 0, 0.18, 6, 0, 1, '', 6750054),
(117, 1, 'Clavo de olor', 0, 0.18, 6, 0, 1, '', 6750054),
(118, 1, 'Comino molido', 0, 0.18, 6, 0, 1, '', 6750054),
(119, 1, 'Hojas de Laurel', 0, 0.18, 6, 0, 1, '', 6750054),
(120, 1, 'Oregano seco', 0, 0.18, 6, 0, 1, '', 6750054),
(121, 1, 'Palillo molido', 0, 0.18, 6, 0, 1, '', 6750054),
(122, 1, 'Pimienta de olor entera', 0, 0.18, 6, 0, 1, '', 6750054),
(123, 1, 'Pimienta molida', 0, 0.18, 6, 0, 1, '', 6750054),
(124, 1, 'Chorizo blanco (Paq x 5 und)', 0, 0.18, 7, 0, 1, '', 6750054),
(125, 1, 'Hot dog salchipapero (Paq. x 3Kg)', 0, 0.18, 7, 0, 1, '', 6750054),
(126, 1, 'Hot dog salchipapero a granel', 0, 0.18, 7, 0, 1, '', 6750054),
(127, 1, 'Queso (Molde)', 0, 0.18, 1, 0, 1, '', 6750054),
(128, 1, 'Salchicha blanca (Paq x 5 und)', 0, 0.18, 7, 0, 1, '', 6750054),
(129, 7, 'Bolsa 5x10', 0, 0.18, 8, 0, 1, '', 6750054),
(130, 7, 'Bolsa transparente 8x12 (Super rollo)', 0, 0.18, 8, 0, 1, '', 6750054),
(131, 8, 'Bolsas 12x16 (Paq x 100 und)', 0, 0.18, 8, 0, 1, '', 6750054),
(132, 7, 'Bolsas 16x19 (Paq x 100 Und)', 0, 0.18, 8, 0, 1, '', 6750054),
(133, 7, 'Bolsas 19x20 (Paq x 100 und)', 0, 0.18, 8, 0, 1, '', 6750054),
(134, 7, 'Bolsas 3x8 (Paq x 200und)', 0, 0.18, 8, 0, 1, '', 6750054),
(135, 7, 'Bolsas negras grandes (Paq x 100 Und)', 0, 0.18, 8, 0, 1, '', 6750054),
(136, 7, 'Cajas de papas - tamaño grande', 0, 0.18, 8, 0, 1, '', 6750054),
(137, 7, 'Cajas de papas - tamaño pequeña', 0, 0.18, 8, 0, 1, '', 6750054),
(138, 7, 'Contenedor CT2 (Paq x 50 Und)', 0, 0.18, 8, 0, 1, '', 6750054),
(139, 7, 'Contenedor CT3 (Paq x 50 Und)', 0, 0.18, 8, 0, 1, '', 6750054),
(140, 7, 'Contenedor CT5 (Paq x 50 Und)', 0, 0.18, 8, 0, 1, '', 6750054),
(141, 7, 'Contenedor blanco CACER 8.0oz (Paq x 25 Und)', 0, 0.18, 8, 0, 1, '', 6750054),
(142, 7, 'Tapas de conten blanco CACER 0.68, 1.2, 8 oz (Paq x 50Und)', 0, 0.18, 8, 0, 1, '', 6750054),
(143, 7, 'Contenedor transparente CACER 8oz (Paq x 25 Und)', 0, 0.18, 8, 0, 1, '', 6750054),
(144, 7, 'Tapas de conten transp CACER 0.68, 1.2, 8 oz (Paq x 50Und)', 0, 0.18, 8, 0, 1, '', 6750054),
(145, 7, 'Contenedor transparente CACER 1/2L (Paq x 25 Und)', 0, 0.18, 8, 0, 1, '', 6750054),
(146, 7, 'Tapas de conten transp CACER 1/4, 1/2 Y 1.0L (Paq x 50Und)', 0, 0.18, 8, 0, 1, '', 6750054),
(147, 7, 'Taper DELI x 1/2L (Paq x 50 Und)', 0, 0.18, 8, 0, 1, '', 6750054),
(148, 7, 'Tapa de taper DELI x 1/2L (Paq x 50 Und)', 0, 0.18, 8, 0, 1, '', 6750054),
(149, 7, 'Taper DELI x 1.0L (Paq x 50 Und)', 0, 0.18, 8, 0, 1, '', 6750054),
(150, 7, 'Tapa de taper DELI x 1.0L (Paq x 50 Und)', 0, 0.18, 8, 0, 1, '', 6750054),
(151, 7, 'Ligas estandar Nro. 18 ALLEANZA (Caja  x 1Libra)', 0, 0.18, 8, 0, 1, '', 6750054),
(152, 7, 'Sorbetes de plastico (Paq x 100 Und) c/p 25', 0, 0.18, 8, 0, 1, '', 6750054),
(153, 1, 'Aceituna negra', 0, 0, 9, 0, 1, '', 6750054),
(154, 1, 'Aji Amarillo', 0, 0, 9, 0, 1, '', 6750054),
(155, 1, 'Ajo', 0, 0, 9, 0, 1, '', 6750054),
(156, 8, 'Ajo Procesado', 0, 0, 6, 0, 1, '', 6750054),
(157, 1, 'Albaca', 0, 0, 9, 0, 1, '', 6750054),
(158, 1, 'Alberja sin pelar', 0, 0, 9, 0, 1, '', 6750054),
(159, 1, 'Alberja pelada', 0, 0, 9, 0, 1, '', 6750054),
(160, 7, 'Apio', 0, 0, 9, 0, 1, '', 6750054),
(161, 1, 'Calabaza', 0, 0, 9, 0, 1, '', 6750054),
(162, 1, 'Carambola', 0, 0, 9, 0, 1, '', 6750054),
(163, 7, 'Cebolla china', 0, 0, 9, 0, 1, '', 6750054),
(164, 1, 'Cebolla roja', 0, 0, 9, 0, 1, '', 6750054),
(165, 1, 'Cedron', 0, 0, 9, 0, 1, '', 6750054),
(166, 7, 'Cilandro', 0, 0, 9, 0, 1, '', 6750054),
(167, 7, 'Coliflor', 0, 0, 9, 0, 1, '', 6750054),
(168, 1, 'Espinacas', 0, 0, 9, 0, 1, '', 6750054),
(169, 1, 'Garbanzof', 0, 0, 9, 0, 1, '', 6750054),
(170, 1, 'Habas', 0, 0, 9, 0, 1, '', 6750054),
(171, 7, 'Hierbabuena', 0, 0, 9, 0, 1, '', 6750054),
(172, 1, 'Kion', 0, 0, 9, 0, 1, '', 6750054),
(173, 1, 'Lechuga organica (Carola)', 0, 0, 9, 0, 1, '', 6750054),
(174, 1, 'Limón', 0, 0, 9, 0, 1, '', 6750054),
(175, 1, 'Maiz Morado', 0, 0, 9, 0, 1, '', 6750054),
(176, 1, 'Manzana', 0, 0, 9, 0, 1, '', 6750054),
(177, 1, 'Maracuya', 0, 0, 9, 0, 1, '', 6750054),
(178, 1, 'Marlo', 0, 0, 9, 0, 1, '', 6750054),
(179, 1, 'Membrillo', 0, 0, 9, 0, 1, '', 6750054),
(180, 7, 'Nabos', 0, 0, 9, 0, 1, '', 6750054),
(181, 7, 'Oregano fresco', 0, 0, 9, 0, 1, '', 6750054),
(182, 1, 'Palta', 0, 0, 9, 0, 1, '', 6750054),
(183, 1, 'Papa procesada', 0, 0.18, 9, 0, 1, '', 6750054),
(184, 1, 'Papa unica', 0, 0, 9, 0, 1, '', 6750054),
(185, 1, 'Pepinillo', 0, 0, 9, 0, 1, '', 6750054),
(186, 7, 'Perejil', 0, 0, 9, 0, 1, '', 6750054),
(187, 1, 'Pimenton', 0, 0, 9, 0, 1, '', 6750054),
(188, 1, 'Piña', 0, 0, 9, 0, 1, '', 6750054),
(189, 7, 'Porro', 0, 0, 9, 0, 1, '', 6750054),
(190, 7, 'Rabanitos', 0, 0, 9, 0, 1, '', 6750054),
(191, 1, 'Remolacha (Beterraga)', 0, 0, 9, 0, 1, '', 6750054),
(192, 7, 'Repollo', 0, 0, 9, 0, 1, '', 6750054),
(193, 1, 'Rocoto seco', 0, 0, 9, 0, 1, '', 6750054),
(194, 1, 'Tomate', 0, 0, 9, 0, 1, '', 6750054),
(195, 1, 'Vainita', 0, 0, 9, 0, 1, '', 6750054),
(196, 1, 'Vainita procesada', 0, 0, 9, 0, 1, '', 6750054),
(197, 1, 'Yuca', 0, 0, 9, 0, 1, '', 6750054),
(198, 1, 'Zanahoria', 0, 0, 9, 0, 1, '', 6750054),
(199, 1, 'Rocoto Fresco', 0, 0, 9, 0, 1, '', 6750054),
(200, 7, 'Huacatay', 0, 0, 9, 0, 1, '', 6750054),
(201, 1, 'Huacatay Seco', 0, 0, 9, 0, 1, '', 6750054),
(202, 1, 'Rocoto Hidratado', 0, 0, 9, 0, 1, '', 6750054),
(203, 1, 'Hongo', 0, 0, 9, 0, 1, '', 6750054),
(204, 8, 'Ají calle nueva (Balde x L)', 0, 0, 10, 0, 1, '', 6750054),
(205, 8, 'Ají Galpon (Balde x L)', 0, 0, 10, 0, 1, '', 6750054),
(206, 1, 'Chimichurri x Kg', 0, 0, 9, 0, 1, '', 6750054),
(207, 7, 'Ketchup (Balde x 20)', 0, 0.18, 10, 0, 1, '', 6750054),
(208, 8, 'Ketchup x 1.0L', 0, 0.18, 10, 0, 1, '', 6750054),
(209, 8, 'Mayonesa preparada (Balde x L)', 0, 0.18, 9, 0, 1, '', 6750054),
(210, 8, 'Mostaza (Balde x 20)', 0, 0.18, 10, 0, 1, '', 6750054),
(211, 8, 'Mostaza x 1.0L', 0, 0.18, 10, 0, 1, '', 6750054),
(212, 8, 'Vinagreta preparada (Balde x L)', 0, 0, 10, 0, 1, '', 6750054),
(213, 7, 'Pollo Pelado', 0, 0.18, 1, 0, 1, '', 6750054),
(214, 1, 'Vaina Pelada', 0, 0.18, 1, 0, 1, '', 6750054),
(215, 1, 'Corazon de Pollo', 0, 0.18, 1, 0, 1, '', 6750054),
(216, 1, 'Higado de Pollo', 0, 0.18, 1, 0, 1, '', 6750054),
(217, 1, 'Patas de Pollo', 0, 0.18, 1, 0, 1, '', 6750054),
(218, 1, 'Panza Cruda', 0, 0.18, 5, 0, 1, '', 6750054),
(219, 1, 'panza Preparada', 0, 0.18, 5, 0, 1, '', 6750054),
(220, 7, 'patita de pollo', 0, 0.18, 1, 0, 1, '', 6750054),
(221, 1, 'merma', 0, 0.18, 1, 0, 1, '', 6750054),
(222, 1, 'Tripa Cruda', 0, 0.18, 5, 0, 1, '', 6750054),
(223, 1, 'Tripa Preparada', 0, 0.18, 5, 0, 1, '', 6750054),
(224, 1, 'Vaina Sin Pelar', 0, 0.18, 9, 0, 1, '', 6750054);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articuloprecio`
--

CREATE TABLE `articuloprecio` (
  `IdArticuloPrecio` int(11) NOT NULL,
  `IdArticulo` int(11) NOT NULL,
  `IdLista` int(11) NOT NULL,
  `ArtPreDes` double DEFAULT NULL,
  `ArtPreEstReg` int(1) DEFAULT '1',
  `ArtPreSerializacion` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `IdCategoria` int(5) NOT NULL,
  `CatNom` varchar(50) NOT NULL,
  `CatEstReg` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`IdCategoria`, `CatNom`, `CatEstReg`) VALUES
(0, 'INSUMOS', 0),
(1, 'Abarrotes', 1),
(2, 'Art. Limpieza', 1),
(3, 'Bebidas', 1),
(4, 'Carbon', 1),
(5, 'Carnes', 1),
(6, 'Condimentos', 1),
(7, 'Embutidos', 1),
(8, 'Envases', 1),
(9, 'Frutas o Verduras', 1),
(10, 'Salsas', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoriaproduccion`
--

CREATE TABLE `categoriaproduccion` (
  `IdCategoriaProduccion` int(11) NOT NULL,
  `CatProDes` varchar(60) NOT NULL,
  `CatProEstReg` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoriaproduccion`
--

INSERT INTO `categoriaproduccion` (`IdCategoriaProduccion`, `CatProDes`, `CatProEstReg`) VALUES
(1, 'Producto', 1),
(2, 'Insumo', 1),
(3, 'Merma', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configurar`
--

CREATE TABLE `configurar` (
  `idconfigurar` int(11) NOT NULL,
  `idUsuario` int(5) NOT NULL,
  `idEmpresasede` int(11) NOT NULL,
  `color` varchar(20) NOT NULL DEFAULT 'WHITE',
  `confserializacion` varchar(50) NOT NULL DEFAULT ' '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `configurar`
--

INSERT INTO `configurar` (`idconfigurar`, `idUsuario`, `idEmpresasede`, `color`, `confserializacion`) VALUES
(1, 1, 4, '255-255-255', ' '),
(2, 20, 1, '255-255-255', ' '),
(3, 20, 1, '255-255-255', ' '),
(4, 22, 1, '255-255-255', ' '),
(5, 23, 1, '255-255-255', ' '),
(6, 23, 1, '255-255-255', ' '),
(7, 24, 1, '255-255-255', ' '),
(9, 25, 2, '255-255-255', ' ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documento`
--

CREATE TABLE `documento` (
  `IdDocumento` int(2) NOT NULL,
  `DocDes` varchar(11) NOT NULL,
  `DocNom` varchar(50) NOT NULL,
  `DocEstReg` varchar(11) NOT NULL,
  `DocSerializacion` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `documento`
--

INSERT INTO `documento` (`IdDocumento`, `DocDes`, `DocNom`, `DocEstReg`, `DocSerializacion`) VALUES
(1, 'DNI', 'DOC. NACIONAL DE IDENTIDAD', '1', ''),
(6, 'RUC', 'RUC', '1', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `IdEmpresa` int(11) NOT NULL,
  `IdEmpresaGrupo` int(11) DEFAULT NULL,
  `EmpRuc` char(20) DEFAULT NULL,
  `EmpNom` char(50) DEFAULT NULL,
  `EmpTel` char(20) DEFAULT '',
  `EmpPer` tinyint(1) DEFAULT NULL,
  `IdDocumento` int(2) DEFAULT NULL,
  `EmpEstReg` int(1) DEFAULT '1',
  `EmpSerializacion` varchar(50) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`IdEmpresa`, `IdEmpresaGrupo`, `EmpRuc`, `EmpNom`, `EmpTel`, `EmpPer`, `IdDocumento`, `EmpEstReg`, `EmpSerializacion`) VALUES
(1, 1, '20454190026', 'DAGROS SRL', '1', 0, 6, 1, ''),
(2, 1, '20455411000', 'INVERSIONES CULINARIAS EL GALPON SA', '1', 0, 6, 1, ''),
(3, 1, '20558142791', 'EL GALPON CENTER SRL', '1', 0, 6, 1, ''),
(4, 1, '72943030', 'INDAGAL', '[]', 0, 6, 1, ''),
(5, 3, '0', 'CLIENTES VARIOS', '[]', NULL, 1, 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresagrupo`
--

CREATE TABLE `empresagrupo` (
  `IdEmpresaGrupo` int(11) NOT NULL,
  `EmpGruNom` varchar(50) NOT NULL,
  `EmpGruTel` char(20) DEFAULT NULL,
  `EmpGruEstReg` int(1) DEFAULT '1',
  `EmpGruSerializacion` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresagrupo`
--

INSERT INTO `empresagrupo` (`IdEmpresaGrupo`, `EmpGruNom`, `EmpGruTel`, `EmpGruEstReg`, `EmpGruSerializacion`) VALUES
(1, 'Empresas Sr Hugo', '54585076', 1, ''),
(2, 'Clientes', '0', 1, ''),
(3, 'Proveedores', '1', 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresasede`
--

CREATE TABLE `empresasede` (
  `IdEmpresaSede` int(11) NOT NULL,
  `IdEmpresa` int(11) DEFAULT NULL,
  `EmpSedNom` varchar(50) DEFAULT NULL,
  `EmpSedDin` double DEFAULT NULL,
  `EmpSedDir` varchar(60) DEFAULT NULL,
  `EmpSedEstReg` int(1) DEFAULT '1',
  `EmpSedSerializacion` varchar(50) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresasede`
--

INSERT INTO `empresasede` (`IdEmpresaSede`, `IdEmpresa`, `EmpSedNom`, `EmpSedDin`, `EmpSedDir`, `EmpSedEstReg`, `EmpSedSerializacion`) VALUES
(1, 1, 'DAGROS SRL', 1, 'Calle Nueva 429', 1, ''),
(2, 2, 'INVERSIONES CULINARIAS EL GALPON SA', 1, 'Santa Rosa', 1, ''),
(3, 3, 'EL GALPON CENTER SRL', 1, 'Estados Unidos 14', 1, ''),
(4, 4, 'INDAGAL', 1, 'Bellapampa', 1, ''),
(5, 1, 'Dagros - Almacen', 1, 'Bellapampa', 1, ''),
(6, 1, 'Dagros - Cocina', 1, 'Bellapampa', 1, ''),
(7, 2, 'Inversiones - Almacen', 1, 'Bellapampa', 1, ''),
(8, 3, 'El Galpon - Almacen', 1, 'Bellapampa', 1, ''),
(9, 5, 'CLIENTES VARIOS', 0, '', 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresasedeserie`
--

CREATE TABLE `empresasedeserie` (
  `IdEmpresaSedeSerie` int(11) NOT NULL,
  `IdEmpresaSede` int(11) NOT NULL DEFAULT '1',
  `idmovimientoDocumento` int(11) NOT NULL DEFAULT '1',
  `EmpSedSerNum` varchar(8) NOT NULL,
  `EmpSedSerSer` varchar(4) NOT NULL DEFAULT '0001',
  `EmpSedSerEstReg` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresasedeserie`
--

INSERT INTO `empresasedeserie` (`IdEmpresaSedeSerie`, `IdEmpresaSede`, `idmovimientoDocumento`, `EmpSedSerNum`, `EmpSedSerSer`, `EmpSedSerEstReg`) VALUES
(1, 5, 1, '1', '1', 0),
(2, 1, 1, '1', '1', 0),
(3, 4, 3, '1', '3', 0),
(4, 4, 4, '1', '4', 0),
(5, 4, 13, '13', '13', 1),
(6, 1, 3, '1', '3', 1),
(7, 4, 1, '2', '1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formulacabecera`
--

CREATE TABLE `formulacabecera` (
  `IdFormulaCabecera` int(11) NOT NULL,
  `IdArticulo` int(11) NOT NULL,
  `ForCabDes` varchar(60) NOT NULL,
  `ForCabDetCan` decimal(11,2) NOT NULL,
  `ForCabEstReg` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `formulacabecera`
--

INSERT INTO `formulacabecera` (`IdFormulaCabecera`, `IdArticulo`, `ForCabDes`, `ForCabDetCan`, `ForCabEstReg`) VALUES
(1, 110, 'Pollo Pelado', '0.00', 1),
(2, 206, 'chimichurri', '0.00', 1),
(3, 218, 'PANZA ', '0.00', 1),
(4, 222, 'Tripa Preparada', '0.00', 1),
(5, 158, 'PelarAlberja', '0.00', 1),
(6, 224, 'VainaPelar', '0.00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formuladetalleinsumo`
--

CREATE TABLE `formuladetalleinsumo` (
  `IdFormulaDetalleInsumo` int(11) NOT NULL,
  `idformulaCabecera` int(11) NOT NULL,
  `IdArticulo` int(11) NOT NULL,
  `ForDetInsCan` int(11) NOT NULL,
  `ForDetInsEstReg` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formuladetalleproducto`
--

CREATE TABLE `formuladetalleproducto` (
  `IdFormulaDetalleProducto` int(11) NOT NULL,
  `IdFormulaCabecera` int(11) NOT NULL,
  `IdArticulo` int(11) NOT NULL,
  `IdCategoriaProduccion` int(11) NOT NULL,
  `ForDetProCan` decimal(11,2) NOT NULL,
  `ForDetProEstReg` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `formuladetalleproducto`
--

INSERT INTO `formuladetalleproducto` (`IdFormulaDetalleProducto`, `IdFormulaCabecera`, `IdArticulo`, `IdCategoriaProduccion`, `ForDetProCan`, `ForDetProEstReg`) VALUES
(1, 1, 110, 2, '1.00', 1),
(2, 1, 213, 1, '1.00', 1),
(3, 1, 205, 3, '1.00', 0),
(4, 2, 155, 2, '1.00', 1),
(5, 2, 186, 2, '1.00', 1),
(6, 2, 206, 1, '1.00', 1),
(7, 1, 108, 1, '2.00', 1),
(8, 1, 216, 1, '1.00', 1),
(9, 1, 215, 1, '1.00', 1),
(10, 1, 220, 1, '1.00', 1),
(11, 1, 221, 3, '1.00', 1),
(12, 3, 218, 2, '10.00', 1),
(13, 3, 219, 1, '1.50', 1),
(14, 3, 221, 3, '8.50', 1),
(15, 4, 222, 2, '10.00', 1),
(16, 4, 223, 1, '1.50', 1),
(17, 4, 221, 3, '1.00', 1),
(18, 5, 158, 2, '10.00', 1),
(19, 5, 159, 1, '3.00', 1),
(20, 5, 221, 3, '7.00', 1),
(21, 6, 224, 2, '10.00', 1),
(22, 6, 214, 2, '9.00', 1),
(23, 6, 221, 2, '1.00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `IdInventario` int(11) NOT NULL,
  `IdArticulo` int(11) NOT NULL,
  `IdEmpresaSede` int(11) NOT NULL,
  `InvCan` int(11) NOT NULL,
  `InvEstReg` int(11) NOT NULL,
  `InvSerializacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lista`
--

CREATE TABLE `lista` (
  `IdLista` int(11) NOT NULL,
  `IdEmpresaSede` int(11) DEFAULT NULL,
  `ListDes` char(50) DEFAULT NULL,
  `ListEstReg` int(1) DEFAULT '1',
  `ListSerializacion` varchar(50) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `lista`
--

INSERT INTO `lista` (`IdLista`, `IdEmpresaSede`, `ListDes`, `ListEstReg`, `ListSerializacion`) VALUES
(1, 3, 'lista 2', 1, '**001**10000139**');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientocab`
--

CREATE TABLE `movimientocab` (
  `IdMovimientoCabecera` int(11) NOT NULL,
  `IdMovimientoDocumento` int(11) DEFAULT NULL,
  `IdUsuario` int(11) DEFAULT NULL,
  `IdEmpresaSede` int(11) DEFAULT NULL COMMENT 'este es el origen ',
  `IdEmpresaSedeDes` int(11) DEFAULT NULL,
  `IdMovimientoTipo` int(11) DEFAULT NULL,
  `MovCabMovSto` tinyint(1) DEFAULT NULL,
  `MovCabFec` date DEFAULT NULL,
  `MovCabSer` char(5) DEFAULT NULL,
  `MovCabNum` char(8) DEFAULT NULL,
  `MovCabEnv` date DEFAULT '1990-01-01',
  `MovCabProBoo` int(11) NOT NULL DEFAULT '1',
  `MovCabEstReg` int(1) DEFAULT '1',
  `IdMovimientoCabeceraAsociado` int(11) NOT NULL DEFAULT '1',
  `MovCabSerializacion` varchar(50) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `movimientocab`
--

INSERT INTO `movimientocab` (`IdMovimientoCabecera`, `IdMovimientoDocumento`, `IdUsuario`, `IdEmpresaSede`, `IdEmpresaSedeDes`, `IdMovimientoTipo`, `MovCabMovSto`, `MovCabFec`, `MovCabSer`, `MovCabNum`, `MovCabEnv`, `MovCabProBoo`, `MovCabEstReg`, `IdMovimientoCabeceraAsociado`, `MovCabSerializacion`) VALUES
(1, NULL, 1, 1, 4, 1, 1, '2018-12-12', '', '2', '1990-01-01', 0, 0, 1, ''),
(2, NULL, 1, 4, 4, NULL, NULL, '2018-12-26', '13', '1', '1990-01-01', 0, 1, 1, ''),
(3, NULL, 1, 4, 4, 1, NULL, '2018-12-26', '13', '1', '1990-01-01', 0, 1, 1, ''),
(4, NULL, 1, 4, 4, NULL, NULL, '2018-12-26', '13', '1', '1990-01-01', 0, 1, 1, ''),
(5, 13, 1, 4, 4, NULL, NULL, '2018-12-26', '13', '2', '1990-01-01', 0, 1, 5, ''),
(6, 13, 1, 4, 4, 2, NULL, '2018-12-26', '13', '2', '1990-01-01', 0, 1, 5, ''),
(7, 13, 1, 4, 4, 1, NULL, '2018-12-26', '13', '2', '1990-01-01', 0, 1, 5, ''),
(8, 13, 1, 4, 4, NULL, NULL, '2018-12-26', '13', '3', '1990-01-01', 0, 1, 8, ''),
(9, 13, 1, 4, 4, 2, NULL, '2018-12-26', '13', '3', '1990-01-01', 0, 1, 8, ''),
(10, 13, 1, 4, 4, 1, NULL, '2018-12-26', '13', '3', '1990-01-01', 0, 1, 8, ''),
(11, 13, 1, 4, 4, 1, NULL, '2018-12-31', '13', '4', '1990-01-01', 1, 1, 11, ''),
(12, 13, 1, 4, 4, 2, NULL, '2018-12-31', '13', '4', '1990-01-01', 2, 1, 11, ''),
(13, 13, 1, 4, 4, 1, NULL, '2018-12-31', '13', '4', '1990-01-01', 3, 1, 11, ''),
(14, 13, 1, 4, 4, 1, NULL, '2018-12-31', '13', '4', '1990-01-01', 1, 1, 14, ''),
(15, 13, 1, 4, 4, 2, NULL, '2018-12-31', '13', '4', '1990-01-01', 2, 1, 14, ''),
(16, 13, 1, 4, 4, 1, NULL, '2018-12-31', '13', '4', '1990-01-01', 3, 1, 14, ''),
(17, 1, 1, 4, 9, 2, 1, '2018-12-31', '1', '2', '1990-01-01', 0, 1, 1, ''),
(18, 13, 1, 4, 4, 1, NULL, '2018-12-31', '13', '6', '1990-01-01', 1, 1, 18, ''),
(19, 13, 1, 4, 4, 2, NULL, '2018-12-31', '13', '6', '1990-01-01', 2, 1, 18, ''),
(20, 13, 1, 4, 4, 1, NULL, '2018-12-31', '13', '6', '1990-01-01', 3, 1, 18, ''),
(21, 13, 1, 4, 4, 1, NULL, '2018-12-31', '13', '7', '1990-01-01', 1, 1, 21, ''),
(22, 13, 1, 4, 4, 2, NULL, '2018-12-31', '13', '7', '1990-01-01', 2, 1, 21, ''),
(23, 13, 1, 4, 4, 1, NULL, '2018-12-31', '13', '7', '1990-01-01', 3, 1, 21, ''),
(24, 13, 1, 4, 4, 1, NULL, '2018-12-31', '13', '8', '1990-01-01', 1, 1, 24, ''),
(25, 13, 1, 4, 4, 2, NULL, '2018-12-31', '13', '8', '1990-01-01', 2, 1, 24, ''),
(26, 13, 1, 4, 4, 1, NULL, '2018-12-31', '13', '8', '1990-01-01', 3, 1, 24, ''),
(27, 13, 1, 4, 4, 1, NULL, '2018-12-31', '13', '9', '1990-01-01', 1, 1, 27, ''),
(28, 13, 1, 4, 4, 2, NULL, '2018-12-31', '13', '9', '1990-01-01', 2, 1, 27, ''),
(29, 13, 1, 4, 4, 1, NULL, '2018-12-31', '13', '9', '1990-01-01', 3, 1, 27, ''),
(30, 13, 1, 4, 4, 1, NULL, '2018-12-31', '13', '10', '1990-01-01', 1, 1, 30, ''),
(31, 13, 1, 4, 4, 2, NULL, '2018-12-31', '13', '10', '1990-01-01', 2, 1, 30, ''),
(32, 13, 1, 4, 4, 1, NULL, '2018-12-31', '13', '10', '1990-01-01', 3, 1, 30, ''),
(33, 13, 1, 4, 4, 1, NULL, '2018-12-31', '13', '11', '1990-01-01', 1, 1, 33, ''),
(34, 13, 1, 4, 4, 2, NULL, '2018-12-31', '13', '11', '1990-01-01', 2, 1, 33, ''),
(35, 13, 1, 4, 4, 1, NULL, '2018-12-31', '13', '11', '1990-01-01', 3, 1, 33, ''),
(36, 13, 1, 4, 4, 1, NULL, '2018-12-31', '13', '12', '1990-01-01', 1, 1, 36, ''),
(37, 13, 1, 4, 4, 2, NULL, '2018-12-31', '13', '12', '1990-01-01', 2, 1, 36, ''),
(38, 13, 1, 4, 4, 1, NULL, '2018-12-31', '13', '12', '1990-01-01', 3, 1, 36, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientodet`
--

CREATE TABLE `movimientodet` (
  `IdMovimientoDetalle` int(11) NOT NULL,
  `IdMovimientoCabecera` int(11) NOT NULL,
  `IdArticulo` int(11) DEFAULT NULL,
  `MovIngreso` int(2) DEFAULT NULL,
  `MovDetCan` decimal(11,2) DEFAULT NULL,
  `MovDetPreTot` double DEFAULT '0',
  `MovDetCosTot` double DEFAULT '0',
  `MovDetIgv` double DEFAULT NULL,
  `MovDetEstReg` int(1) DEFAULT '1',
  `MovDetSerializacion` varchar(50) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `movimientodet`
--

INSERT INTO `movimientodet` (`IdMovimientoDetalle`, `IdMovimientoCabecera`, `IdArticulo`, `MovIngreso`, `MovDetCan`, `MovDetPreTot`, `MovDetCosTot`, `MovDetIgv`, `MovDetEstReg`, `MovDetSerializacion`) VALUES
(1, 1, 0, 1, '1.00', 0, 0.85, 0.15, 0, ''),
(2, 2, 111, NULL, '1.00', NULL, NULL, NULL, NULL, ''),
(3, 3, 110, NULL, '1.00', NULL, NULL, NULL, NULL, ''),
(4, 4, 205, NULL, '1.00', NULL, NULL, NULL, NULL, ''),
(5, 5, 111, NULL, '1.00', NULL, NULL, NULL, NULL, ''),
(6, 6, 110, NULL, '1.00', NULL, NULL, NULL, NULL, ''),
(7, 7, 205, NULL, '1.00', NULL, NULL, NULL, NULL, ''),
(8, 8, 111, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(9, 9, 110, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(10, 10, 205, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(11, 11, 206, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(12, 12, 155, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(13, 12, 186, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(14, 14, 206, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(15, 15, 155, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(16, 15, 186, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(17, 17, 78, 1, '1.00', 0, 0, 0.18, 1, ''),
(18, 18, 206, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(19, 19, 155, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(20, 19, 186, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(21, 21, 206, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(22, 22, 155, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(23, 22, 186, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(24, 24, 111, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(25, 25, 110, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(26, 26, 205, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(27, 27, 111, NULL, '59.00', NULL, NULL, NULL, 1, ''),
(28, 28, 110, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(29, 29, 205, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(30, 30, 111, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(31, 31, 110, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(32, 32, 205, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(33, 33, 111, NULL, '49.00', NULL, NULL, NULL, 1, ''),
(34, 34, 110, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(35, 35, 205, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(36, 36, 111, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(37, 37, 110, NULL, '1.00', NULL, NULL, NULL, 1, ''),
(38, 38, 205, NULL, '1.00', NULL, NULL, NULL, 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientodocumento`
--

CREATE TABLE `movimientodocumento` (
  `IdMovimientoDocumento` int(11) NOT NULL,
  `MovDocDes` char(40) DEFAULT NULL,
  `MovDocAbr` char(10) DEFAULT NULL,
  `MovDocEstReg` int(1) DEFAULT '1',
  `MovDocCodSunat` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `movimientodocumento`
--

INSERT INTO `movimientodocumento` (`IdMovimientoDocumento`, `MovDocDes`, `MovDocAbr`, `MovDocEstReg`, `MovDocCodSunat`) VALUES
(0, 'a', 'a', 0, '0'),
(1, 'FACTURA', 'FAC', 1, '01'),
(2, 'RECIBO POR HONORARIOS', 'RH', 1, '02'),
(3, 'BOLETA DE VENTA', 'BV', 1, '03'),
(4, 'LIQUIDACION DE COMPRA', 'LIQ', 1, '04'),
(5, 'NOTA DE CREDITO', 'NC', 1, '07'),
(6, 'NOTA DE DEBITO', 'ND', 1, '08'),
(7, 'GUIA DE REMISION', 'GR', 1, '09'),
(8, 'RECIBO POR ARRENDAMIENTO', 'RA', 1, '10'),
(9, 'TICKET', 'T', 1, '12'),
(10, 'RECIBO POR SERVICIOS PUBLICOS', 'R', 1, '14'),
(11, 'NOTA DE ENTRADA', 'NE', 1, '98'),
(12, 'Nota de Salida', 'NS', 1, '99'),
(13, 'NOTA PRODUCCION', 'PRO', 1, '00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientotipo`
--

CREATE TABLE `movimientotipo` (
  `IdMovimientoTipo` int(11) NOT NULL,
  `MovTipDes` char(50) DEFAULT NULL,
  `MovTipEstReg` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `movimientotipo`
--

INSERT INTO `movimientotipo` (`IdMovimientoTipo`, `MovTipDes`, `MovTipEstReg`) VALUES
(1, 'ENTRADA', 1),
(2, 'SALIDA', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `observacion`
--

CREATE TABLE `observacion` (
  `IdObservacion` int(11) NOT NULL,
  `IdMovimientoCabecera` int(11) NOT NULL,
  `IdUsuario` int(5) NOT NULL,
  `ObservacionDescripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `observacion`
--

INSERT INTO `observacion` (`IdObservacion`, `IdMovimientoCabecera`, `IdUsuario`, `ObservacionDescripcion`) VALUES
(18, 1, 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `observaciones`
--

CREATE TABLE `observaciones` (
  `idobsevarciones` int(50) NOT NULL,
  `observacion` text NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidadmedida`
--

CREATE TABLE `unidadmedida` (
  `IdUnidadMedida` int(11) NOT NULL,
  `UniMedDes` char(50) DEFAULT NULL,
  `UniMedAbr` varchar(10) DEFAULT NULL,
  `UniMedEstReg` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `unidadmedida`
--

INSERT INTO `unidadmedida` (`IdUnidadMedida`, `UniMedDes`, `UniMedAbr`, `UniMedEstReg`) VALUES
(0, 'a', 'a', 0),
(1, 'KILOGRAMOS', 'KG', 1),
(2, 'LIBRAS', 'LB', 1),
(3, 'TONELADAS LARGAS', 'TL', 1),
(4, 'TONELADAS METRICAS', 'TM', 1),
(5, 'TONELADAS CORTAS', 'TC', 1),
(6, 'GRAMOS', 'GR', 1),
(7, 'UNIDADES', 'U', 1),
(8, 'LITROS', 'LT', 1),
(9, 'GALONES', 'GAL', 1),
(10, 'BARRILES', 'BAR', 1),
(11, 'LATAS', 'L', 1),
(12, 'CAJAS', 'CJ', 1),
(13, 'MILLARES', 'MILL', 1),
(14, 'METROS CUBICOS', 'MC', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `IdUsuario` int(5) NOT NULL,
  `UsuNom` char(20) DEFAULT NULL,
  `UsuApePat` char(20) DEFAULT NULL,
  `UsuApeMat` char(20) DEFAULT NULL,
  `UsuDni` char(8) DEFAULT NULL,
  `UsuCar` varchar(50) NOT NULL,
  `UsuNomCor` varchar(50) NOT NULL,
  `UsuCla` varchar(50) NOT NULL,
  `UsuEstReg` int(1) DEFAULT '1',
  `usuSerializacion` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`IdUsuario`, `UsuNom`, `UsuApePat`, `UsuApeMat`, `UsuDni`, `UsuCar`, `UsuNomCor`, `UsuCla`, `UsuEstReg`, `usuSerializacion`) VALUES
(1, 'admin', 'admin', 'admin', '12345678', 'Administrador', 'admin', 'B1wUR', 1, '**001**10000139**'),
(18, 'EDWRA', 'RODRIGO', 'COAQUIRA', '74543294', 'ADMN', 'admin1', 'B1wUR6', 1, '**001**10000139**'),
(19, 'victor', 'cornejo', 'aparicio', '09', 'vcornejo5@hotmail.com', '', '', 1, '**001**10000139**'),
(20, 'CARMEN', 'DIAZ', 'FERRERA', '29539001', 'CAJERA', 'CARMEN', 'DHDV', 1, '**001**10000139**'),
(22, 'ELVIS', 'TELLEZ', 'MENDOZA', '72943030', 'ADMIN', 'io', 'WX', 1, '**001**10000139**'),
(23, 'VANESSA', 'RODRIGUEZ', 'QUISPE', '46433495', 'ADMINISTRADORA', 'vane', 'He66ejFV', 1, ''),
(24, 'LEONARDO', 'CANSALLA', 'MALDONADO', '29291452', 'ENCARGADO', 'LEO', 't000', 1, ''),
(25, 'ELVIS', 'TELLEZ', 'MENDOZAZ', '78787878', 'CARGO', 'elv', '1od', 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarioacceso`
--

CREATE TABLE `usuarioacceso` (
  `IdUsuAcces` int(5) NOT NULL,
  `IdUsuario` int(5) NOT NULL,
  `IdAcceso` int(5) NOT NULL,
  `usuaccserializacion` varchar(50) NOT NULL DEFAULT ' '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarioacceso`
--

INSERT INTO `usuarioacceso` (`IdUsuAcces`, `IdUsuario`, `IdAcceso`, `usuaccserializacion`) VALUES
(42, 1, 1, '**001**10000139**'),
(43, 1, 2, '**001**10000139**'),
(44, 1, 3, '**001**10000139**'),
(47, 1, 6, '**001**10000139**'),
(48, 1, 7, '**001**10000139**'),
(49, 1, 8, '**001**10000139**'),
(51, 1, 10, '**001**10000139**'),
(54, 1, 13, '**001**10000139**'),
(87, 1, 15, '**001**10000139**'),
(88, 1, 4, '**001**10000139**'),
(89, 1, 16, '**001**10000139**'),
(90, 1, 17, '**001**10000139**'),
(92, 1, 5, '**001**10000139**'),
(93, 1, 14, '**001**10000139**'),
(94, 1, 11, '**001**10000139**'),
(95, 20, 13, '**001**10000139**'),
(96, 20, 14, '**001**10000139**'),
(97, 20, 17, '**001**10000139**'),
(98, 22, 1, '**001**10000139**'),
(99, 22, 5, '**001**10000139**'),
(100, 22, 7, '**001**10000139**'),
(101, 22, 8, '**001**10000139**'),
(102, 22, 10, '**001**10000139**'),
(103, 22, 14, '**001**10000139**'),
(104, 22, 15, '**001**10000139**'),
(105, 22, 16, '**001**10000139**'),
(106, 23, 8, ' '),
(107, 23, 11, ' '),
(108, 23, 12, ' '),
(109, 23, 13, ' '),
(110, 23, 14, ' '),
(111, 23, 15, ' '),
(112, 23, 17, ' '),
(113, 20, 15, ' '),
(114, 24, 8, ' '),
(115, 24, 11, ' '),
(116, 24, 12, ' '),
(117, 24, 13, ' '),
(118, 24, 14, ' '),
(119, 24, 15, ' '),
(120, 24, 17, ' '),
(121, 1, 19, ' '),
(122, 1, 20, ' '),
(123, 1, 21, ' '),
(124, 1, 22, ' '),
(125, 1, 23, ' '),
(126, 1, 24, ' '),
(128, 25, 1, ' '),
(129, 25, 2, ' '),
(130, 25, 3, ' '),
(131, 25, 4, ' '),
(132, 25, 5, ' '),
(133, 25, 6, ' '),
(134, 25, 7, ' '),
(135, 25, 8, ' '),
(136, 25, 9, ' '),
(137, 25, 10, ' '),
(138, 25, 11, ' '),
(139, 25, 12, ' '),
(140, 25, 13, ' '),
(141, 25, 14, ' '),
(142, 25, 15, ' '),
(143, 25, 16, ' '),
(144, 25, 17, ' '),
(145, 25, 18, ' '),
(146, 25, 19, ' '),
(147, 25, 20, ' '),
(148, 25, 21, ' '),
(149, 25, 22, ' '),
(150, 25, 23, ' '),
(151, 25, 24, ' '),
(152, 1, 9, ' '),
(153, 1, 12, ' '),
(154, 1, 18, ' ');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acceso`
--
ALTER TABLE `acceso`
  ADD PRIMARY KEY (`IdAcceso`);

--
-- Indices de la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD PRIMARY KEY (`IdArticulo`),
  ADD UNIQUE KEY `IdArticulo` (`IdArticulo`),
  ADD KEY `IX_Relationship31` (`IdUnidadMedida`),
  ADD KEY `ArtCat` (`ArtCat`);

--
-- Indices de la tabla `articuloprecio`
--
ALTER TABLE `articuloprecio`
  ADD PRIMARY KEY (`IdArticuloPrecio`,`IdArticulo`,`IdLista`),
  ADD KEY `Relationship18` (`IdArticulo`),
  ADD KEY `Relationship19` (`IdLista`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`IdCategoria`);

--
-- Indices de la tabla `categoriaproduccion`
--
ALTER TABLE `categoriaproduccion`
  ADD PRIMARY KEY (`IdCategoriaProduccion`);

--
-- Indices de la tabla `configurar`
--
ALTER TABLE `configurar`
  ADD PRIMARY KEY (`idconfigurar`),
  ADD KEY `elasion2k` (`idUsuario`),
  ADD KEY `relasion2k1` (`idEmpresasede`);

--
-- Indices de la tabla `documento`
--
ALTER TABLE `documento`
  ADD PRIMARY KEY (`IdDocumento`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`IdEmpresa`),
  ADD KEY `IX_Relationship25` (`IdEmpresaGrupo`),
  ADD KEY `EmpPer` (`EmpPer`),
  ADD KEY `Relationship51` (`IdDocumento`);

--
-- Indices de la tabla `empresagrupo`
--
ALTER TABLE `empresagrupo`
  ADD PRIMARY KEY (`IdEmpresaGrupo`);

--
-- Indices de la tabla `empresasede`
--
ALTER TABLE `empresasede`
  ADD PRIMARY KEY (`IdEmpresaSede`),
  ADD KEY `IX_Relationship23` (`IdEmpresa`);

--
-- Indices de la tabla `empresasedeserie`
--
ALTER TABLE `empresasedeserie`
  ADD PRIMARY KEY (`IdEmpresaSedeSerie`),
  ADD KEY `idmovimientoDocumento` (`idmovimientoDocumento`),
  ADD KEY `IdEmpresaSede` (`IdEmpresaSede`);

--
-- Indices de la tabla `formulacabecera`
--
ALTER TABLE `formulacabecera`
  ADD PRIMARY KEY (`IdFormulaCabecera`),
  ADD KEY `IdArticulo` (`IdArticulo`);

--
-- Indices de la tabla `formuladetalleinsumo`
--
ALTER TABLE `formuladetalleinsumo`
  ADD PRIMARY KEY (`IdFormulaDetalleInsumo`),
  ADD KEY `idformulaCabecera` (`idformulaCabecera`),
  ADD KEY `IdArticulo` (`IdArticulo`);

--
-- Indices de la tabla `formuladetalleproducto`
--
ALTER TABLE `formuladetalleproducto`
  ADD PRIMARY KEY (`IdFormulaDetalleProducto`),
  ADD KEY `IdArticulo` (`IdArticulo`),
  ADD KEY `IdFormulaCabecera` (`IdFormulaCabecera`),
  ADD KEY `IdCategoriaProduccion` (`IdCategoriaProduccion`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`IdInventario`),
  ADD UNIQUE KEY `IdEmpresaSede` (`IdEmpresaSede`),
  ADD KEY `IdArticulo` (`IdArticulo`),
  ADD KEY `InvSerializacion` (`InvSerializacion`);

--
-- Indices de la tabla `lista`
--
ALTER TABLE `lista`
  ADD PRIMARY KEY (`IdLista`),
  ADD KEY `IX_Relationship22` (`IdEmpresaSede`);

--
-- Indices de la tabla `movimientocab`
--
ALTER TABLE `movimientocab`
  ADD PRIMARY KEY (`IdMovimientoCabecera`),
  ADD KEY `IX_Relationship26` (`IdMovimientoDocumento`),
  ADD KEY `IX_Relationship28` (`IdEmpresaSede`),
  ADD KEY `IX_Relationship29` (`IdMovimientoTipo`),
  ADD KEY `IX_Relationship32` (`IdUsuario`),
  ADD KEY `Relationship50` (`IdEmpresaSedeDes`);

--
-- Indices de la tabla `movimientodet`
--
ALTER TABLE `movimientodet`
  ADD PRIMARY KEY (`IdMovimientoDetalle`,`IdMovimientoCabecera`),
  ADD KEY `IX_Relationship30` (`IdArticulo`),
  ADD KEY `Relationship15` (`IdMovimientoCabecera`);

--
-- Indices de la tabla `movimientodocumento`
--
ALTER TABLE `movimientodocumento`
  ADD PRIMARY KEY (`IdMovimientoDocumento`);

--
-- Indices de la tabla `movimientotipo`
--
ALTER TABLE `movimientotipo`
  ADD PRIMARY KEY (`IdMovimientoTipo`);

--
-- Indices de la tabla `observacion`
--
ALTER TABLE `observacion`
  ADD PRIMARY KEY (`IdObservacion`),
  ADD KEY `IdMovimientoCabecera` (`IdMovimientoCabecera`),
  ADD KEY `IdUsuario` (`IdUsuario`),
  ADD KEY `IdUsuario_2` (`IdUsuario`);

--
-- Indices de la tabla `observaciones`
--
ALTER TABLE `observaciones`
  ADD PRIMARY KEY (`idobsevarciones`);

--
-- Indices de la tabla `unidadmedida`
--
ALTER TABLE `unidadmedida`
  ADD PRIMARY KEY (`IdUnidadMedida`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`IdUsuario`);

--
-- Indices de la tabla `usuarioacceso`
--
ALTER TABLE `usuarioacceso`
  ADD PRIMARY KEY (`IdUsuAcces`),
  ADD KEY `Relationship40` (`IdAcceso`),
  ADD KEY `Relationship41` (`IdUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `acceso`
--
ALTER TABLE `acceso`
  MODIFY `IdAcceso` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `articulo`
--
ALTER TABLE `articulo`
  MODIFY `IdArticulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=225;

--
-- AUTO_INCREMENT de la tabla `articuloprecio`
--
ALTER TABLE `articuloprecio`
  MODIFY `IdArticuloPrecio` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `IdCategoria` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `configurar`
--
ALTER TABLE `configurar`
  MODIFY `idconfigurar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `IdEmpresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `empresagrupo`
--
ALTER TABLE `empresagrupo`
  MODIFY `IdEmpresaGrupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `empresasede`
--
ALTER TABLE `empresasede`
  MODIFY `IdEmpresaSede` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `empresasedeserie`
--
ALTER TABLE `empresasedeserie`
  MODIFY `IdEmpresaSedeSerie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `formuladetalleproducto`
--
ALTER TABLE `formuladetalleproducto`
  MODIFY `IdFormulaDetalleProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `lista`
--
ALTER TABLE `lista`
  MODIFY `IdLista` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `movimientocab`
--
ALTER TABLE `movimientocab`
  MODIFY `IdMovimientoCabecera` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `movimientodet`
--
ALTER TABLE `movimientodet`
  MODIFY `IdMovimientoDetalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `movimientodocumento`
--
ALTER TABLE `movimientodocumento`
  MODIFY `IdMovimientoDocumento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `movimientotipo`
--
ALTER TABLE `movimientotipo`
  MODIFY `IdMovimientoTipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `observacion`
--
ALTER TABLE `observacion`
  MODIFY `IdObservacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `observaciones`
--
ALTER TABLE `observaciones`
  MODIFY `idobsevarciones` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `unidadmedida`
--
ALTER TABLE `unidadmedida`
  MODIFY `IdUnidadMedida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `IdUsuario` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `usuarioacceso`
--
ALTER TABLE `usuarioacceso`
  MODIFY `IdUsuAcces` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD CONSTRAINT `Relationship31` FOREIGN KEY (`IdUnidadMedida`) REFERENCES `unidadmedida` (`IdUnidadMedida`),
  ADD CONSTRAINT `fg` FOREIGN KEY (`ArtCat`) REFERENCES `categoria` (`IdCategoria`);

--
-- Filtros para la tabla `articuloprecio`
--
ALTER TABLE `articuloprecio`
  ADD CONSTRAINT `Relationship18` FOREIGN KEY (`IdArticulo`) REFERENCES `articulo` (`IdArticulo`),
  ADD CONSTRAINT `Relationship19` FOREIGN KEY (`IdLista`) REFERENCES `lista` (`IdLista`);

--
-- Filtros para la tabla `configurar`
--
ALTER TABLE `configurar`
  ADD CONSTRAINT `elasion2k` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`IdUsuario`),
  ADD CONSTRAINT `relasion2k1` FOREIGN KEY (`idEmpresasede`) REFERENCES `empresasede` (`IdEmpresaSede`);

--
-- Filtros para la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD CONSTRAINT `Relationship25` FOREIGN KEY (`IdEmpresaGrupo`) REFERENCES `empresagrupo` (`IdEmpresaGrupo`),
  ADD CONSTRAINT `Relationship51` FOREIGN KEY (`IdDocumento`) REFERENCES `documento` (`IdDocumento`);

--
-- Filtros para la tabla `empresasede`
--
ALTER TABLE `empresasede`
  ADD CONSTRAINT `Relationship23` FOREIGN KEY (`IdEmpresa`) REFERENCES `empresa` (`IdEmpresa`);

--
-- Filtros para la tabla `empresasedeserie`
--
ALTER TABLE `empresasedeserie`
  ADD CONSTRAINT `empresasedeserie_ibfk_1` FOREIGN KEY (`idmovimientoDocumento`) REFERENCES `movimientodocumento` (`IdMovimientoDocumento`),
  ADD CONSTRAINT `empresasedeserie_ibfk_2` FOREIGN KEY (`IdEmpresaSede`) REFERENCES `empresasede` (`IdEmpresaSede`);

--
-- Filtros para la tabla `formulacabecera`
--
ALTER TABLE `formulacabecera`
  ADD CONSTRAINT `formulacabecera_ibfk_1` FOREIGN KEY (`IdArticulo`) REFERENCES `articulo` (`IdArticulo`);

--
-- Filtros para la tabla `formuladetalleinsumo`
--
ALTER TABLE `formuladetalleinsumo`
  ADD CONSTRAINT `formuladetalleinsumo_ibfk_1` FOREIGN KEY (`IdArticulo`) REFERENCES `articulo` (`IdArticulo`),
  ADD CONSTRAINT `formuladetalleinsumo_ibfk_2` FOREIGN KEY (`idformulaCabecera`) REFERENCES `formulacabecera` (`IdFormulaCabecera`);

--
-- Filtros para la tabla `formuladetalleproducto`
--
ALTER TABLE `formuladetalleproducto`
  ADD CONSTRAINT `formuladetalleproducto_ibfk_1` FOREIGN KEY (`IdArticulo`) REFERENCES `articulo` (`IdArticulo`),
  ADD CONSTRAINT `formuladetalleproducto_ibfk_2` FOREIGN KEY (`IdFormulaCabecera`) REFERENCES `formulacabecera` (`IdFormulaCabecera`),
  ADD CONSTRAINT `formuladetalleproducto_ibfk_3` FOREIGN KEY (`IdCategoriaProduccion`) REFERENCES `categoriaproduccion` (`IdCategoriaProduccion`);

--
-- Filtros para la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD CONSTRAINT `inventario_ibfk_1` FOREIGN KEY (`IdArticulo`) REFERENCES `articulo` (`IdArticulo`),
  ADD CONSTRAINT `inventario_ibfk_2` FOREIGN KEY (`IdEmpresaSede`) REFERENCES `empresasede` (`IdEmpresaSede`);

--
-- Filtros para la tabla `lista`
--
ALTER TABLE `lista`
  ADD CONSTRAINT `Relationship22` FOREIGN KEY (`IdEmpresaSede`) REFERENCES `empresasede` (`IdEmpresaSede`);

--
-- Filtros para la tabla `movimientocab`
--
ALTER TABLE `movimientocab`
  ADD CONSTRAINT `Relationship26` FOREIGN KEY (`IdMovimientoDocumento`) REFERENCES `movimientodocumento` (`IdMovimientoDocumento`),
  ADD CONSTRAINT `Relationship28` FOREIGN KEY (`IdEmpresaSede`) REFERENCES `empresasede` (`IdEmpresaSede`),
  ADD CONSTRAINT `Relationship29` FOREIGN KEY (`IdMovimientoTipo`) REFERENCES `movimientotipo` (`IdMovimientoTipo`),
  ADD CONSTRAINT `Relationship32` FOREIGN KEY (`IdUsuario`) REFERENCES `usuario` (`IdUsuario`),
  ADD CONSTRAINT `Relationship50` FOREIGN KEY (`IdEmpresaSedeDes`) REFERENCES `empresasede` (`IdEmpresaSede`);

--
-- Filtros para la tabla `movimientodet`
--
ALTER TABLE `movimientodet`
  ADD CONSTRAINT `Relationship15` FOREIGN KEY (`IdMovimientoCabecera`) REFERENCES `movimientocab` (`IdMovimientoCabecera`),
  ADD CONSTRAINT `Relationship30` FOREIGN KEY (`IdArticulo`) REFERENCES `articulo` (`IdArticulo`);

--
-- Filtros para la tabla `observacion`
--
ALTER TABLE `observacion`
  ADD CONSTRAINT `observacion_ibfk_1` FOREIGN KEY (`IdMovimientoCabecera`) REFERENCES `movimientocab` (`IdMovimientoCabecera`),
  ADD CONSTRAINT `observacion_ibfk_2` FOREIGN KEY (`IdUsuario`) REFERENCES `usuario` (`IdUsuario`);

--
-- Filtros para la tabla `usuarioacceso`
--
ALTER TABLE `usuarioacceso`
  ADD CONSTRAINT `Relationship40` FOREIGN KEY (`IdAcceso`) REFERENCES `acceso` (`IdAcceso`),
  ADD CONSTRAINT `Relationship41` FOREIGN KEY (`IdUsuario`) REFERENCES `usuario` (`IdUsuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
