/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Maestros.ListaM;
import java.awt.Dimension;
import java.awt.Toolkit;
import Maestros.Maestro;
import javax.swing.*;

/**
 *
 * @author Equipo EPIS
 */
public class ListaGUI extends javax.swing.JFrame {
    public static ListaGUI instancia = null;
    public static final int ANCHO_FORM_LLENAR = 518;
    public static final int ALTO_FORM_LLENAR = 228;
    public static final int ANCHO_FORM_MOSTRAR = 702;
    public static final int ALTO_FORM_MOSTRAR = 257;
    public static final int ANCHO_FORM_BOT = 702;
    public static final int ALTO_FORM_BOT = 125;
    public static final int ANCHO_FORM = 730;
    public static final int ALTO_FORM = 673;
    boolean bandera = true;
    int cantDatos = 5;

   
    Maestro maestro = new Maestro();
    //Este objeto siempre tendra los atributos de la Tabla para que se ejecute sql en la clase EmpresaSql.
    ListaM ejecutor = maestro.gestionarLista();

    public ListaGUI() {
        initComponents();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        FormParaMostrar = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        JTabla = new javax.swing.JTable();
        FormBotones = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        JTBuscar = new javax.swing.JTextField();
        JBBuscar = new javax.swing.JButton();
        JBNuevo = new javax.swing.JButton();
        JBModificar = new javax.swing.JButton();
        JBEliminar = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        FormParaLlenar = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JTCodigo = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        JComboSede = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        JTDesc = new javax.swing.JTextField();
        JBGrabar = new javax.swing.JButton();
        JBSalir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        JTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(JTabla);

        javax.swing.GroupLayout FormParaMostrarLayout = new javax.swing.GroupLayout(FormParaMostrar);
        FormParaMostrar.setLayout(FormParaMostrarLayout);
        FormParaMostrarLayout.setHorizontalGroup(
            FormParaMostrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );
        FormParaMostrarLayout.setVerticalGroup(
            FormParaMostrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormParaMostrarLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel5.setText("BUSCAR:");

        JBBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/BUSCAR.JPG"))); // NOI18N
        JBBuscar.setText("BUSCAR");
        JBBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBBuscarActionPerformed(evt);
            }
        });

        JBNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/NUEVO.JPG"))); // NOI18N
        JBNuevo.setText("NUEVO");
        JBNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBNuevoActionPerformed(evt);
            }
        });

        JBModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/MODIFICAR.JPG"))); // NOI18N
        JBModificar.setText("MODIFICAR");
        JBModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBModificarActionPerformed(evt);
            }
        });

        JBEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/ELIMINAR.JPG"))); // NOI18N
        JBEliminar.setText("ELIMINAR");
        JBEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBEliminarActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/SALIR2.JPG"))); // NOI18N
        jButton1.setText("SALIR");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("IR LISTA");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FormBotonesLayout = new javax.swing.GroupLayout(FormBotones);
        FormBotones.setLayout(FormBotonesLayout);
        FormBotonesLayout.setHorizontalGroup(
            FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormBotonesLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormBotonesLayout.createSequentialGroup()
                        .addComponent(JBNuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(54, 54, 54)
                        .addComponent(JBModificar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(JBEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(45, 45, 45))
                    .addGroup(FormBotonesLayout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(JTBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(54, 54, 54)
                        .addComponent(JBBuscar)
                        .addGap(55, 55, 55)))
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE))
                .addGap(26, 26, 26))
        );
        FormBotonesLayout.setVerticalGroup(
            FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormBotonesLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(JTBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JBBuscar)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JBNuevo)
                    .addComponent(JBModificar)
                    .addComponent(JBEliminar)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        jLabel1.setText("CODIGO:");

        JTCodigo.setEnabled(false);

        jLabel7.setText("EMPRESA SEDE:");

        JComboSede.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel2.setText("DESCRIPCION:");

        JBGrabar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/GUARDAR2.JPG"))); // NOI18N
        JBGrabar.setText("GRABAR");
        JBGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBGrabarActionPerformed(evt);
            }
        });

        JBSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/CANCELAR.JPG"))); // NOI18N
        JBSalir.setText("SALIR SIN GRABAR");
        JBSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FormParaLlenarLayout = new javax.swing.GroupLayout(FormParaLlenar);
        FormParaLlenar.setLayout(FormParaLlenarLayout);
        FormParaLlenarLayout.setHorizontalGroup(
            FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormParaLlenarLayout.createSequentialGroup()
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormParaLlenarLayout.createSequentialGroup()
                        .addGap(72, 72, 72)
                        .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addGap(18, 18, 18)
                        .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(JTDesc, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(JComboSede, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(JTCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(FormParaLlenarLayout.createSequentialGroup()
                        .addGap(62, 62, 62)
                        .addComponent(JBGrabar, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(59, 59, 59)
                        .addComponent(JBSalir)))
                .addContainerGap(80, Short.MAX_VALUE))
        );
        FormParaLlenarLayout.setVerticalGroup(
            FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormParaLlenarLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(JTCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(JComboSede, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(JTDesc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JBGrabar)
                    .addComponent(JBSalir))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(FormParaMostrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(FormBotones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addComponent(FormParaLlenar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(FormParaLlenar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(FormParaMostrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(FormBotones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JBGrabarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBGrabarActionPerformed

        //Datos de caja
        String codigo = JTCodigo.getText();//Nunca es nulo
        String nombre = JTDesc.getText();
	//Combo
        String empSede = JComboSede.getSelectedItem().toString();//Tampoco es nulo
       
        if (nombre == null || nombre.equals("") ) {
            JOptionPane.showMessageDialog(null, "Completar los campos obligatorios");

        } else {
            //Insertar
            if (bandera) {
                String[] data = {empSede, nombre};
                try {
                    ejecutor.insertar(data);
                    JOptionPane.showMessageDialog(null, "Lista guardada correctamente.");
                    estado(1);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "ERROR: Verifique los campos");
                    System.out.println(ex.getMessage());
                }
            //Modificar
            } else {
                String[] data = {codigo,empSede, nombre};
                try {
                    ejecutor.modificar(data);
                    JOptionPane.showMessageDialog(null, "Lista modificada correctamente.");
                    estado(1);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "ERROR: Verifique los campos");
                    System.out.println(ex.getMessage());
                }
            }
        }
    }//GEN-LAST:event_JBGrabarActionPerformed

    private void JBSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBSalirActionPerformed
        estado(1);
        refrescar();
    }//GEN-LAST:event_JBSalirActionPerformed

    private void JBBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBBuscarActionPerformed
        ejecutor.llenarTabla(JTabla, JTBuscar.getText());
        estado(1);
    }//GEN-LAST:event_JBBuscarActionPerformed

    private void JBEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBEliminarActionPerformed
         if (JTabla.getSelectedRow() != -1) {
            String[] datos = new String[cantDatos];
            int indiceFila = JTabla.getSelectedRow();
            int numCol = JTabla.getModel().getColumnCount();
            for (int i = 0; i < numCol; i++) {
                datos[i] = (String) JTabla.getModel().getValueAt(indiceFila, i);
            }
            int res = JOptionPane.showConfirmDialog(null, "¿SEGURO QUE DESEA ELIMINAR?");
            if (JOptionPane.OK_OPTION == res) {
                ejecutor.eliminar(datos);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un item para Eliminar");
        }
        estado(1);
        
        
        
    }//GEN-LAST:event_JBEliminarActionPerformed

    private void JBModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBModificarActionPerformed
        bandera = false;
        String[] datos = new String[cantDatos];
        ejecutor.llenarCaja(JComboSede);
        if (JTabla.getSelectedRow() != -1) {
            estado(2);
            int indiceFila = JTabla.getSelectedRow();
            int numCol = JTabla.getModel().getColumnCount();

            for (int i = 0; i < numCol; i++) {
                datos[i] = (String) JTabla.getModel().getValueAt(indiceFila, i);
            }
            for (int i = 0; i < datos.length; i++) {
                System.out.print(datos[i] + " ");
            }

            JTCodigo.setText(datos[0]);
            JTCodigo.setEditable(false);
            JTDesc.setText(datos[1]);
            

            int i;
            for (i = 0; i < JComboSede.getItemCount(); i++) {
                if (JComboSede.getItemAt(i).equals(datos[3])) {
                    break;
                }
            }

            JComboSede.setSelectedItem(JComboSede.getItemAt(i));
            datos[4] = "1";

        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un item para modificar");

        }
    }//GEN-LAST:event_JBModificarActionPerformed

    private void JBNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBNuevoActionPerformed
        estado(2);
        ejecutor.generarCodigo(JTCodigo);
        ejecutor.llenarCaja(JComboSede);
        bandera = true;
        limpiar();
    }//GEN-LAST:event_JBNuevoActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
       this.setVisible(false);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
         if (JTabla!=null&& JTabla.getSelectedRow()>=0 && JTabla.getValueAt(JTabla.getSelectedRow(), 1)!=null){
           
             ArticuloPrecioGUI artPrecio= new ArticuloPrecioGUI(""+JTabla.getValueAt(JTabla.getSelectedRow(), 0));
             artPrecio.setVisible(true);
             artPrecio.estado(1);
                          System.out.println("GUI.ListaGUI.jButton2ActionPerformed()");

         
         }else{
             JOptionPane.showMessageDialog(this,"Seleccione alguna lista" );
         }
        
    }//GEN-LAST:event_jButton2ActionPerformed

    public void estado(int p) {
        ///*estado para buscar en la grilla*/ 
        if (p == 1) {
            this.setTitle("TABLA LISTA");
           ejecutor.llenarTabla(JTabla, JTBuscar.getText());
            //limpiarFormLlenar();
           dimensionTabla(JTabla);
            System.out.println("this " + this.getWidth() + " " + this.getHeight());
            System.out.println("form para llenar" + FormParaLlenar.getWidth() + "  " + FormParaLlenar.getHeight());
            System.out.println("form mostrar" + FormParaMostrar.getWidth() + "--" + FormParaMostrar.getHeight());
            System.out.println("form botones " + FormBotones.getWidth() + "--" + FormBotones.getHeight());

            FormParaLlenar.setVisible(false);
            FormParaMostrar.setVisible(true);
            FormBotones.setVisible(true);
            this.setSize(ANCHO_FORM_MOSTRAR, ALTO_FORM - ALTO_FORM_LLENAR);
            //      cambiarTamaño(0.435F, 0.74F);
            centrarPantalla();

        } else if (p == 2)/*estado para ingresar un dato*/ {
            this.setTitle("INSERTAR LISTA");
            this.setSize(ANCHO_FORM_LLENAR, ALTO_FORM - (ALTO_FORM_MOSTRAR + ALTO_FORM_BOT));
            centrarPantalla();
            FormParaLlenar.setVisible(true);
            FormParaMostrar.setVisible(false);
            FormBotones.setVisible(false);
        }
    }
    private void centrarPantalla() {
        Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension ventana = this.getSize();
        this.setLocation((int) (pantalla.width - ventana.width) / 2, (int) (pantalla.height - ventana.height) / 2);
    }
    public void refrescar() {
        ejecutor.llenarTabla(JTabla, "");
        limpiar();
    }

    public void limpiar() {
        JTDesc.setText("");
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ListaGUI a = new ListaGUI();
                a.setVisible(true);
                a.estado(1);

            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel FormBotones;
    private javax.swing.JPanel FormParaLlenar;
    private javax.swing.JPanel FormParaMostrar;
    private javax.swing.JButton JBBuscar;
    private javax.swing.JButton JBEliminar;
    private javax.swing.JButton JBGrabar;
    private javax.swing.JButton JBModificar;
    private javax.swing.JButton JBNuevo;
    private javax.swing.JButton JBSalir;
    private javax.swing.JComboBox JComboSede;
    private javax.swing.JTextField JTBuscar;
    private javax.swing.JTextField JTCodigo;
    private javax.swing.JTextField JTDesc;
    private javax.swing.JTable JTabla;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables
    private void dimensionTabla(JTable JTablaArticulos) {
        JTablaArticulos.getColumnModel().getColumn(0).setPreferredWidth(0);
        JTablaArticulos.getColumnModel().getColumn(1).setPreferredWidth(130);
        JTablaArticulos.getColumnModel().getColumn(2).setPreferredWidth(0);
        JTablaArticulos.getColumnModel().getColumn(3).setPreferredWidth(130);
        JTablaArticulos.getColumnModel().getColumn(4).setPreferredWidth(0);
    
    }

public static  ListaGUI generarInstancia(){
    if (instancia == null)instancia=new ListaGUI();
    return instancia ;
    

}


}
