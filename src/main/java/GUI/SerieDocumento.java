/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;


import Bean.Empresa;
import Bean.EmpresaSede;
import Bean.Generico;
import Bean.MovimientoDocumento;
import Bean.empresasedeserie;
import Maestros.*;
import java.awt.Dimension;
import java.awt.Toolkit;
import Maestros.Maestro;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 *
 * @author Equipo EPIS
 */
public class SerieDocumento extends javax.swing.JFrame {

    public static final int ANCHO_FORM_LLENAR = 523;
    public static final int ALTO_FORM_LLENAR = 292;
    public static final int ANCHO_FORM_MOSTRAR = 698;
    public static final int ALTO_FORM_MOSTRAR = 388;
    public static final int ANCHO_FORM_BOT = 702;
    public static final int ALTO_FORM_BOT = 106;
    public static final int ANCHO_FORM = 730;
    public static final int ALTO_FORM = 900;
    boolean bandera = true;
    int cantDatos = 7;
    int codigoEmp;
    int codigoGrupo;
    protected MovimientoDocumento  doc;   
                    String [] cabecera ={"CODIGO","DOCUMENTO ", "SEDE","SERIE","NUMERO"};

            String sql = "SELECT\n"

                    + "     EmpresaSedeSerie.`idEmpresaSedeSerie` AS CODIGO,\n"
                   
                    + "     Doc.`MovDocDes` AS  DOCUMENTO,\n"
                    + "     Sede.`EmpSedNom` AS CODIGOCABECERA,\n"

                    + "     EmpresaSedeSerie.`EmpSedSerSer` AS SERIE,\n"
                    + "     EmpresaSedeSerie.`EmpSedSerNum` AS NUMERO\n"

                    
                    + "FROM EmpresaSedeSerie\n "
                            
                    + "     INNER JOIN `EmpresaSede` Sede ON Sede.`IdEmpresaSede` = `EmpresaSedeSerie`.`IdEmpresaSede`\n"
                  + "     INNER JOIN `MovimientoDocumento` Doc ON Doc.`IdMovimientoDocumento` = `EmpresaSedeSerie`.`IdMovimientoDocumento`\n"

                   +" where   EmpresaSedeSerie.`empsedserestreg`=1" ;

    public SerieDocumento() {

        initComponents();
        llenarCaja(JComboEmpSede);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        FormParaLlenar = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JTCodigo = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        JComboEmpSede = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        JTSerie = new javax.swing.JTextField();
        JBGrabar = new javax.swing.JButton();
        JBSalir = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        JTNumero = new javax.swing.JTextField();
        FormParaMostrar = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        JTabla = new javax.swing.JTable();
        txtCodigoEmp = new javax.swing.JLabel();
        FormBotones = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        JTBuscar = new javax.swing.JTextField();
        JBBuscar = new javax.swing.JButton();
        JBNuevo = new javax.swing.JButton();
        JBModificar = new javax.swing.JButton();
        JBEliminar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jLabel1.setText("CODIGO:");

        JTCodigo.setEnabled(false);

        jLabel7.setText("EMPRESA");

        JComboEmpSede.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel3.setText("SERIE:");

        JTSerie.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                JTSerieKeyTyped(evt);
            }
        });

        JBGrabar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/GUARDAR2.JPG"))); // NOI18N
        JBGrabar.setText("GRABAR");
        JBGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBGrabarActionPerformed(evt);
            }
        });

        JBSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/CANCELAR.JPG"))); // NOI18N
        JBSalir.setText("SALIR SIN GRABAR");
        JBSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBSalirActionPerformed(evt);
            }
        });

        jLabel4.setText("NUMERO");

        JTNumero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                JTNumeroKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout FormParaLlenarLayout = new javax.swing.GroupLayout(FormParaLlenar);
        FormParaLlenar.setLayout(FormParaLlenarLayout);
        FormParaLlenarLayout.setHorizontalGroup(
            FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormParaLlenarLayout.createSequentialGroup()
                .addGap(74, 74, 74)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormParaLlenarLayout.createSequentialGroup()
                        .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(FormParaLlenarLayout.createSequentialGroup()
                                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel1))
                                .addGap(33, 33, 33)
                                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(JTSerie, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(JComboEmpSede, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(JTCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(FormParaLlenarLayout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(JTNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(FormParaLlenarLayout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(JBGrabar, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                        .addComponent(JBSalir)
                        .addGap(60, 60, 60))))
        );
        FormParaLlenarLayout.setVerticalGroup(
            FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormParaLlenarLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(JTCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(58, 58, 58)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JComboEmpSede, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(18, 18, 18)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(JTSerie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(JTNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JBSalir)
                    .addComponent(JBGrabar))
                .addContainerGap())
        );

        JTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(JTabla);

        javax.swing.GroupLayout FormParaMostrarLayout = new javax.swing.GroupLayout(FormParaMostrar);
        FormParaMostrar.setLayout(FormParaMostrarLayout);
        FormParaMostrarLayout.setHorizontalGroup(
            FormParaMostrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        FormParaMostrarLayout.setVerticalGroup(
            FormParaMostrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormParaMostrarLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
                .addContainerGap())
        );

        txtCodigoEmp.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtCodigoEmp.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel5.setText("BUSCAR:");

        JBBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/BUSCAR.JPG"))); // NOI18N
        JBBuscar.setText("BUSCAR");
        JBBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBBuscarActionPerformed(evt);
            }
        });

        JBNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/NUEVO.JPG"))); // NOI18N
        JBNuevo.setText("NUEVO");
        JBNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBNuevoActionPerformed(evt);
            }
        });

        JBModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/MODIFICAR.JPG"))); // NOI18N
        JBModificar.setText("MODIFICAR");
        JBModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBModificarActionPerformed(evt);
            }
        });

        JBEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/ELIMINAR.JPG"))); // NOI18N
        JBEliminar.setText("ELIMINAR");
        JBEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBEliminarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FormBotonesLayout = new javax.swing.GroupLayout(FormBotones);
        FormBotones.setLayout(FormBotonesLayout);
        FormBotonesLayout.setHorizontalGroup(
            FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormBotonesLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormBotonesLayout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(JTBuscar)
                        .addGap(33, 33, 33)
                        .addComponent(JBBuscar))
                    .addGroup(FormBotonesLayout.createSequentialGroup()
                        .addComponent(JBNuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(57, 57, 57)
                        .addComponent(JBModificar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                        .addComponent(JBEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(165, 165, 165)))
                .addGap(27, 27, 27))
        );
        FormBotonesLayout.setVerticalGroup(
            FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormBotonesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(JTBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JBBuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(JBNuevo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(JBModificar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(JBEliminar))
                .addContainerGap(34, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(txtCodigoEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(FormBotones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(FormParaMostrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(FormParaLlenar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(txtCodigoEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FormParaLlenar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(FormParaMostrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FormBotones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JBGrabarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBGrabarActionPerformed

    //Datos de caja
        String codigo = JTCodigo.getText();//Nunca es nulo
        String serie = JTSerie.getText();
        String numero=JTNumero.getText();
        EmpresaSede empresaSede = (EmpresaSede) JComboEmpSede.getSelectedItem();//Tampoco es nulo
          empresasedeserie  ser = new  empresasedeserie();
            ser.setEmpsedsernum(numero);
          ser.setEmpsedserser(serie);
            ser.setIdEmpresaSede(empresaSede.getIdEmpresaSede());
            ser.setIdMovimientoDocumento(doc.getIdMovimientoDocumento()+"");
            ser.setIdempresasedeserie(codigo);
            if (numero == null || numero.equals("")|| serie == null || serie.equals("")  ) {
            JOptionPane.showMessageDialog(null, "Completar los campos obligatorios");

        } else {
            //Insertar  
            try {
                   
            if (bandera) {
          
               ser.insertar();
            //Modificar
            } else {
             ser.modificar();
               
            }
            } catch (SQLException ex) {
                    Logger.getLogger(SerieDocumento.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        estado(1);
    }//GEN-LAST:event_JBGrabarActionPerformed

    private void JBSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBSalirActionPerformed
        estado(1);
        limpiar();
        //refrescar();
    }//GEN-LAST:event_JBSalirActionPerformed

    private void JBBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBBuscarActionPerformed
        llenarTabla(sql+ " and empsedserser  LIKE '%"+JTBuscar.getText()+"%'");
        estado(1);
    }//GEN-LAST:event_JBBuscarActionPerformed

    private void JBEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBEliminarActionPerformed
         if (JTabla.getSelectedRow() != -1) {
            String[] datos = new String[cantDatos];
            int indiceFila = JTabla.getSelectedRow();
            int numCol = JTabla.getModel().getColumnCount();
            for (int i = 0; i < numCol; i++) {
                datos[i] = (String) JTabla.getModel().getValueAt(indiceFila, i);
            }
            int res = JOptionPane.showConfirmDialog(null, "¿SEGURO QUE DESEA ELIMINAR?");
            if (JOptionPane.OK_OPTION == res) {
                empresasedeserie ser= new empresasedeserie();
                ser.setIdempresasedeserie(datos[0]);
                try {
                    ser.Eliminar();
                } catch (SQLException ex) {
                    Logger.getLogger(SerieDocumento.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            JOptionPane.showMessageDialog(this, "SELECCIONE UN ITEM PARA ELIMINAR POR FAVOR");
        }
        estado(1);
        
        
        
    }//GEN-LAST:event_JBEliminarActionPerformed
    public void enviarCodigo(int codEmp)
    {
    }
    private void JBModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBModificarActionPerformed
        bandera = false;
        String[] datos = new String[cantDatos];
         

        if (JTabla.getSelectedRow() != -1) {
            estado(2);
            int indiceFila = JTabla.getSelectedRow();
            int numCol = JTabla.getModel().getColumnCount();

            for (int i = 0; i < numCol; i++) {
                datos[i] = (String) JTabla.getModel().getValueAt(indiceFila, i);
            }
            for (int i = 0; i < datos.length; i++) {
                System.out.print(datos[i] + " ");
            }

            JTCodigo.setText(datos[0]);
            JTCodigo.setEditable(false);
            JTSerie.setText(datos[3]);
            JTNumero.setText(datos[4]);

            int i;
            for (i = 0; i < JComboEmpSede.getItemCount(); i++) {
               EmpresaSede sede =  (EmpresaSede) JComboEmpSede.getItemAt(i);
                        if (sede.getEmpSedNom().equals(datos[2])) {
                    break;
                }
            }

            JComboEmpSede.setSelectedItem(JComboEmpSede.getItemAt(i));
            datos[6] = "1";

        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un item para modificar");

        }
    }//GEN-LAST:event_JBModificarActionPerformed

    private void JBNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBNuevoActionPerformed
        estado(2);
        GenerarCodigo gc = new GenerarCodigo();
        String codigo =gc.generarID("EmpresaSedeSerie","idEmpresaSedeSerie");
        JTCodigo.setText(codigo);
        bandera = true;
        limpiar();
    }//GEN-LAST:event_JBNuevoActionPerformed

    private void JTSerieKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTSerieKeyTyped
        char c=evt.getKeyChar();
        if(Character.isLetter(c))
            evt.consume();
    }//GEN-LAST:event_JTSerieKeyTyped

    private void JTNumeroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTNumeroKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_JTNumeroKeyTyped

    public void estado(int p) {
        ///*estado para buscar en la grilla*/ 
        if (p == 1) {
            this.setTitle("MOVIMIENTO DOCUMENTO");
          llenarTabla(sql);   

dimensionTabla(JTabla);
            //limpiarFormLlenar();
            System.out.println("this " + this.getWidth() + " " + this.getHeight());
            System.out.println("form para llenar" + FormParaLlenar.getWidth() + "  " + FormParaLlenar.getHeight());
            System.out.println("form mostrar" + FormParaMostrar.getWidth() + "--" + FormParaMostrar.getHeight());
            System.out.println("form botones " + FormBotones.getWidth() + "--" + FormBotones.getHeight());

            FormParaLlenar.setVisible(false);
            FormParaMostrar.setVisible(true);
            FormBotones.setVisible(true);
            this.setSize(ANCHO_FORM_MOSTRAR+60, ALTO_FORM - ALTO_FORM_LLENAR);
            //      cambiarTamaño(0.435F, 0.74F);
            centrarPantalla();

        } else if (p == 2)/*estado para ingresar un dato*/ {
            this.setTitle("INSERTAR MOVIMIENTO DOCUMETO ");
            this.setSize(ANCHO_FORM_LLENAR + 60, ALTO_FORM - (ALTO_FORM_MOSTRAR + ALTO_FORM_BOT) );
            centrarPantalla();
            FormParaLlenar.setVisible(true);
            FormParaMostrar.setVisible(false);
            FormBotones.setVisible(false);
        }
    }

    private void centrarPantalla() {
        Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension ventana = this.getSize();
        this.setLocation((int) (pantalla.width - ventana.width) / 2, (int) (pantalla.height - ventana.height) / 2);

    }



    public void limpiar() {
        JTSerie.setText("");
        JTNumero.setText("");
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                SerieDocumento a = new SerieDocumento();
                a.setVisible(true);
                a.estado(1);

            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel FormBotones;
    private javax.swing.JPanel FormParaLlenar;
    private javax.swing.JPanel FormParaMostrar;
    private javax.swing.JButton JBBuscar;
    private javax.swing.JButton JBEliminar;
    private javax.swing.JButton JBGrabar;
    private javax.swing.JButton JBModificar;
    private javax.swing.JButton JBNuevo;
    private javax.swing.JButton JBSalir;
    private javax.swing.JComboBox JComboEmpSede;
    private javax.swing.JTextField JTBuscar;
    private javax.swing.JTextField JTCodigo;
    private javax.swing.JTextField JTNumero;
    private javax.swing.JTextField JTSerie;
    private javax.swing.JTable JTabla;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel txtCodigoEmp;
    // End of variables declaration//GEN-END:variables
private void dimensionTabla(JTable JTabla) {
  
    }

    private void llenarTabla(String sql2) {
        System.err.println(sql2);
          Generico.llenarTabla(JTabla, cabecera, sql2);
    
    }

    private void llenarCaja(JComboBox JComboEmpSede) {
        JComboEmpSede.removeAllItems();
ArrayList <Empresa> empresasHUGO = Empresa.getEmpresasForDato("idempresaGrupo", "1");
       ArrayList <EmpresaSede> empresaSedes = new ArrayList<>();
        for (Empresa empresa : empresasHUGO) {
            ArrayList <EmpresaSede> sedes =EmpresaSede.getEmpresaSedesForDato("IdEmpresa",empresa.getIdEmpresa()) ;
            for (EmpresaSede sede : sedes) {
                empresaSedes.add(sede);
            }
        }
        for (EmpresaSede empresaSede : empresaSedes) {
            JComboEmpSede.addItem(empresaSede);
        }    
    }
}
