/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReporteCostots;
 
import KARDEXRESUMIDO.*;

/**
 *
 * @author Gutierrez
 */
public class Atributos_Kardex {
   
    String fecha;
    String tipo;
    String movimiento;
    String numero;
    String sede;
    String destino;

    public Atributos_Kardex(String fecha, String tipo, String movimiento, String numero, String sede, String destino) {
        this.fecha = fecha;
        this.tipo = tipo;
        this.movimiento = movimiento;
        this.numero = numero;
        this.sede = sede;
        this.destino = destino;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(String movimiento) {
        this.movimiento = movimiento;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }
    

    
}