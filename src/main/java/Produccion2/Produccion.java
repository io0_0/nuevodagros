/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produccion2;

import produccion.*;
import Bean.Articulo;
import Bean.Formula;
import Bean.FormulaDetalle;
import Bean.MovimientoCab;
import Bean.MovimientoDet;
import Bean.MovimientoTipo;
import Bean.UnidadMedida;
import Modificar.MODIFICARCOMPROBANTE;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cesar
 */
public class Produccion {

    static Produccion getByAsociado(String id) {
        try {
            Produccion ret = new Produccion();
            ret.cabeceraInsumo=MovimientoCab.getAsociadoBool(id, ""+INSUMO);
            ret.cabeceraMerma=MovimientoCab.getAsociadoBool(id, ""+MERMA);
            ret.cabeceraProducto=MovimientoCab.getAsociadoBool(id, ""+PRODUCTO);
            ret.DetallesInsumo=MovimientoDet.getByIdCabecera(ret.cabeceraInsumo.getIdMovimientoCabecera()+"");
            ret.DetallesMerma = MovimientoDet.getByIdCabecera(ret.cabeceraMerma.getIdMovimientoCabecera());
            ret.DetallesProducto=MovimientoDet.getByIdCabecera(ret.cabeceraProducto.getIdMovimientoCabecera());
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(Produccion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    static Produccion getByAsociado(String id , int sede) {
        try {
            Produccion ret = new Produccion();
            ret.cabeceraInsumo=MovimientoCab.getAsociadoBool(id, ""+INSUMO,sede);
            ret.cabeceraMerma=MovimientoCab.getAsociadoBool(id, ""+MERMA,sede);
            ret.cabeceraProducto=MovimientoCab.getAsociadoBool(id, ""+PRODUCTO,sede);
            ret.DetallesInsumo=MovimientoDet.getByIdCabecera(ret.cabeceraInsumo.getIdMovimientoCabecera()+"");
            ret.DetallesMerma = MovimientoDet.getByIdCabecera(ret.cabeceraMerma.getIdMovimientoCabecera());
            ret.DetallesProducto=MovimientoDet.getByIdCabecera(ret.cabeceraProducto.getIdMovimientoCabecera());
            System.out.println("La cabecera"+ret.cabeceraProducto.getIdMovimientoCabecera()+"DEtalles perones"+ret.DetallesProducto.size());
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(Produccion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    
    
    /**
     * @param args the command line arguments
     */
    
    
    public ArrayList<MovimientoDet> DetallesMerma  = new ArrayList<>();
    public ArrayList<MovimientoDet> DetallesProducto  = new ArrayList<>() ;
    public ArrayList<MovimientoDet> DetallesInsumo = new ArrayList<>();
    public MovimientoCab cabecera=new MovimientoCab();
    public MovimientoCab cabeceraMerma=new MovimientoCab();
    public MovimientoCab cabeceraInsumo=new MovimientoCab();
    public MovimientoCab cabeceraProducto=new MovimientoCab();
    
    public Formula formula;
    public static final int MERMA = 3;
    
    public static final int PRODUCTO = 1;
    public static  final int INSUMO = 2;
public static final int [] TIPOS = {PRODUCTO,INSUMO,MERMA};
    public Produccion() {
    }
    public ArrayList<MovimientoDet> getDetalleById(int det){
     if(det == PRODUCTO)return DetallesProducto;
     if(det == INSUMO)return DetallesInsumo;
     if(det == MERMA)return DetallesMerma;
    
     
     
     return null;
    } 
    public void formulaAdetalles(){
          
    }
    public void setFormula(Formula f){
        formula = f; 
        DetallesInsumo=new ArrayList<>();
        DetallesMerma=new ArrayList<>();
        DetallesProducto=new ArrayList<>();
        
        for (FormulaDetalle detalle : formula.detalles) {
            MovimientoDet det = new MovimientoDet();
            det.setIdArticulo(""+detalle.getIdArticulo());
            det.setMovDetCan(""+detalle.getForDetProCan());
            
            if (detalle.getIdCategoriaProduccion()==MERMA){
                DetallesMerma.add(det);
            }
            
            if (detalle.getIdCategoriaProduccion()==PRODUCTO){
                DetallesProducto.add(det);
            }
            if (detalle.getIdCategoriaProduccion()==INSUMO){
                DetallesInsumo.add(det);
            }
        }
    
    }
    public static Object[][] paraTabla(ArrayList < MovimientoDet> detalles){
       String [] []  salida = new String[detalles.size()][3];
       int i  = 0 ; 
       for (MovimientoDet detalle : detalles) {
           Articulo ar = new Articulo(Integer.parseInt(detalle.getIdArticulo()));
           salida [i][0]=ar.getArtNom();
           salida [i][1]=new UnidadMedida(ar.getIdUnidadMedida()+"").getUniMedDes();
           
           salida [i][2]=detalle.getMovDetCan();
           i++;
        }
        return salida ;
    }
    public void setearCabeceras(){
       cabeceraInsumo = new MovimientoCab(cabecera);
       cabeceraInsumo.setIdMovimientoTipo(""+MODIFICARCOMPROBANTE.VENTA);
       cabeceraInsumo.setMovCabProBoo(INSUMO);
       
       cabeceraMerma = new MovimientoCab(cabecera);
       cabeceraMerma.setIdMovimientoTipo(""+MODIFICARCOMPROBANTE.COMPRA);
       cabeceraMerma.setMovCabProBoo(MERMA);
       cabeceraProducto = new MovimientoCab(cabecera);
       cabeceraProducto.setIdMovimientoTipo(""+MODIFICARCOMPROBANTE.COMPRA);
       cabeceraProducto.setMovCabProBoo(PRODUCTO);
    
    }
    public int guardar(){
        try {
            setearCabeceras();
            String idProductos = cabeceraProducto.insertarID();
            cabeceraMerma.setIdMovimientoCabeceraAsociado(idProductos);
            cabeceraProducto.setIdMovimientoCabeceraAsociado(idProductos);
            cabeceraInsumo.setIdMovimientoCabeceraAsociado(idProductos);
            cabeceraProducto.modificar();
            for (MovimientoDet movimientoDet : DetallesProducto) {
                movimientoDet.setIdMovimientoCabecera(idProductos);
                movimientoDet.insertarID();
            }
            
            String idInsumo = cabeceraInsumo.insertarID();
            for (MovimientoDet movimientoDet : DetallesInsumo) {
                movimientoDet.setIdMovimientoCabecera(idInsumo);
                movimientoDet.insertarID();

            }
            
            String idMerma = cabeceraMerma.insertarID();
            for (MovimientoDet movimientoDet : DetallesMerma) {
                movimientoDet.setIdMovimientoCabecera(idMerma);

                movimientoDet.insertarID();
            }
        return Integer.parseInt(idProductos); 
        } catch (SQLException ex) {
            Logger.getLogger(Produccion.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       return -1;
    
    }
     public int guardar(int asociado){
        try {
            setearCabeceras();
            cabeceraProducto.setIdMovimientoCabeceraAsociado(asociado+"");
            String idProductos = cabeceraProducto.insertarID();
            cabeceraMerma.setIdMovimientoCabeceraAsociado(asociado+"");
            cabeceraProducto.setIdMovimientoCabeceraAsociado(asociado+"");
            cabeceraInsumo.setIdMovimientoCabeceraAsociado(asociado+"");
            cabeceraProducto.modificar();
            System.out.println("*************************************");
            System.out.println(cabecera.getIdEmpresaSede());    
            
            System.out.println("Productos");

            for (MovimientoDet movimientoDet : DetallesProducto) {
                System.out.println("Produccion2.Produccion.guardar():"+ movimientoDet.getMovDetCan());
                movimientoDet.setIdMovimientoCabecera(idProductos);
                movimientoDet.insertarID();
            }
            
            String idInsumo = cabeceraInsumo.insertarID();
            System.out.println("Insumos");
            for (MovimientoDet movimientoDet : DetallesInsumo) {
                System.out.println("Produccion2.Produccion.guardar():"+ movimientoDet.getMovDetCan());
                movimientoDet.setIdMovimientoCabecera(idInsumo);
                movimientoDet.insertarID();

            }
            
            String idMerma = cabeceraMerma.insertarID();
            for (MovimientoDet movimientoDet : DetallesMerma) {
                System.out.println("Produccion2.Produccion.guardar():"+ movimientoDet.getMovDetCan());

                movimientoDet.setIdMovimientoCabecera(idMerma);

                movimientoDet.insertarID();
            }
        return Integer.parseInt(idProductos); 
        } catch (SQLException ex) {
            Logger.getLogger(Produccion.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       return -1;
    
    }
    
    public static void main(String[] args) {
        // TODO code application logic here

    }
    
}
