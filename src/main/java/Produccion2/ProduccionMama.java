/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produccion2;

import Bean.Articulo;
import java.util.ArrayList;
import Bean.EmpresaSede;
import Bean.MovimientoCab;
import Bean.MovimientoDet;
import Bean.UnidadMedida;
import static Produccion2.Produccion.INSUMO;
import static Produccion2.Produccion.MERMA;
import static Produccion2.Produccion.PRODUCTO;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author io
 */
public class ProduccionMama {

    static ProduccionMama getByAsociado(String id) {
    
          ProduccionMama mama = new ProduccionMama(Produccion.getByAsociado(id,4));
          ArrayList<EmpresaSede> sedes = EmpresaSede.getEmpresasAlmacen();
          mama.empresas=sedes;
          mama.producciones=new ArrayList<>();
         for (EmpresaSede empresa : mama.empresas) {
             mama.producciones.add(Produccion.getByAsociado(id,Integer.parseInt( empresa.getIdEmpresaSede())));
                     
         }
 
            return mama;
   }
    Produccion produccion ;
    ArrayList<Produccion> producciones  = new ArrayList<>();
    ArrayList<EmpresaSede> empresas = new ArrayList<>();
    int idAsociado;

    public ProduccionMama( Produccion pro ) {
       
        
        
        produccion=pro;
        
    }
    
     public ProduccionMama( Produccion pro  ,ArrayList<EmpresaSede> sed) {
       
        
        
        produccion=pro;
        empresas=sed;
        completarPerritas();
        
    }
     public ProduccionMama( Produccion pro  ,ArrayList<EmpresaSede> sed ,double [] proporcion) {
       
        
        
        produccion=pro;
        empresas=sed;
        completarPerritas();
        ajustarProporcion(proporcion);
        
    }
    
    
    public void guardar(int asoc){
        int i  = 0 ;  
        int asociado = 0 ;
        for (Produccion pro : producciones) {
            pro.cabecera = new MovimientoCab(produccion.cabecera);
            pro.cabecera.setIdEmpresaSede(empresas.get(i).getIdEmpresaSede());
            pro.cabecera.setIdEmpresaSedeDes(empresas.get(i).getIdEmpresaSede());
                pro.guardar(asoc);
            
            i ++;
         }
    
    }
       public  Object[][] paraTabla(int queTabla){
       int empSize = empresas.size();
       int productoSize = produccion.getDetalleById(queTabla).size();
       String salida [][] = new String[productoSize][empSize];
       int QueEmpresa = 0 ;
       for (EmpresaSede empresa : empresas) {
         int i  = 0 ; 
           
            for (MovimientoDet detalle : producciones.get(QueEmpresa).getDetalleById(queTabla)) {
          
                 salida [i][QueEmpresa] = detalle.getMovDetCan();
                 i++;
             }
         QueEmpresa++;
        }
        return salida ;
    }
    public void completarPerritas() {
        for (EmpresaSede empresa : empresas) {
            producciones.add(new Produccion());

        }
        for (Produccion produccione : producciones) {
            produccione.DetallesInsumo = new ArrayList<>();
            for (MovimientoDet deti:produccion.DetallesInsumo){
            produccione.DetallesInsumo.add(new MovimientoDet(deti));
            }
            
            produccione.DetallesMerma = new ArrayList<>();
            for (MovimientoDet deti:produccion.DetallesMerma){
            produccione.DetallesMerma.add(new MovimientoDet(deti));
            }
            
            produccione.DetallesProducto = new ArrayList<>();
            for (MovimientoDet deti:produccion.DetallesProducto){
            produccione.DetallesProducto.add(new MovimientoDet(deti));
            }
            
            produccione.cabecera = new MovimientoCab(produccion.cabecera);
        }
        
 
    
    
    }    

    private void ajustarProporcion(double[] proporcion) {
        int i  = 0 ;
            for (Produccion produccione : producciones) {
                for (int s:Produccion.TIPOS) {
                    ArrayList<MovimientoDet> detalles = produccione.getDetalleById(s);
                    for (MovimientoDet detalle : detalles) {
                        double nuevo  = Double.parseDouble(detalle.getMovDetCan())*proporcion[i];
                        detalle.setMovDetCan(nuevo+"");
                    }
                }
                i++;
            }
    
    }
    
    
}
