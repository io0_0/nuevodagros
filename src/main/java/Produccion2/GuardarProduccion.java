/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produccion2;

import produccion.*;
import Bean.Generico;
import Conexion.Mysql;
import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author cesar
 */
public class GuardarProduccion {
    String sql;
    Connection reg;
    //Datos a utilizar para Movimiento Cabecera
    String IdMovCab;    //Id de movimiento
    String IdMovDoc;    //Id de movimiento documento
    String IdUsu;   //Id del usuario que hace la orden
    String IdEmpSed;    //Id de EMpresa sede
    String IdEmpSedDes;    //Id de EMpresa sede Detino 
    String IdMovTip;    //Id de tipo de movimiento
    String MovCabSto;   //Stock
    String MovCabFec;   //Fecha
    String MovCabSer;   //Serie
    String MovCabNum;   //Numero
    String MovCabEnv;   //Envio
    String MovCabEstReg;    //Estado de Registro
    
    //Datos a utilizar para Movimiento Detalle
    String IdMovDet;    //Id de movimiento detalle 
    String IdMovCabe;    //Id de movimiento cabecera
    String IdArt;   //Id del articulo
    String MovIng;    //Movimiento Ingreso
    String MovDetCan;    //Cantidad
    String MovDetPreTot;   //Precio Total
    String MovDetCosTot;   //Costo Total
    String MovDetIgv;   // IGV
    String MovDetEstReg;   //Estado de Registro
    String MovCabProVer;  //Produccion Verificar cantidades (flag= 0, 1)
    
    public String guardarCabecera(String[] datos) {
        /*
        Guardamos datos
        */
        IdMovCab = datos[0];    //Id de movimiento
        IdMovDoc = datos[1];    //Id de movimiento documento
        IdUsu = datos[2];   //Id del usuario que hace la orden
        IdEmpSed = datos[3];    //Id de EMpresa sede
        IdEmpSedDes = datos[4];    //Id de EMpresa sede
        IdMovTip = datos[5];    //Id de tipo de movimiento
        MovCabSto = datos[6];   //Stock
        MovCabFec = datos[7];   //Fecha
        MovCabSer = datos[8];   //Serie
        MovCabNum = datos[9];   //Numero
        MovCabEnv = datos[10];   //Si se envio (produccion=0) no se envia
        MovCabEstReg = datos[11];
        MovCabProVer = datos[12]; // Verificacion de cantidades correctas en OP
        
        /*
        Insertamos en la base de datos
        */
        try {
//          Mysql con = new Mysql();
            reg = Mysql.getConection();

            sql = "INSERT INTO movimientocab (IdMovimientoCabecera, IdMovimientoDocumento, IdUsuario, IdEmpresaSede, IdEmpresaSedeDes, IdMovimientoTipo, MovCabMovSto, MovCabFec, MovCabSer, MovCabNum, MovCabEnv, MovCabEstReg, MovCabProVer)VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            PreparedStatement pst = reg.prepareStatement(sql);
            pst.setInt(1, Integer.parseInt(IdMovCab));
            pst.setInt(2, Integer.parseInt(IdMovDoc));
            pst.setInt(3, Integer.parseInt(IdUsu));
            pst.setInt(4, Integer.parseInt(IdEmpSed));
            pst.setInt(5, Integer.parseInt(IdEmpSedDes));
            pst.setInt(6, Integer.parseInt(IdMovTip));
            pst.setBoolean(7, Boolean.parseBoolean(MovCabSto));
            SimpleDateFormat format = new SimpleDateFormat("dddd/MM/yy");
            pst.setString(8, MovCabFec); 
            pst.setString(9, MovCabSer);
            pst.setString(10, MovCabNum);
            pst.setBoolean(11, Boolean.parseBoolean(MovCabEnv));
            pst.setInt(12, Integer.parseInt(MovCabEstReg));
            pst.setInt(13, Integer.parseInt(MovCabProVer));
            
            int n = pst.executeUpdate();
            if (n > 0) {
                System.out.println("Registrado con exito -> Cabecera Produccion");
            } else {
                JOptionPane.showMessageDialog(null, "Error al registrar (faltan campos) Detalle");
            }
            
            /*
            Actualizar en La tabla de EMpresaSerie
            */ 
            
            String codigoDoc = Generico.sacarDato("movimientodocumento", "MovDocDes", "'PRODUCCION'");
            sql = ("UPDATE empresasedeserie SET EmpSedSerNum = EmpSedSerNum + '" + 1 + "' WHERE IdEmpresaSede='" + IdEmpSed + "' AND idmovimientoDocumento = " + codigoDoc); 
            Statement st = reg.createStatement(); 
            st.execute(sql); 
            System.out.println("Modificado con exito el numero y serie  de empresa");
            reg.close();
        
        } catch (HeadlessException | NumberFormatException | SQLException e) {
            System.out.println(e.getMessage());
        } 

        return IdMovCab;
    }
    
    public String guardarDetalle(String[] datos){
        /*
        Guardamos datos
        */
        IdMovDet = datos[0];    //Id de movimiento detalle
        IdMovCabe = datos[1];   //Id de movimiento cabecera
        IdArt = datos[2];    //Id articulos
        MovIng = datos[3];   // Movimiento de Ingreso (insumos=0 producto=1 merma=2)
        MovDetCan = datos[4];    //Cantidad
        MovDetPreTot = datos[5];    //Precio total
        MovDetCosTot = datos[6];   //Costo total
        MovDetIgv = datos[7];   //IGV
        MovDetEstReg = datos[8];   //EStado de Registro
        
        
        /*
        Insertamos en la base de datos
        */
        try {
//            Mysql con = new Mysql();
            reg = Mysql.getConection();
            sql = "INSERT INTO movimientodet (IdMovimientoCabecera, IdArticulo, "
                    + "MovIngreso, MovDetCan, MovDetCosTot, MovDetIgv, MovDetEstReg)VALUES (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement pst = reg.prepareStatement(sql);
            
            pst.setInt(1, Integer.parseInt(IdMovCabe));
            pst.setInt(2, Integer.parseInt(IdArt));
            pst.setInt(3, Integer.parseInt(MovIng));
            pst.setDouble(4, Double.parseDouble(MovDetCan));
            pst.setDouble(5, Double.parseDouble(MovDetCosTot));
            pst.setDouble(6, Double.parseDouble(MovDetIgv));
            pst.setInt(7, Integer.parseInt(MovDetEstReg));

            int n = pst.executeUpdate();
            if (n > 0) {
                System.out.println("Registrado con exito -> GuardarDetalle");
            } else {
                JOptionPane.showMessageDialog(null, "Error al registrar (faltan campos)");
            }
            reg.close();
        } catch (HeadlessException | NumberFormatException | SQLException e) {
            System.out.println(e.getMessage());
            Logger.getLogger(GuardarProduccion.class.getName()).log(Level.SEVERE, null, e);
        }
        return IdMovDet;
    }

}
