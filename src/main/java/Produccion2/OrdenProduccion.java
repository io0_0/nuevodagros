/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produccion2;

import produccion.*;
import Bean.Articulo;
import Bean.Configurar;
import Bean.Empresa;
import Bean.EmpresaSede;
import Bean.Formula;
import Bean.Generico;
import Bean.MovimientoDet;
import Bean.Usuario;
import Bean.empresasedeserie;
import Conexion.Mysql;
import GUI.Principal;
import IngresoAlmacen.CompraeIngresoAlmacen;
import static Maestros.Atributos.EMPSEDNOM;
import java.awt.Component;
import java.awt.ScrollPane;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.AbstractCellEditor;
import javax.swing.BoundedRangeModel;
import javax.swing.CellEditor;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

/**
 *
 * @author cesar
 */
public final class OrdenProduccion extends javax.swing.JFrame {

    /**
     * Creates new form OrdenProduccion
     */
    Connection cn;
    String sql;
    int IdMovCab;
      ArrayList<EmpresaSede> empresasSede = EmpresaSede.getEmpresasAlmacen();
            
    JComboBox<String> comboBox;
    JComboBox<String> comboBox2;
    JComboBox<String> comboBox3;
    
    ProduccionMama produccionMama ;
    int ceros;
    int seguir_guardando = 0;
    boolean fech = false;
    Produccion produccion = new Produccion();
    //Contadores para llenar en las tablas
    int conTablaPro = 0;
    int conTablaIns = 0;
    int conTablaMer = 0;

    //Flag que verifica si las cantidades completadas estan bien o mal
    int flagVerifCantidades;
    ArrayList<Double> cantIdealesPro = new ArrayList<>();
    ArrayList<Double> cantIdealesIns = new ArrayList<>();
    ArrayList<Double> cantIdealesMer = new ArrayList<>();
    
    ArrayList<Double> excesos = new ArrayList<>();

    //Variables que almacenan  lo que paso
    String loquepaso = "";
    // idLoquePaso = 2, 3, 4 guarda el id de lo que paso, (no todo esta bien)
    int idLoquePaso = 2;
    double cantExceso = 0;
empresasedeserie serieObjeto;
    ArrayList<MovimientoDet> sed = new ArrayList<>();
public OrdenProduccion(String id ){
        try {
            iniMio();
            produccion = Produccion.getByAsociado(id);
            for (MovimientoDet movimientoDet : produccion.DetallesInsumo) {
                MovimientoDet sd = new MovimientoDet();
                sd.setIdMovimientoCabecera(movimientoDet.getIdMovimientoCabecera());
                sed.add(sd);
            }
            for (MovimientoDet movimientoDet : produccion.DetallesProducto) {
                MovimientoDet sd = new MovimientoDet();
                sd.setIdMovimientoCabecera(movimientoDet.getIdMovimientoCabecera());
                sed.add(sd);
            }
            for (MovimientoDet movimientoDet : produccion.DetallesMerma) {
                MovimientoDet sd = new MovimientoDet();
                sd.setIdMovimientoCabecera(movimientoDet.getIdMovimientoCabecera());
                sed.add(sd);
            }
            produccionMama = ProduccionMama.getByAsociado(id);
            
            commpletarTablas("", 2);
            commpletarTablasDetallado();
            String desde = produccion.cabeceraProducto.getMovCabFec();
            
            java.util.Date date1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(desde);
            
            
            fecha.setDate(date1);
        } catch (ParseException ex) {
            Logger.getLogger(OrdenProduccion.class.getName()).log(Level.SEVERE, null, ex);
        }
            
}
    public OrdenProduccion() {
        iniMio();
    }
    public void iniMio(){
        try {
            initComponents();
            initDatos();
            initiar();

            ArrayList<Empresa> empresas = Empresa.getEmpresasForDato("idempresagrupo", "1");

//            restringirEntrada();

            String where = "where ";
            ArrayList<Configurar> configs = Configurar.getCongiguracionesPorDato("idusuario", Principal.jLUsuarioGeneral.getText());
            for (Configurar config : configs) {
                where += "idempresasede = " + config.getIdEmpresaSede() + " or ";
            }
            where = where.substring(0, where.length() - 3);

            Generico.llenarCaja(EmpresaID, "empresasede", "empsednom", where);
            
         String id = Generico.sacarDato("empresasede", "empsednom", "'"+EmpresaID.getSelectedItem().toString()+"'");
            serieObjeto = new empresasedeserie(id,""+13);
            Serie.setText(serieObjeto.getEmpsedserser());
            Numero.setText(serieObjeto.getEmpsedsernum());
            fecha.setDate(new Date());
        
        } catch (SQLException ex) {
            Logger.getLogger(OrdenProduccion.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    
    }
    public void initDatos() {
        try {
            /*
            COmpletar COdigo de Ordenes de Produccion
             */
            cn = Mysql.getConection();

            ResultSet rs;
            Statement st;
            sql = ("SELECT * FROM movimientocab");
            st = cn.createStatement();
            rs = st.executeQuery(sql);

            String datos[] = new String[1];
            while (rs.next()) {
                datos[0] = rs.getString(1);
//                System.err.println("D ->" + datos[0]);
            }
            System.out.println("ultimo codigo :" + datos[0]);

            IdMovCab = Integer.parseInt(datos[0]);
            IdMovCab++;
            System.err.println("codigo " + IdMovCab);
            Codigo.setText(String.valueOf(IdMovCab));
            cn.close();

            /*
            COmpletar Combobox ed empresaSede
             */
            cn = Mysql.getConection();
            sql = ("SELECT * FROM empresasede");
            st = cn.createStatement();
            rs = st.executeQuery(sql);

            while (rs.next()) {
                EmpresaID.addItem(rs.getString(1) + "  " + rs.getString(3));
            }

            cn.close();
            } catch (SQLException ex) {
            Logger.getLogger(OrdenProduccion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        FormLlenar = new javax.swing.JPanel();
        Emp = new javax.swing.JLabel();
        Nume = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        OrdenPro = new javax.swing.JLabel();
        Numero = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        Codigo = new javax.swing.JTextField();
        EmpresaID = new javax.swing.JComboBox<>();
        Serie = new javax.swing.JTextField();
        fecha = new com.toedter.calendar.JDateChooser();
        jLabel6 = new javax.swing.JLabel();
        JCBOrdenProduccion = new javax.swing.JComboBox<>();
        grabar = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jTextFieldProporcion1 = new javax.swing.JTextField();
        jTextFieldProporcion2 = new javax.swing.JTextField();
        jTextFieldProporcion3 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jTextFieldProporcion4 = new javax.swing.JTextField();
        FormInsumos = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TablaInsumos = new javax.swing.JTable();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        TablaInsumos1 = new javax.swing.JTable();
        FormProductos = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        TablaProductos = new javax.swing.JTable();
        jScrollPane6 = new javax.swing.JScrollPane();
        TablaProductos1 = new javax.swing.JTable();
        FormMerma = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        TablaMerma = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        TablaMerma1 = new javax.swing.JTable();
        FormBotoms = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        FormLlenar.setBackground(new java.awt.Color(254, 254, 254));

        Emp.setText("EMPRESA");

        Nume.setText("NÚMERO");

        jLabel5.setText("FECHA");

        OrdenPro.setFont(new java.awt.Font("Ubuntu", 1, 20)); // NOI18N
        OrdenPro.setText("ORDEN DE PRODUCCION ");

        Numero.setEditable(false);
        Numero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NumeroActionPerformed(evt);
            }
        });

        jLabel1.setText("SERIE");

        Codigo.setEditable(false);

        Codigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CodigoActionPerformed(evt);
            }
        });

        EmpresaID.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        EmpresaID.setPreferredSize(new java.awt.Dimension(39, 22));
        EmpresaID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EmpresaIDActionPerformed(evt);
            }
        });

        Serie.setEditable(false);
        Serie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SerieActionPerformed(evt);
            }
        });

        fecha.setDateFormatString("dd-MM-yyyy");

        jLabel6.setText("ORDEN DE PRODUCCIÓN");

        JCBOrdenProduccion.setToolTipText("");
        JCBOrdenProduccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCBOrdenProduccionActionPerformed(evt);
            }
        });

        grabar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/GUARDAR2.JPG"))); // NOI18N
        grabar.setText("SALIR Y GRABAR");
        grabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                grabarActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/SALIR2.JPG"))); // NOI18N
        jButton1.setText("ATRAS");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTextFieldProporcion1.setText("0.25");
        jTextFieldProporcion1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldProporcion1ActionPerformed(evt);
            }
        });
        jTextFieldProporcion1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldProporcion1KeyTyped(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextFieldProporcion1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldProporcion1KeyReleased(evt);
            }
        });

        jTextFieldProporcion2.setText("0");
        jTextFieldProporcion2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldProporcion2KeyReleased(evt);
            }
        });

        jTextFieldProporcion3.setText("0");
        jTextFieldProporcion3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldProporcion3KeyReleased(evt);
            }
        });

        jLabel2.setText("Proporcion");

        jButton2.setText("Actualizar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jTextFieldProporcion4.setText("0.75");
        jTextFieldProporcion4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldProporcion4ActionPerformed(evt);
            }
        });
        jTextFieldProporcion4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldProporcion4KeyReleased(evt);
            }
        });

        javax.swing.GroupLayout FormLlenarLayout = new javax.swing.GroupLayout(FormLlenar);
        FormLlenar.setLayout(FormLlenarLayout);
        FormLlenarLayout.setHorizontalGroup(
            FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormLlenarLayout.createSequentialGroup()
                .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(FormLlenarLayout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(FormLlenarLayout.createSequentialGroup()
                                .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(Emp)
                                    .addComponent(jLabel5))
                                .addGap(18, 18, 18)
                                .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(EmpresaID, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(fecha, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(Nume, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(Numero, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(FormLlenarLayout.createSequentialGroup()
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(34, 34, 34)
                                .addComponent(JCBOrdenProduccion, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(FormLlenarLayout.createSequentialGroup()
                        .addGap(378, 378, 378)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(Serie, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormLlenarLayout.createSequentialGroup()
                        .addGap(217, 217, 217)
                        .addComponent(OrdenPro, javax.swing.GroupLayout.PREFERRED_SIZE, 259, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(Codigo, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormLlenarLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 56, Short.MAX_VALUE)
                        .addComponent(grabar, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(116, 116, 116)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(82, 82, 82))
                    .addGroup(FormLlenarLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldProporcion1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldProporcion2, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldProporcion3, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldProporcion4, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(FormLlenarLayout.createSequentialGroup()
                        .addGap(206, 206, 206)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2)
                        .addGap(94, 94, 94))))
        );
        FormLlenarLayout.setVerticalGroup(
            FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormLlenarLayout.createSequentialGroup()
                .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormLlenarLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(OrdenPro)
                            .addComponent(Codigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(Serie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Emp)
                            .addComponent(EmpresaID, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(FormLlenarLayout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1)
                            .addComponent(grabar))))
                .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormLlenarLayout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Nume)
                            .addComponent(Numero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(FormLlenarLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel2)
                                .addComponent(fecha, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(FormLlenarLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2)))
                .addGap(27, 27, 27)
                .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(JCBOrdenProduccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTextFieldProporcion1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTextFieldProporcion2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTextFieldProporcion3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTextFieldProporcion4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        FormInsumos.setBackground(new java.awt.Color(254, 254, 254));

        jScrollPane1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jScrollPane1KeyPressed(evt);
            }
        });

        TablaInsumos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Descripción", "Unidad", "Cantidad"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TablaInsumos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TablaInsumosMouseClicked(evt);
            }
        });
        TablaInsumos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TablaInsumosKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(TablaInsumos);
        if (TablaInsumos.getColumnModel().getColumnCount() > 0) {
            TablaInsumos.getColumnModel().getColumn(0).setPreferredWidth(110);
            TablaInsumos.getColumnModel().getColumn(2).setPreferredWidth(20);
        }

        jLabel7.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel7.setText("PRODUCTOS  SIN PROCESAR");

        jScrollPane5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jScrollPane5KeyPressed(evt);
            }
        });

        TablaInsumos1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Descripción", "Unidad", "Cantidad"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TablaInsumos1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TablaInsumos1MouseClicked(evt);
            }
        });
        TablaInsumos1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TablaInsumos1KeyPressed(evt);
            }
        });
        jScrollPane5.setViewportView(TablaInsumos1);
        if (TablaInsumos1.getColumnModel().getColumnCount() > 0) {
            TablaInsumos1.getColumnModel().getColumn(0).setPreferredWidth(110);
            TablaInsumos1.getColumnModel().getColumn(2).setPreferredWidth(20);
        }

        javax.swing.GroupLayout FormInsumosLayout = new javax.swing.GroupLayout(FormInsumos);
        FormInsumos.setLayout(FormInsumosLayout);
        FormInsumosLayout.setHorizontalGroup(
            FormInsumosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormInsumosLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(FormInsumosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormInsumosLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 588, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 543, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(83, Short.MAX_VALUE))
        );
        FormInsumosLayout.setVerticalGroup(
            FormInsumosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormInsumosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormInsumosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        FormProductos.setBackground(new java.awt.Color(254, 254, 254));

        jLabel8.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel8.setText("PRODUCTO PROCESADOS ");

        TablaProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Descripción", "Unidad", "Cantidad"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TablaProductos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TablaProductosMouseClicked(evt);
            }
        });
        TablaProductos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TablaProductosKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TablaProductosKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(TablaProductos);
        if (TablaProductos.getColumnModel().getColumnCount() > 0) {
            TablaProductos.getColumnModel().getColumn(0).setPreferredWidth(110);
            TablaProductos.getColumnModel().getColumn(2).setPreferredWidth(20);
        }

        TablaProductos1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Descripción", "Unidad", "Cantidad"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TablaProductos1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TablaProductos1MouseClicked(evt);
            }
        });
        TablaProductos1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TablaProductos1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TablaProductos1KeyReleased(evt);
            }
        });
        jScrollPane6.setViewportView(TablaProductos1);
        if (TablaProductos1.getColumnModel().getColumnCount() > 0) {
            TablaProductos1.getColumnModel().getColumn(0).setPreferredWidth(110);
            TablaProductos1.getColumnModel().getColumn(2).setPreferredWidth(20);
        }

        javax.swing.GroupLayout FormProductosLayout = new javax.swing.GroupLayout(FormProductos);
        FormProductos.setLayout(FormProductosLayout);
        FormProductosLayout.setHorizontalGroup(
            FormProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormProductosLayout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(FormProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormProductosLayout.createSequentialGroup()
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(FormProductosLayout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 584, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 544, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        FormProductosLayout.setVerticalGroup(
            FormProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormProductosLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE))
                .addContainerGap())
        );

        FormMerma.setBackground(new java.awt.Color(254, 254, 254));

        jLabel9.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel9.setText("MERMA");

        TablaMerma.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Descripción", "Unidad", "Cantidad"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TablaMerma.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TablaMermaMouseClicked(evt);
            }
        });
        TablaMerma.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TablaMermaKeyPressed(evt);
            }
        });
        jScrollPane3.setViewportView(TablaMerma);
        if (TablaMerma.getColumnModel().getColumnCount() > 0) {
            TablaMerma.getColumnModel().getColumn(0).setPreferredWidth(110);
            TablaMerma.getColumnModel().getColumn(2).setPreferredWidth(20);
        }

        TablaMerma1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Descripción", "Unidad", "Cantidad"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TablaMerma1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TablaMerma1MouseClicked(evt);
            }
        });
        TablaMerma1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TablaMerma1KeyPressed(evt);
            }
        });
        jScrollPane4.setViewportView(TablaMerma1);
        if (TablaMerma1.getColumnModel().getColumnCount() > 0) {
            TablaMerma1.getColumnModel().getColumn(0).setPreferredWidth(110);
            TablaMerma1.getColumnModel().getColumn(2).setPreferredWidth(20);
        }

        javax.swing.GroupLayout FormMermaLayout = new javax.swing.GroupLayout(FormMerma);
        FormMerma.setLayout(FormMermaLayout);
        FormMermaLayout.setHorizontalGroup(
            FormMermaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormMermaLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(FormMermaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(FormMermaLayout.createSequentialGroup()
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(499, 499, 499))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 591, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 541, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        FormMermaLayout.setVerticalGroup(
            FormMermaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormMermaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormMermaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        FormBotoms.setBackground(new java.awt.Color(254, 254, 254));

        javax.swing.GroupLayout FormBotomsLayout = new javax.swing.GroupLayout(FormBotoms);
        FormBotoms.setLayout(FormBotomsLayout);
        FormBotomsLayout.setHorizontalGroup(
            FormBotomsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        FormBotomsLayout.setVerticalGroup(
            FormBotomsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 57, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(FormLlenar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(FormProductos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(FormInsumos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(FormMerma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(FormBotoms, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(FormLlenar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FormProductos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FormInsumos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FormMerma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FormBotoms, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void grabarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_grabarActionPerformed

        guardar();
  
        this.dispose();
    }//GEN-LAST:event_grabarActionPerformed
public void guardar(){


        try {
            //Control de Serie y Numero
            if (Serie == null || Serie.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "Falta llenar la Serie");
                return;
            }
            
            if (Numero == null || Numero.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "Falta llenar el Número");
                
                return ;
            }
            for (MovimientoDet movimientoDet : sed) {
                movimientoDet.eliminar();
            }
            produccion.cabecera.setIdEmpresaSede(Generico.sacarDato("empresasede","empsednom", "'"+EmpresaID.getSelectedItem().toString()+"'"));
            produccion.cabecera.setIdEmpresaSedeDes(produccion.cabecera.getIdEmpresaSede());
            produccion.cabecera.setIdUsuario(Usuario.getUsuarioActual().getIdUsuario()+"");
            produccion.cabecera.setMovCabEstReg(1+"");
            java.util.Date dia = fecha.getDate();
            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
            produccion.cabecera.setIdMovimientoDocumento(""+13);
            produccion.cabecera.setMovCabFec("'"+sd.format(dia)+"'");
            produccion.cabecera.setMovCabNum(Numero.getText().toString());
            produccion.cabecera.setMovCabSer(Serie.getText().toString());
            int asoc=produccion.guardar();
            produccionMama.produccion=produccion;
            produccionMama.guardar(asoc);
            
            serieObjeto.actualizar();
            JOptionPane.showMessageDialog(null,"GUARDADO EXITOSAMENTE");
        } catch (SQLException ex) {
            Logger.getLogger(OrdenProduccion.class.getName()).log(Level.SEVERE, null, ex);
        }
}

    private void TablaInsumosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TablaInsumosKeyPressed
        // TODO add your handling code here:
//        if (!isEmpty(TablaInsumos)) {
//            verificar(TablaInsumos);
//        }
    }//GEN-LAST:event_TablaInsumosKeyPressed

    private void TablaProductosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TablaProductosKeyPressed
        // TODO add your handling code here:
//        if (!isEmpty(TablaProductos)) {
//            verificar(TablaProductos);
//        }
    }//GEN-LAST:event_TablaProductosKeyPressed

    private void TablaMermaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TablaMermaKeyPressed
        // TODO add your handling code here:
//        if (!isEmpty(TablaMerma)) {
//            verificar(TablaMerma);
//        }
    }//GEN-LAST:event_TablaMermaKeyPressed

    private void TablaInsumosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TablaInsumosMouseClicked
        // TODO add your handling code here:
//        if (!isEmpty(TablaInsumos)) {
        //      verificar(TablaInsumos);
//        }
    }//GEN-LAST:event_TablaInsumosMouseClicked

    private void TablaProductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TablaProductosMouseClicked
        // TODO add your handling code here:
//        if (!isEmpty(TablaProductos)) {
//            verificar(TablaProductos);
//        }
    }//GEN-LAST:event_TablaProductosMouseClicked

    private void TablaMermaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TablaMermaMouseClicked
        // TODO add your handling code here:
//        if (!isEmpty(TablaMerma)) {
//            verificar(TablaMerma);
//        }
    }//GEN-LAST:event_TablaMermaMouseClicked

    private void jScrollPane1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jScrollPane1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jScrollPane1KeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void EmpresaIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EmpresaIDActionPerformed
        if (EmpresaID.getSelectedItem() != null) {
            try {
                // TODO add your handling code here:
                ResultSet rs;
                Statement st;
                cn = Mysql.getConection();

                String idEmpresa = Generico.sacarDato("empresasede", "empsednom", "'" + EmpresaID.getSelectedItem().toString() + "'");
                String codigoDoc = Generico.sacarDato("movimientodocumento", "MovDocDes", "'PRODUCCION'");
                sql = ("SELECT * FROM empresasedeserie WHERE IdEmpresaSede = " + idEmpresa + " AND idmovimientoDocumento = " + codigoDoc);
                st = cn.createStatement();
                rs = st.executeQuery(sql);

                int ultNum = 0;
                boolean flag = false;

                while (rs.next()) {
                    ultNum = Integer.parseInt(rs.getString("EmpSedSerNum")) + 1;
                    Serie.setText(rs.getString("EmpSedSerSer"));
                    flag = true;
                }

                Numero.setText(String.valueOf(ultNum));
                if (!flag) {
                    Serie.setText("");
                    Numero.setText("");
                }
                cn.close();
            } catch (SQLException ex) {
                Logger.getLogger(OrdenProduccion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_EmpresaIDActionPerformed

    private void CodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CodigoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CodigoActionPerformed

    private void NumeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NumeroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NumeroActionPerformed

    private void SerieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SerieActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_SerieActionPerformed

    private void TablaProductosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TablaProductosKeyReleased
        // TODO add your handling code here:
        if (!isEmpty(TablaProductos)) {

//            verificar(TablaProductos);
        }
    }//GEN-LAST:event_TablaProductosKeyReleased

    private void JCBOrdenProduccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCBOrdenProduccionActionPerformed
        try {
            // TODO add your handling code here:
            String idOPCab = Generico.sacarDato("formulacabecera", "ForCabDes", "'" + JCBOrdenProduccion.getSelectedItem().toString() + "'");
            Formula f = Formula.getFormulaID(Integer.parseInt(idOPCab));
            produccion.setFormula(f);
            
            
            if (isEmpty(TablaProductos)) {
                commpletarTablas(idOPCab, 0);
            } else {
                commpletarTablas(idOPCab, 1);
            }
          
     ActualizarDetallado();

        } catch (SQLException ex) {
            Logger.getLogger(OrdenProduccion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_JCBOrdenProduccionActionPerformed
     
    private void TablaMerma1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TablaMerma1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_TablaMerma1MouseClicked

    private void TablaMerma1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TablaMerma1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TablaMerma1KeyPressed

    private void TablaInsumos1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TablaInsumos1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_TablaInsumos1MouseClicked

    private void TablaInsumos1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TablaInsumos1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TablaInsumos1KeyPressed

    private void jScrollPane5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jScrollPane5KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jScrollPane5KeyPressed

    private void TablaProductos1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TablaProductos1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_TablaProductos1MouseClicked

    private void TablaProductos1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TablaProductos1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TablaProductos1KeyPressed

    private void TablaProductos1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TablaProductos1KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_TablaProductos1KeyReleased

    private void jTextFieldProporcion1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldProporcion1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldProporcion1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
ActualizarDetallado();        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jTextFieldProporcion1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldProporcion1KeyPressed
       // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldProporcion1KeyPressed

    private void jTextFieldProporcion1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldProporcion1KeyTyped

// TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldProporcion1KeyTyped

    private void jTextFieldProporcion1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldProporcion1KeyReleased
ActualizarDetallado();         
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldProporcion1KeyReleased

    private void jTextFieldProporcion2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldProporcion2KeyReleased
ActualizarDetallado();         
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldProporcion2KeyReleased

    private void jTextFieldProporcion3KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldProporcion3KeyReleased
ActualizarDetallado();         
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldProporcion3KeyReleased

    private void jTextFieldProporcion4KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldProporcion4KeyReleased

ActualizarDetallado();         

        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldProporcion4KeyReleased

    private void jTextFieldProporcion4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldProporcion4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldProporcion4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(OrdenProduccion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new OrdenProduccion().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Codigo;
    private javax.swing.JLabel Emp;
    private javax.swing.JComboBox<String> EmpresaID;
    private javax.swing.JPanel FormBotoms;
    private javax.swing.JPanel FormInsumos;
    private javax.swing.JPanel FormLlenar;
    private javax.swing.JPanel FormMerma;
    private javax.swing.JPanel FormProductos;
    private javax.swing.JComboBox<String> JCBOrdenProduccion;
    private javax.swing.JLabel Nume;
    private javax.swing.JTextField Numero;
    private javax.swing.JLabel OrdenPro;
    private javax.swing.JTextField Serie;
    private javax.swing.JTable TablaInsumos;
    private javax.swing.JTable TablaInsumos1;
    private javax.swing.JTable TablaMerma;
    private javax.swing.JTable TablaMerma1;
    private javax.swing.JTable TablaProductos;
    private javax.swing.JTable TablaProductos1;
    private static com.toedter.calendar.JDateChooser fecha;
    private javax.swing.JButton grabar;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTextField jTextFieldProporcion1;
    private javax.swing.JTextField jTextFieldProporcion2;
    private javax.swing.JTextField jTextFieldProporcion3;
    private javax.swing.JTextField jTextFieldProporcion4;
    // End of variables declaration//GEN-END:variables

    
    private void initiar() {
        comboBox = new JComboBox<>();
        comboBox2 = new JComboBox<>();
        comboBox3 = new JComboBox<>();

        try {
            Generico.llenarCaja(JCBOrdenProduccion, "formulacabecera", "ForCabDes");

//        initCategoria(jcombocategoriaP);
//        initCategoria(jcombocategoriaM);
//        initCategoria(jcombocategoriaI);
// addComboToTableIns();
//        addComboToTable(TablaProductos, jcombocategoriaP, comboBox);
//        addComboToTable(TablaMerma, jcombocategoriaM, comboBox3);
//        addComboToTable(TablaInsumos, jcombocategoriaI, comboBox2);
        } catch (SQLException ex) {
            Logger.getLogger(OrdenProduccion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
   
    public void crearGrillaParaEmpresas(){
        ArrayList<EmpresaSede> sd = EmpresaSede.getEmpresasAlmacen();
        
        
        int alto = produccion.DetallesInsumo.size();
        int ancho = sd.size();
        
       }

    public void restringirEntrada() {
        //NUMERO SOLO DIGITOS
        Numero.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char caracter = e.getKeyChar();
// Verificar si la tecla pulsada  es un digito
            
if (((caracter < '0') || (caracter > '9')) && (caracter != '\b' /*corresponde a BACK_SPACE*/)) {
                    e.consume(); // ignorar el evento de teclado
                }
            }
        });

        TablaInsumos.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                System.out.println(".keyTyped()");
                char caracter = e.getKeyChar();
// Verificar si la tecla pulsada  es un digito
                if (((caracter < '0') || (caracter > '9')) && (caracter != '\b' /*corresponde a BACK_SPACE*/)) {
                    e.consume(); // ignorar el evento de teclado
                }
            }
        });

        TablaProductos.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char caracter = e.getKeyChar();
// Verificar si la tecla pulsada  es un digito
                if (((caracter < '0') || (caracter > '9')) && (caracter != '\b' /*corresponde a BACK_SPACE*/)) {
                    e.consume(); // ignorar el evento de teclado
                }
            }
        });

        TablaMerma.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char caracter = e.getKeyChar();
// Verificar si la tecla pulsada  es un digito
                if (((caracter < '0') || (caracter > '9')) && (caracter != '\b' /*corresponde a BACK_SPACE*/)) {
                    e.consume(); // ignorar el evento de teclado
                }
            }
        });
    }
    private void createKeybindings(JTable table) {
table.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Enter");
    table.getActionMap().put("Enter", new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent ae) {
              table             .requestFocus();
             table.changeSelection((table.getSelectedRow()+1)%table.getRowCount(),2,false, false);
             table.editCellAt(table.getSelectedRow(),2);
             MyTableCellEditor d = (MyTableCellEditor)table.getCellEditor(table.getSelectedRow(),2);
             ((TextFieldPirata)(d.component)).selectAll();
             String tempo =table.getValueAt((table.getSelectedRow()-1+table.getRowCount())%table.getRowCount(),2).toString();
             System.out.println(tempo);
             try {
                Double.parseDouble(tempo);
                
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Ingreso letras"+tempo);
            }
 
        
        }     
            
//do something on JTable enter pressed
        
    });
}
    ArrayList<TextFieldPirata> conjuntoTextField = new ArrayList<>();
    public void listenerToColumns(int s){
       TextFieldPirata sd = new TextFieldPirata();
       
       sd.addFocusListener(new FocusListener() {
           @Override
           public void focusGained(FocusEvent fe) {
           sd.selectAll();
           }

           @Override
           public void focusLost(FocusEvent fe) {
           }
                
            }
        );
       sd.nextIndex = conjuntoTextField.size();
       sd.addKeyListener(new KeyListener() {
      public void keyPressed(KeyEvent keyEvent) {
       
          printIt("Pressed", keyEvent);
      }

      public void keyReleased(KeyEvent keyEvent) {
        printIt("Released", keyEvent);
      }

      public void keyTyped(KeyEvent keyEvent) {
        printIt("Typed", keyEvent);
      }
      
      private void printIt(String title, KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        String keyText = KeyEvent.getKeyText(keyCode);
        System.out.println(title + " : " + keyText + " / " + keyEvent.getKeyChar());
          try {
          double cantidad  = Double.parseDouble(sd.getText());
        JTable t =(JTable)sd.getParent(); 
        t.getSelectedRow();
        if(s==produccion.MERMA){ 
        actualizarData(produccion.DetallesMerma, t.getSelectedRow(),""+cantidad);
        }
        if(s==produccion.PRODUCTO){ 
        actualizarData(produccion.DetallesProducto, t.getSelectedRow(),""+cantidad);
        }if(s==produccion.INSUMO){ 
        actualizarData(produccion.DetallesInsumo, t.getSelectedRow(),""+cantidad);
        }
    
          } catch (Exception e) {
         
          }
                
        }
      
    });
   conjuntoTextField.add(sd);
            
         if(s==produccion.MERMA){ 
        TablaMerma.getColumn(TablaInsumos.getColumnName(2)).setCellEditor(new MyTableCellEditor(sd));
        }
        if(s==produccion.PRODUCTO){ 
       TablaProductos.getColumn(TablaInsumos.getColumnName(2)).setCellEditor(new MyTableCellEditor(sd));
        }if(s==produccion.INSUMO){ 
       TablaInsumos.getColumn(TablaInsumos.getColumnName(2)).setCellEditor(new MyTableCellEditor(sd));
        }
       
       
    }
    
     public void listenerToColumnsDetalles(int s){
       TextFieldPirata sd = new TextFieldPirata();
       
       sd.addFocusListener(new FocusListener() {
           @Override
           public void focusGained(FocusEvent fe) {
           sd.selectAll();
           }

           @Override
           public void focusLost(FocusEvent fe) {
           }
                
            }
        );
       sd.nextIndex = conjuntoTextField.size();
       sd.addKeyListener(new KeyListener() {
      public void keyPressed(KeyEvent keyEvent) {
       
          printIt("Pressed", keyEvent);
      }

      public void keyReleased(KeyEvent keyEvent) {
        printIt("Released", keyEvent);
      }

      public void keyTyped(KeyEvent keyEvent) {
        printIt("Typed", keyEvent);
      }
      
      private void printIt(String title, KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        String keyText = KeyEvent.getKeyText(keyCode);
        System.out.println(title + " : " + keyText + " / " + keyEvent.getKeyChar());
          try {
          double cantidad  = Double.parseDouble(sd.getText());
        JTable t =(JTable)sd.getParent(); 
        t.getSelectedRow();
        if(s==produccion.MERMA){ 
        actualizarData(produccionMama.producciones.get(t.getSelectedColumn()).DetallesMerma, t.getSelectedRow(),""+cantidad);
        }
        if(s==produccion.PRODUCTO){ 
        actualizarData(produccionMama.producciones.get(t.getSelectedColumn()).DetallesProducto, t.getSelectedRow(),""+cantidad);
        }if(s==produccion.INSUMO){ 
        actualizarData(produccionMama.producciones.get(t.getSelectedColumn()).DetallesInsumo, t.getSelectedRow(),""+cantidad);
        }
    
          } catch (Exception e) {
         
          }
                
        }
      
    });
   conjuntoTextField.add(sd);
            
         if(s==produccion.MERMA){ 
             for (int i = 0; i < empresasSede.size(); i++) {
       TablaMerma1.getColumn(TablaMerma1.getColumnName(i)).setCellEditor(new MyTableCellEditor(sd));
                 
             }
       
        }
        if(s==produccion.PRODUCTO){ 
          for (int i = 0; i < empresasSede.size(); i++) {
       TablaProductos1.getColumn(TablaProductos1.getColumnName(i)).setCellEditor(new MyTableCellEditor(sd));
                 
             }
        }if(s==produccion.INSUMO){ 
          for (int i = 0; i < empresasSede.size(); i++) {
       TablaInsumos1.getColumn(TablaInsumos1.getColumnName(i)).setCellEditor(new MyTableCellEditor(sd));
                 
             }
        }
       
       
    }
    
    public static boolean isEmpty(JTable jTable) {
        if (jTable != null && jTable.getModel() != null) {
            return jTable.getModel().getRowCount() <= 0;
        }
        return false;
    }


  public void eliminar(JTable Jtable){
        DefaultTableModel tb = (DefaultTableModel) Jtable.getModel();
        int a = Jtable.getRowCount()-1;
        for (int i = a; i >= 0; i--) {           
        //tb.removeRow(tb.getRowCount()-1);
        } 
        //cargaTicket();
    }
  
  private void commpletarTablas(String idOrdenProduccion, int flag) {

        
            String [] cabeza = {"Detalle", "Unidad","Cantidad"};
            eliminar(TablaMerma);
            eliminar(TablaInsumos);
            eliminar(TablaProductos);
            TableModel dtInsumo = new DefaultTableModel(Produccion.paraTabla(produccion.DetallesInsumo),cabeza);
            TableModel dtProducto =new DefaultTableModel(Produccion.paraTabla(produccion.DetallesProducto),cabeza);
            TableModel dtMerma = new DefaultTableModel(Produccion.paraTabla(produccion.DetallesMerma),cabeza);
            
            TablaMerma.setModel(dtMerma);
            TablaInsumos.setModel(dtInsumo);
            TablaProductos.setModel(dtProducto);
            TablaMerma.setRowHeight(30);
            TablaInsumos.setRowHeight(30);
            TablaProductos.setRowHeight(30);
            
            addListenersToTablas();
          //  restringirEntrada();
            conjuntoTextField = new ArrayList<>();
            createKeybindings(TablaInsumos);
                        createKeybindings(TablaMerma);
            createKeybindings(TablaProductos);

       listenerToColumns(1); 
    listenerToColumns(2); 
    listenerToColumns(3); 
    
    }
  
    private void commpletarTablasDetallado() {
          
        
            String [] cabeza = new String [empresasSede.size()] ;
            for (int i = 0; i < cabeza.length; i++) {
   cabeza[i]=empresasSede.get(i).getEmpSedNom();
            
        }
            eliminar(TablaMerma1);
            eliminar(TablaInsumos1);
            eliminar(TablaProductos1);
            TableModel dtInsumo = new DefaultTableModel(produccionMama.paraTabla(Produccion.INSUMO),cabeza);
            TableModel dtProducto =new DefaultTableModel(produccionMama.paraTabla(Produccion.PRODUCTO),cabeza);
            TableModel dtMerma = new DefaultTableModel(produccionMama.paraTabla(Produccion.MERMA),cabeza);
            
            TablaMerma1.setModel(dtMerma);
            TablaInsumos1.setModel(dtInsumo);
            TablaProductos1.setModel(dtProducto);
            TablaMerma1.setRowHeight(30);
            TablaInsumos1.setRowHeight(30);
            TablaProductos1.setRowHeight(30);
            
            addListenersToTablasDetalle();
          //  restringirEntrada();
            conjuntoTextField = new ArrayList<>();
            createKeybindings(TablaInsumos);
             createKeybindings(TablaMerma);
            createKeybindings(TablaProductos);

            listenerToColumnsDetalles(1); 
            listenerToColumnsDetalles(2); 
             listenerToColumnsDetalles(3); 
        
        JScrollPane tp1;
        tp1 = jScrollPane6;
    BoundedRangeModel model=tp1.getVerticalScrollBar().getModel();
    jScrollPane2.getVerticalScrollBar().setModel(model);
  
      JScrollPane    tp2 = jScrollPane5;
    BoundedRangeModel model2=tp2.getVerticalScrollBar().getModel();
    jScrollPane1.getVerticalScrollBar().setModel(model2);
  
      JScrollPane    tp3 = jScrollPane4;
    BoundedRangeModel model3=tp3.getVerticalScrollBar().getModel();
    jScrollPane3.getVerticalScrollBar().setModel(model3);
  
    
    }
    
    
    private void commpletarTablasDetallado(int [] proporcion) {
          
        
            String [] cabeza = new String [empresasSede.size()] ;
            for (int i = 0; i < cabeza.length; i++) {
   cabeza[i]=empresasSede.get(i).getEmpSedNom();
            
        }
            eliminar(TablaMerma1);
            eliminar(TablaInsumos1);
            eliminar(TablaProductos1);
            TableModel dtInsumo = new DefaultTableModel(produccionMama.paraTabla(Produccion.INSUMO),cabeza);
            TableModel dtProducto =new DefaultTableModel(produccionMama.paraTabla(Produccion.PRODUCTO),cabeza);
            TableModel dtMerma = new DefaultTableModel(produccionMama.paraTabla(Produccion.MERMA),cabeza);
            
            TablaMerma1.setModel(dtMerma);
            TablaInsumos1.setModel(dtInsumo);
            TablaProductos1.setModel(dtProducto);
            TablaMerma1.setRowHeight(30);
            TablaInsumos1.setRowHeight(30);
            TablaProductos1.setRowHeight(30);
            
            addListenersToTablasDetalle();
          //  restringirEntrada();
            conjuntoTextField = new ArrayList<>();
            createKeybindings(TablaInsumos);
             createKeybindings(TablaMerma);
            createKeybindings(TablaProductos);

            listenerToColumnsDetalles(1); 
            listenerToColumnsDetalles(2); 
             listenerToColumnsDetalles(3); 
        
        JScrollPane tp1;
        tp1 = jScrollPane6;
    BoundedRangeModel model=tp1.getVerticalScrollBar().getModel();
    jScrollPane2.getVerticalScrollBar().setModel(model);
    }
    
        public void addListenersToTablas(){
        
           
            TablaMerma.getModel().addTableModelListener(new TableModelListener() {
                @Override
                public void tableChanged(TableModelEvent e) {
                      try {
                         int row = e.getFirstRow();
                        int column = e.getColumn();
                         TableModel model = (TableModel)e.getSource();

                        String data = (String)model.getValueAt(row, column);

                        actualizarData(produccion.DetallesMerma,row,data);
                
                    } catch (Exception es) {
                    }
                }

                
            });
            
            TablaInsumos.getModel().addTableModelListener(new TableModelListener() {
                @Override
                public void tableChanged(TableModelEvent e) {
                    try {
                               int row = e.getFirstRow();
                        int column = e.getColumn();
                         TableModel model = (TableModel)e.getSource();

                        String data = (String)model.getValueAt(row, column);

                        actualizarData(produccion.DetallesInsumo,row,data);
                
                    } catch (Exception es) {
                    }
                }

                
            });
            TablaProductos.getModel().addTableModelListener(new TableModelListener() {
                @Override
                public void tableChanged(TableModelEvent e) {
                    
                          int row = e.getFirstRow();
                        int column = e.getColumn();
                         TableModel model = (TableModel)e.getSource();

                        String data = (String)model.getValueAt(row, column);

                        actualizarData(produccion.DetallesProducto,row,data);
                
                    
                }

                
            });
            
            
        }
public void ActualizarDetallado(){
    try {
        double [] pro = {Double.parseDouble(jTextFieldProporcion1.getText()),Double.parseDouble(jTextFieldProporcion2.getText()),Double.parseDouble(jTextFieldProporcion3.getText()),Double.parseDouble(jTextFieldProporcion4.getText())};
          
            produccionMama = new ProduccionMama(produccion,empresasSede,pro);
            
            commpletarTablasDetallado();
  

    } catch (Exception e) {
    }
                }
        
        public void addListenersToTablasDetalle(){
        
           
            TablaMerma1.getModel().addTableModelListener(new TableModelListener() {
                @Override
                public void tableChanged(TableModelEvent e) {
                      try {
                         int row = e.getFirstRow();
                        int column = e.getColumn();
                         TableModel model = (TableModel)e.getSource();

                        String data = (String)model.getValueAt(row, column);
                          System.out.println("tangananica");
                        actualizarData(produccionMama.producciones.get(column).DetallesMerma,row,data);

                        
                        
                    } catch (Exception es) {
                    }
                }

                
            });
            
            TablaInsumos1.getModel().addTableModelListener(new TableModelListener() {
                @Override
                public void tableChanged(TableModelEvent e) {
                    try {
                               int row = e.getFirstRow();
                        int column = e.getColumn();
                         TableModel model = (TableModel)e.getSource();

                        String data = (String)model.getValueAt(row, column);

                        actualizarData(produccionMama.producciones.get(column).DetallesInsumo,row,data);
                
                    } catch (Exception es) {
                    }
                }

                
            });
            TablaProductos1.getModel().addTableModelListener(new TableModelListener() {
                @Override
                public void tableChanged(TableModelEvent e) {
                    
                          int row = e.getFirstRow();
                        int column = e.getColumn();
                         TableModel model = (TableModel)e.getSource();

                        String data = (String)model.getValueAt(row, column);

                        actualizarData(produccionMama.producciones.get(column).DetallesProducto,row,data);
                
                    
                }

                
            });
            
            
        }
    
            private void actualizarData(ArrayList<MovimientoDet> Detalles, int row, String data) {
             Detalles.get(row).setMovDetCan(""+data);
                System.out.println("produccion.OrdenProduccion.actualizarData()");
            }
            
            
    private void comprobarCantidadesOP() {
        //Cantidades de verificacion 
        double exc_o_def_Pro = 0, exc_o_def_Ins = 0, exc_o_def_Mer = 0, prop_Pro = 0, prop_Ins = 0, prop_Mer = 0;

        DefaultTableModel dm;
        dm = (DefaultTableModel) TablaProductos.getModel();
        System.err.println("rowCount: " + dm.getRowCount());
        for (int i = 0; i < dm.getRowCount() && dm.getValueAt(i, 2)!=null && dm.getValueAt(i, 0) != null && cantIdealesPro.size()>0; i++) {
            exc_o_def_Pro = cantIdealesPro.get(i) % Double.parseDouble((String) dm.getValueAt(i, 2));
            prop_Pro = cantIdealesPro.get(i) / Double.parseDouble((String) dm.getValueAt(i, 2));
            System.out.println("Entro a la proporcion en producto - Ideal: " + cantIdealesPro.get(i) + " , puesto: " + Double.parseDouble((String) dm.getValueAt(i, 2)));
        }

        dm = (DefaultTableModel) TablaInsumos.getModel();
        for (int i = 0; i < dm.getRowCount() && dm.getValueAt(i, 2)!=null && dm.getValueAt(i, 0) != null && cantIdealesIns.size()>0; i++) {
            exc_o_def_Ins = cantIdealesIns.get(i) % Double.parseDouble((String) dm.getValueAt(i, 2));
            prop_Ins = cantIdealesIns.get(i) / Double.parseDouble((String) dm.getValueAt(i, 2));
            System.out.println("Entro a la proporcion en insumo - Ideal: " + cantIdealesIns.get(i) + " , puesto: " + Double.parseDouble((String) dm.getValueAt(i, 2)));
        }

        dm = (DefaultTableModel) TablaMerma.getModel();
        for (int i = 0; i < dm.getRowCount() && dm.getValueAt(i, 2)!=null && dm.getValueAt(i, 0) != null && cantIdealesMer.size()>0; i++) {
            exc_o_def_Mer = cantIdealesMer.get(i) % Double.parseDouble((String) dm.getValueAt(i, 2));
            prop_Mer = cantIdealesMer.get(i) / Double.parseDouble((String) dm.getValueAt(i, 2));
            System.out.println("Entro a la proporcion en merma - Ideal: " + cantIdealesMer.get(i) + " , puesto: " + Double.parseDouble((String) dm.getValueAt(i, 2)));
        }

        if (prop_Pro == prop_Ins && prop_Pro == prop_Mer && prop_Ins == prop_Mer) {
            loquepaso = "La proporción Producto y Merma es correcta en relación a los Insumos";
            flagVerifCantidades = 1;
            idLoquePaso = 1;
        } else if (prop_Pro == prop_Ins) {
            loquepaso = "La proporción de Productos producidos es correcta en relación a los Insumos consumidos";
            idLoquePaso = 2;
            cantExceso = exc_o_def_Mer;
        } else if (prop_Pro == prop_Mer) {
            loquepaso = "La proporción de Merma producida es correcta en relación a los Productos producidos";
            idLoquePaso = 3;
            cantExceso = exc_o_def_Ins;
        } else if (prop_Ins == prop_Mer) {
            loquepaso = "La proporción de Merma producida es correcta en relación a los Insumos consumidos";
            idLoquePaso = 4;
            cantExceso = exc_o_def_Pro;
        } else {
            loquepaso = "Las proporciones de Insumos, Productos y/o Merma estan mal";
            idLoquePaso = 5;
        }
        
        excesos.add(exc_o_def_Pro);
        excesos.add(exc_o_def_Ins);
        excesos.add(exc_o_def_Mer);

        if (idLoquePaso != 1) {
            flagVerifCantidades = 0;
        }

//        flagVerifCantidades = 1;//Esta bien
//        flagVerifCantidades = 0;//Esta mal
    }


}
class MyTableCellEditor extends AbstractCellEditor implements TableCellEditor {

    JComponent component = new JTextField();
   public MyTableCellEditor(JTextField sd){
   component=sd;
   } 
  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected,
      int rowIndex, int vColIndex) {

    ((JTextField) component).setText((String) value);
      System.out.println("produccion.MyTableCellEditor.getCellEditorValue()");

    return component;
  }

  public Object getCellEditorValue() {
 
      return ((JTextField) component).getText();
  }
  
}
