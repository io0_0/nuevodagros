/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produccion2;
import produccion.*;
import javax.swing.event.*;
import javax.swing.table.TableModel;

public class eventoTabla  implements TableModelListener {
   
    @Override
    public void tableChanged(TableModelEvent e) {
        int row = e.getFirstRow();
        int column = e.getColumn();
        TableModel model = (TableModel)e.getSource();
        String columnName = model.getColumnName(column);
        Object data = model.getValueAt(row, column);
        System.out.println(data.toString()+"  "+ columnName);
    }
    
}