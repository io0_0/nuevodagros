/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produccion2;

import produccion.*;
import Bean.Configurar;
import Bean.Empresa;
import Bean.Generico;
import Bean.MiModel;
import Conexion.Mysql;
import GUI.Principal;
import IngresoAlmacen.CompraeIngresoAlmacen;
import static Maestros.Atributos.EMPSEDNOM;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author cesar
 */
public final class ListaProduccion extends javax.swing.JFrame {

    /**
     * Creates new form OrdenProduccion
     */
    Connection cn = Mysql.getConection();
    String sql;
    
    //Contadores para llenar en las tabla
    int conTabla = 0;

    //Flag que verifica si las cantidades completadas estan bien o mal
    int flagVerifCantidades;
    ArrayList<Double> cantIdealesPro = new ArrayList<>();
    ArrayList<Double> cantIdealesIns = new ArrayList<>();
    ArrayList<Double> cantIdealesMer = new ArrayList<>();
    
    ArrayList<Double> excesos = new ArrayList<>();

    //Variables que almacenan  lo que paso
    String loquepaso = "";
    // idLoquePaso = 2, 3, 4 guarda el id de lo que paso, (no todo esta bien)
    int idLoquePaso = 2;
    double cantExceso = 0;

    public ListaProduccion() {
        initComponents();
        initiar();
        centrarPantalla();
//        String [] cabeceraTabla = {"Tipo de Articulo", "Id Articulo","Nombre del Articulo","Cantidad"};
//        String [][] data = null;
//        MiModel model = new MiModel(data, cabeceraTabla);
//        JTResumenOP.setModel(model);
//            initiar();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        FormLlenar = new javax.swing.JPanel();
        Emp = new javax.swing.JLabel();
        Nume = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        OrdenPro = new javax.swing.JLabel();
        Numero = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        Serie = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        JCBOrdenProduccion = new javax.swing.JComboBox<>();
        fecha = new javax.swing.JTextField();
        EmpresaID = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        obser = new javax.swing.JTextField();
        FormProductos = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        JTResumenOP = new javax.swing.JTable();
        FormBotoms = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        FormLlenar.setBackground(new java.awt.Color(254, 254, 254));

        Emp.setText("EMPRESA");

        Nume.setText("NÚMERO");

        jLabel5.setText("FECHA");

        OrdenPro.setFont(new java.awt.Font("Ubuntu", 1, 20)); // NOI18N
        OrdenPro.setText("ORDEN DE PRODUCCION ");

        Numero.setEditable(false);
        Numero.setEnabled(false);
        Numero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NumeroActionPerformed(evt);
            }
        });

        jLabel1.setText("SERIE");

        Serie.setEditable(false);
        Serie.setEnabled(false);
        Serie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SerieActionPerformed(evt);
            }
        });

        jLabel6.setText("ORDEN DE PRODUCCIÓN");

        JCBOrdenProduccion.setToolTipText("");
        JCBOrdenProduccion.setEnabled(false);
        JCBOrdenProduccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCBOrdenProduccionActionPerformed(evt);
            }
        });

        fecha.setEnabled(false);

        EmpresaID.setEnabled(false);

        jLabel7.setText("SITUACIÓN");

        obser.setEnabled(false);

        javax.swing.GroupLayout FormLlenarLayout = new javax.swing.GroupLayout(FormLlenar);
        FormLlenar.setLayout(FormLlenarLayout);
        FormLlenarLayout.setHorizontalGroup(
            FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormLlenarLayout.createSequentialGroup()
                .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(FormLlenarLayout.createSequentialGroup()
                        .addGap(217, 217, 217)
                        .addComponent(OrdenPro, javax.swing.GroupLayout.PREFERRED_SIZE, 259, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FormLlenarLayout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(FormLlenarLayout.createSequentialGroup()
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(2, 2, 2)
                                .addComponent(JCBOrdenProduccion, 0, 111, Short.MAX_VALUE))
                            .addGroup(FormLlenarLayout.createSequentialGroup()
                                .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(Emp)
                                    .addComponent(jLabel5))
                                .addGap(18, 18, 18)
                                .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(fecha)
                                    .addComponent(EmpresaID, 0, 197, Short.MAX_VALUE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 64, Short.MAX_VALUE)
                        .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormLlenarLayout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(Serie, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormLlenarLayout.createSequentialGroup()
                                .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(Nume, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(obser)
                                    .addComponent(Numero, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE))))))
                .addGap(0, 20, Short.MAX_VALUE))
        );
        FormLlenarLayout.setVerticalGroup(
            FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormLlenarLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OrdenPro)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(Serie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Emp)
                    .addComponent(EmpresaID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormLlenarLayout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Nume)
                            .addComponent(Numero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(FormLlenarLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(fecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addGroup(FormLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JCBOrdenProduccion, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(obser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        FormProductos.setBackground(new java.awt.Color(254, 254, 254));

        jLabel8.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel8.setText("ARTCULOS UTILIZADOS");

        JTResumenOP.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "TIPO DE ARTICULO", "ID ARTICULO", "NOMBRE ARTICULO", "CANTIDAD"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(JTResumenOP);

        javax.swing.GroupLayout FormProductosLayout = new javax.swing.GroupLayout(FormProductos);
        FormProductos.setLayout(FormProductosLayout);
        FormProductosLayout.setHorizontalGroup(
            FormProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormProductosLayout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(FormProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 585, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        FormProductosLayout.setVerticalGroup(
            FormProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormProductosLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(24, Short.MAX_VALUE))
        );

        FormBotoms.setBackground(new java.awt.Color(254, 254, 254));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/SALIR2.JPG"))); // NOI18N
        jButton1.setText("ATRAS");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FormBotomsLayout = new javax.swing.GroupLayout(FormBotoms);
        FormBotoms.setLayout(FormBotomsLayout);
        FormBotomsLayout.setHorizontalGroup(
            FormBotomsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormBotomsLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70))
        );
        FormBotomsLayout.setVerticalGroup(
            FormBotomsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormBotomsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(FormLlenar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(FormProductos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(FormBotoms, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(FormLlenar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FormProductos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FormBotoms, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void NumeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NumeroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NumeroActionPerformed

    private void SerieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SerieActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_SerieActionPerformed

    private void JCBOrdenProduccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCBOrdenProduccionActionPerformed
        
    }//GEN-LAST:event_JCBOrdenProduccionActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ListaProduccion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new ListaProduccion().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Emp;
    private javax.swing.JComboBox<String> EmpresaID;
    private javax.swing.JPanel FormBotoms;
    private javax.swing.JPanel FormLlenar;
    private javax.swing.JPanel FormProductos;
    private javax.swing.JComboBox<String> JCBOrdenProduccion;
    private javax.swing.JTable JTResumenOP;
    private javax.swing.JLabel Nume;
    private javax.swing.JTextField Numero;
    private javax.swing.JLabel OrdenPro;
    private javax.swing.JTextField Serie;
    private javax.swing.JTextField fecha;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTextField obser;
    // End of variables declaration//GEN-END:variables

    private double obtenerIgvArticulo(String idArt) throws SQLException {

        double igv;
        Connection reg = Mysql.getConection();
        ResultSet rs;
        Statement st;
        sql = ("SELECT ArtIgv FROM articulo WHERE IdArticulo =" + idArt);
        st = reg.createStatement();
        rs = st.executeQuery(sql);
        String ArtIgv = "";
        while (rs.next()) {
            ArtIgv = rs.getString(1);
        }
        igv = Double.parseDouble(ArtIgv);
        return igv;
    }

    private void initiar() {
        try {
            ArrayList<Empresa> empresas = Empresa.getEmpresasForDato("idempresagrupo", "1");
            
            String where = "where ";
            ArrayList<Configurar> configs = Configurar.getCongiguracionesPorDato("idusuario", Principal.jLUsuarioGeneral.getText());
            for (Configurar config : configs) {
                where += "idempresasede = " + config.getIdEmpresaSede() + " or ";
            }
            where = where.substring(0, where.length() - 3);
            
            Generico.llenarCaja(EmpresaID, "empresasede", "empsednom", where);
        } catch (SQLException ex) {
            Logger.getLogger(ListaProduccion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static boolean isEmpty(JTable jTable) {
        if (jTable != null && jTable.getModel() != null) {
            return jTable.getModel().getRowCount() <= 0;
        }
        return false;
    }

    private String obtenerEmpresa(String empresa) {
        String emp = "";
        String caracter;
        boolean flag = false;
        /*for (int i = 0; i < empresa.length(); i++) {
            caracter = empresa.substring(i, i + 1);
            if (!caracter.equals(" ") && !flag) {
                emp += caracter;
            } else {
                flag = true;
            }
        }*/
        try {
            emp = Generico.sacarDato("empresasede", "empsednom", "'" + EmpresaID.getSelectedItem().toString() + "'");
        } catch (SQLException ex) {
            Logger.getLogger(CompraeIngresoAlmacen.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("De obtenerEmpresa() " + emp);
        return emp;
    }

   
    private void limpiarTabla(JTable table) {
        DefaultTableModel dm = (DefaultTableModel) table.getModel();
        for (int i = 0; i < dm.getRowCount(); i++) {
            for (int j = 0; j < dm.getColumnCount(); j++) {
                if (dm.getValueAt(i, j) != null) {
                    dm.setValueAt(null, i, j);
                }
            }
        }
    }
    
    private void initCategoria(JComboBox jcombo) {
        try {
            cn = Mysql.getConection();
            String sql = "Select * from categoria where catestreg =1";
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                jcombo.addItem(rs.getString("catnom"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(CompraeIngresoAlmacen.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    void vieneDeLista(String[] idCab, String [] estadoCab) {
        // Siempre deberian ser de longitud 2 (Salida y Entrada)
        //COmpletamos la cabecera de la orden de produccion
        fecha.setText(estadoCab[0]);
        Serie.setText(estadoCab[1]);
        Numero.setText(estadoCab[2]);
        obser.setText(estadoCab[4]);
        
        //COmpletamos los detalles de la orden de produccion
        commpletarTablasReporte(idCab[0], estadoCab[0]);
        commpletarTablasReporte(idCab[1], estadoCab[1]);
        JTResumenOP.setEnabled(false);
    }

    private void commpletarTablasReporte(String idCabEleg, String estadoCabEleg) {
        String sqlTabla = "SELECT movimientodet.MovIngreso, movimientodet.IdArticulo, articulo.ArtNom, movimientodet.MovDetCan FROM movimientodet"
                    + " INNER JOIN articulo ON movimientodet.IdArticulo = articulo.IdArticulo"
                    + " WHERE IdMovimientoCabecera = " + idCabEleg +"  and movimientodet.movdetestreg=1 " ;
        
//        Generico.llenarTabla(JTResumenOP, cabecera, sql);
        try {
            Statement sent;
            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sqlTabla);
            String tipoArt, identifTipo;
            
            while (rs.next()) {
                tipoArt = rs.getString(1);
                if(tipoArt.contains("0")){
                    identifTipo = "INSUMO";
                }else if(tipoArt.contains("1")){
                    identifTipo = "PRODUCTO";
                }else{
                    identifTipo = "MERMA";
                }
                JTResumenOP.setValueAt(identifTipo, conTabla, 0);
                JTResumenOP.setValueAt(rs.getString(2), conTabla, 1);
                JTResumenOP.setValueAt(rs.getString(3), conTabla, 2);
                JTResumenOP.setValueAt(rs.getString(4), conTabla, 3);
                conTabla++;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ModificarProduccion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private void centrarPantalla() {
        Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension ventana = this.getSize();
        this.setLocation((int) (pantalla.width - ventana.width) / 2, (int) (pantalla.height - ventana.height) / 2);

    }
    

}
