package Facturador;

import Bean.Articulo;
import Bean.Empresa;
import Bean.UnidadMedida;
import Bean.MovimientoDet;
import Bean.MovimientoCab;
import Bean.EmpresaSede;
import Maestros.ArticuloM;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import Conexion.Mysql;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class factura {
	public static final String BOLETA = "3";
	public static final String FACTURA = "1";

	private MovimientoCab cabecera = new MovimientoCab();
	static Connection cn = Mysql.getConection();
	static int datos = 11;
	private EmpresaSede empresaSedeOri;
	private Empresa empresaOri;
	private EmpresaSede empresaSedeDes;
	private Empresa empresaDes;
	private ArrayList<MovimientoDet> detalles;

	public factura() {
		detalles = new ArrayList<MovimientoDet>();

	}

	public MovimientoCab getCabecera() {
		return cabecera;
	}

	public void setCabecera(MovimientoCab cabecera1) {
		this.cabecera = cabecera1;
		// empresaSedeOri = new EmpresaSede("'"+de+"'");
		// empresaOri = new Empresa(empresaSedeOri.getIdEmpresa()) ;
		// empresaSedeDes = new EmpresaSede(cabecera1.getIdEmpresaSedeDes());
		// empresaDes = new Empresa(empresaSedeDes.getIdEmpresa()) ;
		// ajustar();
	}

	public ArrayList<MovimientoDet> getDetalles() {
		return detalles;
	}

	public void setDetalles(ArrayList<MovimientoDet> detalles) {
		this.detalles = detalles;
	}

	public void ajustar() {
		ArrayList<MovimientoDet> detallesFinal = new ArrayList<MovimientoDet>();
		boolean[] visitado = new boolean[detalles.size()];
		for (int i = 0; i < visitado.length; i++) {
			visitado[i] = false;

		}
		for (int i = 0; i < detalles.size(); i++) {
			MovimientoDet acDet = detalles.get(i);
			String articulo = acDet.getIdArticulo();
			double cantidadActual = Double.parseDouble(acDet.getMovDetCan());
			if (!visitado[i])
				detallesFinal.add(acDet);
			for (int j = i + 1; j < detalles.size(); j++) {
				if (visitado[j])
					continue;
				MovimientoDet temp = detalles.get(j);
				if (temp.getIdArticulo().equals(articulo)) {
					visitado[j] = true;
					cantidadActual += Double.parseDouble(temp.getMovDetCan());
				}
			}

			acDet.setMovDetCan("" + cantidadActual);
		}
		detalles = detallesFinal;

	}

	public void obtenerDetalles() throws SQLException {
		Statement sent;
		String sql_Cons = "Select * from MovimientoDet  where idMovimientoCabecera= "
				+ getCabecera().getIdMovimientoCabecera() + " and MovDetEstReg = 1";
		System.out.println(sql_Cons);
		sent = cn.createStatement();
		ResultSet rs = sent.executeQuery(sql_Cons);
		ArrayList<MovimientoDet> det = new ArrayList<MovimientoDet>();
		while (rs.next()) {
			MovimientoDet temo = new MovimientoDet();
			temo.setIdMovimientoDetalle(rs.getString("IdMovimientoDetalle"));
			temo.setIdMovimientoCabecera(rs.getString("IdMovimientoCabecera"));
			temo.setIdArticulo(rs.getString("IdArticulo"));
			temo.setMovIngreso(rs.getString("MovIngreso"));
			temo.setMovDetCan(rs.getString("MovDetCan"));
			temo.setMovDetPreTot(rs.getString("MovDetPreTot"));
			temo.setMovDetCosTot(rs.getString("MovDetCosTot"));
			temo.setMovDetIgv(rs.getString("MovDetIgv"));
			temo.setMovDetEstReg(rs.getString("MovDetEstReg"));
			System.out.println(temo);

			if (temo.getMovDetEstReg().equals("1")) {
				det.add(temo);
			}

		}
		setDetalles(det);
		System.out.println(detalles.size());

	}

	public void enviar() throws IOException, SQLException {
		String de = cabecera.getIdEmpresaSede();
		empresaSedeOri = new EmpresaSede("'" + de + "'");
		empresaOri = new Empresa(empresaSedeOri.getIdEmpresa());
		empresaSedeDes = new EmpresaSede(cabecera.getIdEmpresaSedeDes());
		empresaDes = new Empresa(empresaSedeDes.getIdEmpresa());
		obtenerDetalles();
		if (cabecera.getIdMovimientoDocumento().equals(BOLETA)) {

			enviarBoleta();

			return;
		}
		if (cabecera.getIdMovimientoDocumento().equals(FACTURA)) {
			enviarFactura();

			return;
		}
		if (cabecera.getIdMovimientoDocumento().equals("36")) {
			enviarNotaCredito();

			return;
		}

	}

	// 01|2016-11-10|000|6|20100282721|JUAN LENG DELGADO
	// SAC|PEN|0.00|0.00|0.00|927.96|0.00|0.00|167.04|0.00|0.00|1095.00
	private void enviarBoleta() throws IOException, SQLException {

		crearCabecera(BOLETA);
		crearDetalles();
		crearLeyenda(BOLETA);
		crearTributosGenerales(BOLETA);

		System.out.println("" + detalles.size());

	}

	private void enviarNotaCredito() {
		String archivo = "";
		String Tipo = "07";
		String Fecha = cabecera.getMovCabFec();

		String Domicilio = "Estados unidos";
		Domicilio = Domicilio.toUpperCase();
		DecimalFormat df = new DecimalFormat("#.00");

		String DocumentoTipo = empresaDes.getIdDocumento();

		String DocumentoNu = empresaDes.getEmpRUC();
		String NomApe = empresaDes.getEmpNom();
		String MonedaTipo = "PEN";
		String DescGlo = "0.00";
		String OtrosCargos = "0.00";
		String TotalDescuento = "0.00";
		String OperacionesInafectas = "" + "0.00";
		String OperacionesExoneradas = "" + "0.00";
		MovimientoCab ant = new MovimientoCab(cabecera.getIdMovimientoCabeceraAsociado());
		double SumatoriaIGVNum = 0;
		for (MovimientoDet a : detalles) {
			SumatoriaIGVNum += Double.parseDouble(a.getMovDetPreTot());
		}
		double OperacionesGravadasNum = SumatoriaIGVNum / 1.18;
		String OperacionesGravadas = "" + df.format(OperacionesGravadasNum);
		OperacionesGravadas = OperacionesGravadas.replace(",", ".");
		if (OperacionesGravadas.charAt(0) == '.')
			OperacionesGravadas = OperacionesGravadas.replace(".", "0.");
		SumatoriaIGVNum = SumatoriaIGVNum * 0.18 / 1.18;
		String SumatoriaIGV = "" + df.format(SumatoriaIGVNum);
		SumatoriaIGV = SumatoriaIGV.replace(",", ".");
		if (SumatoriaIGV.charAt(0) == '.')
			SumatoriaIGV = SumatoriaIGV.replace(".", "0.");
		double ImporteNum = 0;

		double SumatoriaISCNum = 0;
		for (MovimientoDet a : detalles) {
			ImporteNum += Double.parseDouble(a.getMovDetPreTot());
		}
		String SumatoriaISC = "" + df.format(SumatoriaISCNum);

		SumatoriaISC = SumatoriaISC.replace(",", ".");
		if (SumatoriaISC.charAt(0) == '.')
			SumatoriaISC = SumatoriaISC.replace(".", "0.");
		String SumatoriaOtros = "0.00";
		Calendar calendario = new GregorianCalendar();
		int hora, minutos, segundos;
		hora = calendario.get(Calendar.HOUR_OF_DAY);
		minutos = calendario.get(Calendar.MINUTE);
		segundos = calendario.get(Calendar.SECOND);
		String shora = (hora >= 10) ? hora + "" : "0" + hora;
		String sMin = (minutos >= 10) ? minutos + "" : "0" + minutos;
		String sSeg = (segundos >= 10) ? segundos + "" : "0" + segundos;

		String horaString = shora + ":" + sMin + ":" + sSeg;
		String ImporteTotal = "" + df.format(ImporteNum);

		ImporteTotal = ImporteTotal.replace(",", ".");
		if (ImporteTotal.charAt(0) == '.')
			ImporteTotal = ImporteTotal.replace(".", "0.");
		archivo += "0101";
		archivo += "|" + Fecha;
		archivo += "|" + horaString;
		archivo += "|";
		archivo += "|" + "0" + ant.getIdMovimientoDocumento();

		archivo += "|0" + empresaDes.getIdDocumento();
		archivo += "|" + NomApe;
		archivo += "|" + MonedaTipo;
		archivo += "|" + "01";

		archivo += "|" + "CLIENTE DISCONFOME CON EL PRODUCTOM  ESTE FUE DEVUELTO ";
		archivo += "|0" + ant.getIdMovimientoDocumento();
		if (ant.getIdMovimientoDocumento().equals("3")) {
			archivo += "|B" + ant.getMovCabSer() + "-" + ant.getMovCabNum();
		}
		if (ant.getIdMovimientoDocumento().equals("1")) {
			archivo += "|F" + ant.getMovCabSer() + "-" + ant.getMovCabNum();
		}
		archivo += "0.00";
		archivo += "0.00";
		archivo += "0.00";
		archivo += "|" + SumatoriaIGV;
		archivo += "|" + OperacionesGravadas;

		archivo += "|" + ImporteTotal;
		archivo += "0.00";
		archivo += "0.00";
		archivo += "0.00";
		archivo += "|" + ImporteTotal;
		archivo += "2.1";
		archivo += "2.0";
		// System.out.println(archivo);
		archivo = archivo.toUpperCase();
		String identificador = empresaOri.getEmpRUC();
		String documento = "07";

		String serie = cabecera.getMovCabSer();
		String numero = cabecera.getMovCabNum();
		String nombre = "";
		if (ant.getIdMovimientoDocumento().equals("3")) {
			nombre = identificador + "-" + documento + "-" + "B" + serie + "-" + numero + ".NOT";
		}
		if (ant.getIdMovimientoDocumento().equals("1")) {
			nombre = identificador + "-" + documento + "-" + "B" + serie + "-" + numero + ".NOT";
		}
		try {
			crearLeyenda(BOLETA);
			crearTributosGenerales(BOLETA);

		} catch (IOException ex) {
			Logger.getLogger(factura.class.getName()).log(Level.SEVERE, null, ex);
		}
		ArrayList<String> mandar = new ArrayList<String>();
		mandar.add(archivo);
		try {
			crearArchivo(nombre, mandar);
		} catch (IOException ex) {
			System.out.println("eror");
			Logger.getLogger(factura.class.getName()).log(Level.SEVERE, null, ex);
		}
		crearDetallesNota();
	}

	private void crearDetallesNota() {
		try {
			String de = cabecera.getIdEmpresaSede();
			empresaSedeOri = new EmpresaSede("'" + de + "'");
			empresaOri = new Empresa(empresaSedeOri.getIdEmpresa());
			empresaSedeDes = new EmpresaSede(cabecera.getIdEmpresaSedeDes());
			empresaDes = new Empresa(empresaSedeDes.getIdEmpresa());
			// TODO Auto-generaNIted method stub
			ArrayList<String> mandar = new ArrayList<String>();
			for (MovimientoDet det : detalles) {
				Articulo art = new Articulo(Integer.parseInt(det.getIdArticulo()));
				System.err.println("error esta por aqui" + detalles.size());

				String uniMed = new UnidadMedida("" + art.getIdUnidadMedida()).getUniMedAbr();
				System.err.println("error esta por aqui 2" + detalles.size());

				String cant = det.getMovDetCan();
				// se esta viendo si se manda el archivo o no
				// String CodigoPro=art.getIdArticulo();
				String CodigoPro = "";
				String CodigoProSun = "";
				String Descripcion = art.getArtNom();
				DecimalFormat df = new DecimalFormat("#.00");
				Double precioUnitario = Double.parseDouble(det.getMovDetPreTot()) / Double.parseDouble(cant);
				Double valoruni = 0.0;
				valoruni = precioUnitario;

				Double valoruniSinIGV = valoruni / 1.18;

				// Double valoruniSinIGV=valoruni*(1-0.18/1.18);
				String valorCompra = df.format(valoruniSinIGV);
				valorCompra = valorCompra.replace(",", ".");
				if (valorCompra.charAt(0) == '.') {
					valorCompra = valorCompra.replace(".", "0.");

				}
				String Desc = "0.00";
				String IGVPorItem = df.format(valoruni * 0.18 / 1.18);
				IGVPorItem = IGVPorItem.replace(",", ".");

				if (IGVPorItem.charAt(0) == '.') {
					IGVPorItem = IGVPorItem.replace(".", "0.");

				}
				valorCompra = valorCompra.replace(",", ".");
				if (valorCompra.charAt(0) == '.') {
					valorCompra = valorCompra.replace(".", "0.");

				}
				// operacion onerosa por defecto de ser posible hacer otra tabala para los demas
				String IGVAfectadoPor = "10";
				String ISC = "0.00";
				String TipoISC = "02";
				double PrecioVentaUnitarioNum = valoruni;
				String PrecioVentaUnitario = "" + PrecioVentaUnitarioNum + "0";
				double ValorVentaNum = valoruni * Double.parseDouble(cant);
				String ValorVenta = "" + ValorVentaNum + "0";

				String dar = "";
				dar += uniMed;
				dar += "|" + cant;
				dar += "|" + CodigoPro;
				dar += "|" + CodigoProSun;
				dar += "|" + Descripcion;
				dar += "|" + valorCompra;
				dar += "|" + Desc;
				dar += "|" + IGVPorItem;
				dar += "|" + IGVAfectadoPor;
				dar += "|" + ISC;
				dar += "|" + TipoISC;
				dar += "|" + PrecioVentaUnitario;
				dar += "|" + ValorVenta;
				dar = dar.toUpperCase();
				mandar.add(dar);

			}
			String identificador = empresaOri.getEmpRUC();
			String documento = "07";
			MovimientoCab ant = new MovimientoCab(cabecera.getIdMovimientoCabeceraAsociado());

			String serie = cabecera.getMovCabSer();
			String numero = cabecera.getMovCabNum();
			String nombre = "";
			if (ant.getIdMovimientoDocumento().equals("3"))
				nombre = identificador + "-" + documento + "-B" + serie + "-" + numero + ".DET";
			if (ant.getIdMovimientoDocumento().equals("1"))
				nombre = identificador + "-" + documento + "-F" + serie + "-" + numero + ".DET";
			System.err.println("este es el nombre " + nombre);
			crearArchivo(nombre, mandar);
		} catch (IOException ex) {
			Logger.getLogger(factura.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	private String formatear(double a) {
		DecimalFormat df = new DecimalFormat("#.00");
		String rpta = df.format(a);
		rpta = rpta.replace(",", ".");
		if (rpta.charAt(0) == '.') {
			rpta = rpta.replace(".", "0.");

		}
		return rpta;
	}

	private String crearLineaDetalle(MovimientoDet det) {
		Articulo art = new Articulo(Integer.parseInt(det.getIdArticulo()));
		String uniMed = new UnidadMedida("" + art.getIdUnidadMedida()).getUniMedAbr();
		String cant = det.getMovDetCan();
		String Descripcion = art.getArtNom();
		double precioTotal = Double.parseDouble(det.getMovDetPreTot());
		double precioUnitario = Double.parseDouble(det.getMovDetPreTot()) / Double.parseDouble(cant);
		double valorUnitario = precioUnitario / 1.18;
		String valorUnitarioString = formatear(valorUnitario);
		double valor = precioTotal / 1.18;
		double IGV = (precioTotal / 1.18) * 0.18;
		double MontoISC = precioTotal;
		double ISC = 0;
		if (Integer.parseInt(det.getIdArticulo()) == 2) {
			ISC = precioTotal * art.getArtIgv();
		}
		Double sumatoriaTributos = ISC + IGV;
		// se esta viendo si se manda el archivo o no
		// String CodigoPro=art.getIdArticulo();
		// NIU|1.00|||AGUA MINERAL SIN GAS|2.12|0.38|1000|0.38|IGV|1000|S|
		String dar = "";
		dar += uniMed;
		dar += "|" + cant;
		dar += "|00";
		dar += "|-";
		dar += "|" + Descripcion;
		dar += "|" + valorUnitarioString;

		dar += "|" + formatear(sumatoriaTributos);

// esta al reves  sumatoria de tributos esta los valroes sin igv  y en valor compra esta la sumatoria de igv
		dar += "|" + 1000;
		dar += "|" + formatear(IGV);
		dar += "|" + formatear(valor);
		dar += "|" + "IGV";
		dar += "|" + "VAT";

		dar += "|" + "10";
		dar += "|" + "18.00";

		dar += "|" + "-";
		dar += "|" + "0.00";
		dar += "|" + "0.00";
		dar += "|" + "ISC";
		dar += "|" + "EXC";
		dar += "|" + "01";
		dar += "|" + "0.00";
		dar += "|" + "-";
		dar += "|" + "0.00";
		dar += "|" + "0.00";
		dar += "|" + "OTH";
		dar += "|" + "S";
		dar += "|" + "0.00";

		if (art.getIdArticulo() == 2) {

			dar += "|" + "7152";
			dar += "|" + formatear(ISC);
			dar += "|" + cant.substring(0, cant.length() - 3);
			dar += "|" + "ICBPER";

			dar += "|" + "OTH";

			dar += "|" + formatear(art.getArtIgv() * 0.1);
		} else {

			dar += "|" + "-";
			dar += "|" + formatear(ISC);
			dar += "|" + 0;
			dar += "|" + "ICBPER";

			dar += "|" + "OTH";

			dar += "|" + formatear(art.getArtIgv() * 0.1);

		}

		dar += "|" + formatear(precioUnitario);
		dar += "|" + formatear(valor);
		dar += "|" + "0.00";

		// 2.50|2.12|0.00|SIN DESCUENTO|0.00|0.00|0.00|SIN CARGO|0.00|0.00|0.00

		dar = dar.toUpperCase();
		return dar;

	}

	public ArrayList<String> getContenidoCabecera() throws ParseException {
		ArrayList<String> mandar = new ArrayList<>();
		String archivo = "";
		String Tipo = "0" + cabecera.getIdMovimientoDocumento();
		java.util.Date date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(cabecera.getMovCabFec());
		SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
		String Fecha = sd.format(date1);
		String Domicilio = "EstadosUnidos14";
		Domicilio = Domicilio.toUpperCase();
		DecimalFormat df = new DecimalFormat("#.00");
		String DocumentoTipo = empresaDes.getIdDocumento();
		String DocumentoNu = empresaDes.getEmpRUC();
		String NomApe = empresaDes.getEmpNom();
		String MonedaTipo = "PEN";
		double SumatoriaImpuestos = 0;
		double valorOperacion = 0;
		double precioOperacion = 0;
		for (MovimientoDet a : detalles) {
			double PrecioItem = Double.parseDouble(a.getMovDetPreTot());
			valorOperacion += PrecioItem / 1.18;
			SumatoriaImpuestos += (PrecioItem / 1.18) * 0.18;

			if (Integer.parseInt(a.getIdArticulo()) == 2) {

				Articulo bolsaArticulo = new Articulo(Integer.parseInt(a.getIdArticulo()));
				double ISC = PrecioItem * bolsaArticulo.getArtIgv();
				SumatoriaImpuestos += ISC;

			}
		}
		precioOperacion = SumatoriaImpuestos + valorOperacion;
		Calendar calendario = new GregorianCalendar();
		int hora, minutos, segundos;
		hora = calendario.get(Calendar.HOUR_OF_DAY);
		minutos = calendario.get(Calendar.MINUTE);
		segundos = calendario.get(Calendar.SECOND);
		String shora = (hora >= 10) ? hora + "" : "0" + hora;
		String sMin = (minutos >= 10) ? minutos + "" : "0" + minutos;
		String sSeg = (segundos >= 10) ? segundos + "" : "0" + segundos;
		String horaString = shora + ":" + sMin + ":" + sSeg;

		// 03|2018-07-17|12:37:42|2018-07-17||1|00000000|CLIENTES
		// VARIOS|PEN|0.00|0.00|0.00|0.38|2.12|2.50|0.00|0.00|0.00|2.50|2.1|2.O
		archivo += "0101";
		archivo += "|" + Fecha;
		archivo += "|" + horaString;
		archivo += "|" + Fecha;
		archivo += "|" + "01";
		archivo += "|" + DocumentoTipo;
		archivo += "|" + DocumentoNu;
		archivo += "|" + NomApe;
		archivo += "|" + MonedaTipo;
		archivo += "|" + formatear(SumatoriaImpuestos);
		archivo += "|" + formatear(valorOperacion);
		archivo += "|" + formatear(precioOperacion);
		archivo += "|" + "0.00";
		archivo += "|" + "0.00";
		archivo += "|" + "0.00";
		archivo += "|" + formatear(precioOperacion);
		archivo += "|" + "2.1";
		archivo += "|" + "2.O";
		System.out.println(archivo);
		archivo = archivo.toUpperCase();

		mandar.add(archivo);
		return mandar;

	}

	public ArrayList<String> getContenidoImpuestos() {
		ArrayList<String> mandar = new ArrayList<>();

		String dar = getTributosIGV();
		mandar.add(dar);
		dar = getTributosISC();
		if (dar.length() >= 2) {
			mandar.add(dar);

		}
		return mandar;

	}

	public ArrayList<String> getContenidoDetalles() {
		ArrayList<String> mandar = new ArrayList<>();
		for (MovimientoDet det : detalles) {
			String dar = crearLineaDetalle(det);
			mandar.add(dar);

		}
		return mandar;
	}

	private void crearDetalles() {
		try {
			String de = cabecera.getIdEmpresaSede();
			empresaSedeOri = new EmpresaSede("'" + de + "'");
			empresaOri = new Empresa(empresaSedeOri.getIdEmpresa());
			empresaSedeDes = new EmpresaSede(cabecera.getIdEmpresaSedeDes());
			empresaDes = new Empresa(empresaSedeDes.getIdEmpresa());
			// TODO Auto-generaNIted method stub
			ArrayList<String> mandar = getContenidoDetalles();

			String identificador = empresaOri.getEmpRUC();
			String documento = "03";
			if (cabecera.getIdMovimientoDocumento().equals("3")) {
				documento = "03";
			}
			if (cabecera.getIdMovimientoDocumento().equals("1")) {
				documento = "01";
			}
			String serie = cabecera.getMovCabSer();
			String numero = cabecera.getMovCabNum();
			String nombre = "";
			if (cabecera.getIdMovimientoDocumento().equals("3"))
				nombre = identificador + "-" + documento + "-B" + serie + "-" + numero + ".DET";
			if (cabecera.getIdMovimientoDocumento().equals("1"))
				nombre = identificador + "-" + documento + "-F" + serie + "-" + numero + ".DET";
			System.err.println("este es el nombre " + nombre);
			crearArchivo(nombre, mandar);
		} catch (IOException ex) {
			Logger.getLogger(factura.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	private void crearCabecera(String doc) throws IOException {
		try {
			String identificador = empresaOri.getEmpRUC();
			String documento = "03";
			if (cabecera.getIdMovimientoDocumento().equals("3"))
				documento = "03";
			if (cabecera.getIdMovimientoDocumento().equals("1"))
				documento = "01";
			String serie = cabecera.getMovCabSer();
			String numero = cabecera.getMovCabNum();
			String nombre = "";
			if (cabecera.getIdMovimientoDocumento().equals("3")) {
				nombre = identificador + "-" + documento + "-" + "B" + serie + "-" + numero + ".CAB";
			}
			if (cabecera.getIdMovimientoDocumento().equals("1")) {
				nombre = identificador + "-" + documento + "-" + "F" + serie + "-" + numero + ".CAB";
			}
			ArrayList<String> mandar = getContenidoCabecera();
			crearArchivo(nombre, mandar);
		} catch (ParseException ex) {
			Logger.getLogger(factura.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	private void crearLeyenda(String doc) throws IOException {
		String archivo = "";
		String Tipo = "0" + cabecera.getIdMovimientoDocumento();
		String Fecha = cabecera.getMovCabFec();

		String Domicilio = "EstadosUnidos14";
		Domicilio = Domicilio.toUpperCase();
		DecimalFormat df = new DecimalFormat("#.00");

		String DocumentoTipo = empresaDes.getIdDocumento();

		String DocumentoNu = empresaDes.getEmpRUC();
		String NomApe = empresaDes.getEmpNom();
		String MonedaTipo = "PEN";
		String DescGlo = "0.00";

		String OtrosCargos = "0.00";
		String TotalDescuento = "0.00";
		String OperacionesInafectas = "" + "0.00";
		String OperacionesExoneradas = "" + "0.00";

		double SumatoriaIGVNum = 0;
		for (MovimientoDet a : detalles) {
			SumatoriaIGVNum += Double.parseDouble(a.getMovDetPreTot());
		}
		double OperacionesGravadasNum = SumatoriaIGVNum / 1.18;
		String OperacionesGravadas = "" + df.format(OperacionesGravadasNum);
		OperacionesGravadas = OperacionesGravadas.replace(",", ".");
		if (OperacionesGravadas.charAt(0) == '.')
			OperacionesGravadas = OperacionesGravadas.replace(".", "0.");
		SumatoriaIGVNum = SumatoriaIGVNum * 0.18 / 1.18;

		String SumatoriaIGV = "" + df.format(SumatoriaIGVNum);
		SumatoriaIGV = SumatoriaIGV.replace(",", ".");
		if (SumatoriaIGV.charAt(0) == '.')
			SumatoriaIGV = SumatoriaIGV.replace(".", "0.");
		double ImporteNum = 0;

		double SumatoriaISCNum = 0;
		for (MovimientoDet a : detalles) {
			ImporteNum += Double.parseDouble(a.getMovDetPreTot());
		}
		Double valor = ImporteNum - SumatoriaIGVNum;
		String valorVenta = df.format(valor);
		String SumatoriaISC = "" + df.format(SumatoriaISCNum);

		SumatoriaISC = SumatoriaISC.replace(",", ".");
		if (SumatoriaISC.charAt(0) == '.')
			SumatoriaISC = SumatoriaISC.replace(".", "0.");
		String SumatoriaOtros = "0.00";
		Calendar calendario = new GregorianCalendar();
		String ImporteTotal = "" + df.format(ImporteNum);
		ImporteTotal = ImporteTotal.replace(",", ".");
		valorVenta = valorVenta.replace(",", ".");
		if (valorVenta.charAt(0) == '.') {
			valorVenta = "0" + valorVenta;
		}
//03|2018-07-17|12:37:42|2018-07-17||1|00000000|CLIENTES VARIOS|PEN|0.00|0.00|0.00|0.38|2.12|2.50|0.00|0.00|0.00|2.50|2.1|2.O
		if (ImporteTotal.charAt(0) == '.')
			ImporteTotal = ImporteTotal.replace(".", "0.");
		Numero_a_Letra num2Letras = new Numero_a_Letra();
		archivo += "1000";
		archivo += "|" + num2Letras.Convertir(ImporteTotal, true);

//System.out.println(archivo);
		archivo = archivo.toUpperCase();
		String identificador = empresaOri.getEmpRUC();
		String documento = "03";
		if (cabecera.getIdMovimientoDocumento().equals("3"))
			documento = "03";
		if (cabecera.getIdMovimientoDocumento().equals("1"))
			documento = "01";
		String serie = cabecera.getMovCabSer();
		String numero = cabecera.getMovCabNum();
		String nombre = "";
		if (cabecera.getIdMovimientoDocumento().equals("3")) {
			nombre = identificador + "-" + documento + "-" + "B" + serie + "-" + numero + ".LEY";
		}
		if (cabecera.getIdMovimientoDocumento().equals("1")) {
			nombre = identificador + "-" + documento + "-" + "F" + serie + "-" + numero + ".LEY";
		}
		ArrayList<String> mandar = new ArrayList<String>();
		mandar.add(archivo);
		crearArchivo(nombre, mandar);

	}

	private String getTributosISC() {
		int n = 1000;
		double SumatoriaIGVNum = 0;
		for (MovimientoDet det : detalles) {
			if (Integer.parseInt(det.getIdArticulo()) == 2) {
				SumatoriaIGVNum += Double.parseDouble(det.getMovDetPreTot());
				Articulo bolsa = new Articulo(2);
				Double BaseImponibleDouble = SumatoriaIGVNum;
				DecimalFormat df = new DecimalFormat("#.00");
				String BaseImponible = df.format(BaseImponibleDouble);
				String SumatoriaImpuestos = df.format(BaseImponibleDouble * bolsa.getArtIgv());
				BaseImponible = BaseImponible.replace(",", ".");
				if (BaseImponible.charAt(0) == '.') {
					BaseImponible = BaseImponible.replace(".", "0.");

				}
				SumatoriaImpuestos = SumatoriaImpuestos.replace(",", ".");
				if (SumatoriaImpuestos.charAt(0) == '.') {
					SumatoriaImpuestos = SumatoriaImpuestos.replace(".", "0.");

				}

				String dar = "7152";

				dar += "|" + "ICBPER";
				dar += "|" + "OTH";
				dar += "|" + BaseImponible;
				dar += "|" + SumatoriaImpuestos;

				dar = dar.toUpperCase();
				return dar;

			}
		}
		return "";
	}

	private String getTributosIGV() {
		int n = 1000;
		double SumatoriaIGVNum = 0;
		for (MovimientoDet det : detalles) {
			SumatoriaIGVNum += Double.parseDouble(det.getMovDetPreTot());

		}
		Double BaseImponibleDouble = SumatoriaIGVNum / (1.18);
		DecimalFormat df = new DecimalFormat("#.00");
		String BaseImponible = df.format(BaseImponibleDouble);
		String SumatoriaImpuestos = df.format(BaseImponibleDouble * 0.18);
		BaseImponible = BaseImponible.replace(",", ".");
		if (BaseImponible.charAt(0) == '.') {
			BaseImponible = BaseImponible.replace(".", "0.");

		}
		SumatoriaImpuestos = SumatoriaImpuestos.replace(",", ".");
		if (SumatoriaImpuestos.charAt(0) == '.') {
			SumatoriaImpuestos = SumatoriaImpuestos.replace(".", "0.");

		}

		String dar = "1000";

		dar += "|" + "IGV";
		dar += "|" + "VAT";
		dar += "|" + BaseImponible;
		dar += "|" + SumatoriaImpuestos;

		dar = dar.toUpperCase();
		return dar;

	}

	private void crearTributosGenerales(String doc) throws IOException {
		try {
			String de = cabecera.getIdEmpresaSede();
			empresaSedeOri = new EmpresaSede("'" + de + "'");
			empresaOri = new Empresa(empresaSedeOri.getIdEmpresa());
			empresaSedeDes = new EmpresaSede(cabecera.getIdEmpresaSedeDes());
			empresaDes = new Empresa(empresaSedeDes.getIdEmpresa());
			// TODO Auto-generaNIted method stub
			ArrayList<String> mandar = getContenidoImpuestos();

			String identificador = empresaOri.getEmpRUC();
			String documento = "03";
			if (cabecera.getIdMovimientoDocumento().equals("3")) {
				documento = "03";
			}
			if (cabecera.getIdMovimientoDocumento().equals("1")) {
				documento = "01";
			}
			String serie = cabecera.getMovCabSer();
			String numero = cabecera.getMovCabNum();
			String nombre = "";
			if (cabecera.getIdMovimientoDocumento().equals("3"))
				nombre = identificador + "-" + documento + "-B" + serie + "-" + numero + ".TRI";
			if (cabecera.getIdMovimientoDocumento().equals("1"))
				nombre = identificador + "-" + documento + "-F" + serie + "-" + numero + ".TRI";
			crearArchivo(nombre, mandar);
		} catch (IOException ex) {
			Logger.getLogger(factura.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	private Boolean crearArchivo(String name, ArrayList<String> nombre) throws IOException {
		String ruta = "D:\\sunat_archivos\\sfs\\DATA\\";
		ruta += name;
		File archivo = new File(ruta);
		BufferedWriter bw;
		if (archivo.exists()) {
			bw = new BufferedWriter(new FileWriter(archivo));
			PrintWriter pw = new PrintWriter(bw);

			for (String desu : nombre) {
				pw.println(desu);
			}
		} else {
			bw = new BufferedWriter(new FileWriter(archivo));
			PrintWriter pw = new PrintWriter(bw);
			for (String desu : nombre) {
				pw.println(desu);
			}
		}
		bw.close();
		return true;
	}

	public void enviarFactura() {

		try {
			
			crearCabecera(FACTURA);
			crearLeyenda(FACTURA);
			crearTributosGenerales(FACTURA);

		} catch (IOException ex) {
			Logger.getLogger(factura.class.getName()).log(Level.SEVERE, null, ex);
		}
		crearDetalles();

	}

	public static void enviarFacturaSunat() {
		try {

			System.out.println("imprime de mas  :# #!");
			/* directorio/ejecutable es el path del ejecutable y un nombre */
			/*
			 * Process p = Runtime.getRuntime().exec("cmd /c  D: &" + "cd D:\\SFS_v1.1 &" +
			 * "java  -Djavax.net.ssl.trustStore=\"%JAVA_HOME%\\lib\\security\\cacerts\" -Djavax.net.ssl.trustStorePassword=changeit -jar facturadorApp-1.1.jar server prod.yaml"
			 * );
			 */// Runtime.getRuntime().runFinalization();
				// Runtime.getRuntime().exit(0);

			// System.out.println("lo hizo ");

			// Runtime.getRuntime().gc();
			// Runtime.getRuntime().wait();
			// p.wait();

			// System.out.println(""+detalles.size());

		} catch (Exception e) {
			/*
			 * Se lanza una excepción si no se encuentra en ejecutable o e1l fichero no es
			 * ejecutable.
			 */
		}

	}

	public static void main(String[] args) throws SQLException, IOException {
//		enviarFacturaSunat();

	}

	public void insertarWithNewId() {
		try {
			String max = MovimientoCab.generarCodigo();
			cabecera.setIdMovimientoCabecera(max);
			cabecera.insertar();
			int n = Integer.parseInt(MovimientoDet.generarCodigo());
			for (MovimientoDet detalle : detalles) {
				detalle.setIdMovimientoCabecera(cabecera.getIdMovimientoCabecera());
				detalle.setIdMovimientoDetalle("" + n);
				detalle.insertar();
				n++;
			}
		} catch (SQLException ex) {
			Logger.getLogger(factura.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	public void insertarWithNewId(int id) {
		try {
			String idDiferenciado = MovimientoCab.diferenteAsociado(id);
			MovimientoCab sd = new MovimientoCab();
			sd.setIdMovimientoCabecera(idDiferenciado);
			sd.eliminar();
			String max = MovimientoCab.generarCodigo();
			cabecera.setIdMovimientoCabecera(max);
			cabecera.insertar();

			int n = Integer.parseInt(MovimientoDet.generarCodigo());
			for (MovimientoDet detalle : detalles) {
				detalle.setIdMovimientoCabecera(cabecera.getIdMovimientoCabecera());
				detalle.setIdMovimientoDetalle("" + n);
				detalle.insertar();
				n++;
			}
		} catch (SQLException ex) {
			Logger.getLogger(factura.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

}
