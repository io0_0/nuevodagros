/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facturador;

import Modificar.MODIFICARCOMPROBANTE;
import Bean.Articulo;
import Bean.Configurar;
import Maestros.GenerarCodigo;
import Maestros.Maestro;
import GUI.EmpresaGUI;
import Maestros.ArticuloM;
import static Maestros.ArticuloM.cn;
import Bean.Empresa;
import Bean.EmpresaSede;
import Bean.Lista;
import Bean.MovimientoCab;
import Bean.MovimientoDet;
import Bean.empresasedeserie;
import Conexion.Mysql;
import GUI.Principal;
import Maestros.Atributos;
import Maestros.MovimientoDetalleM;
import br.com.adilson.util.Extenso;
import br.com.adilson.util.PrinterMatrix;
import com.mysql.jdbc.MySQLConnection;
import com.toedter.calendar.JCalendar;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.swing.*;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Equipo EPIS
 */
public class facturadorGUI extends javax.swing.JFrame implements Atributos {

    protected factura FACTURA = new factura();
    ArrayList<Component> editables = new ArrayList<Component>();
    public static final int ANCHO_FORM_LLENAR = 972;
    public static final int ALTO_FORM_LLENAR = 375;
    public static final int ANCHO_FORM_MOSTRAR = 1400;
    public static final int ALTO_FORM_MOSTRAR = 755;
    public static final int ANCHO_FORM_BOT = 335;
    public static final int ALTO_FORM_BOT = 10;
    public static final int ANCHO_FORM = 1400;
    public static final int ALTO_FORM = 1100;
    boolean bandera = true;
    int cantDatos = 9;
    public int indiceFila = 0;
    public static facturadorGUI actual;
    Maestro maestro = new Maestro();
    //Este objeto siempre tendra los atributos de la Tabla para que se ejecute sql en la clase EmpresaSql.
    MovimientoDetalleM ejecutor = maestro.gestionarMovimientoDetalle();
    MovimientoCab cabe = new MovimientoCab();
    private String usuario = "";
    protected String otroCamp = "";
    boolean isModificando = false;
    public panelPedido[] paneles = new panelPedido[50];
    static int anchoFinal = 0;
    static int altoFinal = 0;

    public facturadorGUI(String desu) throws SQLException {
        if (actual != null) {
            actual.dispose();
        }
        initComponents();
        jButtonSoloImprimir.setEnabled(false);

        usuario = desu;
        iniMio();
        actual = this;
    }

    public facturadorGUI(factura fac) throws ParseException {
        if (actual != null) {
            actual.dispose();
        }

        initComponents();

        iniMio();
        ejecutor.llenarCajaArtVentas(JComboArt);

        JTabla.setForeground(Color.red);
        jComboDocTipo.setEnabled(false);
        jComboComprobate.setEnabled(false);
        try {
            fac.obtenerDetalles();
        } catch (SQLException ex) {

            System.err.println("mal ");

            Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (MovimientoDet det : fac.getDetalles()) {

            det.setMovDetCan(det.getMovDetCan().substring(0, det.getMovDetCan().length() - 3));
        }
        isModificando = true;
        FACTURA = fac;
        FACTURA.getDetalles();

        llenarDatos();
        boolean [] paraLlevar = new boolean[FACTURA.getDetalles().size()];
        int rts = 0;
        for (MovimientoDet detallito : FACTURA.getDetalles()) {
            
            int idArticulo = Integer.parseInt(detallito.getIdArticulo()) ;
            if (idArticulo>10000){
                idArticulo-=10000;
                detallito.setIdArticulo(idArticulo+"");
                paraLlevar[rts]=true;        
            }
            rts++;
        }
        try {
            refrescar();
        } catch (SQLException ex) {
            Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
         rts=0;
        for (boolean b : paraLlevar) {
            JTabla.getModel().setValueAt(b, rts, 5);
            rts++;
        }
        ajustarBotonones();
        
        this.setSize(FormParaLlenar.getWidth() + jPolloContainer.getWidth(), FormParaLlenar.getHeight() + 100);

        actual = this;

    }

    public facturadorGUI() throws SQLException {
        //    FACTURA.setCabecera(new MovimientoCab());
        if (actual != null) {
            actual.dispose();
        }

        initComponents();
        iniMio();
        jButtonSoloImprimir.setEnabled(false);

        ejecutor.llenarCajaArtVentas(JComboArt);

        paraPruebas();
        actual = this;

    }

    public void iniMio() {
        try {
            List<String> lista = PrintUtility.getPrinterServiceNameList();
            for (String a : lista) {
                System.out.println(a);
            }
            JTabla.setForeground(Color.red);
            jComboDocTipo.setEnabled(false);
            refrescar();
            JCalendar jj = new JCalendar();
            Fecha1.setDate(jj.getDate());
            cabe.llenarCaja(jComboDocTipo, "DOCUMENTO", "DocDes");
            cabe.llenarCaja(jComboComprobate, "MOVIMIENTODocumento", "MOVDocDes", 2);

            String sql_Cons = "SELECT * FROM empresa where idempresaGrupo =1";

            // trabajr con limites de porciones 
            //el asunto limites de recursos 
            //aguadito de pollo para las parillas
            String where = "where ";
            ArrayList<Configurar> configs = Configurar.getCongiguracionesPorDato("idusuario", Principal.jLUsuarioGeneral.getText());
            for (Configurar config : configs) {
                where += "idempresasede = " + config.getIdEmpresaSede() + " or ";
            }
            where = where.substring(0, where.length() - 3);

            cabe.llenarCaja(jComboEmpSedOrig, "EMPRESASEDE", EMPSEDNOM, where);
            //ejecutor.llenarCajaArt(JComboArt);

            //FACTURA.getCabecera().generarCodigo(JTCodigoCab);
            MovimientoCab movi = new MovimientoCab();
            movi.setIdEmpresaSede(cabe.sacarDato("empresaSEde ", "empsednom", "'" + jComboEmpSedOrig.getSelectedItem().toString() + "'"));
            //String revisar=RevisarDocumento();
            //movi.setIdEmpresaSedeDes(revisar);
            movi.setIdMovimientoCabecera("''");
            String idmovdoc = cabe.sacarDato("movimientodocumento", "movdocDes", "'" + jComboComprobate.getSelectedItem().toString() + "'");
            movi.setIdMovimientoDocumento(idmovdoc);
            movi.setIdUsuario("'1'");
            movi.setIdMovimientoTipo("1");

//            movi.setMovCabEnv("0");
            movi.setMovCabEstReg("1");
            //Date da=new Date();

            movi.setMovCabFec("'" + ((JTextField) (Fecha1.getDateEditor().getUiComponent())).getText() + "'");
            movi.setMovCabMovSto("1");
            movi.setMovCabNum("'" + jTextNumero.getText() + "'");
            movi.setMovCabSer("'" + jTextSerie.getText() + "'");
            FACTURA.setCabecera(movi);
            jTextNumero.setEditable(false);
            jTextSerie.setEditable(false);
            jComboComprobate.setSelectedIndex(1);
            jComboDocTipo.setSelectedIndex(0);

            JTIGVDet.setText("" + 0.18);

// TODO add your handling code here:
            empresasedeserie data;
            String idDocumento = "";
            if (jComboComprobate.getSelectedItem().toString().equals("FACTURA")) {
                idDocumento = "1";
            } else {
                idDocumento = "3";
            }

            // FACTURA.getCabecera().setIdEmpresaSede(jComboEmpSedOrig.getSelectedItem().toString());
            data = new empresasedeserie(FACTURA.getCabecera().sacarDato("empresasede", "empsednom", "'" + jComboEmpSedOrig.getSelectedItem().toString() + "'"), idDocumento);
            int datos2 = Integer.parseInt(data.getEmpsedsernum());
            datos2++;
            String cero = "";
            for (int i = (int) (Math.floor(Math.log10(datos2))); i < data.getEmpsedsernum().length() - 1; i++) {
                cero += 0;

            }
            jTextNumero.setText(cero + "" + datos2);
            jTextSerie.setText(data.getEmpsedserser());
        } catch (SQLException ex) {
            Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        jComboDocTipo.setEnabled(false);
        editables.add(jComboComprobate);
        editables.add(jComboDocTipo);
        editables.add(jComboEmpSedOrig);
        editables.add(JBEliminar);
        editables.add(JBGrabar);
        editables.add(jBGrabarEImprimir);
        iniciarPaneles();
        anchoFinal = FormParaLlenar.getWidth() + jPolloContainer.getWidth();
        altoFinal = FormParaLlenar.getHeight() + 100;

    }

    public void seteditables() {
        for (Component editable : editables) {
            editable.setEnabled(false);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        fecha1 = new com.toedter.calendar.JDateChooser();
        jPolloContainer2 = new javax.swing.JPanel();
        panelPedido51 = new Facturador.panelPedido();
        panelPedido52 = new Facturador.panelPedido();
        panelPedido53 = new Facturador.panelPedido();
        panelPedido54 = new Facturador.panelPedido();
        panelPedido55 = new Facturador.panelPedido();
        panelPedido56 = new Facturador.panelPedido();
        panelPedido57 = new Facturador.panelPedido();
        panelPedido58 = new Facturador.panelPedido();
        panelPedido59 = new Facturador.panelPedido();
        panelPedido60 = new Facturador.panelPedido();
        panelPedido61 = new Facturador.panelPedido();
        panelPedido62 = new Facturador.panelPedido();
        panelPedido63 = new Facturador.panelPedido();
        panelPedido64 = new Facturador.panelPedido();
        panelPedido65 = new Facturador.panelPedido();
        panelPedido66 = new Facturador.panelPedido();
        panelPedido67 = new Facturador.panelPedido();
        panelPedido68 = new Facturador.panelPedido();
        panelPedido69 = new Facturador.panelPedido();
        panelPedido70 = new Facturador.panelPedido();
        panelPedido71 = new Facturador.panelPedido();
        panelPedido72 = new Facturador.panelPedido();
        panelPedido73 = new Facturador.panelPedido();
        panelPedido74 = new Facturador.panelPedido();
        panelPedido75 = new Facturador.panelPedido();
        jButton4 = new javax.swing.JButton();
        FormParaLlenar = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JTCodigoCab = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabelCambiar = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        JTDocumentoDestino = new javax.swing.JTextField();
        JTNombre = new javax.swing.JTextField();
        jComboComprobate = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();
        jComboEmpSedOrig = new javax.swing.JComboBox();
        jComboDocTipo = new javax.swing.JComboBox();
        jLabel10 = new javax.swing.JLabel();
        jLabelTitulo = new javax.swing.JLabel();
        FormBotones = new javax.swing.JPanel();
        JBNuevo = new javax.swing.JButton();
        JBModificar = new javax.swing.JButton();
        JBEliminar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        JTabla = new javax.swing.JTable();
        jTextFieldTotal = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        JBGrabar = new javax.swing.JButton();
        JBSalir = new javax.swing.JButton();
        jTextFieldIGV = new javax.swing.JTextField();
        JTextFieldsumatoPre = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jButtonSoloImprimir = new javax.swing.JButton();
        jBGrabarEImprimir = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel20 = new javax.swing.JLabel();
        jTextFieldISC = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTextFieldTotalFinal = new javax.swing.JTextField();
        jTextSerie = new javax.swing.JTextField();
        jTextNumero = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabelParaNoobs = new javax.swing.JLabel();
        JTDireccion = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jButton5 = new javax.swing.JButton();
        Fecha1 = new com.toedter.calendar.JDateChooser();
        FormParaLlenar1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        JBGrabar1 = new javax.swing.JButton();
        JBSalir1 = new javax.swing.JButton();
        JComboArt = new javax.swing.JComboBox();
        jLabel14 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        JTCantidad = new javax.swing.JTextField();
        JTIGVDet = new javax.swing.JTextField();
        jtbuscar = new javax.swing.JTextField();
        jPolloContainer = new javax.swing.JPanel();
        panelPedido1 = new Facturador.panelPedido();
        panelPedido2 = new Facturador.panelPedido();
        panelPedido3 = new Facturador.panelPedido();
        panelPedido4 = new Facturador.panelPedido();
        panelPedido5 = new Facturador.panelPedido();
        panelPedido6 = new Facturador.panelPedido();
        panelPedido7 = new Facturador.panelPedido();
        panelPedido8 = new Facturador.panelPedido();
        panelPedido9 = new Facturador.panelPedido();
        panelPedido10 = new Facturador.panelPedido();
        panelPedido11 = new Facturador.panelPedido();
        panelPedido12 = new Facturador.panelPedido();
        panelPedido13 = new Facturador.panelPedido();
        panelPedido14 = new Facturador.panelPedido();
        panelPedido15 = new Facturador.panelPedido();
        panelPedido16 = new Facturador.panelPedido();
        panelPedido17 = new Facturador.panelPedido();
        panelPedido18 = new Facturador.panelPedido();
        panelPedido19 = new Facturador.panelPedido();
        panelPedido20 = new Facturador.panelPedido();
        panelPedido21 = new Facturador.panelPedido();
        panelPedido22 = new Facturador.panelPedido();
        panelPedido23 = new Facturador.panelPedido();
        panelPedido24 = new Facturador.panelPedido();
        panelPedido25 = new Facturador.panelPedido();
        jButton3 = new javax.swing.JButton();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        fecha1.setDateFormatString("dd-MM-yyyy");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("COMPROBANTE DE VENTA");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setForeground(new java.awt.Color(255, 255, 255));
        setResizable(false);

        jPolloContainer2.setBackground(new java.awt.Color(255, 255, 255));

        jButton4.setText("Atras");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPolloContainer2Layout = new javax.swing.GroupLayout(jPolloContainer2);
        jPolloContainer2.setLayout(jPolloContainer2Layout);
        jPolloContainer2Layout.setHorizontalGroup(
            jPolloContainer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPolloContainer2Layout.createSequentialGroup()
                .addGroup(jPolloContainer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPolloContainer2Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(jPolloContainer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPolloContainer2Layout.createSequentialGroup()
                                .addComponent(panelPedido51, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(panelPedido52, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(panelPedido53, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(panelPedido54, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(panelPedido55, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPolloContainer2Layout.createSequentialGroup()
                                .addGroup(jPolloContainer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPolloContainer2Layout.createSequentialGroup()
                                        .addComponent(panelPedido56, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(panelPedido57, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPolloContainer2Layout.createSequentialGroup()
                                        .addGroup(jPolloContainer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(panelPedido66, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(panelPedido61, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPolloContainer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(panelPedido62, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(panelPedido67, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPolloContainer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPolloContainer2Layout.createSequentialGroup()
                                        .addComponent(panelPedido58, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(panelPedido59, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(panelPedido60, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPolloContainer2Layout.createSequentialGroup()
                                        .addComponent(panelPedido63, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(panelPedido64, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(panelPedido65, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPolloContainer2Layout.createSequentialGroup()
                                        .addComponent(panelPedido68, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(panelPedido69, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(panelPedido70, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(jPolloContainer2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panelPedido71, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panelPedido72, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panelPedido73, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panelPedido74, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panelPedido75, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPolloContainer2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(41, Short.MAX_VALUE))
        );
        jPolloContainer2Layout.setVerticalGroup(
            jPolloContainer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPolloContainer2Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPolloContainer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPolloContainer2Layout.createSequentialGroup()
                        .addGroup(jPolloContainer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(panelPedido52, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido51, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido53, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido54, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido55, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(11, 11, 11)
                        .addGroup(jPolloContainer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(panelPedido56, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido57, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido58, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido60, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido59, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPolloContainer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(panelPedido61, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido62, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido63, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido64, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(panelPedido65, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPolloContainer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelPedido66, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelPedido67, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelPedido68, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelPedido69, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelPedido70, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPolloContainer2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelPedido71, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelPedido72, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelPedido73, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelPedido74, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelPedido75, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        FormParaLlenar.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setText("CODIGO:");

        JTCodigoCab.setEnabled(false);
        JTCodigoCab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTCodigoCabActionPerformed(evt);
            }
        });

        jLabel3.setText("FECHA:");

        jLabel4.setText("SEDE DE EMPRESA ORIGEN:");

        jLabelCambiar.setText("NUMERO DE DOCUMENTO");

        jLabel8.setText("NOMBRE Y APELLIDO");

        JTDocumentoDestino.setText("0");
        JTDocumentoDestino.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTDocumentoDestinoActionPerformed(evt);
            }
        });
        JTDocumentoDestino.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                JTDocumentoDestinoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                JTDocumentoDestinoKeyTyped(evt);
            }
        });

        JTNombre.setText("CLIENTES VARIOS");
        JTNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                JTNombreKeyTyped(evt);
            }
        });

        jComboComprobate.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboComprobate.setName("combo"); // NOI18N
        jComboComprobate.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboComprobateItemStateChanged(evt);
            }
        });
        jComboComprobate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboComprobateActionPerformed(evt);
            }
        });

        jLabel9.setText("COMPROBANTE");

        jComboEmpSedOrig.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboEmpSedOrig.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboEmpSedOrigItemStateChanged(evt);
            }
        });
        jComboEmpSedOrig.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboEmpSedOrigActionPerformed(evt);
            }
        });

        jComboDocTipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel10.setText("TIPO DOCUMENTO");

        jLabelTitulo.setFont(new java.awt.Font("Tahoma", 0, 22)); // NOI18N
        jLabelTitulo.setText("COMPROBANTE DE VENTA: BOLETA");

        FormBotones.setBackground(new java.awt.Color(255, 255, 255));

        JBNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/NUEVO.JPG"))); // NOI18N
        JBNuevo.setText("Agregar");
        JBNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBNuevoActionPerformed(evt);
            }
        });

        JBModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/MODIFICAR.JPG"))); // NOI18N
        JBModificar.setText("Modificar");
        JBModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBModificarActionPerformed(evt);
            }
        });

        JBEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/ELIMINAR.JPG"))); // NOI18N
        JBEliminar.setText("Eliminar");
        JBEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBEliminarActionPerformed(evt);
            }
        });

        JTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "null", "null"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        JTabla.setGridColor(new java.awt.Color(0, 0, 0));
        jScrollPane2.setViewportView(JTabla);

        jTextFieldTotal.setEditable(false);
        jTextFieldTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldTotalActionPerformed(evt);
            }
        });

        jLabel11.setText("TOTAL");

        JBGrabar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/GUARDAR2.JPG"))); // NOI18N
        JBGrabar.setText("Grabar");
        JBGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBGrabarActionPerformed(evt);
            }
        });

        JBSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/CANCELAR.JPG"))); // NOI18N
        JBSalir.setText("Salir sin Grabar");
        JBSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBSalirActionPerformed(evt);
            }
        });

        jTextFieldIGV.setEditable(false);
        jTextFieldIGV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldIGVActionPerformed(evt);
            }
        });

        JTextFieldsumatoPre.setEditable(false);

        jLabel18.setText("PRETOTAL");

        jLabel19.setText("TOTAL IGV");

        jButtonSoloImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/IMPRESORA.jpg"))); // NOI18N
        jButtonSoloImprimir.setText("IMPRIMIR");
        jButtonSoloImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSoloImprimirActionPerformed(evt);
            }
        });

        jBGrabarEImprimir.setText("Grabar e Imprimir");
        jBGrabarEImprimir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jBGrabarEImprimirMouseEntered(evt);
            }
        });
        jBGrabarEImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGrabarEImprimirActionPerformed(evt);
            }
        });

        jButton2.setText("TICKET");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel20.setText("TOTAL ISC");

        jTextFieldISC.setEditable(false);
        jTextFieldISC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldISCActionPerformed(evt);
            }
        });

        jLabel12.setText("TOTAL");

        jTextFieldTotalFinal.setEditable(false);
        jTextFieldTotalFinal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldTotalFinalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FormBotonesLayout = new javax.swing.GroupLayout(FormBotones);
        FormBotones.setLayout(FormBotonesLayout);
        FormBotonesLayout.setHorizontalGroup(
            FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormBotonesLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(JBSalir)
                .addGap(195, 195, 195))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormBotonesLayout.createSequentialGroup()
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2)
                    .addGroup(FormBotonesLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(FormBotonesLayout.createSequentialGroup()
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                                .addComponent(jButtonSoloImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(FormBotonesLayout.createSequentialGroup()
                                .addComponent(JBGrabar)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(277, 277, 277)
                        .addComponent(jBGrabarEImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JBModificar)
                    .addComponent(JBNuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JBEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(193, 193, 193))
            .addGroup(FormBotonesLayout.createSequentialGroup()
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(JTextFieldsumatoPre, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormBotonesLayout.createSequentialGroup()
                        .addComponent(jLabel20)
                        .addGap(10, 10, 10)
                        .addComponent(jTextFieldISC, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldTotalFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FormBotonesLayout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addGap(10, 10, 10)
                        .addComponent(jTextFieldIGV, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(445, 445, 445))
        );
        FormBotonesLayout.setVerticalGroup(
            FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormBotonesLayout.createSequentialGroup()
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(FormBotonesLayout.createSequentialGroup()
                        .addComponent(JBNuevo)
                        .addGap(18, 18, 18)
                        .addComponent(JBModificar)
                        .addGap(18, 18, 18)
                        .addComponent(JBEliminar)))
                .addGap(12, 12, 12)
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldIGV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19)
                    .addComponent(JTextFieldsumatoPre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(jTextFieldTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormBotonesLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldTotalFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(JBSalir, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)
                            .addComponent(jBGrabarEImprimir, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(FormBotonesLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldISC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel20))
                        .addGap(5, 5, 5)
                        .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonSoloImprimir)
                            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(JBGrabar)))
                .addContainerGap(38, Short.MAX_VALUE))
        );

        jTextSerie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextSerieActionPerformed(evt);
            }
        });
        jTextSerie.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextSerieKeyTyped(evt);
            }
        });

        jTextNumero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextNumeroActionPerformed(evt);
            }
        });
        jTextNumero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextNumeroKeyTyped(evt);
            }
        });

        jLabel7.setText("SERIE");

        jLabel17.setText("NUMERO");

        jLabelParaNoobs.setText("el ruc tiene que tener 11 digitos ");

        JTDireccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTDireccionActionPerformed(evt);
            }
        });
        JTDireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                JTDireccionKeyTyped(evt);
            }
        });

        jLabel15.setText("DIRECCION ");

        jButton5.setText("MODIFICAR");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FormParaLlenarLayout = new javax.swing.GroupLayout(FormParaLlenar);
        FormParaLlenar.setLayout(FormParaLlenarLayout);
        FormParaLlenarLayout.setHorizontalGroup(
            FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormParaLlenarLayout.createSequentialGroup()
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormParaLlenarLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel4))
                    .addGroup(FormParaLlenarLayout.createSequentialGroup()
                        .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(FormParaLlenarLayout.createSequentialGroup()
                                .addGap(55, 55, 55)
                                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel10)
                                    .addComponent(jLabel15)
                                    .addComponent(jLabel7)))
                            .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(34, 34, 34)))
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormParaLlenarLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(JTDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(FormParaLlenarLayout.createSequentialGroup()
                                .addComponent(jTextSerie, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(27, 27, 27)
                                .addComponent(jLabel17)
                                .addGap(82, 82, 82)
                                .addComponent(jTextNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(FormParaLlenarLayout.createSequentialGroup()
                                .addComponent(JTCodigoCab, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel9))
                            .addComponent(JTNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(FormParaLlenarLayout.createSequentialGroup()
                                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(FormParaLlenarLayout.createSequentialGroup()
                                        .addComponent(jComboDocTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabelCambiar))
                                    .addGroup(FormParaLlenarLayout.createSequentialGroup()
                                        .addComponent(jComboEmpSedOrig, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel3)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(JTDocumentoDestino, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(FormParaLlenarLayout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addComponent(jLabelParaNoobs))
                                    .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(Fecha1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jComboComprobate, javax.swing.GroupLayout.Alignment.LEADING, 0, 153, Short.MAX_VALUE)))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(FormParaLlenarLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 378, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormParaLlenarLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(FormBotones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(179, 179, 179))
        );
        FormParaLlenarLayout.setVerticalGroup(
            FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormParaLlenarLayout.createSequentialGroup()
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormParaLlenarLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(jLabelTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormParaLlenarLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(JTCodigoCab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormParaLlenarLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(jComboComprobate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(jComboEmpSedOrig, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3))
                    .addComponent(Fecha1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jComboDocTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelCambiar)
                    .addComponent(JTDocumentoDestino, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelParaNoobs)
                .addGap(18, 18, 18)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(JTNombre, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextSerie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel17))
                .addGap(11, 11, 11)
                .addComponent(FormBotones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27))
        );

        FormParaLlenar1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setText("BUSCAR:");

        jLabel5.setText("ARTICULO:");

        JBGrabar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/GUARDAR2.JPG"))); // NOI18N
        JBGrabar1.setText("Grabar");
        JBGrabar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBGrabar1ActionPerformed(evt);
            }
        });

        JBSalir1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/CANCELAR_1.JPG"))); // NOI18N
        JBSalir1.setText("Salir sin Grabar");
        JBSalir1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBSalir1ActionPerformed(evt);
            }
        });

        JComboArt.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        JComboArt.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                JComboArtItemStateChanged(evt);
            }
        });

        jLabel14.setText("CANTIDAD:");

        jLabel16.setText("IGV:");

        JTCantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTCantidadActionPerformed(evt);
            }
        });
        JTCantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                JTCantidadKeyTyped(evt);
            }
        });

        JTIGVDet.setEditable(false);
        JTIGVDet.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                JTIGVDetKeyTyped(evt);
            }
        });

        jtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jtbuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtbuscarKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout FormParaLlenar1Layout = new javax.swing.GroupLayout(FormParaLlenar1);
        FormParaLlenar1.setLayout(FormParaLlenar1Layout);
        FormParaLlenar1Layout.setHorizontalGroup(
            FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormParaLlenar1Layout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5)
                    .addComponent(jLabel14)
                    .addComponent(jLabel16))
                .addGap(63, 63, 63)
                .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(FormParaLlenar1Layout.createSequentialGroup()
                        .addComponent(JBGrabar1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
                        .addComponent(JBSalir1))
                    .addComponent(JComboArt, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(JTCantidad)
                    .addComponent(JTIGVDet)
                    .addComponent(jtbuscar))
                .addContainerGap(39, Short.MAX_VALUE))
        );
        FormParaLlenar1Layout.setVerticalGroup(
            FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormParaLlenar1Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(55, 55, 55)
                .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(JComboArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(JTCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(56, 56, 56)
                .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(JTIGVDet, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JBSalir1)
                    .addComponent(JBGrabar1))
                .addGap(42, 42, 42))
        );

        jPolloContainer.setBackground(new java.awt.Color(255, 255, 255));

        jButton3.setText("Siguiente");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPolloContainerLayout = new javax.swing.GroupLayout(jPolloContainer);
        jPolloContainer.setLayout(jPolloContainerLayout);
        jPolloContainerLayout.setHorizontalGroup(
            jPolloContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPolloContainerLayout.createSequentialGroup()
                .addGroup(jPolloContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPolloContainerLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(jPolloContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPolloContainerLayout.createSequentialGroup()
                                .addComponent(panelPedido1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(panelPedido2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(panelPedido3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(panelPedido4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(panelPedido5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPolloContainerLayout.createSequentialGroup()
                                .addGroup(jPolloContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPolloContainerLayout.createSequentialGroup()
                                        .addComponent(panelPedido6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(panelPedido7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPolloContainerLayout.createSequentialGroup()
                                        .addGroup(jPolloContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(panelPedido16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(panelPedido11, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPolloContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(panelPedido12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(panelPedido17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPolloContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPolloContainerLayout.createSequentialGroup()
                                        .addComponent(panelPedido8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(panelPedido9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(panelPedido10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPolloContainerLayout.createSequentialGroup()
                                        .addComponent(panelPedido13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(panelPedido14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(panelPedido15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPolloContainerLayout.createSequentialGroup()
                                        .addComponent(panelPedido18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(panelPedido19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(panelPedido20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(jPolloContainerLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPolloContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton3)
                            .addGroup(jPolloContainerLayout.createSequentialGroup()
                                .addComponent(panelPedido21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(panelPedido22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(panelPedido23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(panelPedido24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(panelPedido25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(41, Short.MAX_VALUE))
        );
        jPolloContainerLayout.setVerticalGroup(
            jPolloContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPolloContainerLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPolloContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPolloContainerLayout.createSequentialGroup()
                        .addGroup(jPolloContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(panelPedido2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(11, 11, 11)
                        .addGroup(jPolloContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(panelPedido6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPolloContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(panelPedido11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelPedido14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(panelPedido15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPolloContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelPedido16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelPedido17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelPedido18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelPedido19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelPedido20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPolloContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelPedido21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelPedido22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelPedido23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelPedido24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelPedido25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(FormParaLlenar, javax.swing.GroupLayout.PREFERRED_SIZE, 686, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(FormParaLlenar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPolloContainer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPolloContainer2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(FormParaLlenar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPolloContainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPolloContainer2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(FormParaLlenar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JBSalir1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBSalir1ActionPerformed
        estado(1);
        try {
            refrescar();
        } catch (SQLException ex) {
            Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_JBSalir1ActionPerformed

    private void JBGrabar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBGrabar1ActionPerformed

        grabarDetalle();
        ajustarBotonones();

    }//GEN-LAST:event_JBGrabar1ActionPerformed

    private void JTCantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTCantidadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JTCantidadActionPerformed

    private void JTCantidadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTCantidadKeyTyped
        char c = evt.getKeyChar();

        if (!Character.isDigit(c)) {
            getToolkit().beep();
            evt.consume();

        }
    }//GEN-LAST:event_JTCantidadKeyTyped

    private void JTIGVDetKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTIGVDetKeyTyped
        char c = evt.getKeyChar();
        if (Character.isLetter(c)) {
            getToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_JTIGVDetKeyTyped

    private void jTextNumeroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextNumeroKeyTyped
        char c = evt.getKeyChar();

        if (Character.isLetter(c)) {
            getToolkit().beep();
            evt.consume();

        }
    }//GEN-LAST:event_jTextNumeroKeyTyped

    private void jTextNumeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextNumeroActionPerformed

    }//GEN-LAST:event_jTextNumeroActionPerformed

    private void jTextSerieKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextSerieKeyTyped
        char c = evt.getKeyChar();

        if (Character.isLetter(c)) {
            getToolkit().beep();
            evt.consume();

        }
    }//GEN-LAST:event_jTextSerieKeyTyped

    private void jTextSerieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextSerieActionPerformed

    }//GEN-LAST:event_jTextSerieActionPerformed

    private void JBEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBEliminarActionPerformed
        if (JTabla.getSelectedRow() != -1 && JTabla.getSelectedRow() < FACTURA.getDetalles().size()) {
            try {
                MovimientoDet de = FACTURA.getDetalles().get(JTabla.getSelectedRow());

                Articulo articulo = new Articulo(Integer.parseInt(de.getIdArticulo()));
                double a = getPrecioUni(de);
                DecimalFormat df = new DecimalFormat("#.00");
                double precio = a * Double.parseDouble(de.getMovDetCan());
                precio = Double.parseDouble(jTextFieldTotal.getText()) - precio;
                jTextFieldTotal.setText(df.format(precio));
                double subtotal = precio / 1.18;
                JTextFieldsumatoPre.setText(df.format(subtotal));
                double IGV = precio - subtotal;
                jTextFieldIGV.setText(df.format(IGV));
                FACTURA.getDetalles().remove(JTabla.getSelectedRow());
            } catch (SQLException ex) {
                Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un item  valido para Eliminar");
        }
        estado(1);
        ajustarBotonones();
    }//GEN-LAST:event_JBEliminarActionPerformed

    private void JBModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBModificarActionPerformed
        bandera = false;
        String[] datos = new String[cantDatos];
        ejecutor.llenarCajaArtVentas(JComboArt);

        if (JTabla.getSelectedRow() != -1 && JTabla.getSelectedRow() < FACTURA.getDetalles().size()) {
            estado(2);
            indiceFila = JTabla.getSelectedRow();

            JTCodigoCab.setText(FACTURA.getDetalles().get(indiceFila).getIdMovimientoDetalle());
            JTCodigoCab.setEditable(false);

            JTCantidad.setText(FACTURA.getDetalles().get(indiceFila).getMovDetCan());

            int j;
            Map<Integer, Articulo> art = new HashMap<Integer, Articulo>();
            for (j = 0; j < JComboArt.getItemCount(); j++) {
                String idAr = FACTURA.getDetalles().get(indiceFila).getIdArticulo();
                if (JComboArt.getItemAt(j).equals((new Articulo(Integer.parseInt(idAr))).getArtNom())) {
                    break;
                }
            }
            JComboArt.setSelectedItem(JComboArt.getItemAt(j));

            datos[8] = "1";

        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un item valido para modificar");
        }
    }//GEN-LAST:event_JBModificarActionPerformed

    private void JBNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBNuevoActionPerformed
        estado(2);
        jtbuscar.setText("");
        if (FACTURA.getDetalles().size() == 0) {
            otroCamp = ejecutor.generarCodigo();
            // System.out.println("este es el id q estas sacando "+otroCamp);

        } else {
            int valor = -1;
            for (MovimientoDet col : FACTURA.getDetalles()) {
                valor = Math.max(valor, Integer.parseInt(col.getIdMovimientoDetalle()));
            }
            valor++;
            otroCamp = "" + valor;

        }

        ejecutor.llenarCajaArtVentas(JComboArt);
        JTCantidad.setText("");
        bandera = true;
        //  limpiar();

    }//GEN-LAST:event_JBNuevoActionPerformed

    private void jComboEmpSedOrigActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboEmpSedOrigActionPerformed
        // TODO add your handling code here:
        // FACTURA.getCabecera().setIdEmpresaSede(jComboEmpSedOrig.getSelectedItem().toString());
    }//GEN-LAST:event_jComboEmpSedOrigActionPerformed

    private void jComboEmpSedOrigItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboEmpSedOrigItemStateChanged
        /*empresasedeserie data;
        if (jComboEmpSedOrig.getSelectedItem() != null) {

            try {
                // FACTURA.getCabecera().setIdEmpresaSede(jComboEmpSedOrig.getSelectedItem().toString());
                data = new empresasedeserie(MovimientoCab.sacarDato("empresasede", "empsednom", "'" + jComboEmpSedOrig.getSelectedItem().toString() + "'"));
                int datos =Integer.parseInt(data.getEmpsedsernum());
                datos++;
                jTextNumero.setText(""+datos);
                jTextSerie.setText(data.getEmpsedserser());
            } catch (SQLException ex) {
                Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }*/
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboEmpSedOrigItemStateChanged

    private void jComboComprobateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboComprobateActionPerformed

        // los nuevos registros son agregados al MODEL del JCombo HIJO

    }//GEN-LAST:event_jComboComprobateActionPerformed

    private void JTDocumentoDestinoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTDocumentoDestinoKeyTyped
        char c = evt.getKeyChar();

        if (Character.isLetter(c)) {
            getToolkit().beep();
            evt.consume();

        }
    }//GEN-LAST:event_JTDocumentoDestinoKeyTyped

    private void JTDocumentoDestinoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTDocumentoDestinoActionPerformed
        String entrada = jComboComprobate.getSelectedItem().toString();
        switch (entrada) {

        }
        // TODO add your handling code here:
    }//GEN-LAST:event_JTDocumentoDestinoActionPerformed

    private void JBSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBSalirActionPerformed

        this.dispose();

    }//GEN-LAST:event_JBSalirActionPerformed

    private void JBGrabarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBGrabarActionPerformed

        guardar();
    }//GEN-LAST:event_JBGrabarActionPerformed

    private void jtbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtbuscarKeyPressed

        try {
            String condicion = "Select * from articulo where ArtEstReg =1 AND ArtNom like '%" + jtbuscar.getText() + "%'";
            //   System.err.println(condicion);
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(condicion);
            JComboArt.removeAllItems();
            //     System.err.println("presionoooo¡¡¡");
            while (rs.next()) {
                JComboArt.addItem(rs.getString("ArtNom"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_jtbuscarKeyPressed

    private void jTextFieldTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldTotalActionPerformed

    private void jTextFieldIGVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldIGVActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldIGVActionPerformed

    private void JTDocumentoDestinoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTDocumentoDestinoKeyReleased
        // TODO add your handling code here:

        String consulta = "select * from empresa where empruc ='" + JTDocumentoDestino.getText() + "'";

        try {
            Statement sent;
            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(consulta);

            if (rs.next()) {
                JTNombre.setText(rs.getString("empnom"));
                String consulta2 = "select * from empresasede where idempresa =" + rs.getString("idempresa");
                Statement sent2;
                sent2 = cn.createStatement();
                ResultSet rs2 = sent2.executeQuery(consulta2);
                if (rs2.next()) {
                    JTDireccion.setText(rs2.getString("EmpSedDir"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }//GEN-LAST:event_JTDocumentoDestinoKeyReleased

    private void jtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtbuscarKeyReleased

        try {
            String condicion = "Select * from articulo where ArtEstReg =1 AND ArtNom like '%" + jtbuscar.getText() + "%'";
            //System.err.println(condicion);
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(condicion);
            JComboArt.removeAllItems();
            while (rs.next()) {
                JComboArt.addItem(rs.getString("ArtNom"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_jtbuscarKeyReleased

    private void jButtonSoloImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSoloImprimirActionPerformed
        imprimirfactura2();

        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonSoloImprimirActionPerformed

    private void jBGrabarEImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGrabarEImprimirActionPerformed

        boolean a = guardar();
        if (a) {

            imprimirfactura2();
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_jBGrabarEImprimirActionPerformed

    private void jBGrabarEImprimirMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBGrabarEImprimirMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_jBGrabarEImprimirMouseEntered

    private void JComboArtItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_JComboArtItemStateChanged
        if (JComboArt.getSelectedItem() != null)
            try {
                String id = FACTURA.getCabecera().sacarDato("articulo", "artnom", "'" + JComboArt.getSelectedItem().toString() + "'");
                Articulo art = new Articulo(Integer.parseInt(id));
                JTIGVDet.setText("" + art.getArtIgv());

// TODO add your handling code here:
            } catch (SQLException ex) {
                Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
    }//GEN-LAST:event_JComboArtItemStateChanged

    private void jComboComprobateItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboComprobateItemStateChanged
        // TODO add your handling code here:
        empresasedeserie data = new empresasedeserie();
        String idDocumento = "";
        if (isModificando == true || jComboComprobate == null || jComboComprobate.getSelectedItem() == null) {
            return;
        }
        if (jComboComprobate.getSelectedItem().toString().equals("FACTURA")) {
            idDocumento = "1";
        } else {
            idDocumento = "3";
        }
        try {
            // FACTURA.getCabecera().setIdEmpresaSede(jComboEmpSedOrig.getSelectedItem().toString());
            String num = FACTURA.getCabecera().sacarDato("empresasede", "empsednom", "'" + jComboEmpSedOrig.getSelectedItem().toString() + "'");
            if (num.equals("0")) {
                num = "1";
            }
            data = new empresasedeserie(num, idDocumento);
            int datos2 = Integer.parseInt(data.getEmpsedsernum());
            datos2++;
            String cero = "";
            for (int i = (int) (Math.floor(Math.log10(datos2))); i < data.getEmpsedsernum().length() - 1; i++) {
                cero += 0;

            }
            jTextNumero.setText(cero + "" + datos2);
            jTextSerie.setText(data.getEmpsedserser());

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "A ocurrido un Error Verifique si existe una serie y numero para el documento solicitado");
            Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        Object item = jComboComprobate.getSelectedItem();

        if (item != null && item.toString().equals("FACTURA")) {
            int i;
            jLabelParaNoobs.setText("El ruc tiene que tener 11 digitos ");
            jLabelTitulo.setText("COMPROBANTE DE VENTA:FACTURA");
            for (i = 0; i < jComboDocTipo.getItemCount(); i++) {
                if (jComboDocTipo.getItemAt(i).equals("RUC")) {
                    break;
                }
            }
            jComboDocTipo.setSelectedItem(jComboDocTipo.getItemAt(i));
        } else if (item != null && item.toString().equals("BOLETA")) {
            jLabelParaNoobs.setText("El DNI tiene que tener 8 digitos ");
            jLabelTitulo.setText("COMPROBANTE DE VENTA: BOLETA");

            int i;
            for (i = 0; i < jComboDocTipo.getItemCount(); i++) {
                if (jComboDocTipo.getItemAt(i).equals("DNI")) {
                    break;
                }
            }
            jComboDocTipo.setSelectedItem(jComboDocTipo.getItemAt(i));
        }

        jComboDocTipo.setEnabled(false);


    }//GEN-LAST:event_jComboComprobateItemStateChanged

    private void JTNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTNombreKeyTyped
        char c = evt.getKeyChar();
        if (Character.isDigit(c))
            evt.consume();
        else
            evt.setKeyChar(Character.toUpperCase(c));
    }//GEN-LAST:event_JTNombreKeyTyped

    private void JTDireccionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTDireccionKeyTyped

    }//GEN-LAST:event_JTDireccionKeyTyped

    private void JTCodigoCabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTCodigoCabActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JTCodigoCabActionPerformed

    private void JTDireccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTDireccionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JTDireccionActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        imprimirTiket();
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        jPolloContainer.setVisible(false);
        jPolloContainer2.setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        jPolloContainer2.setVisible(false);
      jPolloContainer.setVisible(true);    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        MODIFICARCOMPROBANTE a;

        a = new MODIFICARCOMPROBANTE(MODIFICARCOMPROBANTE.VENTA);
        a.setVisible(true);
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jTextFieldISCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldISCActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldISCActionPerformed

    private void jTextFieldTotalFinalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldTotalFinalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldTotalFinalActionPerformed

    public void estado(int p) {
        ///*estado para buscar en la grilla*/ 
        if (p == 1) {
            try {
                this.setTitle("COMPROBANTE DE VENTA");
                //limpiarFormLlenar();
                //      System.out.println("this " + this.getWidth() + " " + this.getHeight());
                //    System.out.println("form para llenar" + FormParaLlenar.getWidth() + "  " + FormParaLlenar.getHeight());
                //  System.out.println("form botones " + FormBotones.getWidth() + "--" + FormBotones.getHeight());

                FormParaLlenar.setVisible(true);
                FormBotones.setVisible(true);
                FormParaLlenar1.setVisible(false);
                jPolloContainer.setVisible(true);
                this.setSize(anchoFinal, altoFinal);
                //      cambiarTamaño(0.435F, 0.74F);
                centrarPantalla();
                refrescar();
            } catch (SQLException ex) {
                Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (p == 2)/*estado para ingresar un dato*/ {
            this.setTitle("INSERTAR PEDIDO ");
            this.setSize(ANCHO_FORM_LLENAR - 450, ALTO_FORM - (ALTO_FORM_MOSTRAR + ALTO_FORM_BOT) + 100);
            centrarPantalla();
            FormParaLlenar.setVisible(false);
            FormBotones.setVisible(false);
            FormParaLlenar1.setVisible(true);
            jPolloContainer.setVisible(false);

        }
        if (p == 3) {

        }
    }

    private double getPrecioUni(MovimientoDet detallito) throws SQLException {
        Lista a = new Lista();
        String sql_Cons = "SELECT * FROM lista where idEmpresaSede =" + FACTURA.getCabecera().getIdEmpresaSede() + " and ListEstReg=1";
        String listilla = "";
        try {
            Statement sent;

            sent = cn.createStatement();
            //  System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);

            while (rs.next()) {
                listilla = rs.getString("idlista");
                System.out.println(listilla);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Statement sent;

        sent = cn.createStatement();
        sql_Cons = "Select * From articuloprecio where  idLista= '" + listilla + "' AND idArticulo='" + detallito.getIdArticulo() + "'";
        System.out.println(sql_Cons);

        ResultSet rs = sent.executeQuery(sql_Cons);
        double resp = 0;
        if (rs.next()) {
            resp = Double.parseDouble(rs.getString("ArtPreDes"));

        }
        return resp;
    }

    private void centrarPantalla() {
        Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension ventana = this.getSize();
        this.setLocationRelativeTo(null);

    }

    public void refrescar() throws SQLException {
        ejecutor.llenarTablaFactura(JTabla, "sadasdsadadwqeqsxqweqwsdxcascqw", FACTURA.getDetalles(), FACTURA.getCabecera());

        // limpiar();
        //  JTabla.setEnabled(false);
    }

    public void limpiar() {
        JTCantidad.setText("");
        JTDocumentoDestino.setText("");
        JTNombre.setText("");

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    facturadorGUI a = new facturadorGUI();
                    a.setVisible(true);
                    a.estado(1);
                } catch (SQLException ex) {
                    Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser Fecha1;
    private javax.swing.JPanel FormBotones;
    private javax.swing.JPanel FormParaLlenar;
    private javax.swing.JPanel FormParaLlenar1;
    private javax.swing.JButton JBEliminar;
    private javax.swing.JButton JBGrabar;
    private javax.swing.JButton JBGrabar1;
    private javax.swing.JButton JBModificar;
    private javax.swing.JButton JBNuevo;
    private javax.swing.JButton JBSalir;
    private javax.swing.JButton JBSalir1;
    protected javax.swing.JComboBox JComboArt;
    protected javax.swing.JTextField JTCantidad;
    private javax.swing.JTextField JTCodigoCab;
    private javax.swing.JTextField JTDireccion;
    private javax.swing.JTextField JTDocumentoDestino;
    private javax.swing.JTextField JTIGVDet;
    private javax.swing.JTextField JTNombre;
    private javax.swing.JTable JTabla;
    private javax.swing.JTextField JTextFieldsumatoPre;
    private static com.toedter.calendar.JDateChooser fecha1;
    private javax.swing.JButton jBGrabarEImprimir;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButtonSoloImprimir;
    private javax.swing.JComboBox jComboComprobate;
    private javax.swing.JComboBox jComboDocTipo;
    private javax.swing.JComboBox jComboEmpSedOrig;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelCambiar;
    private javax.swing.JLabel jLabelParaNoobs;
    private javax.swing.JLabel jLabelTitulo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPolloContainer;
    private javax.swing.JPanel jPolloContainer2;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTextFieldIGV;
    private javax.swing.JTextField jTextFieldISC;
    private javax.swing.JTextField jTextFieldTotal;
    private javax.swing.JTextField jTextFieldTotalFinal;
    private javax.swing.JTextField jTextNumero;
    private javax.swing.JTextField jTextSerie;
    private javax.swing.JTextField jtbuscar;
    private Facturador.panelPedido panelPedido1;
    private Facturador.panelPedido panelPedido10;
    private Facturador.panelPedido panelPedido11;
    private Facturador.panelPedido panelPedido12;
    private Facturador.panelPedido panelPedido13;
    private Facturador.panelPedido panelPedido14;
    private Facturador.panelPedido panelPedido15;
    private Facturador.panelPedido panelPedido16;
    private Facturador.panelPedido panelPedido17;
    private Facturador.panelPedido panelPedido18;
    private Facturador.panelPedido panelPedido19;
    private Facturador.panelPedido panelPedido2;
    private Facturador.panelPedido panelPedido20;
    private Facturador.panelPedido panelPedido21;
    private Facturador.panelPedido panelPedido22;
    private Facturador.panelPedido panelPedido23;
    private Facturador.panelPedido panelPedido24;
    private Facturador.panelPedido panelPedido25;
    private Facturador.panelPedido panelPedido3;
    private Facturador.panelPedido panelPedido4;
    private Facturador.panelPedido panelPedido5;
    private Facturador.panelPedido panelPedido51;
    private Facturador.panelPedido panelPedido52;
    private Facturador.panelPedido panelPedido53;
    private Facturador.panelPedido panelPedido54;
    private Facturador.panelPedido panelPedido55;
    private Facturador.panelPedido panelPedido56;
    private Facturador.panelPedido panelPedido57;
    private Facturador.panelPedido panelPedido58;
    private Facturador.panelPedido panelPedido59;
    private Facturador.panelPedido panelPedido6;
    private Facturador.panelPedido panelPedido60;
    private Facturador.panelPedido panelPedido61;
    private Facturador.panelPedido panelPedido62;
    private Facturador.panelPedido panelPedido63;
    private Facturador.panelPedido panelPedido64;
    private Facturador.panelPedido panelPedido65;
    private Facturador.panelPedido panelPedido66;
    private Facturador.panelPedido panelPedido67;
    private Facturador.panelPedido panelPedido68;
    private Facturador.panelPedido panelPedido69;
    private Facturador.panelPedido panelPedido7;
    private Facturador.panelPedido panelPedido70;
    private Facturador.panelPedido panelPedido71;
    private Facturador.panelPedido panelPedido72;
    private Facturador.panelPedido panelPedido73;
    private Facturador.panelPedido panelPedido74;
    private Facturador.panelPedido panelPedido75;
    private Facturador.panelPedido panelPedido8;
    private Facturador.panelPedido panelPedido9;
    // End of variables declaration//GEN-END:variables

    private String RevisarDocumento() throws SQLException {
        ArrayList<String> desu = new ArrayList<String>();
        Statement sent2 = cn.createStatement();
        String consulta = "select * from empresa where ";
        if (consulta.length() != 0) {
            consulta += "  empRUC= " + JTDocumentoDestino.getText();
        }
        //  System.out.println("el chato es 2k confirmed  y si no la saco yo tambien " + consulta);

        ResultSet rs2 = sent2.executeQuery(consulta);
        String retorno = "";
        if (rs2.next()) {
            Statement sent3 = cn.createStatement();
            String idempresa = rs2.getString("idempresa");
            Empresa empresaModi = new Empresa(idempresa);
            String consulta1 = "select * from empresaSede where idEmpresa = " + idempresa;
            ResultSet rs3 = sent3.executeQuery(consulta1);
            if (rs3.next()) {
                retorno = rs3.getString("idempresasede");
                EmpresaSede empresaSedeModi = new EmpresaSede(rs3.getString("idempresasede"));
                empresaSedeModi.setEmpSedDir("'" + JTDireccion.getText() + "'");
                empresaSedeModi.setEmpSedNom("'" + empresaSedeModi.getEmpSedNom() + "'");
                empresaSedeModi.modificar();
            } else {
                EmpresaSede ahora = new EmpresaSede();
                ahora.setEmpSedDin("'0'");
                ahora.setEmpSedDir("'" + JTDireccion.getText() + "'");
                ahora.setEmpSedEstReg(1);
                ahora.setEmpSedNom("'" + JTNombre.getText() + "'");
                ahora.setIdEmpresa(idempresa);
                GenerarCodigo cd = new GenerarCodigo();
                String sede = cd.generarID("empresaSEde", "idempresaSEde");
                ahora.setIdEmpresaSede(sede);
                ahora.insertar();
                retorno = ahora.getIdEmpresaSede();
            }

        } else {
            Empresa empre = new Empresa();
            empre.setEmpEstReg(1);
            empre.setEmpNom("'" + JTNombre.getText() + "'");

            if (jComboComprobate.getSelectedItem().toString().equals("factura")) {
                empre.setEmpPer("0");

            }
            if (jComboComprobate.getSelectedItem().toString().equals("boleta")) {
                empre.setEmpPer("1");

            }

            empre.setEmpRUC(JTDocumentoDestino.getText());
            empre.setEmpTel(" '[]' ");
            String doctipe = FACTURA.getCabecera().sacarDato("documento", "docDes", "'" + jComboDocTipo.getSelectedItem().toString() + "'");
            empre.setIdDocumento(doctipe);
            //empre.setEmpTipDoc(jComboDocTipo.getSelectedItem().toString());
            empre.setIdEmpGrup("3");
            GenerarCodigo cd = new GenerarCodigo();

            String sede = cd.generarID("empresa", "idempresa");
            empre.setIdEmpresa(sede);
            empre.insertarId();
            EmpresaSede ahora = new EmpresaSede();
            ahora.setEmpSedDin("0");
            ahora.setEmpSedDir("'" + JTDireccion.getText() + "'");
            ahora.setEmpSedEstReg(1);
            ahora.setEmpSedNom("'" + JTNombre.getText() + "'");
            ahora.setIdEmpresa(sede);
            GenerarCodigo cd2 = new GenerarCodigo();
            String sede1 = cd2.generarID("empresaSEde", "idempresaSEde");
            ahora.setIdEmpresaSede(sede1);
            ahora.insertar();
            retorno = ahora.getIdEmpresaSede();
        }
        //actualizando datos  
        String consulta4 = "select * from empresa where empruc =" + JTDocumentoDestino.getText();

        return retorno;
    }

    public void paraPruebas() {
        /*
         JTNombre.setText("Elvis Tellez Mendoza ");
         JTDocumentoDestino.setText("72943030");
         jTextSerie.setText("00001");
         jTextNumero.setText("00001");
         MovimientoDet de = new MovimientoDet("NULL",JTCodigoCab.getText(), "1", "1", "100", "2000", "900", "0.18", "1");
         FACTURA.getDetalles().add(de);
         */
    }

    private boolean camposBienLlenados() {
        boolean val = true;
        String err = "";
        boolean a = jTextNumero.getText().length() == 8;
        a = true;
        if (a) {
            val = val && true;
        } else {
            val = val && false;
            err += "LONGITUD NO VALIDA PARA EL NUMERO \n ";
        }
        if (jComboDocTipo.getSelectedItem().equals("DNI") && false) {
            val = val && false;
            err += "EL DNI NO CUMPLE CON LOS DIGITOS NESESARIOS \n ";
        }
        if (Fecha1.getDate() == null) {
            val = val && false;
            err += "FECHA NO VALIDA \n";
        }
        if (FACTURA.getDetalles().isEmpty()) {
            val = val && false;
            err += "INGRESE ALGUN DETELLE \n";

        }
        if (JTNombre.getText().length() <= 2) {
            val = val && false;
            err += "NOMBRE NO VALIDO \n";
        }

        if (jComboDocTipo.getSelectedItem().equals("RUC") && JTDocumentoDestino.getText().length() != 11) {
            val = val && false;
            err += "EL RUC NO CUMPLE CON LOS CARACTERES DEFINIDOS \n";
        }
        if (jComboDocTipo.getSelectedItem().equals("RUC") && JTDireccion.getText().length() <= 1) {
            val = val && false;
            err += "LA DIRECCION NO ES VALIDA \n";
        }

        if (!val) {
            //   System.out.println(err);
            JOptionPane.showMessageDialog(null, err);

        }
        return val;
    }

    public static boolean entero(String str) {
        boolean a = true;
        for (int i = 0; i < str.length(); i++) {
            if (!(str.charAt(i) >= 0 && str.charAt(i) <= 9)) {
                a = false;
            }
        }
        return a;
    }

    private void llenarDatos() throws ParseException {

        try {
            JTCodigoCab.setText(FACTURA.getCabecera().getIdMovimientoCabecera());
            jTextNumero.setText(FACTURA.getCabecera().getMovCabNum());
            jTextSerie.setText(FACTURA.getCabecera().getMovCabSer());
            String destino = MovimientoCab.sacarDato("empresasede", "idempresasede", FACTURA.getCabecera().getIdEmpresaSedeDes(), "idempresa");

            String nombre = FACTURA.getCabecera().sacarDato("empresa", "idempresa", destino, "empNom");
            JTNombre.setText(nombre);
            destino = MovimientoCab.sacarDato("empresa", "idempresa", destino, "empruc");
            JTDocumentoDestino.setText(destino);
            String comprobante = FACTURA.getCabecera().sacarDato("movimientodocumento", "idmovimientodocumento", FACTURA.getCabecera().getIdMovimientoDocumento(), "movdocdes");
            String doc = FACTURA.getCabecera().sacarDato("empresasede", "idempresasede", FACTURA.getCabecera().getIdEmpresaSedeDes(), "idempresa");
            doc = FACTURA.getCabecera().sacarDato("empresa", "idempresa", doc, "iddocumento");
            doc = FACTURA.getCabecera().sacarDato("documento", "iddocumento", doc, "docdes");

            String empSedeString = FACTURA.getCabecera().sacarDato("empresasede", "idempresasede", FACTURA.getCabecera().getIdEmpresaSede(), "empsednom");

            //   System.err.println("el doc es "+doc +" "+comprobante);
            jComboComprobate.setSelectedItem(comprobante);
            jComboDocTipo.setSelectedItem(doc);
            jComboEmpSedOrig.setSelectedItem(empSedeString);
            String desde = FACTURA.getCabecera().getMovCabFec();
            // System.out.println(desde);
            refrescar();

            EmpresaSede de = new EmpresaSede(FACTURA.getCabecera().getIdEmpresaSedeDes());
            String direccion = de.getEmpSedDir();
            JTDireccion.setText(direccion);
            llenarImpuestos();
            java.util.Date date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(desde);

            Fecha1.setDate(date1);

            /* dia = fecha.substring(0, fecha.indexOf("/"));
                String mes = fecha.substring(fecha.indexOf("/") + 1, fecha.lastIndexOf("/"));
                String año = fecha.substring(fecha.lastIndexOf("/") + 1);
             */
        } catch (SQLException ex) {
            Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    // dagros 

    void imprimirfactura() {
        PrinterMatrix printer = new PrinterMatrix();

        Extenso e = new Extenso();

        e.setNumber(FACTURA.getDetalles().size() + 10);
        // nombre 
        // ruc origen 
        //nombre destino
        // fecha
        // serie  y numero 
        String nombre = jComboEmpSedOrig.getSelectedItem().toString();
        EmpresaSede empsed = new EmpresaSede(FACTURA.getCabecera().getIdEmpresaSede());
        Empresa emp = new Empresa(empsed.getIdEmpresa());
        String titulo = "";
        if (!jComboComprobate.getSelectedItem().toString().equalsIgnoreCase("FACTURA")) {
            titulo = "\t" + jComboComprobate.getSelectedItem().toString() + " ELECTRONICA" + "N°:B" + jTextSerie.getText() + "-" + jTextNumero.getText();;
        } else {

            titulo = "\t" + jComboComprobate.getSelectedItem().toString() + " ELECTRONICA" + "N°:F" + jTextSerie.getText() + "-" + jTextNumero.getText();;
        }
        String ruc = "RUC: " + emp.getEmpRUC();
        String destino = "CLIENTE: " + JTNombre.getText();
        String fecha = "FECHA: " + ((JTextField) (Fecha1.getDateEditor().getUiComponent())).getText();
        java.util.Date datey = new java.util.Date();
//Caso 1: obtener la hora y salida por pantalla con formato:

        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
        String dateSalida = "HORA : " + hourdateFormat.format(datey);

        //String  serie_numero ="NRO de DOC: "+jTextSerie.getText()+"-"+jTextNumero.getText();
        String docu = "DNI /RUC:" + JTDocumentoDestino.getText();
        String direccion = "" + JTDireccion.getText();
        String direccionNuestra = "" + empsed.getEmpSedDir();
        int constante = 42;
        int lineasAimprimir = 27 + FACTURA.getDetalles().size();
        String cabecera = "|         PRODUCTO      |CANTIDAD|IMPORTE|";
        String linea = "";
        for (int i = 0; i < 41; i++) {
            linea += "-";

        }
        String[] arr = {titulo, nombre, ruc, direccionNuestra, destino, docu, direccion, fecha, dateSalida, " ", cabecera, linea};
        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(lineasAimprimir, constante);
        int i = 2;
        for (; i < arr.length + 2; i++) {
            String arr1 = arr[i - 2];
            printer.printTextWrap(i, i, 1, constante, arr1);

        }
        i++;
        int a = 0;
        for (a = i; a < FACTURA.getDetalles().size() + i; a++) {
            String descripcion = JTabla.getValueAt(a - i, 2).toString();

            String cantidad = JTabla.getValueAt(a - i, 1).toString();
            String importe = JTabla.getValueAt(a - i, 4).toString();
            String splitS = cabecera.substring(1, cabecera.length() - 1);
            String split[] = {"       PRODUCTO      ", "CANTIDAD", "IMPORTE"};
            while (descripcion.length() < split[0].length()) {
                if (descripcion.length() > split[0].length()) {
                    break;
                }
                descripcion = " " + descripcion;

                if (descripcion.length() > split[0].length()) {
                    break;
                }
                descripcion = descripcion + " ";

            }
            while (cantidad.length() < split[1].length()) {
                if (cantidad.length() > split[1].length()) {
                    break;
                }
                cantidad = " " + cantidad;

                if (cantidad.length() > split[1].length()) {
                    break;
                }
                cantidad = cantidad + " ";

            }
            while (importe.length() < split[2].length()) {
                if (importe.length() > split[2].length()) {
                    break;
                }
                importe = " " + importe;

                if (importe.length() > split[2].length()) {
                    break;
                }
                importe = importe + " ";

            }
            String datos = "|" + descripcion + "|" + cantidad + "|" + importe + "|";
            //  System.err.println(datos );
            printer.printTextWrap(a, a, 1, constante, datos);

        }
        printer.printTextWrap(a, a, 1, constante, linea);
        a++;
        printer.printTextWrap(a, a, 1, constante, "");
        a++;
        if (jComboComprobate.getSelectedItem().toString().equalsIgnoreCase("FACTURA")) {
            printer.printTextWrap(a, a, 1, constante, "MONTO:" + JTextFieldsumatoPre.getText() + "\tTIGV:" + jTextFieldIGV.getText() + "   TOTAL:" + jTextFieldTotal.getText());
            a++;
        } else {
            printer.printTextWrap(a, a, 1, constante, "\t\t\t TOTAL:" + jTextFieldTotal.getText());
            a++;

        }
        printer.printTextWrap(a, a, 1, constante, "");
        a++;
        printer.printTextWrap(a, a, 1, constante, "Representacion impresa de la factura  ");
        a++;
        printer.printTextWrap(a, a, 1, constante, "electrónica generada desde el sistema");
        a++;
        printer.printTextWrap(a, a, 1, constante, "facturador SUNAT. Puede verificarla");
        a++;
        printer.printTextWrap(a, a, 1, constante, "utilizando su clave SOL ");
        a++;
        //Imprimir * de la 2da linea a 25 en la columna 1;
        // printer.printCharAtLin(2, 25, 1, "*");
        //Imprimir * 1ra linea de la columa de 1 a 80
        // printer.printCharAtCol(1, 1, constante, "=");
        //Imprimir Encabezado nombre del La EMpresa
        // printer.printTextWrap(1, 2, 1, constante, "FACTURA DE VENTA");
        //printer.printTextWrap(linI, linE, colI, colE, null);t
        //printer.printTextWrap(2, 3, 1, 22, "Num. Boleta : " );
        //    printer.printTextWrap(3, 3, 1, 22, "Num. Boleta : " );
        // printer.printTextWrap(4, 4, 1, 22, "Num. Boleta : " );
        //  printer.printTextWrap(4, 4, 1, 22, "Num. Boleta : " );
        // printer.printTextWrap(4, 4, 1, 22, "Num. Boleta : " );

        // la primer linea es la fila donde se imprime 
        /*
        for (int i = 0; i &lt; filas; i++) { printer.printTextWrap(9 + i, 10, 1, 80, tblVentas.getValueAt(i,0).toString()+"|"+tblVentas.getValueAt(i,1).toString()+"| "+tblVentas.getValueAt(i,2).toString()+"| "+tblVentas.getValueAt(i,3).toString()+"|"+ tblVentas.getValueAt(i,4).toString()); } if(filas &gt; 15){
        printer.printCharAtCol(filas + 1, 1, 80, "=");
        printer.printTextWrap(filas + 1, filas + 2, 1, 80, "TOTAL A PAGAR " + txtVentaTotal.getText());
        printer.printCharAtCol(filas + 2, 1, 80, "=");
        printer.printTextWrap(filas + 2, filas + 3, 1, 80, "Esta boleta no tiene valor fiscal, solo para uso interno.: + Descripciones........");
        }else{
        printer.printCharAtCol(25, 1, 80, "=");
        printer.printTextWrap(26, 26, 1, 80, "TOTAL A PAGAR " + txtVentaTotal.getText());
        printer.printCharAtCol(27, 1, 80, "=");
        printer.printTextWrap(27, 28, 1, 80, "Esta boleta no tiene valor fiscal, solo para uso interno.: + Descripciones........");
 
        }*/
        printer.toFile("impresion.txt");

        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream("impresion.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();

        if (defaultPrintService != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);
                byte[] bytes = {27, 105, 3};
                DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
                DocPrintJob job = PrintServiceLookup.lookupDefaultPrintService().createPrintJob();
                Doc doc = new SimpleDoc(bytes, flavor, null);
                //  Thread.sleep(3000);
                job.print(doc, null);
                System.err.println("paso esta linea");

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            //  System.err.println("No existen impresoras instaladas");
        }

        //inputStream.close();
    }

    void imprimirfactura2() {
        imprimirFacturaEEUU();

    }

    void imprimirBoletaMatricial() {
        PrinterMatrix printer = new PrinterMatrix();

        // nombre 
        // ruc origen 
        //nombre destino
        // fecha
        // serie  y numero 
        String nombre = jComboEmpSedOrig.getSelectedItem().toString();
        EmpresaSede empsed = new EmpresaSede(FACTURA.getCabecera().getIdEmpresaSede());
        Empresa emp = new Empresa(empsed.getIdEmpresa());
        String titulo = "";
        if (!jComboComprobate.getSelectedItem().toString().equalsIgnoreCase("FACTURA")) {
            titulo = "\t" + jComboComprobate.getSelectedItem().toString() + " ELECTRONICA" + "N°:B" + jTextSerie.getText() + "-" + jTextNumero.getText();;
        } else {

            titulo = "\t" + jComboComprobate.getSelectedItem().toString() + " ELECTRONICA" + "N°:F" + jTextSerie.getText() + "-" + jTextNumero.getText();;
        }
        String ruc = "RUC: " + emp.getEmpRUC();
        String destino = "               " + JTNombre.getText();
        String fecha = "                                                  " + ((JTextField) (Fecha1.getDateEditor().getUiComponent())).getText();
        //String  serie_numero ="NRO de DOC: "+jTextSerie.getText()+"-"+jTextNumero.getText();
        String docu = "DNI /RUC:" + JTDocumentoDestino.getText();
        String direccion = "" + JTDireccion.getText();
        fecha = fecha.replaceAll("-", "     ");
        String direccionNuestra = "" + empsed.getEmpSedDir();
        int constante = 81;
        int lineasAimprimir = 24;
        String cabecera = "|         PRODUCTO      |CANTIDAD|IMPORTE|";
        String linea = "";
        for (int i = 0; i < 80; i++) {
            linea += "-";

        }
        String[] arr = {" ", " ", " ", fecha, "", destino, " "};
        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(lineasAimprimir, constante);
        int i = 0;
        for (; i < arr.length; i++) {
            String arr1 = arr[i];
            printer.printTextWrap(i, i, 1, constante, arr1);

        }
        i++;
        int a = 0;
        for (a = i; a < FACTURA.getDetalles().size() + i; a++) {
            String descripcion = JTabla.getValueAt(a - i, 2).toString();

            String cantidad = JTabla.getValueAt(a - i, 1).toString();
            String costoUnitario = JTabla.getValueAt(a - i, 3).toString();
            String importe = JTabla.getValueAt(a - i, 4).toString();
            String espacioCantidad = getEspacios(cantidad, 8);
            String espacioDescripcion = getEspacios(descripcion, 37);
            String espacioCostoUnitario = getEspacios(costoUnitario, 11);
            String datos = "   " + cantidad + espacioCantidad + descripcion + espacioDescripcion
                    + costoUnitario + espacioCostoUnitario + importe;
            //  System.err.println(datos );
            printer.printTextWrap(a, a, 1, constante, datos);

        }
        for (int j = 0; j < 11 - FACTURA.getDetalles().size(); j++) {
            printer.printTextWrap(a, a, 1, constante, "");
            a++;

        }

        if (jComboComprobate.getSelectedItem().toString().equalsIgnoreCase("FACTURA")) {
            printer.printTextWrap(a, a, 1, constante, "MONTO:" + JTextFieldsumatoPre.getText() + "\tTIGV:" + jTextFieldIGV.getText() + "   TOTAL:" + jTextFieldTotal.getText());
            a++;
        } else {
            printer.printTextWrap(a, a, 1, constante, "\t\t\t\t\t\t\t\t" + jTextFieldTotal.getText());
            a++;

        }

        printer.toFile("impresion2.txt");

        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream("impresion2.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintUtility.findPrintService("E3");

        if (defaultPrintService != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);
                System.err.println("paso esta linea");

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            //  System.err.println("No existen impresoras instaladas");
        }

        //inputStream.close();
    }

    void imprimirFacturaMatricial() {
        PrinterMatrix printer = new PrinterMatrix();

        // nombre 
        // ruc origen 
        //nombre destino
        // fecha
        // serie  y numero 
        String nombre = jComboEmpSedOrig.getSelectedItem().toString();
        EmpresaSede empsed = new EmpresaSede(FACTURA.getCabecera().getIdEmpresaSede());
        Empresa emp = new Empresa(empsed.getIdEmpresa());
        String titulo = "";
        String blancosIzq = "               ";
        String ruc = "RUC: " + emp.getEmpRUC();
        String destino = blancosIzq + JTNombre.getText();
        String fecha = blancosIzq + ((JTextField) (Fecha1.getDateEditor().getUiComponent())).getText();
        //String  serie_numero ="NRO de DOC: "+jTextSerie.getText()+"-"+jTextNumero.getText();
        String docu = blancosIzq + JTDocumentoDestino.getText();
        String direccion = blancosIzq + JTDireccion.getText();
        fecha = fecha.replaceAll("-", "     ");
        String direccionNuestra = "" + empsed.getEmpSedDir();
        int constante = 137;
        int lineasAimprimir = 40;
        String cabecera = "|         PRODUCTO      |CANTIDAD|IMPORTE|";
        String linea = "";
        for (int i = 0; i < 160; i++) {
            linea += "-";

        }
        String[] arr = {"", "", "", "", "", "", destino, direccion, docu, fecha, " ", " "};
        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(lineasAimprimir, constante);
        int i = 0;
        for (; i < arr.length; i++) {
            String arr1 = arr[i];
            printer.printTextWrap(i, i, 1, constante, arr1);

        }
        i++;
        int a = 0;
        for (a = i; a < FACTURA.getDetalles().size() + i; a++) {
            String descripcion = JTabla.getValueAt(a - i, 2).toString();

            String cantidad = JTabla.getValueAt(a - i, 1).toString();
            String costoUnitario = JTabla.getValueAt(a - i, 3).toString();
            String importe = JTabla.getValueAt(a - i, 4).toString();
            String espacioCantidad = getEspacios(cantidad, 8);
            String espacioDescripcion = getEspacios(descripcion, 110);
            String espacioCostoUnitario = getEspacios(costoUnitario, 11);
            String datos = "   " + cantidad + espacioCantidad + descripcion + espacioDescripcion
                    + costoUnitario + espacioCostoUnitario + importe;
            //  System.err.println(datos );
            printer.printTextWrap(a, a, 1, constante, datos);

        }
        for (int j = 0; j < 20 - FACTURA.getDetalles().size(); j++) {
            printer.printTextWrap(a, a, 1, constante, "");
            a++;

        }

        printer.printTextWrap(a, a, 1, constante, "\t\t\t\t\t\t\t\t\t\t\t\t\t" + JTextFieldsumatoPre.getText() + "\t\t" + jTextFieldIGV.getText() + "\t\t" + jTextFieldTotal.getText());
        a++;

        printer.toFile("impresion.txt");

        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream("impresion.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();

        if (defaultPrintService != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);
                System.err.println("paso esta linea");

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            //  System.err.println("No existen impresoras instaladas");
        }

        //inputStream.close();
    }

    public boolean guardar() {
        if (camposBienLlenados()) {

            try {
                double dinero = 0;
                //ESTE VALE

                FACTURA.ajustar();

                for (MovimientoDet det : FACTURA.getDetalles()) {

                    dinero += Double.parseDouble(det.getMovDetPreTot());
                }

                EmpresaSede emp = new EmpresaSede(FACTURA.getCabecera().getIdEmpresaSede());

                dinero += Double.parseDouble(emp.getEmpSedDin());
                emp.setEmpSedDin("'" + dinero + "'");
                emp.setEmpSedDir("'" + emp.getEmpSedDir() + "'");
                emp.setEmpSedNom("'" + emp.getEmpSedNom() + "'");
                emp.modificar();

                String identificador = RevisarDocumento();
                // System.out.println(identificador);
                MovimientoCab movi = new MovimientoCab();
                movi.setIdEmpresaSede(cabe.sacarDato("empresaSEde ", "empsednom", "'" + jComboEmpSedOrig.getSelectedItem().toString() + "'"));
                //String revisar=RevisarDocumento();
                //movi.setIdEmpresaSedeDes(revisar);
                String idmovdoc = cabe.sacarDato("movimientodocumento", "movdocDes", "'" + jComboComprobate.getSelectedItem().toString() + "'");
                movi.setIdMovimientoDocumento(idmovdoc);
                String a = Principal.jLUsuarioGeneral != null ? Principal.jLUsuarioGeneral.getText() : "1";
                movi.setIdUsuario("'" + a + "'");
                movi.setIdMovimientoTipo("2");

//                movi.setMovCabEnv("0");
                movi.setMovCabEstReg("1");

                /* dia = fecha.substring(0, fecha.indexOf("/"));
                String mes = fecha.substring(fecha.indexOf("/") + 1, fecha.lastIndexOf("/"));
                String año = fecha.substring(fecha.lastIndexOf("/") + 1);
                 */
                //  String fechaF = año + "-" + mes + "-" + dia;
                //    System.err.println(fechaF);
                java.util.Date d = Fecha1.getDate();
                SimpleDateFormat da = new SimpleDateFormat("yyyy/MM/dd");
                java.util.Date datey = new java.util.Date();
//Caso 1: obtener la hora y salida por pantalla con formato:

                DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
                String dateSalida = hourdateFormat.format(datey);

                movi.setMovCabFec("'" + da.format(d) + " " + dateSalida + "'");

                movi.setMovCabMovSto("1");
                movi.setMovCabNum("'" + jTextNumero.getText() + "'");
                movi.setMovCabSer("'" + jTextSerie.getText() + "'");
                movi.setIdEmpresaSedeDes(identificador);
                //FACTURA.getCabecera().setIdEmpresaSedeDes(identificador);
                FACTURA.setCabecera(movi);

                if (!isModificando) {

                    try {
                        empresasedeserie data;
                        String idDocumento = "";
                        if (jComboComprobate.getSelectedItem().toString().equals("FACTURA")) {
                            idDocumento = "1";
                        } else {
                            idDocumento = "3";
                        }

                        // FACTURA.getCabecera().setIdEmpresaSede(jComboEmpSedOrig.getSelectedItem().toString());
                        data = new empresasedeserie(FACTURA.getCabecera().sacarDato("empresasede", "empsednom", "'" + jComboEmpSedOrig.getSelectedItem().toString() + "'"), idDocumento);
                        int datos2 = Integer.parseInt(data.getEmpsedsernum());
                        datos2++;
                        String cero = "";
                        for (int i = (int) (Math.floor(Math.log10(datos2))); i < data.getEmpsedsernum().length() - 1; i++) {
                            cero += 0;

                        }
                        jTextNumero.setText(cero + "" + datos2);
                        jTextSerie.setText(data.getEmpsedserser());
                    } catch (SQLException ex) {
                        Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    movi.setMovCabNum("'" + jTextNumero.getText() + "'");
                    movi.setMovCabSer("'" + jTextSerie.getText() + "'");
                    FACTURA.getCabecera().generarCodigo(JTCodigoCab);
                    movi.setIdMovimientoCabecera("'" + JTCodigoCab.getText() + "'");
                    movi.setMovCabProBoo(0);
                    FACTURA.setCabecera(movi);

                    FACTURA.getCabecera().insertar();
                    int indiceDetalles = 0;
                    for (MovimientoDet det : FACTURA.getDetalles()) {
                        try {
                            if (Integer.parseInt(det.getIdArticulo()) != 2 && (Boolean) (JTabla.getModel().getValueAt(indiceDetalles, 5))) {
                                System.out.println("Facturador.facturadorGUI.guardar() con alteracion");
                                int idAnterior = Integer.parseInt(det.getIdArticulo());
                                int idNuevo = (idAnterior + 10000) % 20000;
                                Articulo sd = new Articulo(idNuevo);
                                if (sd.getArtNom().length() >= 2) {
                                    det.setIdArticulo("" + idNuevo);
                                }
                            }
                        } catch (Exception e) {
                            System.out.println("Facturador.facturadorGUI.guardar()" + e.getMessage());

                        }

                        det.setIdMovimientoCabecera(JTCodigoCab.getText());
                        det.insertar();
                    }
                    String idDocumento = "";
                    if (jComboComprobate.getSelectedItem().toString().equals("FACTURA")) {
                        idDocumento = "1";
                    } else {
                        idDocumento = "3";
                    }

                    empresasedeserie serie = new empresasedeserie(FACTURA.getCabecera().getIdEmpresaSede(), idDocumento);
                    int numero = Integer.parseInt(serie.getEmpsedsernum()) + 1;
                    String ceros = "";
                    for (int i = (int) (Math.floor(Math.log10(numero))); i < serie.getEmpsedsernum().length() - 1; i++) {
                        ceros += "0";
                    }
                    serie.setEmpsedsernum(ceros + "" + numero);
                    serie.modificar();
                } else {
                    FACTURA.getCabecera().setMovCabSer(FACTURA.getCabecera().getMovCabSer().substring(1, FACTURA.getCabecera().getMovCabSer().length() - 1));
                    FACTURA.getCabecera().setMovCabNum(FACTURA.getCabecera().getMovCabNum().substring(1, FACTURA.getCabecera().getMovCabNum().length() - 1));
                    factura fac = new factura();

                    fac.setCabecera(new MovimientoCab());
                    fac.getCabecera().setIdMovimientoCabecera(JTCodigoCab.getText());
                    fac.obtenerDetalles();
                    FACTURA.getCabecera().modificar();
                    GenerarCodigo gc = new GenerarCodigo();
                    String cod = gc.generarID("MovimientoDet", "IdMovimientoDetalle");
                    int numCod = Integer.parseInt(cod);
                    int indiceDetalles =0 ;
                    for (MovimientoDet det : FACTURA.getDetalles()) {
                        try {
                            if (Integer.parseInt(det.getIdArticulo()) != 2 && (Boolean) (JTabla.getModel().getValueAt(indiceDetalles, 5))) {
                                System.out.println("Facturador.facturadorGUI.guardar() con alteracion");
                                int idAnterior = Integer.parseInt(det.getIdArticulo());
                                int idNuevo = (idAnterior + 10000) % 20000;
                                Articulo sd = new Articulo(idNuevo);
                                if (sd.getArtNom().length() >= 2) {
                                    det.setIdArticulo("" + idNuevo);
                                }
                            }
                        } catch (Exception e) {
                            System.out.println("Facturador.facturadorGUI.guardar()" + e.getMessage());

                        }

   
                        det.setIdMovimientoDetalle("" + numCod);
                        det.insertar();
                        indiceDetalles++;
                        numCod++;
                    }
                    for (MovimientoDet det : fac.getDetalles()) {
                        det.eliminar();
                    }
                }
                JOptionPane.showMessageDialog(null, "GRABADO HECHO EXITOSAMENTE");

                try {
                    facturadorGUI adeqw = new facturadorGUI();
                    adeqw.setVisible(true);
                    adeqw.estado(1);
                    System.gc();
                    this.dispose();

                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "NO GRABADO ");

                    Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (SQLException ex) {
                Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            return true;
        }
        return false;
    }

    protected void grabarDetalle() {

        DecimalFormat df = new DecimalFormat("#.00");

        //Insertar segun bandera
        Pattern expresionRegular = Pattern.compile("[0-9]*");
        Matcher ma = expresionRegular.matcher(JTCantidad.getText());

        if (!ma.matches() || JTIGVDet.getText().length() == 0 || entero(JTCantidad.getText())) {
            JOptionPane.showMessageDialog(null, "Por favor ingrese todos los campos");
        } else {
            MovimientoDet actual;
            if (bandera) {
                FACTURA.getDetalles().add(new MovimientoDet());
                actual = FACTURA.getDetalles().get(FACTURA.getDetalles().size() - 1);

            } else {
                //modificar

                actual = FACTURA.getDetalles().get(indiceFila);

            }
            String nombre = JComboArt.getSelectedItem().toString();

            String dato = " ";
            try {
                dato = actual.sacarDato("articulo", "artnom", nombre);
            } catch (SQLException ex) {
                Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            actual.setIdArticulo(dato);
            actual.setIdMovimientoCabecera(JTCodigoCab.getText());
            actual.setIdMovimientoDetalle(otroCamp);
            actual.setMovDetCan(JTCantidad.getText());
            actual.setMovDetCosTot("0");
            double de = Double.parseDouble(JTIGVDet.getText()) * Double.parseDouble(actual.getMovDetCan());
            String d = df.format(de);
            d = d.replace(",", ".");
            actual.setMovDetIgv(d);
            Articulo art = new Articulo(Integer.parseInt(actual.getIdArticulo()));
            try {
                de = getPrecioUni(actual);
            } catch (SQLException ex) {
                Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            de *= Double.parseDouble(actual.getMovDetCan());
            actual.setMovDetPreTot("" + de);
            actual.setMovIngreso("" + 1);
            actual.setMovDetEstReg("" + 1);
            if (bandera) {
                for (int w = 0; w < FACTURA.getDetalles().size() - 1; w++) {
                    MovimientoDet det = FACTURA.getDetalles().get(w);
                    boolean articuloyadigitado = det.getIdArticulo().equals(actual.getIdArticulo());
                    if (articuloyadigitado) {
                        int cantidad = Integer.parseInt(actual.getMovDetCan());
                        cantidad += Integer.parseInt(det.getMovDetCan());
                        double precioNuevo = Double.parseDouble(actual.getMovDetPreTot());
                        precioNuevo += Double.parseDouble(det.getMovDetPreTot());
                        det.setMovDetCan("" + cantidad);
                        det.setMovDetPreTot("" + precioNuevo);
                        FACTURA.getDetalles().remove(actual);

                    }
                }
            }
            llenarImpuestos();

            estado(1);

        }
    }

    private void llenarImpuestos() {
        DecimalFormat df = new DecimalFormat("#.00");

        double sumaTotalSinISC = 0;
        double sumaTotal = 0;

        double sumaSinIGV = 0;
        double sumaIGV = 0;
        double sumaISC = 0;
        for (MovimientoDet det : FACTURA.getDetalles()) {
            double precioDetalle = Double.parseDouble(det.getMovDetPreTot());
            sumaTotalSinISC += precioDetalle;
            sumaSinIGV += precioDetalle / (1.18);
            sumaIGV += (precioDetalle / 1.18) * 0.18;

            if (Integer.parseInt(det.getIdArticulo()) == 2) {
                Articulo artBolsa = new Articulo(2);
                double precioISC = Double.parseDouble(det.getMovDetPreTot()) * (artBolsa.getArtIgv());

                sumaISC += precioISC;

            }

        }
        sumaTotal = sumaTotalSinISC + sumaISC;
        String sumaSinIGVString = df.format(sumaSinIGV);
        String sumaIGVString = df.format(sumaIGV);
        String sumaISCString = df.format(sumaISC);

        jTextFieldIGV.setText(sumaIGVString);
        JTextFieldsumatoPre.setText(sumaSinIGVString);
        JTextFieldsumatoPre.setText(sumaSinIGVString);
        jTextFieldISC.setText(sumaISCString);
        jTextFieldTotal.setText("" + String.format("%,.2f", sumaTotalSinISC));
        jTextFieldTotalFinal.setText("" + String.format("%,.2f", sumaTotal));

    }

    public void iniciarPaneles() {
        try {
            paneles[0] = panelPedido1;
            paneles[1] = panelPedido2;
            paneles[2] = panelPedido3;
            paneles[3] = panelPedido4;
            paneles[4] = panelPedido5;
            paneles[5] = panelPedido6;
            paneles[6] = panelPedido7;
            paneles[7] = panelPedido8;
            paneles[8] = panelPedido9;
            paneles[9] = panelPedido10;
            paneles[10] = panelPedido11;
            paneles[11] = panelPedido12;
            paneles[12] = panelPedido13;
            paneles[13] = panelPedido14;
            paneles[14] = panelPedido15;
            paneles[15] = panelPedido16;
            paneles[16] = panelPedido17;
            paneles[17] = panelPedido18;
            paneles[18] = panelPedido19;
            paneles[19] = panelPedido20;
            paneles[20] = panelPedido21;
            paneles[21] = panelPedido22;
            paneles[22] = panelPedido23;
            paneles[23] = panelPedido24;
            paneles[24] = panelPedido25;
            paneles[25] = panelPedido51;
            paneles[26] = panelPedido52;
            paneles[27] = panelPedido53;
            paneles[28] = panelPedido54;
            paneles[29] = panelPedido55;
            paneles[30] = panelPedido56;
            paneles[31] = panelPedido57;
            paneles[32] = panelPedido58;
            paneles[33] = panelPedido59;
            paneles[34] = panelPedido60;
            paneles[35] = panelPedido61;
            paneles[36] = panelPedido62;
            paneles[37] = panelPedido63;
            paneles[38] = panelPedido64;
            paneles[39] = panelPedido65;
            paneles[40] = panelPedido66;
            paneles[41] = panelPedido67;
            paneles[42] = panelPedido68;
            paneles[43] = panelPedido69;
            paneles[44] = panelPedido70;
            paneles[45] = panelPedido71;
            paneles[46] = panelPedido72;
            paneles[47] = panelPedido73;
            paneles[48] = panelPedido74;
            paneles[49] = panelPedido75;

            //             int arreglo []= {5,6,7,8,9,10,11,12,13,24,25,16,17,18,19};
//9,7,5,24,25,26,14,15,19,12,13,11,17,36,18,34,33,6,8,4,10,20,21,22,23
            // int arreglo []= {9,7,5,24,25,78,16,11,210,214,218,222,174,178,182,186,84,34,33,36,3,8,4,10,82,79,80,81,20,21,85,86,87,22,23,14,15,19,12,13,1,1,1,1,1,1,1,1,42,78};
            int arreglo[] = leerDeCSV();
            String sql_Cons = "SELECT * FROM articulo where   ( ";
            for (int i = 0; i < arreglo.length; i++) {
                sql_Cons += "idarticulo =" + arreglo[i] + " or ";

            }
            System.out.println(sql_Cons);
            sql_Cons = sql_Cons.substring(0, sql_Cons.length() - 3);
            sql_Cons += ")";
            Statement sent;

            sent = cn.createStatement();
            //System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            int a = 0;
            while (rs.next()) {
                panelPedido pan = new panelPedido();
                Articulo art = new Articulo();

                art.setArtCos(Double.parseDouble(rs.getString("ArtCos")));
                art.setArtEstReg(Integer.parseInt(rs.getString("artEstReg")));
                art.setArtIgv(Double.parseDouble(rs.getString("artIgv")));
                art.setArtNom(rs.getString("artNom"));
                art.setIdArticulo(Integer.parseInt(rs.getString("idArticulo")));
                art.setIdUnidadMedida(Integer.parseInt(rs.getString("idUnidadMedida")));
                art.setArtCol(rs.getInt("artcol"));
                for (int i = 0; i < arreglo.length; i++) {
                    if (art.getIdArticulo() == arreglo[i]) {
                        pan.articulo = art;
                        pan.papa = this;
                        pan.cambiarNombre();
                        //  categoria = rs.getInt("artsubcat");
                        if (pan.articulo.getIdArticulo() == 1) {
                            paneles[i].setVisible(false);
                        }
                        paneles[i].setNuevaImagen(pan);
                        a++;

                    }
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void ajustarBotonones() {
        for (int j = 0; j < paneles.length; j++) {
            boolean hay = !(paneles[j].jTCantidad.getText().equals("0"));
            for (MovimientoDet de : FACTURA.getDetalles()) {
                //   System.err.println(FACTURA.getDetalles().size());
                if (Integer.parseInt(de.getIdArticulo()) == (paneles[j].articulo.getIdArticulo())) {

                    paneles[j].jTCantidad.setText(de.getMovDetCan());
                    if (paneles[j].jTCantidad.getText().length() >= 3 && paneles[j].jTCantidad.getText().substring(paneles[j].jTCantidad.getText().length() - 3).equals(".00")) {
                        paneles[j].jTCantidad.setText(paneles[j].jTCantidad.getText().substring(0, paneles[j].jTCantidad.getText().length() - 3));

                    }
                    paneles[j].jTCantidad.setBackground(Color.yellow);
                    hay = false;
                }
            }
            if (hay) {
                //  System.err.println("nunca dentre ");

                paneles[j].jTCantidad.setText("0");
                paneles[j].jTCantidad.setBackground(Color.white);
            }
        }

    }

    private String getEspacios(String cantidad, int i) {
        String res = "";
        for (int j = 0; j < i - cantidad.length(); j++) {
            res = res + " ";
        }
        return res;
    }

    private void imprimirTiket() {
        PrinterMatrix printer = new PrinterMatrix();

        Extenso e = new Extenso();

        e.setNumber(FACTURA.getDetalles().size() + 10);
        // nombre 
        // ruc origen 
        //nombre destino
        // fecha
        // serie  y numero 
        String nombre = jComboEmpSedOrig.getSelectedItem().toString();
        EmpresaSede empsed = new EmpresaSede(FACTURA.getCabecera().getIdEmpresaSede());
        Empresa emp = new Empresa(empsed.getIdEmpresa());
        String titulo = "";
        if (!jComboComprobate.getSelectedItem().toString().equalsIgnoreCase("FACTURA")) {
            titulo = "TICKET " + "N°:B" + jTextSerie.getText() + "-" + jTextNumero.getText();;
        } else {

            titulo = "TICKET " + "N°:F" + jTextSerie.getText() + "-" + jTextNumero.getText();;
        }
        String ruc = "RUC: " + emp.getEmpRUC();
        String destino = "CLIENTE: " + JTNombre.getText();
        long timeInMillis = System.currentTimeMillis();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(timeInMillis);
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd/MM/yyyy hh:mm:ss a");
        String dateforrow = dateFormat.format(cal1.getTime());
        String fecha = "FECHA: " + dateforrow;
        //String  serie_numero ="NRO de DOC: "+jTextSerie.getText()+"-"+jTextNumero.getText();
        String docu = "DNI /RUC:" + JTDocumentoDestino.getText();
        String direccion = "" + JTDireccion.getText();
        String direccionNuestra = "" + empsed.getEmpSedDir();
        int constante = 40;
        int lineasAimprimir = 17 + FACTURA.getDetalles().size();
        String cabecera = "|       PRODUCTO      |CANTIDAD|IMPORTE|";
        String linea = "";
        for (int i = 0; i < 39; i++) {
            linea += "-";

        }
        String[] arr = {titulo, fecha, " ", cabecera, linea};
        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(lineasAimprimir, constante);
        int i = 0;
        for (; i < arr.length; i++) {
            String arr1 = arr[i];
            printer.printTextWrap(i, i, 1, constante, arr1);

        }
        i++;
        int a = 0;
        for (a = i; a < FACTURA.getDetalles().size() + i; a++) {
            String descripcion = JTabla.getValueAt(a - i, 2).toString();

            String cantidad = JTabla.getValueAt(a - i, 1).toString();
            String importe = JTabla.getValueAt(a - i, 4).toString();
            String splitS = cabecera.substring(1, cabecera.length() - 1);
            String split[] = {"     PRODUCTO      ", "CANTIDAD", "IMPORTE"};
            while (descripcion.length() < split[0].length()) {
                if (descripcion.length() > split[0].length()) {
                    break;
                }
                descripcion = " " + descripcion;

                if (descripcion.length() > split[0].length()) {
                    break;
                }
                descripcion = descripcion + " ";

            }
            while (cantidad.length() < split[1].length()) {
                if (cantidad.length() > split[1].length()) {
                    break;
                }
                cantidad = " " + cantidad;

                if (cantidad.length() > split[1].length()) {
                    break;
                }
                cantidad = cantidad + " ";

            }
            while (importe.length() < split[2].length()) {
                if (importe.length() > split[2].length()) {
                    break;
                }
                importe = " " + importe;

                if (importe.length() > split[2].length()) {
                    break;
                }
                importe = importe + " ";

            }
            String datos = "|" + descripcion + "|" + cantidad + "|" + importe + "|";
            //  System.err.println(datos );
            printer.printTextWrap(a, a, 1, constante, datos);

        }
        printer.printTextWrap(a, a, 1, constante, linea);
        a++;
        printer.printTextWrap(a, a, 1, constante, "");
        a++;
        printer.printTextWrap(a, a, 1, constante, "MONTO:" + JTextFieldsumatoPre.getText() + "\tTIGV:" + jTextFieldIGV.getText() + "\tTISC:" + jTextFieldISC.getText());
        a++;
        printer.printTextWrap(a, a, 1, constante, "\t\t\tTOTAL:" + jTextFieldTotalFinal.getText());
        a++;
        //Imprimir * de la 2da linea a 25 en la columna 1;
        // printer.printCharAtLin(2, 25, 1, "*");
        //Imprimir * 1ra linea de la columa de 1 a 80
        // printer.printCharAtCol(1, 1, constante, "=");
        //Imprimir Encabezado nombre del La EMpresa
        // printer.printTextWrap(1, 2, 1, constante, "FACTURA DE VENTA");
        //printer.printTextWrap(linI, linE, colI, colE, null);t
        //printer.printTextWrap(2, 3, 1, 22, "Num. Boleta : " );
        //    printer.printTextWrap(3, 3, 1, 22, "Num. Boleta : " );
        // printer.printTextWrap(4, 4, 1, 22, "Num. Boleta : " );
        //  printer.printTextWrap(4, 4, 1, 22, "Num. Boleta : " );
        // printer.printTextWrap(4, 4, 1, 22, "Num. Boleta : " );

        // la primer linea es la fila donde se imprime 
        /*
        for (int i = 0; i &lt; filas; i++) { printer.printTextWrap(9 + i, 10, 1, 80, tblVentas.getValueAt(i,0).toString()+"|"+tblVentas.getValueAt(i,1).toString()+"| "+tblVentas.getValueAt(i,2).toString()+"| "+tblVentas.getValueAt(i,3).toString()+"|"+ tblVentas.getValueAt(i,4).toString()); } if(filas &gt; 15){
        printer.printCharAtCol(filas + 1, 1, 80, "=");
        printer.printTextWrap(filas + 1, filas + 2, 1, 80, "TOTAL A PAGAR " + txtVentaTotal.getText());
        printer.printCharAtCol(filas + 2, 1, 80, "=");
        printer.printTextWrap(filas + 2, filas + 3, 1, 80, "Esta boleta no tiene valor fiscal, solo para uso interno.: + Descripciones........");
        }else{
        printer.printCharAtCol(25, 1, 80, "=");
        printer.printTextWrap(26, 26, 1, 80, "TOTAL A PAGAR " + txtVentaTotal.getText());
        printer.printCharAtCol(27, 1, 80, "=");
        printer.printTextWrap(27, 28, 1, 80, "Esta boleta no tiene valor fiscal, solo para uso interno.: + Descripciones........");
 
        }*/
        printer.toFile("impresion.txt");
        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream("impresion.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();
        PrintService defaultPrintService = PrintUtility.findPrintService("E1");

        if (defaultPrintService != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();

            try {
                printJob.print(document, attributeSet);
                byte[] bytes = {27, 105, 3};
                DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
                DocPrintJob job = PrintServiceLookup.lookupDefaultPrintService().createPrintJob();
                Doc doc = new SimpleDoc(bytes, flavor, null);
                //  Thread.sleep(3000);
                job.print(doc, null);
                System.err.println("paso esta linea");

                //  Thread.sleep(3000);
                System.err.println("paso esta linea");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            //  System.err.println("No existen impresoras instaladas");
        }
        try {
            inputStream.close();
        } catch (IOException ex) {
            Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void imprimirFacturaEEUU() {
        PrinterMatrix printer = new PrinterMatrix();

        Extenso e = new Extenso();

        e.setNumber(FACTURA.getDetalles().size() + 10);
        // nombre 
        // ruc origen 
        //nombre destino
        // fecha
        // serie  y numero 

        // fecha
        // serie  y numero 
        String nombre = jComboEmpSedOrig.getSelectedItem().toString();
        EmpresaSede empsed = new EmpresaSede(FACTURA.getCabecera().getIdEmpresaSede());
        Empresa emp = new Empresa(empsed.getIdEmpresa());
        String titulo = "";
        if (!jComboComprobate.getSelectedItem().toString().equalsIgnoreCase("FACTURA")) {
            titulo = jComboComprobate.getSelectedItem().toString() + " ELECTRONICA" + "N°:B" + jTextSerie.getText() + "-" + jTextNumero.getText();;
        } else {

            titulo = jComboComprobate.getSelectedItem().toString() + " ELECTRONICA" + "N°:F" + jTextSerie.getText() + "-" + jTextNumero.getText();;
        }
        String ruc = "RUC: " + emp.getEmpRUC();
        String destino = "CLIENTE: " + JTNombre.getText();
        String fecha = "FECHA: " + ((JTextField) (Fecha1.getDateEditor().getUiComponent())).getText();
        //String  serie_numero ="NRO de DOC: "+jTextSerie.getText()+"-"+jTextNumero.getText();
        String docu = "DNI /RUC:" + JTDocumentoDestino.getText();
        String direccion = "" + JTDireccion.getText();
        String direccionNuestra = "" + empsed.getEmpSedDir();
        long timeInMillis = System.currentTimeMillis();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(timeInMillis);
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd/MM/yyyy hh:mm:ss a");
        String dateforrow = dateFormat.format(cal1.getTime());
        java.util.Date datey = new java.util.Date();
//Caso 1: obtener la hora y salida por pantalla con formato:

        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
        String dateSalida = "HORA : " + hourdateFormat.format(datey);

//String  serie_numero ="NRO de DOC: "+jTextSerie.getText()+"-"+jTextNumero.getText();
        int constante = 40;
        int lineasAimprimir = 32 + FACTURA.getDetalles().size();
        String cabecera = "|       PRODUCTO      |CANTIDAD|IMPORTE|";
        String linea = "";
        for (int i = 0; i < 39; i++) {
            linea += "-";

        }
        String[] arr = {titulo, nombre, ruc, direccionNuestra, destino, docu, direccion, fecha, dateSalida, " ", cabecera, linea};

        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(lineasAimprimir, constante);
        int i = 0;
        for (; i < arr.length; i++) {
            String arr1 = arr[i];
            printer.printTextWrap(i, i, 1, constante, arr1);

        }
        i++;
        int a = 0;
        for (a = i; a < FACTURA.getDetalles().size() + i; a++) {
            String descripcion = JTabla.getValueAt(a - i, 2).toString();

            String cantidad = JTabla.getValueAt(a - i, 1).toString();
            String importe = JTabla.getValueAt(a - i, 4).toString();
            String splitS = cabecera.substring(1, cabecera.length() - 1);
            String split[] = {"     PRODUCTO      ", "CANTIDAD", "IMPORTE"};
            if (descripcion.length() > split[0].length()) {
                descripcion = descripcion.substring(0, split[0].length() - 2);
            }
            while (descripcion.length() < split[0].length()) {
                if (descripcion.length() > split[0].length()) {
                    break;
                }
                descripcion = " " + descripcion;

                if (descripcion.length() > split[0].length()) {
                    break;
                }
                descripcion = descripcion + " ";

            }
            while (cantidad.length() < split[1].length()) {
                if (cantidad.length() > split[1].length()) {
                    break;
                }
                cantidad = " " + cantidad;

                if (cantidad.length() > split[1].length()) {
                    break;
                }
                cantidad = cantidad + " ";

            }
            while (importe.length() < split[2].length()) {
                if (importe.length() > split[2].length()) {
                    break;
                }
                importe = " " + importe;

                if (importe.length() > split[2].length()) {
                    break;
                }
                importe = importe + " ";

            }
            String datos = "|" + descripcion + "|" + cantidad + "|" + importe + "|";
            //  System.err.println(datos );
            printer.printTextWrap(a, a, 1, constante, datos);

        }
        printer.printTextWrap(a, a, 1, constante, linea);
        a++;
        printer.printTextWrap(a, a, 1, constante, "");
        a++;
        printer.printTextWrap(a, a, 1, constante, "MONTO:" + JTextFieldsumatoPre.getText() + "\tTIGV:" + jTextFieldIGV.getText() + "\tTISC:" + jTextFieldISC.getText());
        a++;
        printer.printTextWrap(a, a, 1, constante, "\t\t\t   TOTAL:" + jTextFieldTotalFinal.getText());
        a++;
        printer.printTextWrap(a, a, 1, constante, "");
        a++;
        printer.printTextWrap(a, a, 1, constante, "Representacion impresa de la factura  ");
        a++;
        printer.printTextWrap(a, a, 1, constante, "electrónica generada desde el sistema");
        a++;
        printer.printTextWrap(a, a, 1, constante, "facturador SUNAT. Puede verificarla");
        a++;
        printer.printTextWrap(a, a, 1, constante, "utilizando su clave SOL ");
        a++;      //Imprimir * de la 2da linea a 25 en la columna 1;
        printer.printTextWrap(a, a, 1, constante, "");
        a++;
        printer.printTextWrap(a, a, 1, constante, "\t Cuidemos el medio Ambiente ");
        a++;      //Imprimir * de la 2da linea a 25 en la columna 1;
        printer.printTextWrap(a, a, 1, constante, " \t \t Ley 30884");
        a++;      //Imprimir * de la 2da linea a 25 en la columna 1;
        printer.printTextWrap(a, a, 1, constante, " ");
        a++;
        // printer.printCharAtLin(2, 25, 1, "*");
        //Imprimir * 1ra linea de la columa de 1 a 80
        // printer.printCharAtCol(1, 1, constante, "=");
        //Imprimir Encabezado nombre del La EMpresa
        // printer.printTextWrap(1, 2, 1, constante, "FACTURA DE VENTA");
        //printer.printTextWrap(linI, linE, colI, colE, null);t
        //printer.printTextWrap(2, 3, 1, 22, "Num. Boleta : " );
        //    printer.printTextWrap(3, 3, 1, 22, "Num. Boleta : " );
        // printer.printTextWrap(4, 4, 1, 22, "Num. Boleta : " );
        //  printer.printTextWrap(4, 4, 1, 22, "Num. Boleta : " );
        // printer.printTextWrap(4, 4, 1, 22, "Num. Boleta : " );

        // la primer linea es la fila donde se imprime 
        /*
        for (int i = 0; i &lt; filas; i++) { printer.printTextWrap(9 + i, 10, 1, 80, tblVentas.getValueAt(i,0).toString()+"|"+tblVentas.getValueAt(i,1).toString()+"| "+tblVentas.getValueAt(i,2).toString()+"| "+tblVentas.getValueAt(i,3).toString()+"|"+ tblVentas.getValueAt(i,4).toString()); } if(filas &gt; 15){
        printer.printCharAtCol(filas + 1, 1, 80, "=");
        printer.printTextWrap(filas + 1, filas + 2, 1, 80, "TOTAL A PAGAR " + txtVentaTotal.getText());
        printer.printCharAtCol(filas + 2, 1, 80, "=");
        printer.printTextWrap(filas + 2, filas + 3, 1, 80, "Esta boleta no tiene valor fiscal, solo para uso interno.: + Descripciones........");
        }else{
        printer.printCharAtCol(25, 1, 80, "=");
        printer.printTextWrap(26, 26, 1, 80, "TOTAL A PAGAR " + txtVentaTotal.getText());
        printer.printCharAtCol(27, 1, 80, "=");
        printer.printTextWrap(27, 28, 1, 80, "Esta boleta no tiene valor fiscal, solo para uso interno.: + Descripciones........");
 
        }*/
        printer.toFile("impresion.txt");
        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream("impresion.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();
        PrintService defaultPrintService = PrintUtility.findPrintService("E1");

        if (defaultPrintService != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();

            try {
                printJob.print(document, attributeSet);
                byte[] bytes = {27, 105, 3};
                DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
                DocPrintJob job = PrintServiceLookup.lookupDefaultPrintService().createPrintJob();
                Doc doc = new SimpleDoc(bytes, flavor, null);
                //  Thread.sleep(3000);
                job.print(doc, null);
                System.err.println("paso esta linea");

                //  Thread.sleep(3000);
                System.err.println("paso esta linea");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            //  System.err.println("No existen impresoras instaladas");
        }
        try {
            inputStream.close();
        } catch (IOException ex) {
            Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private int[] leerDeCSV() {
        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("archivoVentas.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.length() < 1 || line.substring(0, 1).equals("#")) {
                    continue;
                }
                String[] values = line.split(";");
                if (values.length < 2) {
                    values = line.split(",");
                }
                records.add(Arrays.asList(values));
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        int[] retorno = new int[50];
        int i = 0;

        for (List<String> s : records) {
            for (String string : s) {
                try {
                    retorno[i] = Integer.parseInt(string);
                    i++;
                } catch (Exception e) {
                }

            }
        }
        return retorno;
    }

}
