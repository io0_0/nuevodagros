/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arqueo;

/**
 *
 * @author Edward
 */
public class Arqueo {
    String fecha;
    String nombreSede;
    String dinero;
    String movimiento;

    public Arqueo(String fecha, String nombreSede, String dinero, String movimiento) {
        this.fecha = fecha;
        this.nombreSede = nombreSede;
        this.dinero = dinero;
        this.movimiento = movimiento;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombreSede() {
        return nombreSede;
    }

    public void setNombreSede(String nombreSede) {
        this.nombreSede = nombreSede;
    }

    public String getDinero() {
        return dinero;
    }

    public void setDinero(String dinero) {
        this.dinero = dinero;
    }

    public String getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(String movimiento) {
        this.movimiento = movimiento;
    }
    
    
}
