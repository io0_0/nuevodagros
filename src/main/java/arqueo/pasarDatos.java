/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arqueo;

/**
 *
 * @author Edward
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Maestros.Atributos;
import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 *
 * @author Gutierrez
 */
public class pasarDatos implements JRDataSource {

    private List <Arqueo> atributos = new ArrayList<>();
    private int indiceParticipantes=-1;

    pasarDatos() {
       
    }
    
    @Override
    public boolean next() throws JRException {
    return ++indiceParticipantes<atributos.size();
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
        
        Object valor=null;
        
        if("fecha".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getFecha();
            
        }else if("nombreSede".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getNombreSede();
        
        }else if("movimiento".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getMovimiento();
       
        }else if("dinero".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getDinero();
        
        }
        return valor;
    }
    
    public void addDatos(Arqueo atributo ){
        this.atributos.add(atributo);
    }
    
}
