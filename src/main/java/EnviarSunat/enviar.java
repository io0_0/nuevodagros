/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package    EnviarSunat;

import Facturador.*;
import    Bean.MovimientoCab;
import static    GUI.ArticuloGUI.cn;
import java.awt.List;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.ListModel;

/**
 *
 * @author USUARIO
 */
public class enviar extends javax.swing.JFrame {

    /**
     * Creates new form enviar
     */
    ArrayList<factura> movimientos = new ArrayList<factura>();         
    boolean [] de = new  boolean [movimientos.size()];

    public enviar() { 
      
        initComponents();
        iniciarMovimientos ();
        llenar();
    }
    public void iniciarMovimientos (){
        String sql_Cons="SELECT * FROM MovimientoCab where movCabEnv = 0";
      try {
          Statement sent;
        
          System.out.println("prueba ");
          sent = cn.createStatement();
          System.out.println(sql_Cons);
          ResultSet rs = sent.executeQuery(sql_Cons);
          while (rs.next()) {
                      MovimientoCab movi = new MovimientoCab();

              movi.setIdMovimientoCabecera(rs.getString("IdMovimientoCabecera"));
              movi.setIdMovimientoDocumento(rs.getString("IdMovimientoDocumento"));
              movi.setIdUsuario(rs.getString("IdUsuario"));
               movi.setIdEmpresaSede(rs.getString("IdEmpresaSede"));
              movi.setIdMovimientoTipo(rs.getString("IdMovimientoTipo"));
              movi.setMovCabMovSto( rs.getString("MovCabMovSto"));
             movi.setMovCabFec(rs.getString("MovCabFec"));
                  
             movi.setMovCabSer(rs.getString("MovCabSer"));
             movi.setMovCabNum(rs.getString("MovCabNum"));
             movi.setIdEmpresaSedeDes(rs.getString("IdEmpresaSedeDes"));
             factura fac = new factura ();
             fac.setCabecera(movi);
             movimientos.add(fac);
          }
      } catch (Exception e) {
          e.printStackTrace();
      }
    
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jListNoEnviados = new javax.swing.JList();
        jScrollPane4 = new javax.swing.JScrollPane();
        jListEnviado = new javax.swing.JList();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButton1.setText("Deseo enviar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Deseo enviar Sunat");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel1.setText("No enviados");

        jLabel2.setText("enviados");

        jScrollPane3.setViewportView(jListNoEnviados);

        jScrollPane4.setViewportView(jListEnviado);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(81, 81, 81)
                        .addComponent(jButton2))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(138, 138, 138)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(26, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(137, 137, 137)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(88, 88, 88))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 119, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton2)
                            .addComponent(jButton1))
                        .addGap(48, 48, 48))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
       
        /*
        Enviar de JListNoEnviado a JListEnviado
        */
        DefaultListModel model = new DefaultListModel();
        ArrayList seleccionados = (ArrayList) jListNoEnviados.getSelectedValuesList();
        
        for(Object valor: seleccionados){
            model.addElement(valor.toString());
        }
        for (int j = 0; j < jListEnviado.getModel().getSize(); j++) {
                    model.addElement(jListEnviado.getModel().getElementAt(j));
                
            }       
        
        jListEnviado.setModel(model);
        
        /*
        Borrando de jListNoEnviado
        */
        DefaultListModel modelBorrado = new DefaultListModel();
        int tamaño = jListNoEnviados.getModel().getSize();
        int [] lista = jListNoEnviados.getSelectedIndices();
        
        for (int i = 0; i < lista.length; i++) {
            System.out.println(lista[i]);
        }
        
        for (int i = 0; i < tamaño; i++) {
            for (int j = 0; j < lista.length; j++) {
                if(lista[j]!= i){
                    modelBorrado.addElement(jListNoEnviados.getModel().getElementAt(i));
                }
            }    
        }
        jListNoEnviados.setModel(modelBorrado);
        
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(enviar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(enviar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(enviar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(enviar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new enviar().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JList jListEnviado;
    private javax.swing.JList jListNoEnviados;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    // End of variables declaration//GEN-END:variables

    private void llenar() {
       DefaultListModel<String>  model = new DefaultListModel<String>();
       jListNoEnviados.setModel(model);
       

        for(factura fac: movimientos ){
           String con =  fac.getCabecera().getMovCabSer()+"-"+fac.getCabecera().getMovCabNum()+"\n";
             model.addElement(con);
           //  mandar+=con;
        }
                     jListNoEnviados.setModel(model);

      //  jTextNoEnviados.setText(mandar);
    }
}
