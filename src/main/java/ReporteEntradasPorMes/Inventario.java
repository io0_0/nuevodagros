/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReporteEntradasPorMes;

import Bean.Articulo;
import static Bean.Articulo.cn;
import Bean.ArticuloPrecio;
import Bean.MovimientoCab;
import Bean.MovimientoDet;
import Movimiento.Movimiento;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JTable;

/**
 *
 * @author USUARIO
 */
public class Inventario {
    public   ArrayList<MovimientoDet> detalles = new ArrayList<MovimientoDet>();
    public void  llenarInventario(int idArt, int idSede, ArrayList<Integer > cantidad, ArrayList<Integer > costo){
       ArrayList<Articulo> art = new ArrayList<Articulo>();
       for(int i = 1 ; i < 40; i++){
           art.add(new Articulo(i));
       
       }
       ArrayList<MovimientoDet> detalles = new ArrayList<MovimientoDet>();
       MovimientoCab cabecera = new MovimientoCab();
       cabecera.setIdMovimientoDocumento(""+idArt);
       cabecera.setIdEmpresaSede(""+idSede);
       for(Articulo ar: art){
        MovimientoDet ActualDet = new MovimientoDet();
        ActualDet.setIdArticulo(""+ar.getIdArticulo());
        ActualDet.setIdMovimientoCabecera(cabecera.getIdMovimientoCabecera());
        ActualDet.setMovDetCan(""+cantidad.get(ar.getIdArticulo()));
        ActualDet.setMovDetCosTot(""+costo.get(ar.getIdArticulo()));

       }
       
    }
    public void HacerInventario(){
        String sql_Cons = "select * from movimientocab where idmovimientodocumento = 33";
        MovimientoCab movi=new MovimientoCab() ;
        
        try {
            Statement sent;
          
            sent = cn.createStatement();
           // System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            if(rs.next()) {
            movi = new MovimientoCab(rs.getString("idmovimientoCu"));
                    
             }
        } catch (Exception e) {
            e.printStackTrace();
        }
         String cons2="select * from movimientodet where  idmovimientocab= '"+movi.getIdMovimientoCabecera()+"'" ; 
        try {
            Statement sent2;
          
            sent2 = cn.createStatement();
            System.out.println(cons2);
            ResultSet rs2 = sent2.executeQuery(sql_Cons);
            
            if(rs2.next()) {
              detalles.add(new MovimientoDet(rs2.getString("idmovimientodet")));
             }
        } catch (Exception e) {
            e.printStackTrace();
        }
       
    
    
    }
    public static void main(String args[]){
       ArrayList<Articulo> art = new ArrayList<Articulo>();
       for(int i = 1 ; i < 40; i++){
           art.add(new Articulo(i));
       
       }
       ArrayList<Integer> in= new ArrayList<Integer>();
       for (int i =0; i<art.size();i++){
           in.add(i);
       
       }
       ArrayList<ArticuloPrecio> artPrecio = new ArrayList<ArticuloPrecio>();
       
    }
    
    public static  void llenarTabla(JTable JTabla , String articuloNombre) {
                String sql ="SELECT SUM(movimientodet.MovDetCan *( movimientocab.IdMovimientoTipo*2-3 )*-1) as cantidad,"
                        + " empresasede.EmpSedNom  as nombre "
                        + "from movimientodet"
                        + " INNER JOIN movimientocab on movimientodet.IdMovimientoCabecera = movimientocab.IdMovimientoCabecera "
                        + "INNER JOIN empresasede on movimientocab.IdEmpresaSede=empresasede.IdEmpresaSede "
                        + "inner join empresa on empresasede.IdEmpresa=empresa.IdEmpresa "
                        + "inner join empresagrupo on empresa.IdEmpresaGrupo = empresagrupo.IdEmpresaGrupo"
                        + " inner join articulo on movimientodet.idarticulo = articulo.idarticulo  "
                        + "where articulo.artnom = '"+articuloNombre+"' and movimientodet.MovDetEstReg = 1 "
                        + "and movimientocab.MovCabEstReg=1 "
                        + "and empresagrupo.IdEmpresaGrupo=1 GROUP BY empresasede.EmpSedNom" ;
    
  try {
            double [] NI=new double[2];
            
            Statement sent;
            sent = cn.createStatement();
            String sql1=sql;
          //  System.err.println(sql1);
            
            
            ResultSet rs;
            
            rs = sent.executeQuery(sql1);
            
            
            
            
            //rs = sent.executeQuery(sql1);
            
            //INVENTARIO INICIAL
            
            
            int i =0;
            ArrayList<ArrayList <String> > datos= new ArrayList<ArrayList<String>>();
           // datos.add(new ArrayList<String>());
            //  tabla[i][10] = Double.parseDouble(tabla[i][8].toString())-Double.parseDouble(tabla[i][9].toString());
            //  tabla[i][6] = Double.parseDouble(tabla[i][4].toString())-Double.parseDouble(tabla[i][5].toString());
            //  tabla[i][7] = Math.rint(Double.parseDouble(tabla[i][10].toString())/Double.parseDouble(tabla[i][6].toString())*10000)/10000;
            //   i++;
            while(rs.next()) {
                double cantidad = rs.getDouble("cantidad");
                String  nombre=rs.getString("nombre");
                    datos.add(new ArrayList<String>());

               datos .get(i).add(""+nombre);
               datos.get(i).add(""+cantidad);
                    
                    i++;
                    
            }
            String[][] tabla = new String[datos.size()][2];
            for (int j = 0; j < tabla.length; j++) {
                for (int k = 0; k < tabla[0].length; k++) {
                    tabla[j][k]=datos.get(j).get(k);
                    
                }
                
            }
            
            
            JTabla.setModel(new javax.swing.table.DefaultTableModel(
            tabla,
            new String[]{
                "Empresa", "Inventario"
            }
            ) {
            Class[] types = new Class[]{
            java.lang.Object.class, java.lang.Object.class
            };
            
            @Override
            public Class getColumnClass(int columnIndex) {
            return types[columnIndex];
            }
                                 
            }
            
            );
            int constante=1;
            JTabla.getColumnModel().getColumn(0).setPreferredWidth(constante);
            JTabla.getColumnModel().getColumn(1).setPreferredWidth(constante*10);
            
            
            rs.close();
                      System.out.println("lleno tabla ");
        } catch (SQLException ex) {
        }       
    }
}
