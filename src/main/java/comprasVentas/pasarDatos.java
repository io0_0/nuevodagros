/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comprasVentas;

/**
 *
 * @author Edward
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Maestros.Atributos;
import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 *
 * @author Gutierrez
 */
public class pasarDatos implements JRDataSource {

    private List <Compras> atributos = new ArrayList<>();
    private int indiceParticipantes=-1;

    pasarDatos() {
       
    }
    
    @Override
    public boolean next() throws JRException {
    return ++indiceParticipantes<atributos.size();
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
        
        Object valor=null;
        
        if("fecha".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getFecha();
            
        }else if("tipo".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getTipo();
        
        }else if("movimiento".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getMovimiento();
       
        }else if("numero".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getNumero();
       
        }else if("sede".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getSede();
        
        }else if("destino".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getDestino();
        
        }else if("documento".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getDocumento();
        
        }else if("dinero".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getDinero();
        
        }else if("serieb".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getSerieb();
        
        }else if("numerob".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getNumerob();
        
        }
        return valor;
    }
    
    public void addDatos(Compras atributo ){
        this.atributos.add(atributo);
    }
    
}
