/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comprasVentas;

/**
 *
 * @author Edward
 */

public class Compras {
public Compras(){


}
    String fecha;
    String documento;
    String movimiento;
    String sede;
    String tipo;
    String destino;
    String numero;
    Double dinero;
    String serieb;
    String numerob;

    public Compras(String fecha, String documento, String movimiento, String sede, String tipo, String destino, String numero, Double dinero, String serieb, String numerob) {
        this.fecha = fecha;
        this.documento = documento;
        this.movimiento = movimiento;
        this.sede = sede;
        this.tipo = tipo;
        this.destino = destino;
        this.numero = numero;
        this.dinero = dinero;
        this.serieb = serieb;
        this.numerob = numerob;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(String movimiento) {
        this.movimiento = movimiento;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Double getDinero() {
        return dinero;
    }

    public void setDinero(Double dinero) {
        this.dinero = dinero;
    }

    public String getSerieb() {
        return serieb;
    }

    public void setSerieb(String serieb) {
        this.serieb = serieb;
    }

    public String getNumerob() {
        return numerob;
    }

    public void setNumerob(String numerob) {
        this.numerob = numerob;
    }

   
}
