/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import static Bean.Articulo.cn;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USUARIO
 */
public class FormulaCabecera {
      private int idFormulaCabecera;
    private String ForCabDes;
    private int IdArticulo ;
    private double ForCabDetCan;
    private int ForCabEstReg;

    public int getIdArticulo() {
        return IdArticulo;
    }

    public void setIdArticulo(int IdArticulo) {
        this.IdArticulo = IdArticulo;
    }
    
    public FormulaCabecera(){
    }
    public FormulaCabecera(int id ){
    idFormulaCabecera= id ;
        String sql_Cons="SELECT * FROM formulaCabecera where idFormulaCabecera ="+id;
        try {
            Statement sent;
          
            sent = cn.createStatement();
            System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                setIdArticulo(rs.getInt("IdArticulo"));
                setForCabDetCan(rs.getDouble("forcabdetcan"));
                setForCabEstReg(rs.getInt("forcabestreg"));
                setForCabDes(rs.getString("forCabDes"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    	
    
    }
    public int insertar(){
          try {
              //INSERT INTO `empresa`(`IdEmpresa`, `IdEmpresaGrupo`, `EmpRuc`, `EmpNom`, `EmpTel`, `EmpDir`, `EmpEstReg`)
              String sentIn = "INSERT INTO FormulaCabecera (idFormulaCabecera, idArticulo,forCabDes,ForCabDetCan,ForCabEstReg)";
              sentIn = sentIn + "Values (" +getIdFormulaCabecera() +","+getIdArticulo()+ ",'" +getForCabDes()+ "',"+ getForCabDetCan()  +","+ getForCabEstReg()+")";
              System.out.println(sentIn);
              Statement sent = cn.createStatement();
              sent.executeUpdate(sentIn);
              System.out.println("guardado");
          } catch (SQLException ex) {
              Logger.getLogger(FormulaCabecera.class.getName()).log(Level.SEVERE, null, ex);
              return 1 ;

          }
          return 0 ;
    }
     public void modificar() throws SQLException {

        String sentIn = "update FormulaCabecera set  ";
                sentIn += "IdArticulo"+ "=" + getIdArticulo()+ ",";

        sentIn += "forCabDes"+ "='" + getForCabDes()+ "' ,";
        sentIn += "forCabDetCan" + "=" + getForCabDetCan()+ ",";
        sentIn += "ForCabEstReg" + "=" + getForCabEstReg()+ " "+
      "  where ";
        sentIn += "IdFormulaCabecera" + "=" + getIdFormulaCabecera();

        System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("guardado");
    }
       public void eliminar() throws SQLException {

        String sentIn = "update FormulaCabecera set  ";
        sentIn += "ForCabEstReg" + "=0 " +
      " where ";
        sentIn += "idFormulaCabecera" + "=" + getIdFormulaCabecera();

        System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("guardado");
    }
    public int getIdFormulaCabecera() {
        return idFormulaCabecera;
    }

    public void setIdFormulaCabecera(int idFormulaCabecera) {
        this.idFormulaCabecera = idFormulaCabecera;
    }

  

    public double getForCabDetCan() {
        return ForCabDetCan;
    }

    public void setForCabDetCan(double ForCabDetCan) {
        this.ForCabDetCan = ForCabDetCan;
    }

    public int getForCabEstReg() {
        return ForCabEstReg;
    }

    public void setForCabEstReg(int ForCabEstReg) {
        this.ForCabEstReg = ForCabEstReg;
        
}

    public String getForCabDes() {
        return ForCabDes;
    }

    public void setForCabDes(String ForCabDes) {
        this.ForCabDes = ForCabDes;
    }
    
    
    
}