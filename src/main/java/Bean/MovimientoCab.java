package Bean;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import static     GUI.ArticuloGUI.cn;
import     Maestros.GenerarCodigo;
import java.io.Serializable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 *
 * @author USUARIO
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MovimientoCab  implements  Serializable{
    
        static String[] atributos = {"Codigo", "Documento ", "usuario", "empresasede","empresasede destino", "tipo movimiento", "Mueve stock  ", "fecha", "serie","numero","enviado"};

        private static  DefaultTableModel model = new DefaultTableModel(null, atributos);

        static int datos =11;

    public static void cambiarHechoProduccion() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static void cambiarHechoProduccion(String date) {
    
            try {
                String sentIn = "update MovimientoCab set  ";
                sentIn += "`MovCabProBoo` " + "='" + 1 + "'  ";
                sentIn += "  where ";
                
                sentIn +="`MovCabFec`"  + "='" +date+"'";
                
                System.out.println(sentIn);
                Statement sent = cn.createStatement();
                sent.executeUpdate(sentIn);
            } catch (SQLException ex) {
                Logger.getLogger(MovimientoCab.class.getName()).log(Level.SEVERE, null, ex);
            }
    
    }

    public static String generarCodigo() {
        GenerarCodigo gc = new GenerarCodigo();
     //  System.out.println(gc.generarID("movimientocab", "IdMovimientocabecera")); 
    
       return gc.generarID("movimientocab", "IdMovimientocabecera");
     }

    public static String diferenteAsociado(int id) {
      String sql_Cons="SELECT * FROM MovimientoCab where IdMovimientoCabeceraAsociado ="+id+" and idMovimientoCabecera !="+id;
        String waka=1+"";
       
      try {
          Statement sent;
        
      //    System.out.println("prubea ");
          sent = cn.createStatement();
        //  System.out.println(sql_Cons);
          ResultSet rs = sent.executeQuery(sql_Cons);
          while (rs.next()) {
               waka= rs.getString("IDMovimientoCAbecera");
          }
      } catch (Exception e) {
          e.printStackTrace();
      }

return waka;
    }

    public static ArrayList<MovimientoCab> getByAsociados(String idMovimientoCabeceraAsociado) {
        return select("SELECT * FROM `movimientocab` WHERE MovCabEstReg =1 and IdMovimientoCabeceraAsociado = '"+idMovimientoCabeceraAsociado+"'");
    
    
    }
      
    private   String IdMovimientoCabecera;
private String IdMovimientoCabeceraAsociado="1";
private  String IdMovimientoDocumento;
  private String IdUsuario;
  private  String IdEmpresaSede;
  private  String IdEmpresaSedeDes;
  private    String IdMovimientoTipo;
 private   String MovCabMovSto;
  private   String MovCabFec;
 private    String MovCabSer;
  private   String MovCabNum;
  private   String MovCabEstReg;
  private Date  MovCabEnv;
  private int MovCabProBoo;

    public MovimientoCab(MovimientoCab ProduccionSalida) {
        setIdMovimientoCabecera(ProduccionSalida.getIdMovimientoCabecera());
              setIdMovimientoDocumento(ProduccionSalida.getIdMovimientoDocumento());
              setIdUsuario(ProduccionSalida.getIdUsuario());
               setIdEmpresaSede(ProduccionSalida.getIdEmpresaSede());
               setIdEmpresaSedeDes(ProduccionSalida.getIdEmpresaSedeDes());
              setIdMovimientoTipo(ProduccionSalida.getIdMovimientoTipo());
              setMovCabMovSto( ProduccionSalida.getMovCabMovSto());
             setMovCabFec(ProduccionSalida.getMovCabFec());
              setMovCabProBoo(ProduccionSalida.getMovCabProBoo());
             setMovCabSer(ProduccionSalida.getMovCabSer());
             setMovCabNum(ProduccionSalida.getMovCabNum());
              setMovCabEnv(ProduccionSalida.getMovCabEnv());
             setIdEmpresaSedeDes(ProduccionSalida.getIdEmpresaSedeDes());
              setIdMovimientoCabeceraAsociado(ProduccionSalida.getIdMovimientoCabeceraAsociado());
            setMovCabEstReg("1");

    }

    public static MovimientoCab copiarAtributos (MovimientoCab original,MovimientoCab copia) {
              copia.setIdMovimientoDocumento(original.getIdMovimientoDocumento());
             copia. setIdUsuario(original.getIdUsuario());
            copia.   setIdEmpresaSede(original.getIdEmpresaSede());
              copia. setIdEmpresaSedeDes(original.getIdEmpresaSedeDes());
             copia. setIdMovimientoTipo(original.getIdMovimientoTipo());
              copia.setMovCabMovSto( original.getMovCabMovSto());
             copia.setMovCabFec(original.getMovCabFec());
              copia.setMovCabProBoo(original.getMovCabProBoo());
             copia.setMovCabSer(original.getMovCabSer());
             copia.setMovCabNum(original.getMovCabNum());
              copia.setMovCabEnv(original.getMovCabEnv());
             copia.setIdEmpresaSedeDes(original.getIdEmpresaSedeDes());
             copia.setMovCabEstReg("1");
return copia;
    }

    public int getMovCabProBoo() {
        return MovCabProBoo;
    }

    public void setMovCabProBoo(int MovCabProBoo) {
        this.MovCabProBoo = MovCabProBoo;
    }
  
  public MovimientoCab(String idMovimientoCabecera, String idMovimientoDocumento,
		String idUsuario, String idEmpresaSede, String idMovimientoTipo,
		String movCabMovSto, String movCabFec, String movCabSer,
		String movCabNum, String movCabEstReg, Date movCabEnv) {
	super();
	IdMovimientoCabecera = idMovimientoCabecera;
	IdMovimientoDocumento = idMovimientoDocumento;
	IdUsuario = idUsuario;
	IdEmpresaSede = idEmpresaSede;
	IdMovimientoTipo = idMovimientoTipo;
	MovCabMovSto = movCabMovSto;
	MovCabFec = movCabFec;
	MovCabSer = movCabSer;
	MovCabNum = movCabNum;
	MovCabEstReg = movCabEstReg;
	MovCabEnv = movCabEnv;
}
public MovimientoCab(String id ) {
      IdMovimientoCabecera=id;
      String sql_Cons="SELECT * FROM MovimientoCab where IdMovimientoCabecera ="+id;
      try {
          Statement sent;
        
      //    System.out.println("prubea ");
          sent = cn.createStatement();
        //  System.out.println(sql_Cons);
          ResultSet rs = sent.executeQuery(sql_Cons);
          
          while (rs.next()) {
              setIdMovimientoCabecera(rs.getString("IdMovimientoCabecera"));
              setIdMovimientoDocumento(rs.getString("IdMovimientoDocumento"));
              setIdUsuario(rs.getString("IdUsuario"));
               setIdEmpresaSede(rs.getString("IdEmpresaSede"));
               setIdEmpresaSedeDes(rs.getString("idempresasededes"));
              setIdMovimientoTipo(rs.getString("IdMovimientoTipo"));
              setMovCabMovSto( rs.getString("MovCabMovSto"));
             setMovCabFec(rs.getString("MovCabFec"));
              setMovCabProBoo(rs.getInt("MovCabProBoo"));
             setMovCabSer(rs.getString("MovCabSer"));
             setMovCabNum(rs.getString("MovCabNum"));
              setMovCabEnv(rs.getDate("MOVCABENV"));
             setIdEmpresaSedeDes(rs.getString("IdEmpresaSedeDes"));
              setIdMovimientoCabeceraAsociado(rs.getString("IdMovimientoCabeceraAsociado"));
           
          }
      } catch (Exception e) {
          e.printStackTrace();
      }
      
   }
    @Override
public String toString() {
	return getIdMovimientoCabecera()+"-"+getMovCabSer()+"-"+getMovCabNum();
}
	public String[] getAtributos() {
        return atributos;
    }
public static MovimientoCab getAsociadoBool(String idasoc , String bool){
      MovimientoCab ret  = new MovimientoCab();
      String sql_Cons="SELECT * FROM MovimientoCab where IdMovimientoCabeceraAsociado ="+idasoc+ " and movcabproboo= "+bool;
      
      try {
          Statement sent;
        
      //    System.out.println("prubea ");
          sent = cn.createStatement();
         //System.out.println(sql_Cons);
          ResultSet rs = sent.executeQuery(sql_Cons);
          
          while (rs.next()) {
              ret.setIdMovimientoCabecera(rs.getString("IdMovimientoCabecera"));
              ret.setIdMovimientoDocumento(rs.getString("IdMovimientoDocumento"));
              ret.setIdUsuario(rs.getString("IdUsuario"));
               ret.setIdEmpresaSede(rs.getString("IdEmpresaSede"));
               ret.setIdEmpresaSedeDes(rs.getString("idempresasededes"));
              ret.setIdMovimientoTipo(rs.getString("IdMovimientoTipo"));
              ret.setMovCabMovSto( rs.getString("MovCabMovSto"));
             ret.setMovCabFec(rs.getString("MovCabFec"));
              ret.setMovCabProBoo(rs.getInt("MovCabProBoo"));
             ret.setMovCabSer(rs.getString("MovCabSer"));
             ret.setMovCabNum(rs.getString("MovCabNum"));
              ret.setMovCabEnv(rs.getDate("MOVCABENV"));
             ret.setIdEmpresaSedeDes(rs.getString("IdEmpresaSedeDes"));
              ret.setIdMovimientoCabeceraAsociado(rs.getString("IdMovimientoCabeceraAsociado"));
           
          }
      } catch (Exception e) {
          e.printStackTrace();
      }

return  ret;
}
public static MovimientoCab getAsociadoBool(String idasoc , String bool, int idEmpsedOrig){
      MovimientoCab ret  = new MovimientoCab();
      String sql_Cons="SELECT * FROM MovimientoCab where IdMovimientoCabeceraAsociado ="+idasoc+ " and movcabproboo= "+bool+" and idEmpresaSede = "+idEmpsedOrig;
    //  if (bool.equals( Produccion2.Produccion.PRODUCTO))
         // System.out.println(sql_Cons);
      
      try {
          Statement sent;
        
      //    System.out.println("prubea ");
          sent = cn.createStatement();
         //System.out.println(sql_Cons);
          ResultSet rs = sent.executeQuery(sql_Cons);
          
          while (rs.next()) {
              ret.setIdMovimientoCabecera(rs.getString("IdMovimientoCabecera"));
              ret.setIdMovimientoDocumento(rs.getString("IdMovimientoDocumento"));
              ret.setIdUsuario(rs.getString("IdUsuario"));
               ret.setIdEmpresaSede(rs.getString("IdEmpresaSede"));
               ret.setIdEmpresaSedeDes(rs.getString("idempresasededes"));
              ret.setIdMovimientoTipo(rs.getString("IdMovimientoTipo"));
              ret.setMovCabMovSto( rs.getString("MovCabMovSto"));
             ret.setMovCabFec(rs.getString("MovCabFec"));
              ret.setMovCabProBoo(rs.getInt("MovCabProBoo"));
             ret.setMovCabSer(rs.getString("MovCabSer"));
             ret.setMovCabNum(rs.getString("MovCabNum"));
              ret.setMovCabEnv(rs.getDate("MOVCABENV"));
             ret.setIdEmpresaSedeDes(rs.getString("IdEmpresaSedeDes"));
              ret.setIdMovimientoCabeceraAsociado(rs.getString("IdMovimientoCabeceraAsociado"));
             
          }
      } catch (Exception e) {
          e.printStackTrace();
      }

return  ret;
}
public static ArrayList<MovimientoCab> select(String consulta){
             String sql_Cons=consulta;
             ArrayList <MovimientoCab> movimientos = new ArrayList<>();
             try {
          Statement sent;
            // System.out.println("prubea ");
          sent = cn.createStatement();
       //   System.out.println(sql_Cons);
          ResultSet rs = sent.executeQuery(sql_Cons);
          
          while (rs.next()) {
              MovimientoCab movi = new MovimientoCab();

              movi.setIdMovimientoCabecera(rs.getString("IdMovimientoCabecera"));
              movi.setIdMovimientoDocumento(rs.getString("IdMovimientoDocumento"));
              movi.setIdUsuario(rs.getString("IdUsuario"));
               movi.setIdEmpresaSede(rs.getString("IdEmpresaSede"));
               movi.setIdEmpresaSedeDes(rs.getString("idempresasededes"));
              movi.setIdMovimientoTipo(rs.getString("IdMovimientoTipo"));
              movi.setMovCabMovSto( rs.getString("MovCabMovSto"));
             movi.setMovCabFec(rs.getString("MovCabFec"));
              movi.setMovCabEnv(rs.getDate("MovCabEnv"));
             movi.setMovCabSer(rs.getString("MovCabSer"));
             movi.setMovCabNum(rs.getString("MovCabNum"));
             movi.setIdEmpresaSedeDes(rs.getString("IdEmpresaSedeDes"));
             
             movimientos.add(movi);
          }
      } catch (Exception e) {
          e.printStackTrace();
      }
        return movimientos;

}       
public static ArrayList<MovimientoCab> select(){
        return select("SELECT * FROM `movimientocab` ");
        
        }
   

    public static int getDatos() {
        return datos;
    }

    public String getIdMovimientoCabecera() {
        return IdMovimientoCabecera;
    }
 public String getIdMovimientoCabeceraAsociado() {
        return IdMovimientoCabeceraAsociado;
    }

    public String getIdMovimientoDocumento() {
        return IdMovimientoDocumento;
    }

    public String getIdUsuario() {
        return IdUsuario;
    }

    public String getIdEmpresaSede() {
        return IdEmpresaSede;
    }

    public String getIdMovimientoTipo() {
        return IdMovimientoTipo;
    }

    public String getMovCabMovSto() {
        return MovCabMovSto;
    }

    public String getMovCabFec() {
        return MovCabFec;
    }

    public String getMovCabSer() {
        return MovCabSer;
    }

    public String getMovCabNum() {
        return MovCabNum;
    }

    public String getMovCabEstReg() {
        return MovCabEstReg;
    }

    public void setAtributos(String[] atributos) {
        this.atributos = atributos;
    }

  

    public static void setDatos(int datos) {
        MovimientoCab.datos = datos;
    }

    public void setIdMovimientoCabecera(String IdMovimientoCabecera) {
        this.IdMovimientoCabecera = IdMovimientoCabecera;
    }
    public void setIdMovimientoCabeceraAsociado(String IdMovimientoCabeceraAsociado) {
        this.IdMovimientoCabeceraAsociado = IdMovimientoCabeceraAsociado;
    }
    public void setIdMovimientoDocumento(String IdMovimientoDocumento) {
        this.IdMovimientoDocumento = IdMovimientoDocumento;
    }

    public void setIdUsuario(String IdUsuario) {
        this.IdUsuario = IdUsuario;
    }

    public void setIdEmpresaSede(String IdEmpresaSede) {
        this.IdEmpresaSede = IdEmpresaSede;
    }

    public void setIdMovimientoTipo(String IdMovimientoTipo) {
        this.IdMovimientoTipo = IdMovimientoTipo;
    }

    public void setMovCabMovSto(String MovCabMovSto) {
        this.MovCabMovSto = MovCabMovSto;
    }

    public void setMovCabFec(String MovCabFec) {
        this.MovCabFec = MovCabFec;
    }

    public void setMovCabSer(String MovCabSer) {
        this.MovCabSer = MovCabSer;
    }

    public void setMovCabNum(String MovCabNum) {
        this.MovCabNum = MovCabNum;
    }

    public void setMovCabEstReg(String MovCabEstReg) {
        this.MovCabEstReg = MovCabEstReg;
    }
    public MovimientoCab(String IdMovimientoCabecera, String IdMovimientoDocumento, String IdUsuario, String IdEmpresaSede, String IdMovimientoTipo, String MovCabMovSto, String MovCabFec, String MovCabSer, String MovCabNum, String MovCabEstReg) {
        this.IdMovimientoCabecera = IdMovimientoCabecera;
        this.IdMovimientoDocumento = IdMovimientoDocumento;
        this.IdUsuario = IdUsuario;
        this.IdEmpresaSede = IdEmpresaSede;
        this.IdMovimientoTipo = IdMovimientoTipo;
        this.MovCabMovSto = MovCabMovSto;
        this.MovCabFec = MovCabFec;
        this.MovCabSer = MovCabSer;
        this.MovCabNum = MovCabNum;
        this.MovCabEstReg = MovCabEstReg;
    }

    public MovimientoCab(String IdMovimientoDocumento, String IdUsuario, String IdEmpresaSede, String IdMovimientoTipo, String MovCabMovSto, String MovCabFec, String MovCabSer, String MovCabNum) {
        this.IdMovimientoDocumento = IdMovimientoDocumento;
        this.IdUsuario = IdUsuario;
        this.IdEmpresaSede = IdEmpresaSede;
        this.IdMovimientoTipo = IdMovimientoTipo;
        this.MovCabMovSto = MovCabMovSto;
        this.MovCabFec = MovCabFec;
        this.MovCabSer = MovCabSer;
        this.MovCabNum = MovCabNum;
        MovCabEstReg="'1'";
       IdMovimientoCabecera="NULL";
    }
    
 public MovimientoCab()
 {
     
 } 

 
 public MovimientoCab(String []a ){
 
          IdMovimientoCabecera=a[0];
    IdMovimientoDocumento=a[1];
    IdUsuario=a[2];
     IdEmpresaSede=a[3];
      IdMovimientoTipo=a[4];
     MovCabMovSto=a[5];
      MovCabFec=a[6];
      MovCabSer=a[7];
      MovCabNum=a[8];
      MovCabEstReg=a[9];
 }
 public MovimientoCab(String idMovimientoCabecera,
			String idMovimientoDocumento, String idUsuario,
			String idEmpresaSede, String idEmpresaSedeDes,
			String idMovimientoTipo, String movCabMovSto, String movCabFec,
			String movCabSer, String movCabNum, String movCabEstReg,
			Date movCabEnv) {
		super();
		IdMovimientoCabecera = idMovimientoCabecera;
		IdMovimientoDocumento = idMovimientoDocumento;
		IdUsuario = idUsuario;
		IdEmpresaSede = idEmpresaSede;
		IdEmpresaSedeDes = idEmpresaSedeDes;
		IdMovimientoTipo = idMovimientoTipo;
		MovCabMovSto = movCabMovSto;
		MovCabFec = movCabFec;
		MovCabSer = movCabSer;
		MovCabNum = movCabNum;
		MovCabEstReg = movCabEstReg;
		MovCabEnv = movCabEnv;
	}
 public String insertarID() throws SQLException{
      String codigo = generarCodigo();
  //    JOptionPane.showMessageDialog(null, "este es el codigo"+codigo);
     setIdMovimientoCabecera(codigo);
     insertar();
     return codigo;
 } 
 public void insertar() throws SQLException {
   //   System.out.println("eres un pendje");
    String en="";
        if(MovCabEnv!=null)
                en=new SimpleDateFormat("yyyy-MM-dd").format(MovCabEnv);
        else{
  en ="1990-01-01";
        }
    String sentIn="INSERT INTO `movimientocab`(`IdMovimientoCabecera`, `IdMovimientoDocumento`, `IdUsuario`, `IdEmpresaSede`, `IdEmpresaSedeDes`, `IdMovimientoTipo`, `MovCabMovSto`, `MovCabFec`, `MovCabSer`, `MovCabNum`,`MovCabEnv`,`MovCabProBoo`,  `MovCabEstReg`,`idmovimientoCabeceraAsociado`) VALUES";

    sentIn=sentIn+"("+IdMovimientoCabecera+"," +IdMovimientoDocumento+"," +IdUsuario+"," ;
    
    sentIn=sentIn+IdEmpresaSede+"," +IdEmpresaSedeDes+"," +IdMovimientoTipo+"," +MovCabMovSto+"," +MovCabFec+"," +MovCabSer+"," +MovCabNum+",'" +en+"',"+getMovCabProBoo()+"," +MovCabEstReg+","+IdMovimientoCabeceraAsociado+")";  
   System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
     //   System.out.println("guardado");
    }

    public void modificar() throws SQLException {

        String sentIn = "update MovimientoCab set  ";
        sentIn += "`IdMovimientoDocumento`" + "=" + IdMovimientoDocumento + ",";
        sentIn += "`IdUsuario` " + "=" + IdUsuario + ",";
        sentIn += "`IdEmpresaSede`" + "=" + IdEmpresaSede + ",";
        sentIn += "`IdEmpresaSedeDes`" + "=" + IdEmpresaSedeDes + ",";

        sentIn += "`IdMovimientoTipo`" + "=" + IdMovimientoTipo + ",";
        sentIn += " `MovCabMovSto`" + "=" + MovCabMovSto + ",";
        sentIn += "`MovCabFec`" + "=" + MovCabFec + " , ";
        
        sentIn += "`MovCabSer` " + "='" + MovCabSer + "' , ";    
        String en="";
        if(MovCabEnv!=null)
                en=new SimpleDateFormat("yyyy-MM-dd").format(MovCabEnv);
        else{
  en ="1990-01-01";
        }
        sentIn += "`MovCabEnv` " + "='" + en + "' , ";
                sentIn += "`MovCabProBoo` " + "='" + getMovCabProBoo() + "' , ";

        sentIn += "`IdMovimientoCabeceraAsociado` " + "=" + IdMovimientoCabeceraAsociado + " , ";    

        sentIn +="`MovCabNum`"  + "='" + MovCabNum + "' where ";

        sentIn +="`IdMovimientoCabecera`"  + "=" + IdMovimientoCabecera;

       System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
    //    System.out.println("guardado");
    }
        public void modificarEST() throws SQLException {

        String sentIn = "update MovimientoCab set  ";
        sentIn += "`IdMovimientoDocumento`" + "=" + IdMovimientoDocumento + ",";
        sentIn += "`IdUsuario` " + "=" + IdUsuario + ",";
        sentIn += "`IdEmpresaSede`" + "=" + IdEmpresaSede + ",";
        sentIn += "`IdMovimientoTipo`" + "=" + IdMovimientoTipo + ",";
        sentIn += " `MovCabMovSto`" + "=" + MovCabMovSto + ",";
        sentIn += "`MovCabFec`" + "=" + MovCabFec + " , ";
        
        sentIn += "`MovCabSer` " + "='" + MovCabSer + "' , ";    
        String en="";
        if(MovCabEnv!=null)
                en=new SimpleDateFormat("yyyy-MM-dd").format(MovCabEnv);
        else{
  en ="1990-01-01";
        }
        sentIn += "`MovCabEnv` " + "='" + en + "' , ";
                sentIn += "`MovCabProBoo` " + "='" + getMovCabProBoo() + "' , ";

        sentIn += "`IdMovimientoCabeceraAsociado` " + "=" + IdMovimientoCabeceraAsociado + " , ";    
        sentIn += "`MovCabEstReg` " + "=" + getMovCabEstReg() + " , ";    

        sentIn +="`MovCabNum`"  + "='" + MovCabNum + "' where ";

        sentIn +="`IdMovimientoCabecera`"  + "=" + IdMovimientoCabecera;

       System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
    //    System.out.println("guardado");
    }

  public int consulta(String nomTabla, String nomAtrib, String buscar, String atrBuscar) throws SQLException
    {
        int id = -1;
        Statement sent2 = cn.createStatement();
        ResultSet rs2 = sent2.executeQuery("select * from "+nomTabla+" WHERE "+nomAtrib+"='" + buscar + "'");

        if (rs2.next()) {
            id = Integer.parseInt(rs2.getString(atrBuscar));
        }
       // System.out.println("--> "+id);
        return id;
        
    }
  public ArrayList<String> consultaCon(String nomTabla,  String atrBuscar) throws SQLException
    {
        ArrayList<String> desu = new ArrayList<String>();
        Statement sent2 = cn.createStatement();
        ResultSet rs2 = sent2.executeQuery("select * from "+nomTabla );

        while  (rs2.next()) {
            desu.add(rs2.getString(atrBuscar));
        }
        return desu;
        
    } 
   public ArrayList<String> consultaCon(String nomTabla,  String atrBuscar,int i ) throws SQLException
    {
        ArrayList<String> desu = new ArrayList<String>();
        Statement sent2 = cn.createStatement();
        ResultSet rs2 = sent2.executeQuery("select * from "+nomTabla );
        int a = 0 ;
        while  (rs2.next()&&a<i) {
            desu.add(rs2.getString(atrBuscar));
            a++;
        }
        return desu;
        
    } 
  public ArrayList<String> consultaCon(String nomTabla,  String atrBuscar,String where) throws SQLException
    {
        ArrayList<String> desu = new ArrayList<String>();
        Statement sent2 = cn.createStatement();
        String sql_cons="select * from "+nomTabla+" "+where;
        //System.out.println(sql_cons);
        ResultSet rs2 = sent2.executeQuery( sql_cons);

        while  (rs2.next()) {
            desu.add(rs2.getString(atrBuscar));
        }
        return desu;
        
    }
    public String generarCodigo(JTextField a)
    {
       GenerarCodigo gc = new GenerarCodigo();
     //  System.out.println(gc.generarID("movimientocab", "IdMovimientocabecera")); 
       a.setText(gc.generarID("movimientocab", "IdMovimientocabecera"));
       return gc.generarID("movimientocab", "IdMovimientocabecera");
    }
  public static void eliminarByAsociado(String idAsociado) throws SQLException {
        String sentIn = "update MovimientoCab set  ";
        sentIn += "MovCabEstReg "+ "=" + 0 + " where ";
        sentIn += "IdMovimientoCabeceraAsociado" + "=" + idAsociado;

      //  System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
       // System.out.println("guardado");
        
    }
    public void eliminar() throws SQLException {
        String sentIn = "update MovimientoCab set  ";
        sentIn += "MovCabEstReg "+ "=" + 0 + " where ";
        sentIn += "IdMovimientoCabecera" + "=" + IdMovimientoCabecera;

      //  System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
       // System.out.println("guardado");
        
    }
  
    public void llenarTabla(JTable tabla, String id,JComboBox combo) {
         String sql_Cons="SELECT * FROM MovimientoCab where MovCabEstReg =1";
        try {
            Statement sent;
            if (id.length() != 0) {

               sql_Cons = "SELECT * FROM MovimientoCAb WHERE"+(String)combo.getSelectedItem()+ "LIKE '%" + id + "%' and MovCabEstReg=1";
            }
          //  System.out.println("prubea ");
            sent = cn.createStatement();
          //  System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            String[] fila = new String[datos];
            while (rs.next()) {
                fila[0] = rs.getString("IdMovimientoCabecera");
                fila[1] = rs.getString("IdMovimientoDocumento");
               fila [1]=sacarDato("MovimientoDocumento", "IdMovimientoDocumento", fila[1],"movdocdes");
                fila[2] = rs.getString("IdUsuario");
               fila [2]=sacarDato("usuario", "IdUsuario", fila[2],"usunom");
                 fila[3] = rs.getString("IdEmpresaSede");
               fila [3]=sacarDato("EmpresaSede", "IdEmpresaSede", fila[3],"empsednom");
                fila[4] = rs.getString("IdEmpresaSedeDes");
               fila [4]=sacarDato("EmpresaSede", "IdEmpresaSede", fila[4],"empsednom");
                fila[5] = rs.getString("IdMovimientoTipo");
               fila [5]=sacarDato("MovimientoTipo", "IdMovimientoTipo", fila[5],"movtipdes");
                fila[6] = rs.getString("MovCabMovSto");
                fila[7] = rs.getString("MovCabFec");
                    
               fila[8]=rs.getString("MovCabSer");
               fila[9]=rs.getString("MovCabNum");
                if(rs.getDate("MovCabEnv")==null)
                    fila[10]="no enviado";
                else{fila [10]="enviado";}

      
             
                model.addRow(fila);
            }
            tabla.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
 public static String sacarDato  (String tabla , String valor, String dato) throws SQLException{
      Statement sent2 = cn.createStatement();
     String de ="select * from "+tabla+" WHERE "+valor+" = " + dato + ""; 
//    System.out.println(de);           
     ResultSet rs2 = sent2.executeQuery(de);
                  if(rs2.next())     
                return  rs2.getString(1);
                  else{ 
                      return ""+0 ;}

 
 }
  public static String sacarDato  (String tabla , String valor, String dato,String sacado) throws SQLException{
      Statement sent2 = cn.createStatement();
     String de ="select * from "+tabla+" WHERE "+valor+" = " + dato + ""; 
  //   System.out.println(de);           
     ResultSet rs2 = sent2.executeQuery(de);
                  if(rs2.next())     
                return  rs2.getString(sacado);
                  else{ 
                      return ""+0 ;}

 
 }
    public void llenarCaja(JComboBox jComboBox1) {
        String str= jComboBox1.getName();
        str = str.substring(7);
        try {
            jComboBox1.removeAllItems();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("select * from "+str);
            ArrayList <String > datos = new ArrayList<String>();
            while (rs.next()) //if(jComrs.getString("DesUniMed"))
            {
                jComboBox1.addItem(rs.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Empresa.class.getName()).log(Level.SEVERE, null, ex);
        }

           
    }
    
    public ArrayList<String>  llenarCaja(JComboBox jComboBox1,String tabla , String dato) throws SQLException{
        String str= jComboBox1.getName();
            ArrayList<String> desu=consultaCon(tabla, dato);
            jComboBox1.removeAllItems();
            for(String de :desu){
               jComboBox1.addItem(de);
            }
            return desu;
        
    }
     public void llenarCaja(JComboBox jComboBox1,String tabla , String dato,int num) throws SQLException{
        String str= jComboBox1.getName();
            ArrayList<String> desu=consultaCon(tabla, dato,num);
            jComboBox1.removeAllItems();
            for(String de :desu){
               jComboBox1.addItem(de);
            }

        
    }
    public ArrayList<String>  llenarCaja(JComboBox jComboBox1,String tabla , String dato,String where) throws SQLException{
        String str= jComboBox1.getName();
            ArrayList<String> desu=consultaCon(tabla, dato,where);
            jComboBox1.removeAllItems();
            for(String de :desu){
               jComboBox1.addItem(de);
            }
            return desu;

        
    }
    public ArrayList<String>  llenarCajaId(JComboBox jComboBox1,String tabla , String dato,String where ,String id ) throws SQLException{
        String str= jComboBox1.getName();
            ArrayList<String> desu=consultaCon(tabla, dato,where);
            jComboBox1.removeAllItems();
            int a  = 0;
            int i = 0 ;
            for(String de :desu){
                if (de.equals(id)){
                a= i;
                }
                i++;
                jComboBox1.addItem(de);
            }
            jComboBox1.setSelectedIndex(a);
            return desu;

        
    }
    public static void main(String args[]) throws  SQLException{
        MovimientoCab macho = new MovimientoCab("'1'", "'1'", "'1'", "'1'", "'1'", "'1'", "'2016-11-22'", "'1'", "'1'", "'1'");
        macho.eliminar();
    }
	public Date getMovCabEnv() {
		return MovCabEnv;
	}
	public void setMovCabEnv(Date movCabEnv) {
		MovCabEnv = movCabEnv;
	}
	public String getIdEmpresaSedeDes() {
		return IdEmpresaSedeDes;
	}
	public void setIdEmpresaSedeDes(String idEmpresaSedeDes) {
		IdEmpresaSedeDes = idEmpresaSedeDes;
	}

    public void llenarCajaOBJ(JComboBox jCSede, String empresasede, String empsednom) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public void modificarAsociado() throws SQLException {
        String sentIn = "update MovimientoCab set  ";
        
        sentIn += "IdMovimientoCabeceraAsociado " + "=" + IdMovimientoCabeceraAsociado + " where ";

        sentIn += "IdMovimientoCabecera" + "=" + IdMovimientoCabecera;

        System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        //    System.out.println("guardado");
    
    }
}
