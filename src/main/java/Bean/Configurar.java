/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import static Kardex.Kardex.cn;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USUARIO
 */
public class Configurar {
    
    private int  idConfigurar ;
    private int idUsuario;
    private int idEmpresaSede;
    private String Color;
    public Configurar(){}
    public Configurar (int id ){
 idConfigurar=id;
        String sql_Cons="SELECT * FROM configurar where idconfigurar ="+id;
        try {
            Statement sent;
          
            sent = cn.createStatement();
            //System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                setColor(rs.getString("color"));
                setIdConfigurar(rs.getInt("idConfigurar"));
                setIdEmpresaSede(rs.getInt("idEmpresaSede"));
                setIdUsuario(rs.getInt("idUsuario"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }    
    }
     public Configurar (int id ,int idUsuario){
 idConfigurar=id;
        String sql_Cons="SELECT * FROM configurar where idUsuario ="+idUsuario;
        try {
            Statement sent;
          
            sent = cn.createStatement();
            //System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                idConfigurar = rs.getInt("idConfigurar");
                setColor(rs.getString("color"));
                setIdConfigurar(rs.getInt("idConfigurar"));
                setIdEmpresaSede(rs.getInt("idEmpresaSede"));
                setIdUsuario(rs.getInt("idUsuario"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }    
    }
    
    public  void  insert (){
        try {
            String sentIn = "INSERT INTO `configurar`( `idUsuario`, `idEmpresasede`, `color`) ";
            sentIn = sentIn + "Values (" +getIdUsuario()+ "," +getIdEmpresaSede() + ","+ getColor() +")";
            System.out.println(sentIn);
            Statement sent = cn.createStatement();
            sent.executeUpdate(sentIn);
            System.out.println("guardado");
        } catch (SQLException ex) {
            Logger.getLogger(Configurar.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    	    }
    public static ArrayList<Configurar> getCongiguracionesPorDato(String columna , String dato){
       ArrayList<Configurar> configurarciones = new ArrayList<Configurar>();
        
        String sql_Cons="SELECT * FROM configurar where "+columna +" ="+dato;
        try {
            
            Statement sent;
          
            sent = cn.createStatement();
           System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                 Configurar actual = new Configurar();
               actual.setColor(rs.getString("color"));
               actual.  setIdConfigurar(rs.getInt("idConfigurar"));
              actual.   setIdEmpresaSede(rs.getInt("idEmpresaSede"));
                 actual.setIdUsuario(rs.getInt("idUsuario"));
             configurarciones.add(actual);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return configurarciones;
    
    
    }
    public void modificar(){
    
        try {
            String sentIn = "    UPDATE `configurar` SET " ;
            
            sentIn += "idusuario" + "=" + getIdUsuario()+ ",";
            sentIn += "idempresasede" + "=" + getIdEmpresaSede()+ ",";
            sentIn += "color" + "=" + getColor() +  " where ";
            sentIn += "idconfigurar" + "=" + getIdConfigurar();
            
            System.out.println(sentIn);
            Statement sent = cn.createStatement();
            sent.executeUpdate(sentIn);
            System.out.println("guardado");
        } catch (SQLException ex) {
            Logger.getLogger(Configurar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void eliminar(){
    
    
    }

    public int getIdConfigurar() {
        return idConfigurar;
    }

    public void setIdConfigurar(int idConfigurar) {
        this.idConfigurar = idConfigurar;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdEmpresaSede() {
        return idEmpresaSede;
    }

    public void setIdEmpresaSede(int idEmpresaSede) {
        this.idEmpresaSede = idEmpresaSede;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }
    
}
