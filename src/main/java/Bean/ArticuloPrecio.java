package Bean;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import Conexion.Mysql;
import Maestros.*;

/**
 *
 * @author Equipo EPIS
 */
public class ArticuloPrecio implements Atributos {

    public static Connection cn = Mysql.getConection();
    private int IdArticuloPrecio;
    private int IdArticulo;
    private int IdLista;
    private double ArtPreDes=-1;
    private int ArtPreEstReg = 1;

    int datos = 7;//Para la JTable

    String[] atributos = {"Codigo", "CodigoArticulo", "NombreArticulo", "CodigoLista", "NombreLista", "Precio","Estado Registro"};

    String sql_Cons="SELECT * FROM ARTICULOPRECIO WHERE "+ARTPREESTREG+"=1";
    private MiModel model = new MiModel(null, atributos);

    public ArticuloPrecio(int idArt, int idList, double desLis) {
        IdArticulo=idArt;
        IdLista=idList;
        ArtPreDes=desLis;
    }
  public ArticuloPrecio(int idArt, int idList) {
        IdArticulo=idArt;
        IdLista=idList;
        String sql_Cons="SELECT * FROM articuloPrecio where idArticulo = "+idArt +" and idlista = "+ idList+" and "+ARTPREESTREG+"=1";
        try {
            Statement sent;
          
            sent = cn.createStatement();
            System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            if (rs.next()) {
                setArtPreDes(rs.getDouble("ArtPreDes"));
                setArtPreEstReg(rs.getInt("ArtPreEstReg"));
                setIdArticulo(rs.getInt("IdArticulo"));
                setIdArticuloPrecio(rs.getInt("IdArticuloPrecio"));
                setIdLista(rs.getInt("IdLista"));
                System.err.println("dentro while ");
            }else{
                setArtPreDes(-1);
            } 
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArticuloPrecio() {
    }

    public void insertar() throws SQLException {
        String sentIn = "INSERT INTO ArticuloPrecio (" + IDARTICULO + "," + IDLISTA + "," + ARTPREDES + "," + ARTPREESTREG + ")";
        sentIn = sentIn + "Values (" + this.getIdArticulo() + "," + this.getIdLista() + "," + this.getArtPreDes() + "," + this.getArtPreEstReg() + ")";
        System.out.println(sentIn);//Imprimir
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("Guardado");
    }

    public void modificar() throws SQLException {

        String sentUp = "UPDATE ARTICULOPRECIO SET  ";
        sentUp += IDARTICULO + "=" + getIdArticulo() + ",";
        sentUp += IDLISTA + "=" + getIdLista() + ",";
        sentUp += ARTPREDES + "=" + getArtPreDes() + ",";
        sentUp += ARTPREESTREG + "=" + getArtPreEstReg() + " WHERE ";
        sentUp += IDARTICULOPRECIO + "=" + this.getIdArticuloPrecio();

        System.out.println(sentUp);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentUp);
        System.out.println("Modificado");
    }

    public void eliminar() throws SQLException {
        String sentEl = "UPDATE ARTICULOPRECIO SET  ";
        sentEl += ARTPREESTREG + "=" + 0 + " WHERE ";
        sentEl += IDARTICULOPRECIO + "=" + getIdArticuloPrecio();

        System.out.println(sentEl);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentEl);
        System.out.println("Eliminado");
    }

    public void llenarTabla(JTable tabla, String id) {
        try {
            System.out.println("-->"+id);
             Statement sent0 = cn.createStatement();
             ResultSet rs0 = sent0.executeQuery("select * from ARTICULO WHERE "+ARTNOM + " LIKE '%" + id + "%' and " + ARTESTREG + "=1" );
             int idL=-1;
             if (rs0.next()){
                idL = Integer.parseInt(rs0.getString(IDARTICULO));
             }
          
            Statement sent;
            if (id.length() != 0) {
                sql_Cons = "SELECT * FROM ARTICULOPRECIO WHERE " + IDARTICULO + " LIKE '%" + idL + "%' and " + ARTPREESTREG + "=1";
                System.out.println(sql_Cons);
            }
            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sql_Cons);

            String[] fila = new String[datos];
            while (rs.next()) {
                fila[0] = rs.getString(IDARTICULOPRECIO);
                fila[1] = rs.getString(IDARTICULO);
                Statement sent2 = cn.createStatement();
                ResultSet rs2 = sent2.executeQuery("select * from ARTICULO WHERE IdArticulo='" + fila[1] + "'");
                
                if (rs2.next()) {
                    fila[2] = rs2.getString(ARTNOM);
                }
                
                fila[3] = rs.getString(IDLISTA);
                Statement sent3 = cn.createStatement();
                ResultSet rs3 = sent3.executeQuery("select * from LISTA WHERE IdLista='" + fila[3] + "'");
                
                if (rs3.next()) {
                    fila[4] = rs3.getString(LISTDES);
                }
                
                fila[5] = rs.getString(ARTPREDES);
                fila[6] = "ACTIVO";
                model.addRow(fila);
            }
            tabla.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void llenarCajaArt(JComboBox jComboBox1) {

        try {
            jComboBox1.removeAllItems();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM ARTICULO WHERE "+ARTESTREG+"=1");
            while (rs.next()) //if(jComrs.getString("DesUniMed"))
            {
 
                jComboBox1.addItem(rs.getString(ARTNOM));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ArticuloPrecio.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    /***
     * 
    * @param jComboBox1
     * @param conPrecio
     * @param lista
     * @see  si quieres que obtener los q  no tienen precio el valor de conprecio es false 
     */
  public void llenarCajaArt(JComboBox jComboBox1 , boolean conPrecio, int  lista ) {

        try {
            jComboBox1.removeAllItems();
            Statement st = cn.createStatement();
            
            ResultSet rs = st.executeQuery("SELECT * FROM ARTICULO WHERE "+ARTESTREG+"=1");
            while (rs.next()) //if(jComrs.getString("DesUniMed"))
            {
                ArticuloPrecio artPre = new ArticuloPrecio( rs.getInt("idArticulo"),lista);
                if ( !conPrecio && artPre.getArtPreDes()==-1)
                jComboBox1.addItem(rs.getString(ARTNOM));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ArticuloPrecio.class.getName()).log(Level.SEVERE, null, ex);
        }

    }    
     public void llenarCajaList(JComboBox jComboBox1) {

        try {
            jComboBox1.removeAllItems();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM LISTA WHERE "+LISTESTREG+"=1");
            while (rs.next()) //if(jComrs.getString("DesUniMed"))
            {
                jComboBox1.addItem(rs.getString(LISTDES));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ArticuloPrecio.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public int consulta(String nomTabla, String nomAtrib, String buscar, String atrBuscar) throws SQLException
    {
        int id = -1;
        Statement sent2 = cn.createStatement();
        ResultSet rs2 = sent2.executeQuery("select * from "+nomTabla+" WHERE "+nomAtrib+"='" + buscar + "'");

        if (rs2.next()) {
            id = Integer.parseInt(rs2.getString(atrBuscar));
        }
        System.out.println("--> "+id);
        return id;
        
    }
    public int getIdArticuloPrecio() {
        return IdArticuloPrecio;
    }

    public void setIdArticuloPrecio(int IdArticuloPrecio) {
        this.IdArticuloPrecio = IdArticuloPrecio;
    }

    public int getIdArticulo() {
        return IdArticulo;
    }

    public void setIdArticulo(int IdArticulo) {
        this.IdArticulo = IdArticulo;
    }

    public int getIdLista() {
        return IdLista;
    }

    public void setIdLista(int IdLista) {
        this.IdLista = IdLista;
    }

    public double getArtPreDes() {
        return ArtPreDes;
    }

    public void setArtPreDes(double ArtPreDes) {
        this.ArtPreDes = ArtPreDes;
    }

    public int getArtPreEstReg() {
        return ArtPreEstReg;
    }

    public void setArtPreEstReg(int ArtPreEstReg) {
        this.ArtPreEstReg = ArtPreEstReg;
    }
    
}
