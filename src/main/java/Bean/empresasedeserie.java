/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package     Bean;

import static     Maestros.ArticuloM.cn;
import Maestros.GenerarCodigo;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USUARIO
 */
public class empresasedeserie {
   private String idempresasedeserie;
   private String idEmpresaSede;
   private String idMovimientoDocumento ;
   private String empsedsernum;
   private String empsedserser;
   private int  empsedserEstReg=1;
   public empresasedeserie (String id ){
    	idempresasedeserie=id;
        String sql_Cons="SELECT * FROM empresasedeserie where idempresasedeserie ="+id;
        try {
            Statement sent;
          
            sent = cn.createStatement();
            System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                setEmpsedsernum(rs.getString("empsedsernum"));
                setEmpsedserser(rs.getString("empsedserser"));
                setIdempresasedeserie(rs.getString("idempresasedeserie"));
                    setEmpsedserEstReg(rs.getInt("empsedserestreg"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    	
    	
    }
     public empresasedeserie (String sede , String idDocumento  ){
        String sql_Cons="SELECT * FROM empresasedeserie where idempresasede ="+sede +" and idMovimientoDocumento = "+ idDocumento;
        try {
            Statement sent;
          
            sent = cn.createStatement();
            System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                setEmpsedsernum(rs.getString("empsedsernum"));
                setEmpsedserser(rs.getString("empsedserser"));
                setIdempresasedeserie(rs.getString("idempresasedeserie"));
                setIdMovimientoDocumento(rs.getString("idmovimientodocumento"));
                setIdEmpresaSede(rs.getString("idempresasede"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    	 if (getEmpsedsernum()==null){
            try {
                setIdMovimientoDocumento(""+idDocumento);
                setEmpsedserser("000"+idDocumento);
                setEmpsedsernum("000001");
                setIdEmpresaSede(""+sede);
                setIdempresasedeserie(empresasedeserie.generarCodigo());
                insertar();
            } catch (SQLException ex) {
                Logger.getLogger(empresasedeserie.class.getName()).log(Level.SEVERE, null, ex);
            }
                    }
    	
    	
    }

    public empresasedeserie() {
    }
   public void insertar() throws SQLException, SQLException{
      String sentIn = "INSERT INTO EmpresaSedeSerie ( idempresasedeserie,  idEmpresaSede, idMovimientoDocumento ,empsedsernum, empsedserser, empsedserEstReg )";
        sentIn = sentIn + "Values (" +getIdempresasedeserie() + "," +getIdEmpresaSede() + ","+ getIdMovimientoDocumento()  +","+getEmpsedsernum()+","+ getEmpsedserser()+","+getEmpsedserEstReg() +")";
        System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("guardado");
    
   
   
   }
   
       public void modificar() throws SQLException {

        String sentUp = "UPDATE Empresasedeserie  SET  ";
        sentUp += "idempresasedeserie " + " = " + getIdempresasedeserie()+ ",";
                sentUp += "idempresasede " + " = " + getIdEmpresaSede()+ ",";
        sentUp += "idMovimientoDocumento " + " = " + getIdMovimientoDocumento()+ ",";
        sentUp += "empsedserser" + " = '" + getEmpsedserser()+"',";

        sentUp += "empsedsernum" + " = '" + getEmpsedsernum()+"',";
                sentUp += "empsedserEstReg" + " = '" + getEmpsedserEstReg()+"'";
           
        sentUp +=  " where ";
        sentUp+= " idempresasedeserie = "+getIdempresasedeserie();
      

        System.out.println(sentUp);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentUp);
        System.out.println("Modificado");
    }
   
   
       public void Eliminar() throws SQLException {

        String sentUp = "UPDATE Empresasedeserie  SET  ";
       
                sentUp += "empsedserEstReg" + " = '" + 0+"'";
           
        sentUp +=  " where ";
        sentUp+= " idempresasedeserie = "+getIdempresasedeserie();
      

        System.out.println(sentUp);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentUp);
        System.out.println("Modificado");
    }
   
   
    public String getIdempresasedeserie() {
        return idempresasedeserie;
    }

    public String getEmpsedsernum() {
        return empsedsernum;
    }

    public String getEmpsedserser() {
        return empsedserser;
    }

    public void setIdempresasedeserie(String idempresasedeserie) {
        this.idempresasedeserie = idempresasedeserie;
    }

    public void setEmpsedsernum(String empsedsernum) {
        this.empsedsernum = empsedsernum;
    }

    public void setEmpsedserser(String empsedserser) {
        this.empsedserser = empsedserser;
    }

    public String getIdEmpresaSede() {
        return idEmpresaSede;
    }

    public void setIdEmpresaSede(String idEmpresaSede) {
        this.idEmpresaSede = idEmpresaSede;
    }

    public String getIdMovimientoDocumento() {
        return idMovimientoDocumento;
    }

    public void setIdMovimientoDocumento(String idMovimientoDocumento) {
        this.idMovimientoDocumento = idMovimientoDocumento;
    }

    public int getEmpsedserEstReg() {
        return empsedserEstReg;
    }

    public void setEmpsedserEstReg(int empsedserEstReg) {
        this.empsedserEstReg = empsedserEstReg;
    }
  public static String generarCodigo(){
        GenerarCodigo gc = new GenerarCodigo();
     //  System.out.println(gc.generarID("movimientocab", "IdMovimientocabecera")); 
    
       return gc.generarID("empresasedeserie", "idempresasedeserie");
       }  

    public void actualizar() {
        
        
       try {
           
           int numero = Integer.parseInt(empsedsernum);
           numero ++;
           empsedsernum=numero+"";
           modificar();
       } catch (SQLException ex) {
           Logger.getLogger(empresasedeserie.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
}
