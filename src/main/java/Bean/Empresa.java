package Bean;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import Conexion.Mysql;
import Maestros.*;
import java.util.ArrayList;

/**
 *
 * @author Equipo EPIS
 */
public class Empresa implements Atributos {

    public static Connection cn = Mysql.getConection();

    public static ArrayList<Empresa> getEmpresasForDato(String col, String dato) {
        ArrayList<Empresa>  retorno = new ArrayList<Empresa >();

        String sql_Cons="SELECT * FROM Empresa where "+col+"="+dato;
        try {
            Statement sent;
          
           // System.out.println("prubea ");
            sent = cn.createStatement();
           // System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                Empresa emp = new Empresa();
               emp.setEmpEstReg(Integer.parseInt(rs.getString(EMPESTREG)));
              emp. setEmpNom(rs.getString(EMPNOM));
               emp.setEmpPer(rs.getString(EMPPER));
               emp.setEmpRUC(rs.getString(EMPRUC));
               emp.setEmpTel(rs.getString(EMPTEL));
               emp.setIdEmpGrup(rs.getString(IDEMPRESAGRUPO));
               emp.setIdEmpresa(rs.getString(IDEMPRESA));
             emp.  setIdDocumento(rs.getString(IDDOCUMENTO));
             retorno.add(emp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    return retorno ; 
    
    }
     public static ArrayList<Empresa> getEmpresasForDatoLike (String col, String dato) {
        ArrayList<Empresa>  retorno = new ArrayList<Empresa >();
       // if (dato.length()==0)
        String sql_Cons="SELECT * FROM Empresa where "+col+" like '%"+dato+"%'";
        
        
            
            try {
            Statement sent;
          
           // System.out.println("prubea ");
            sent = cn.createStatement();
            System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                Empresa emp = new Empresa();
               emp.setEmpEstReg(Integer.parseInt(rs.getString(EMPESTREG)));
              emp. setEmpNom(rs.getString(EMPNOM));
               emp.setEmpPer(rs.getString(EMPPER));
               emp.setEmpRUC(rs.getString(EMPRUC));
               emp.setEmpTel(rs.getString(EMPTEL));
               emp.setIdEmpGrup(rs.getString(IDEMPRESAGRUPO));
               emp.setIdEmpresa(rs.getString(IDEMPRESA));
             emp.  setIdDocumento(rs.getString(IDDOCUMENTO));
             retorno.add(emp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    return retorno ; 
    
    }
 public static ArrayList<Empresa> getEmpresasForDatoIzq (String col, String dato) {
        ArrayList<Empresa>  retorno = new ArrayList<Empresa >();
       // if (dato.length()==0)
        String sql_Cons="SELECT * FROM Empresa where "+col+" like '"+dato+"%'";
        
        
            
            try {
            Statement sent;
          
           // System.out.println("prubea ");
            sent = cn.createStatement();
            System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                Empresa emp = new Empresa();
               emp.setEmpEstReg(Integer.parseInt(rs.getString(EMPESTREG)));
              emp. setEmpNom(rs.getString(EMPNOM));
               emp.setEmpPer(rs.getString(EMPPER));
               emp.setEmpRUC(rs.getString(EMPRUC));
               emp.setEmpTel(rs.getString(EMPTEL));
               emp.setIdEmpGrup(rs.getString(IDEMPRESAGRUPO));
               emp.setIdEmpresa(rs.getString(IDEMPRESA));
             emp.  setIdDocumento(rs.getString(IDDOCUMENTO));
             retorno.add(emp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    return retorno ; 
    
    }

  
    private String IdEmpresa;
    private String IdEmpGrup;
    private String EmpRUC;
    private String EmpNom;
    private String EmpTel;
    private String EmpPer;
    private String IdDocumento;

    public String getEmpPer() {
        return EmpPer;
    }
    
     public void insertarId() throws SQLException {
        String sentIn = "INSERT INTO Empresa ("+ IDEMPRESA+ ","+ IDEMPRESAGRUPO + "," + EMPRUC + "," + EMPNOM + "," + EMPTEL +"," +EMPPER+ "," +"iddocumento"+ ","+ EMPESTREG + ")";
        sentIn = sentIn + "Values (" + getIdEmpresa()+ ","+ getIdEmpGrup() + "," + getEmpRUC() + "," + getEmpNom() + "," + getEmpTel() + "," + getEmpPer() + "," + getIdDocumento()+ "," +getEmpEstReg() + ")";
        //System.out.println(sentIn);//Imprimir
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
       // System.out.println("Guardado");
    }


    public void setEmpPer(String EmpPer) {
        this.EmpPer = EmpPer;
    }

    public String getIdDocumento() {
        return IdDocumento;
    }

    public void setIdDocumento(String IdDocumento) {
        this.IdDocumento = IdDocumento;
    }
    private int  EmpEstReg = 1;

    int datos = 9;//Para la JTable

    String[] atributos = {"Codigo", "Codigo Grupo", "NombreGrupo", "RUC/DNI", "Nombre", "Telefono", "Persona","IdDocumento" ,"Estado Registro"};

    String sql_Cons;//="SELECT * FROM EMPRESA WHERE "+EMPESTREG+"=1";
    private MiModel model = new MiModel(null, atributos);

    public Empresa(String grupEmp, String rucEmp, String nomEmp, String telEmp, String perEmp, String idDoc) {
        IdEmpGrup = grupEmp;
        EmpRUC = rucEmp;
        EmpNom = nomEmp;
        EmpTel = telEmp;
        EmpPer=perEmp;
        IdDocumento=idDoc;
    }

    public Empresa() {
    }

    public void insertar() throws SQLException {
        String sentIn = "INSERT INTO Empresa (" + IDEMPRESAGRUPO + "," + EMPRUC + "," + EMPNOM + "," + EMPTEL + "," + EMPPER + "," + IDDOCUMENTO+","+EMPESTREG + ")";
        sentIn = sentIn + "Values (" + getIdEmpGrup() + "," + getEmpRUC() + "," + getEmpNom() + "," + getEmpTel() + "," + getEmpPer() + "," +getIdDocumento()+","+ getEmpEstReg() + ")";
       // System.out.println(sentIn);//Imprimir
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("Guardado");
    }

    public void modificar() throws SQLException {

        String sentUp = "UPDATE Empresa SET  ";
        sentUp += IDEMPRESAGRUPO + "=" + getIdEmpGrup() + ",";
        sentUp += EMPRUC + "=" + getEmpRUC() + ",";
        sentUp += EMPNOM + "=" + getEmpNom() + ",";
        sentUp += EMPTEL + "=" + getEmpTel() + ",";
        sentUp += EMPPER + "=" + getEmpPer() + ",";
        sentUp += IDDOCUMENTO + "=" + getIdDocumento() + ",";
        sentUp += EMPESTREG + "=" + getEmpEstReg() + " WHERE ";
        sentUp += IDEMPRESA + "=" + getIdEmpresa();

     //   System.out.println(sentUp);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentUp);
        System.out.println("Modificado");
    }

    public void eliminar() throws SQLException {
        String sentEl = "UPDATE Empresa SET  ";
        sentEl += EMPESTREG + "=" + 0 + " WHERE ";
        sentEl += IDEMPRESA + "=" + getIdEmpresa();

      //  System.out.println(sentEl);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentEl);
        System.out.println("Eliminado");
    }

    public void llenarTabla(JTable tabla, String id, String idEmp) {
        try {
            Statement sent;
            //if (id.length() != 0) {
                sql_Cons = "SELECT * FROM Empresa WHERE " + EMPNOM + " LIKE '%" + id + "%' and " + EMPESTREG + "=1"
                +" and "+ IDEMPRESAGRUPO +"="+idEmp;
                //System.out.println(sql_Cons);
            //}
            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sql_Cons);

            String[] fila = new String[datos];
            while (rs.next()) {
                fila[0] = rs.getString(IDEMPRESA);
                fila[1] = rs.getString(IDEMPRESAGRUPO);
                Statement sent2 = cn.createStatement();
                ResultSet rs2 = sent2.executeQuery("select * from empresagrupo WHERE IdEmpresaGrupo='" + fila[1] + "'");
                //DUDOSO
                if (rs2.next()) {
                    fila[2] = rs2.getString(EMPGRUNOM);
                }
                fila[3] = rs.getString(EMPRUC);
                fila[4] = rs.getString(EMPNOM);
                fila[5] = rs.getString(EMPTEL);
                fila[6] = rs.getString(EMPPER);
                Statement sent3 = cn.createStatement();
                ResultSet rs3 = sent3.executeQuery("select * from documento WHERE IdDocumento='" + rs.getString(IDDOCUMENTO) + "'");
                //DUDOSO
                if (rs3.next()) {
                    fila[7] = rs3.getString(DOCDES);
                }
                                
                //fila[7] = rs.getString(EMPESTREG);
                fila[8] = "ACTIVO";
                model.addRow(fila);
            }
            tabla.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void llenarCaja(JComboBox jComboBox1) {

        try {
            jComboBox1.removeAllItems();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM EmpresaGrupo WHERE "+EMPGRUESTREG+"=1");
            while (rs.next()) //if(jComrs.getString("DesUniMed"))
            {
                jComboBox1.addItem(rs.getString(EMPGRUNOM));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Empresa.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public void llenarCajaDocumento(JComboBox jComboBox1) {

        try {
            jComboBox1.removeAllItems();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM DOCUMENTO WHERE "+DOCESTREG+"=1");
            while (rs.next()) //if(jComrs.getString("DesUniMed"))
            {
                jComboBox1.addItem(rs.getString(DOCDES));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Empresa.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public String consulta(String nomTabla, String nomAtrib, String buscar, String atrBuscar) throws SQLException
    {
        String id = ""+-1;
        Statement sent2 = cn.createStatement();
        ResultSet rs2 = sent2.executeQuery("select * from "+nomTabla+" WHERE "+nomAtrib+"='" + buscar + "'");

        if (rs2.next()) {
            id = rs2.getString(atrBuscar);
        }
        return id;
    }

    public void setIdEmpresa(String IdEmpresa) {
        this.IdEmpresa = IdEmpresa;
    }

    public String getIdEmpresa() {
        return IdEmpresa;
    }

    public String getIdEmpGrup() {
        return IdEmpGrup;
    }

    public void setIdEmpGrup(String IdEmpGrup) {
        this.IdEmpGrup = IdEmpGrup;
    }

    public String getEmpRUC() {
        return EmpRUC;
    }

    public void setEmpRUC(String EmpRUC) {
        this.EmpRUC = EmpRUC;
    }

    public String getEmpNom() {
        return EmpNom;
    }

    public void setEmpNom(String EmpNom) {
        this.EmpNom = EmpNom;
    }

    public String getEmpTel() {
        return EmpTel;
    }

    public void setEmpTel(String EmpTel) {
        this.EmpTel = EmpTel;
    }

    

    public int  getEmpEstReg() {
        return EmpEstReg;
    }

    public void setEmpEstReg(int EmpEstReg) {
        this.EmpEstReg = EmpEstReg;
    }
    public void llenarCajaMismo(JComboBox jComboBox1) {
        try {
            jComboBox1.removeAllItems();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM Empresa WHERE "+EMPESTREG+"=1");
            while (rs.next()) //if(jComrs.getString("DesUniMed"))
            {
                jComboBox1.addItem(rs.getString(EMPNOM));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Empresa.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    public Empresa (String id ){
    	IdEmpresa=id;
        String sql_Cons="SELECT * FROM Empresa where idEmpresa ="+id;
        try {
            Statement sent;
          
           // System.out.println("prubea ");
            sent = cn.createStatement();
           // System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
               setEmpEstReg(Integer.parseInt(rs.getString(EMPESTREG)));
               setEmpNom(rs.getString(EMPNOM));
               setEmpPer(rs.getString(EMPPER));
               setEmpRUC(rs.getString(EMPRUC));
               setEmpTel(rs.getString(EMPTEL));
               setIdEmpGrup(rs.getString(IDEMPRESAGRUPO));
               setIdEmpresa(rs.getString(IDEMPRESA));
               setIdDocumento(rs.getString(IDDOCUMENTO));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

}
