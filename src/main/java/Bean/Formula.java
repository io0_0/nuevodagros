/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.util.ArrayList;

/**
 *
 * @author USUARIO
 */
public class Formula {
    public FormulaCabecera cabecera ;
    public  ArrayList<FormulaDetalle> detalles;
    public static Formula getFormulaID(int id ){
        Formula f = new Formula();
        f.cabecera = new FormulaCabecera(id);
        f.detalles = FormulaDetalle.getFormulaDetalleByCabecera(id);
        return f ; 
    }
}
