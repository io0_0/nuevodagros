package Bean;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import static     GUI.ArticuloGUI.cn;
import Maestros.GenerarCodigo;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 *
 * @author USUARIO
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MovimientoDet implements Serializable{
       static  String[] atributos = {"Codigo", "Codigo cabecera", "Codigo articulo", "MovIngreso", "Cantidad ", "Precio Total", "Costo Total","IGV"};

        private static DefaultTableModel model = new DefaultTableModel(null, atributos);

   static int datos =9;
public static Date sumarRestarDiasFecha(Date fecha, int dias){
 
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(fecha); // Configuramos la fecha que se recibe
      calendar.add(Calendar.DAY_OF_YEAR, dias);  // numero de días a añadir, o restar en caso de días<0
 
      return calendar.getTime(); // Devuelve el objeto Date con los nuevos días añadidos

 }
    public static double getPrecioPromedio(String idArticulo, String idEmpresaSede) {
           
        try {
               Statement sent;
               Date d = new Date();
               Date d1 =sumarRestarDiasFecha(d, -30);
               SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
               String sql = "SELECT (SUM(movimientodet.MovDetCosTot)-SUM(movimientodet.MovDetPreTot))/ SUM(movimientodet.MovDetCan *( movimientocab.IdMovimientoTipo*2-3 )*-1)   as total FROM `movimientodet`  inner join movimientocab on movimientodet.IdMovimientoCabecera = movimientocab.IdMovimientoCabecera WHERE movimientocab.IdEmpresaSede="+ idEmpresaSede +" and movimientodet.IdArticulo ="+idArticulo+"  and movimientodet.MovDetEstReg=1 "
                       +    "and movimientodet.MovDetPreTot is not null and movimientodet.MovDetCan is not null ";
               sent = cn.createStatement();
              // System.out.println(sql);
              // JOptionPane.showMessageDialog(null, sql);
               ResultSet rs = sent.executeQuery(sql);
               double rpta=0;
               while (rs.next()) {
                   rpta=rs.getDouble("total");
               }
               return rpta ;
           } catch (SQLException ex) {
               Logger.getLogger(MovimientoDet.class.getName()).log(Level.SEVERE, null, ex);
           }
           return -1;
    }

 
   
   private String IdMovimientoDetalle;
   private String IdMovimientoCabecera;
   private String IdArticulo;
   private String MovIngreso;
   private String MovDetCan;
   private String MovDetPreTot;
   private String MovDetCosTot;

   private String MovDetIgv;
   private String MovDetEstReg=1+"";
    
    public MovimientoDet() {
    
    }
    
    public MovimientoDet(MovimientoDet de) {
             setIdMovimientoCabecera(de.getIdMovimientoCabecera());
              setIdArticulo(de.getIdArticulo());
              setMovDetCan(de.getMovDetCan());
               setMovDetCosTot(de.getMovDetCosTot());
               setMovDetEstReg(de.getMovDetEstReg());
              setMovIngreso(de.getMovIngreso());
              setMovDetIgv(de.getMovDetIgv());
             setMovDetPreTot(de.getMovDetPreTot());
           
     }
    public MovimientoDet(String id ){
    
        setIdMovimientoDetalle(id);
         String sql_Cons="SELECT * FROM Movimientodet where IdMovimientodetalle ="+id;
      try {
          Statement sent;
        
        //  System.out.println("prubea ");
          sent = cn.createStatement();
      //    System.out.println(sql_Cons);
          ResultSet rs = sent.executeQuery(sql_Cons);
          
          while (rs.next()) {
              setIdMovimientoCabecera(rs.getString("IdMovimientoCabecera"));
              setIdArticulo(rs.getString("IdArticulo"));
              setMovDetCan(rs.getString("movdetcan"));
               setMovDetCosTot(rs.getString("movdetcostot"));
               setMovDetEstReg(rs.getString("movdetestreg"));
              setMovIngreso(rs.getString("movingreso"));
              setMovDetIgv(rs.getString("Movdetigv"));
             setMovDetPreTot(rs.getString("Movdetpretot"));
           
           
          }
      } catch (Exception e) {
          e.printStackTrace();
      }
      
    
    
    }
    public String[] getAtributos() {
        return atributos;
    }
    public static ArrayList<MovimientoDet> getByIdCabecera(String id ) throws SQLException{
    Statement sent;
		String sql_Cons = "Select * from MovimientoDet  where idMovimientoCabecera= "+id + " and MovDetEstReg = 1";
	//	System.out.println(sql_Cons);
		sent = cn.createStatement();
		ResultSet rs = sent.executeQuery(sql_Cons);
		ArrayList<MovimientoDet> det = new ArrayList<MovimientoDet>();
		while (rs.next()) {
			MovimientoDet temo = new MovimientoDet();
			temo.setIdMovimientoDetalle(rs.getString("IdMovimientoDetalle"));
			temo.setIdMovimientoCabecera(rs.getString("IdMovimientoCabecera"));
			temo.setIdArticulo(rs.getString("IdArticulo"));
			temo.setMovIngreso(rs.getString("MovIngreso"));
			temo.setMovDetCan(rs.getString("MovDetCan"));
			temo.setMovDetPreTot(rs.getString("MovDetPreTot"));
                        temo.setMovDetCosTot(rs.getString("MovDetCosTot"));
			temo.setMovDetIgv(rs.getString("MovDetIgv"));
			temo.setMovDetEstReg(rs.getString("MovDetEstReg"));
		//	System.out.println(temo);

			if(Integer.parseInt(temo.getMovDetEstReg())==1){
				det.add(temo);
			}
     
		}
           return det;

    
    }

    public static int getDatos() {
        return datos;
    }

    public String getIdMovimientoDetalle() {
        return IdMovimientoDetalle;
    }

    public String getIdMovimientoCabecera() {
        return IdMovimientoCabecera;
    }

    public String getIdArticulo() {
        return IdArticulo;
    }

    public String getMovIngreso() {
        return MovIngreso;
    }

    public String getMovDetCan() {
        return MovDetCan;
    }

    public String getMovDetPreTot() {
        return MovDetPreTot;
    }

    public String getMovDetIgv() {
        return MovDetIgv;
    }

    public String getMovDetEstReg() {
        return MovDetEstReg;
    }
    
 
 public MovimientoDet(String []a ){
 
  IdMovimientoDetalle=a[0];
  IdMovimientoCabecera=a[1];
  IdArticulo=a[2];
  MovIngreso=a[3];
  MovDetCan=a[4];
  MovDetPreTot=a[5];
  MovDetIgv=a[6];
  MovDetEstReg=a[7];

 }
 public static String generarCodigo()
    {
       GenerarCodigo gc = new GenerarCodigo();
     //  System.out.println(gc.generarID("movimientocab", "IdMovimientocabecera")); 
       return gc.generarID("movimientoDet", "IdMovimientoDetalle");
    }
  public void insertar() throws SQLException {
    String sentIn="INSERT INTO `movimientodet` (`IdMovimientoDetalle`, `IdMovimientoCabecera`, `IdArticulo`, `MovIngreso`, `MovDetCan`, `MovDetPreTot`,`MovDetCosTot`, `MovDetIgv`, `MovDetEstReg`) VALUES(";
    sentIn=sentIn+getIdMovimientoDetalle()+"," +IdMovimientoCabecera+"," +IdArticulo+"," +MovIngreso+"," +MovDetCan+"," +MovDetPreTot+","+MovDetCosTot+","+MovDetIgv+"," +MovDetEstReg+");";  
   //System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
      //  System.out.println("guardado");
    }
  public String insertarID()throws SQLException{
      String codigo = generarCodigo();
      setIdMovimientoDetalle(codigo);
      insertar();
      return codigo;
  }
    public MovimientoDet(String IdMovimientoDetalle, String IdMovimientoCabecera, String IdArticulo, String MovIngreso, String MovDetCan, String MovDetPreTot, String MovDetCosTot, String MovDetIgv, String MovDetEstReg) {
        this.IdMovimientoDetalle = IdMovimientoDetalle;
        this.IdMovimientoCabecera = IdMovimientoCabecera;
        this.IdArticulo = IdArticulo;
        this.MovIngreso = MovIngreso;
        this.MovDetCan = MovDetCan;
        this.MovDetPreTot = MovDetPreTot;
        this.MovDetCosTot = MovDetCosTot;
        this.MovDetIgv = MovDetIgv;
        this.MovDetEstReg = MovDetEstReg;
    }

    public void modificar() throws SQLException {

        String sentIn = "update MovimientoDet  set  ";
        sentIn += " `IdMovimientoCabecera`" + "=" + IdMovimientoCabecera + ",";
        sentIn += "  `IdArticulo`" + "=" + IdArticulo + ",";
        sentIn += " `MovIngreso`" + "=" + MovIngreso + ",";
        sentIn += "  `MovDetCan`" + "=" + MovDetCan + ",";
        sentIn += " `MovDetPreTot`" + "=" + MovDetPreTot + " , ";
        sentIn += " `MovDetCosTot`" + "=" + MovDetCosTot + " , ";

        sentIn += " `MovDetIgv` " + "=" + MovDetIgv + " , ";
        sentIn +=" `MovDetEstReg` "  + "=" + MovDetEstReg+ " where ";

        sentIn +=" `IdMovimientoDetalle` "  + "=" + IdMovimientoDetalle;

     //   System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
      //  System.out.println("guardado");
    }

    public void eliminar() throws SQLException {
        String sentIn = "update MovimientoDet set  ";
        sentIn += "MovDetEstReg "+ "=" + 0 + " where ";
        sentIn += "IdMovimientoDetalle" + "=" + IdMovimientoDetalle;

      //  System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
     //   System.out.println("guardado");
        
    }
    public static String sacarDato  (String tabla , String valor, String dato) throws SQLException{
      Statement sent2 = cn.createStatement();
      String desu ="select * from "+tabla+" WHERE "+valor+"='" + dato + "'";
               // System.out.println(desu);        
 String resp="";
      ResultSet rs2 = sent2.executeQuery(desu);
      if (rs2.next()){
     resp= rs2.getString(1);
      }
      else{
      resp="0";
      }
                return  resp;

    }
    public static  void llenarTabla(JTable tabla, String id,JComboBox combo) {
         String sql_Cons="SELECT * FROM MovimientoCab where MovCabEstReg =1";
        try {
            Statement sent;
            if (id.length() != 0) {

               sql_Cons = "SELECT * FROM MovimientoCAb WHERE"+(String)combo.getSelectedItem()+ "LIKE '%" + id + "%' and MovCabEstReg=1";
            }

            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            String[] fila = new String[datos];
            while (rs.next()) {
               fila[0] = rs.getString("IdMovimientoDetalle");
                fila[1] = rs.getString("IdMovimientoCabecera");
               fila [1]=sacarDato("MovimientoDocumento", "IdMovimientoDocumento", fila[1]);
                fila[2] = rs.getString("IdArticulo");
               fila [2]=sacarDato("usuario", "IdUsuario", fila[2]);
                 fila[3] = rs.getString("MovIngreso");
               fila [3]=sacarDato("EmpresaSede", "IdEmpresaSede", fila[3]);
                fila[4] = rs.getString("MovDetCan");
               fila [4]=sacarDato("MovimientoTipo", "IdMovimientoTipo", fila[4]);
                fila[5] = rs.getString("MovDetPreTot");
                fila[6] = rs.getString("MovDetCosTot");

                fila[7] = rs.getString("MovDetIgv");
                    
          
             
                model.addRow(fila);
            }
            tabla.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void llenarCaja(JComboBox jComboBox1) {
        String str= jComboBox1.getName();
        str = str.substring(7);
        try {
            jComboBox1.removeAllItems();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("select * from "+str);
            while (rs.next()) //if(jComrs.getString("DesUniMed"))
            {
                jComboBox1.addItem(rs.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MovimientoDet.class.getName()).log(Level.SEVERE, null, ex);
        }

        
    }
    public void setAtributos(String[] atributos) {
        this.atributos = atributos;
    }

    public static void setDatos(int datos) {
        MovimientoDet.datos = datos;
    }

    public void setIdMovimientoDetalle(String IdMovimientoDetalle) {
        this.IdMovimientoDetalle = IdMovimientoDetalle;
    }

    public void setIdMovimientoCabecera(String IdMovimientoCabecera) {
        this.IdMovimientoCabecera = IdMovimientoCabecera;
    }

    public void setIdArticulo(String IdArticulo) {
        this.IdArticulo = IdArticulo;
    }

    public void setMovIngreso(String MovIngreso) {
        this.MovIngreso = MovIngreso;
    }

    public void setMovDetCan(String MovDetCan) {
        this.MovDetCan = MovDetCan;
    }

    public void setMovDetPreTot(String MovDetPreTot) {
        this.MovDetPreTot = MovDetPreTot;
    }

    public void setMovDetIgv(String MovDetIgv) {
        this.MovDetIgv = MovDetIgv;
    }

    public void setMovDetEstReg(String MovDetEstReg) {
        this.MovDetEstReg = MovDetEstReg;
    }

    public MovimientoDet(String IdMovimientoDetalle, String IdMovimientoCabecera, String IdArticulo, String MovIngreso, String MovDetCan, String MovDetPreTot, String MovDetIgv, String MovDetEstReg) {
        this.IdMovimientoDetalle = IdMovimientoDetalle;
        this.IdMovimientoCabecera = IdMovimientoCabecera;
        this.IdArticulo = IdArticulo;
        this.MovIngreso = MovIngreso;
        this.MovDetCan = MovDetCan;
        this.MovDetPreTot = MovDetPreTot;
        this.MovDetIgv = MovDetIgv;
        this.MovDetEstReg = MovDetEstReg;
    }
    public static void main (String []args) throws SQLException{
                 Date d = new Date();
               Date d1 =sumarRestarDiasFecha(d, -30);
               SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
             //  System.out.println(sd.format(d1));
               //               System.out.println(sd.format(d));

    }

	public String getMovDetCosTot() {
		return MovDetCosTot;
	}

	public void setMovDetCosTot(String movDetCosTot) {
		MovDetCosTot = movDetCosTot;
	}
}

