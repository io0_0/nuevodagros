package Bean;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import static Bean.Articulo.cn;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import Conexion.Mysql;
import Maestros.*;

/**
 *
 * @author Equipo EPIS
 */
public class Categoria implements Atributos {

    public static Connection cn = Mysql.getConection();
    private int IdCategoria;
    private String CatNom;
    
    private int CatEstReg = 1;

    public int getIdCategoria() {
        return IdCategoria;
    }

    public void setIdCategoria(int IdCategoria) {
        this.IdCategoria = IdCategoria;
    }

    public String getCatNom() {
        return CatNom;
    }

    public void setCatNom(String CatNom) {
        this.CatNom = CatNom;
    }

  
    public int getCatEstReg() {
        return CatEstReg;
    }

    public void setCatEstReg(int CatEstReg) {
        this.CatEstReg = CatEstReg;
    }

    
    int datos = 4;//Para la JTable

 String[] atributos = {"Codigo", "Descripcion","Estado Registro"};

 String sql_Cons = "SELECT * FROM CATEGORIA WHERE " + CATESTREG + "=1 ";
                 private MiModel model = new MiModel(null, atributos);

    public Categoria(String desMovDoc) {
        CatNom=desMovDoc;
      //  MovDocAbr=abrMovDoc;
    }
    public Categoria(int id ){
        IdCategoria=id;
           String sql_Cons="SELECT * FROM categoria where IdCategoria ="+id+" and movdocestreg = 1";
        try {
            Statement sent;
          
            sent = cn.createStatement();
          //  System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                
                setCatNom(rs.getString("CatNom"));
               
                setCatEstReg(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    
    }
    public Categoria() {
    }

    public void insertar() throws SQLException {
        String sentIn = "INSERT INTO CATEGORIA (" + CATNOM+ ","+ CATESTREG +")";
       sentIn = sentIn + "Values (" + this.getCatNom() + "," + this.getCatEstReg()+")";
     //   System.out.println(sentIn);//Imprimir
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
      //  System.out.println("Guardado");
    }

    public void modificar() throws SQLException {

        String sentUp = "UPDATE CATEGORIA SET  ";
        sentUp += CATNOM + "=" + this.getCatNom() + ",";
        sentUp += CATESTREG + "=" + this.getCatEstReg() + " WHERE ";
        sentUp += IDCATEGORIA + "=" + this.getIdCategoria();

     //   System.out.println(sentUp);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentUp);
     //   System.out.println("Modificado");
    }

    public void eliminar() throws SQLException {
        String sentEl = "UPDATE CATEGORIA SET  ";
        sentEl += CATESTREG + "=" + 0 + " WHERE ";
        sentEl += IDCATEGORIA + "=" + this.getIdCategoria();

      //  System.out.println(sentEl);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentEl);
     //   System.out.println("Eliminado");
    }

    public void llenarTabla(JTable tabla, String id) {
        try {
            Statement sent;
            if (id.length() != 0) {
                sql_Cons = "SELECT * FROM CATEGORIA WHERE " + CATNOM+ " LIKE '%" + id + "%' and " + CATESTREG + "=1 ";
            }
                           System.out.println(sql_Cons);

            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sql_Cons);

            String[] fila = new String[datos];
            while (rs.next()) {
                
                fila[0] = rs.getString(IDCATEGORIA);
              
                fila[1] = rs.getString(CATNOM);
              
                fila[2] = "ACTIVO";
                model.addRow(fila);
            }
            tabla.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   
    
   

    
}
