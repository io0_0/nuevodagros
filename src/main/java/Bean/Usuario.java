/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import Conexion.Mysql;
import Maestros.*;
import java.awt.Checkbox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author Administrador
 */
public class Usuario implements Atributos {

    public static Connection cn = Mysql.getConection();
    private static ArrayList<String> arregloEncryp = new ArrayList<>();
    private static ArrayList<String> arregloReal = new ArrayList<>();
    private int idUsuario;
    private String usuNom;
    private String usuApePat;
    private String usuApeMat;
    private String usuDni;
    private int usuEstReg = 1;
    private String cargo;
    private String usuario;
    private String clave;
    int datos = 9;
   public static Usuario usu;
    private int  idEmpresaSede;
    private  String EmpresaSedeNombre;

    public int getIdEmpresaSede() {
        return idEmpresaSede;
    }

    public void setIdEmpresaSede(int idEmpresaSede) {
        this.idEmpresaSede = idEmpresaSede;
    }

    public String getEmpresaSedeNombre() {
        return EmpresaSedeNombre;
    }

    public void setEmpresaSedeNombre(String EmpresaSedeNombre) {
        this.EmpresaSedeNombre = EmpresaSedeNombre;
    }
    
    String[] atributos = {"Codigo", "Nombre", "Apellido Paterno", "Apellido Materno", "DNI", "Cargo", "Usuario", "Clave", "ESTADOO REGISTRO"};

    String sql_Cons = "SELECT * FROM USUARIO WHERE " + USUESTREG + "=1";
    private MiModel model = new MiModel(null, atributos);
 public static Usuario getUsuarioActual(){
 return usu ;
 
 }
 public static void setUsuarioActual(Usuario de){
  usu  = de ;
 
 }
 public Usuario(String usuNom, String usuApePat, String usuApeMat, String usuDni, String cargo, String usuario, String clave) {
        this.usuNom = usuNom;
        this.usuApePat = usuApePat;
        this.usuApeMat = usuApeMat;
        this.usuDni = usuDni;
        this.cargo = cargo;
        this.usuario = usuario;
        this.clave = clave;
     
    }

    public Usuario() {
        
    }
    
    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuNom() {
        return usuNom;
    }

    public void setUsuNom(String usuNom) {
        this.usuNom = usuNom;
    }

    public String getUsuApePat() {
        return usuApePat;
    }

    public void setUsuApePat(String usuApePat) {
        this.usuApePat = usuApePat;
    }

    public String getUsuApeMat() {
        return usuApeMat;
    }

    public void setUsuApeMat(String usuApeMat) {
        this.usuApeMat = usuApeMat;
    }

    public String getUsuDni() {
        return usuDni;
    }

    public void setUsuDni(String usuDni) {
        this.usuDni = usuDni;
    }

    public int getUsuEstReg() {
        return usuEstReg;
    }

    public void setUsuEstReg(int usuEstReg) {
        this.usuEstReg = usuEstReg;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public void insertar() throws SQLException {
        String sentIn = "INSERT INTO Usuario (" + USUNOM + "," + USUAPEPAT + "," + USUAPEMAT + "," + USUDNI + "," + USUCAR + "," + USUNOMCOR + "," + USUCLA + "," + USUESTREG + ")";
        sentIn = sentIn + "Values (" + getUsuNom() + "," + getUsuApePat() + "," + getUsuApeMat() + "," + getUsuDni() + "," + getCargo() + "," + getUsuario() + "," + getClave() + "," + getUsuEstReg() + ")";
        System.out.println(sentIn);//Imprimir
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("Guardado");
    }

    public void modificar() throws SQLException {

        String sentUp = "UPDATE Usuario SET  ";
        sentUp += USUNOM + "=" + getUsuNom() + ",";
        sentUp += USUAPEPAT + "=" + getUsuApePat() + ",";
        sentUp += USUAPEMAT + "=" + getUsuApeMat() + ",";
        sentUp += USUDNI + "=" + getUsuDni() + ",";
        sentUp += USUCAR + "=" + getCargo()+ ",";
        sentUp += USUNOMCOR + "=" + getUsuario() + ",";
        sentUp += USUCLA + "=" + getClave() + ",";
        sentUp += USUESTREG + "=" + getUsuEstReg() + " WHERE ";
        sentUp += IDUSUARIO + "=" + getIdUsuario();

        System.out.println(sentUp);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentUp);
        System.out.println("Modificado");
    }

    public void eliminar() throws SQLException {
        String sentEl = "UPDATE Usuario SET  ";
        sentEl += USUESTREG + "=" + 0 + " WHERE ";
        sentEl += IDUSUARIO + "=" + getIdUsuario();

        System.out.println(sentEl);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentEl);
        System.out.println("Eliminado");
    }

    public void llenarTabla(JTable tabla, String id) {
        try {
            Statement sent;
            if (id.length() != 0) {
                sql_Cons = "SELECT * FROM Usuario WHERE " + USUNOM + " LIKE '%" + id + "%' and " + USUESTREG + "=1";
                System.out.println(sql_Cons);
            }
            System.out.print(sql_Cons);
            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sql_Cons);

            String[] fila = new String[datos];
            while (rs.next()) {
                fila[0] = rs.getString(IDUSUARIO);
                fila[1] = rs.getString(USUNOM);
                fila[2] = rs.getString(USUAPEPAT);
                fila[3] = rs.getString(USUAPEMAT);
                fila[4] = rs.getString(USUDNI);
                fila[5] = rs.getString(USUCAR);
                fila[6] = rs.getString(USUNOMCOR);
                fila[7] = rs.getString(USUCLA);
                fila[8] = "ACTIVO";
                model.addRow(fila);
            }
            tabla.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void llenarTablaPestaña(JTable Pestania) {
        try {
            String sqlCont = "Select count(*) from Acceso";
            Statement stCont = cn.createStatement();
            ResultSet rsCont = stCont.executeQuery(sqlCont);
            int Cont = 0;
            if(rsCont.next())
                Cont=rsCont.getInt(1);
            else
                Cont=0;
            //try {
            Object[][] tabla = new Object[Cont][3];
            Statement sent;
            String sql = "Select * from acceso";
            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sql);
            Object[] fila = new Object[3];
            int i = 0;

            while (rs.next()) {
                tabla[i][0] = rs.getString("IdAcceso");
                tabla[i][1] = rs.getString("AcceNombre");
                tabla[i][2] = new Boolean(false);
                i++;
            }

            Pestania.setModel(new javax.swing.table.DefaultTableModel(
                    tabla,
                    new String[]{
                        "ID", "Usuario", "Estado"
                    }
            ) {
                Class[] types = new Class[]{
                    java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
                };

                public Class getColumnClass(int columnIndex) {
                    return types[columnIndex];
                }
            });
            //System.out.println(Pestania.getModel().getValueAt(1, 2));

            /* Object[] at = {"ID","NOMBRE","ESTADO"};
             DefaultTableModel model2 = new DefaultTableModel(null,at );
             Statement sent;
             String sql = "Select * from acceso";
             System.out.print(sql);
             sent = cn.createStatement();
             ResultSet rs = sent.executeQuery(sql);
             Object[] fila = new Object[3];
            
             while (rs.next()) {
             fila[0] = rs.getString("IdAcceso");
             fila[1] = rs.getString("AcceNombre");
             fila[2] = new Boolean(false);
            
             model2.addRow(fila);
            
             }
            
             Pestania.setModel(model2);
             } catch (Exception e) {
             e.printStackTrace();
             }*/
        } catch (SQLException ex) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void llenarTablaPestaña(JTable Pestania, String id) {
        try {
            String sqlCont = "Select count(*) from Acceso";
            Statement stCont = cn.createStatement();
            ResultSet rsCont = stCont.executeQuery(sqlCont);
            int Cont = 0;
            if(rsCont.next())
                Cont=rsCont.getInt(1);
            else
                Cont=0;
            //try {
            Object[][] tabla = new Object[Cont][3];
            Statement sent;
            String sql = "Select * from acceso";
            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sql);
            Object[] fila = new Object[3];
            int i = 0;
            while (rs.next()) {
                tabla[i][0] = rs.getString("IdAcceso");
                tabla[i][1] = rs.getString("AcceNombre");
                tabla[i][2] = new Boolean(false);
                i++;
            }
            Statement sen2 = cn.createStatement();
            String  con = "SELECT * from usuarioacceso where IdUsuario="+id;
            ResultSet rs2 = sen2.executeQuery( con);
            while (rs2.next()) {
                tabla[rs2.getInt("IdAcceso") - 1][2] = new Boolean(true);
            }

            Pestania.setModel(new javax.swing.table.DefaultTableModel(
                    tabla,
                    new String[]{
                        "ID", "Usuario", "Estado"
                    }
            ) {
                Class[] types = new Class[]{
                    java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
                };

                public Class getColumnClass(int columnIndex) {
                    return types[columnIndex];
                }
            });
            //System.out.println(Pestania.getModel().getValueAt(1, 2));

            /* Object[] at = {"ID","NOMBRE","ESTADO"};
             DefaultTableModel model2 = new DefaultTableModel(null,at );
             Statement sent;
             String sql = "Select * from acceso";
             System.out.print(sql);
             sent = cn.createStatement();
             ResultSet rs = sent.executeQuery(sql);
             Object[] fila = new Object[3];
            
             while (rs.next()) {
             fila[0] = rs.getString("IdAcceso");
             fila[1] = rs.getString("AcceNombre");
             fila[2] = new Boolean(false);
            
             model2.addRow(fila);
            
             }
            
             Pestania.setModel(model2);
             } catch (Exception e) {
             e.printStackTrace();
             }*/
        } catch (SQLException ex) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String encryptar(String dato) {
        //System.out.println("sdassadasdsadasencrpiha");
        arregloReal.clear();
        crearArregloEncryp();
        String ret = "";
        for (int i = 0; i < dato.length(); i++) {
            int j = arregloReal.indexOf(dato.charAt(i) + "");
            ret += arregloEncryp.get((j + i + 1) % arregloEncryp.size());
        }
        
        return ret;
    }
    public static String desencryptar(String dato){
        arregloReal.clear();
        arregloEncryp.clear();
        crearArregloEncryp();

        String ret="";
        for (int i = 0; i < dato.length(); i++) {
            int j = arregloEncryp.indexOf(dato.charAt(i) + "");
         
            int k;
            if(j-(i+1)<0){
              
                k=arregloEncryp.size()-(i+1);
                
            }else{
                k=j-(i+1);
            }
            ret += arregloReal.get(k);
        }
        return ret;
    }
    private static void crearArregloEncryp() {
        
        for(int i=97;i<123;i++){
            arregloReal.add((char)i+"");
        }
        for(int i=65;i<91;i++){
            arregloReal.add((char)i+"");
        }
        for(int i=48;i<58;i++){
            arregloReal.add((char)i+"");
        }
        
        
        arregloEncryp.add("j");
        arregloEncryp.add("B");
        arregloEncryp.add("g");
        arregloEncryp.add("V");
        arregloEncryp.add("C");
        arregloEncryp.add("1");
        arregloEncryp.add("F");
        arregloEncryp.add("I");
        arregloEncryp.add("v");
        arregloEncryp.add("W");
        arregloEncryp.add("c");
        arregloEncryp.add("s");
        arregloEncryp.add("U");
        arregloEncryp.add("o");
        arregloEncryp.add("u");
        arregloEncryp.add("w");
        arregloEncryp.add("X");
        arregloEncryp.add("p");
        arregloEncryp.add("R");
        arregloEncryp.add("n");
        arregloEncryp.add("S");
        arregloEncryp.add("y");
        arregloEncryp.add("5");
        arregloEncryp.add("m");
        arregloEncryp.add("d");
        arregloEncryp.add("f");
        arregloEncryp.add("h");
        arregloEncryp.add("8");
        arregloEncryp.add("b");
        arregloEncryp.add("A");
        arregloEncryp.add("4");
        arregloEncryp.add("N");
        arregloEncryp.add("9");
        arregloEncryp.add("i");
        arregloEncryp.add("J");
        arregloEncryp.add("E");
        arregloEncryp.add("7");
        arregloEncryp.add("3");
        arregloEncryp.add("T");
        arregloEncryp.add("x");
        arregloEncryp.add("G");
        arregloEncryp.add("r");
        arregloEncryp.add("P");
        arregloEncryp.add("Z");
        arregloEncryp.add("M");
        arregloEncryp.add("q");
        arregloEncryp.add("2");
        arregloEncryp.add("L");
        arregloEncryp.add("K");
        arregloEncryp.add("O");
        arregloEncryp.add("z");
        arregloEncryp.add("Y");
        arregloEncryp.add("l");
        arregloEncryp.add("k");
        arregloEncryp.add("t");
        arregloEncryp.add("D");
        arregloEncryp.add("a");
        arregloEncryp.add("H");
        arregloEncryp.add("0");
        arregloEncryp.add("6");
        arregloEncryp.add("e");
        arregloEncryp.add("Q");

    }
public  Usuario(String id ){

idUsuario=Integer.parseInt (id);
        String sql_Cons="SELECT * FROM Usuario where idUsuario ="+id;
        try {
            Statement sent;
          
            sent = cn.createStatement();
            System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                setIdUsuario(idUsuario);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    	

}
        public static void main(String[] args) {
        System.out.println(encryptar("cesar"));
        System.out.println(desencryptar("VFyC5"));
    }
}
