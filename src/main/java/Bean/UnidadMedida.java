package Bean;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import Conexion.Mysql;
import Maestros.*;

/**
 *
 * @author Equipo EPIS
 */
public class UnidadMedida implements Atributos {

    public static Connection cn = Mysql.getConection();
    private int IdUnidadMedida;
    private String UniMedDes;
    private String UniMedAbr;
    private int UniMedEstReg = 1;

    
    
    int datos = 4;//Para la JTable

    String[] atributos = {"Codigo", "Descripcion", "Abreviatura", "EstadoRegistro"};

    String sql_Cons="SELECT * FROM UNIDADMEDIDA WHERE "+UNIMEDESTREG+"=1";
    private MiModel model = new MiModel(null, atributos);
    public UnidadMedida(String desUniMed, String abrUniMed) {
        UniMedDes=desUniMed;
        UniMedAbr=abrUniMed;
    }
    public  UnidadMedida(String id ){
       	IdUnidadMedida=Integer.parseInt(id);
        String sql_Cons="SELECT * FROM Unidadmedida where idunidadmedida ="+id+" and unimedestreg="+1;
        try {
            Statement sent;
          
            sent = cn.createStatement();
        //    System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                   setUniMedAbr(rs.getString("unimedabr"));
                   setUniMedDes(rs.getString("unimeddes"));
                   setUniMedEstReg(1);
              
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    }
    public UnidadMedida() {
    }

    public void insertar() throws SQLException {
        String sentIn = "INSERT INTO UNIDADMEDIDA (" + UNIMEDDES+ "," + UNIMEDABR + ")";
        sentIn = sentIn + "Values (" + this.getUniMedDes() + "," + this.getUniMedAbr()+ ")";
      //  System.out.println(sentIn);//Imprimir
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        //System.out.println("Guardado");
    }

    public void modificar() throws SQLException {

        String sentUp = "UPDATE UNIDADMEDIDA SET  ";
        sentUp += UNIMEDDES + "=" + getUniMedDes() + ",";
        sentUp += UNIMEDABR + "=" + getUniMedAbr() + ",";
        sentUp += UNIMEDESTREG + "=" + getUniMedEstReg() + " WHERE ";
        sentUp += IDUNIDADMEDIDA + "=" + this.getIdUnidadMedida();

      //  System.out.println(sentUp);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentUp);
        //System.out.println("Modificado");
    }

    public void eliminar() throws SQLException {
        String sentEl = "UPDATE UNIDADMEDIDA SET  ";
        sentEl += UNIMEDESTREG + "=" + 0 + " WHERE ";
        sentEl += IDUNIDADMEDIDA+ "=" + this.getIdUnidadMedida();

  //      System.out.println(sentEl);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentEl);
    //    System.out.println("Eliminado");
    }

    public void llenarTabla(JTable tabla, String id) {
        try {
            Statement sent;
            if (id.length() != 0) {
                sql_Cons = "SELECT * FROM UNIDADMEDIDA WHERE " + UNIMEDDES + " LIKE '%" + id + "%' and " + UNIMEDESTREG + "=1";
             //   System.out.println(sql_Cons);
            }
            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sql_Cons);

            String[] fila = new String[datos];
            while (rs.next()) {
                fila[0] = rs.getString(IDUNIDADMEDIDA);
                fila[1] = rs.getString(UNIMEDDES);
                fila[2] = rs.getString(UNIMEDABR);
                fila[3] = "ACTIVO";
                model.addRow(fila);
            }
            tabla.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   public int getIdUnidadMedida() {
        return IdUnidadMedida;
    }

    public void setIdUnidadMedida(int IdUnidadMedida) {
        this.IdUnidadMedida = IdUnidadMedida;
    }

    public String getUniMedDes() {
        return UniMedDes;
    }

    public void setUniMedDes(String UniMedDes) {
        this.UniMedDes = UniMedDes;
    }

    public String getUniMedAbr() {
        return UniMedAbr;
    }

    public void setUniMedAbr(String UniMedAbr) {
        this.UniMedAbr = UniMedAbr;
    }

    public int getUniMedEstReg() {
        return UniMedEstReg;
    }

    public void setUniMedEstReg(int UniMedEstReg) {
        this.UniMedEstReg = UniMedEstReg;
    }

    
   

    
}
