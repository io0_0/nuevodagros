package Bean;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import Conexion.Mysql;
import Maestros.*;

/**
 *
 * @author Equipo EPIS
 */
public class MovimientoDetalle implements Atributos {

    public static Connection cn = Mysql.getConection();

    private int IdMovimientoDetalle;
    private int IdMovimientoCabecera;
    private int IdArticulo;
    private int MovIngreso;
    private int MovDetCan;
    private double MovDetPreTot;
    private double MovDetIgv;
    private int MovDetEstReg = 1;

    int datos = 9;//Para la JTable

    String[] atributos = {"CodigoDetalle", "CodigoCabecera", "CodigoArticulo", "NombreArticulo", "MovIngreso", "MovDetCan","MovDetPreTot","MovDetIgv","Estado Registro"};

    String sql_Cons="SELECT * FROM MOVIMIENTODET WHERE "+MOVDETESTREG+"=1";
    private DefaultTableModel model = new DefaultTableModel(null, atributos);

    public MovimientoDetalle(int idMovCab, int idArt,int movIng, int movDetCan, double movDetPreTot, double movDetIgv) {
       IdMovimientoCabecera=idMovCab;
       IdArticulo=idArt;
       MovIngreso=movIng;
       MovDetCan=movDetCan;
       MovDetPreTot=movDetPreTot;
       MovDetIgv=movDetIgv;
    }

    public MovimientoDetalle() {
    }

    public void insertar() throws SQLException {
        String sentIn = "INSERT INTO MovimientoDet (" + IDMOVIMIENTOCABECERA + "," + IDARTICULO + "," + MOVINGRESO + "," +MOVDETCAN +","+ MOVDETPRETOT +","+MOVDETIGV +","+MOVDETESTREG + ")";
        sentIn = sentIn + "Values (" + this.getIdMovimientoCabecera() + "," + this.getIdArticulo() + "," + this.getMovIngreso() + "," +   this.getMovDetCan() + "," + this.getMovDetPreTot() + "," +this.getMovDetIgv()+ "," + this.getMovDetEstReg() + ")";
        System.out.println(sentIn);//Imprimir
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("Guardado");
    }

    public void modificar() throws SQLException {

        String sentUp = "UPDATE MOVIMIENTODET SET  ";
        sentUp += IDMOVIMIENTOCABECERA + "=" + getIdMovimientoCabecera() + ",";
        sentUp += IDARTICULO + "=" + getIdArticulo() + ",";
        sentUp += MOVINGRESO + "=" + getMovIngreso() + ",";
        sentUp += MOVDETCAN + "=" + getMovDetCan() + ",";
        sentUp += MOVDETPRETOT + "=" + getMovDetPreTot() + ",";
        sentUp += MOVDETIGV + "=" + getMovDetIgv() + ",";
        sentUp += MOVDETESTREG + "=" + getMovDetEstReg() + " WHERE ";
        sentUp += IDMOVIMIENTODETALLE + "=" + this.getIdMovimientoDetalle();

        System.out.println(sentUp);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentUp);
        System.out.println("Modificado");
    }

    public void eliminar() throws SQLException {
        String sentEl = "UPDATE MOVIMIENTODET SET  ";
        sentEl += MOVDETESTREG + "=" + 0 + " WHERE ";
        sentEl += IDMOVIMIENTODETALLE + "=" + getIdMovimientoDetalle();

        System.out.println(sentEl);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentEl);
        System.out.println("Eliminado");
    }

    public void llenarTabla(JTable tabla, String id) {
        try {
            //System.out.println("-->"+id);
             /*Statement sent0 = cn.createStatement();
             ResultSet rs0 = sent0.executeQuery("select * from LISTA WHERE "+LISTDES + " LIKE '%" + id + "%' and " + LISTESTREG + "=1" );
             int idL=-1;
             if (rs0.next()){
                idL = Integer.parseInt(rs0.getString(IDLISTA));
             }*/
          
            Statement sent;
            if (id.length() != 0) {
                sql_Cons = "SELECT * FROM MOVIMIENTODET WHERE " + IDMOVIMIENTOCABECERA + " LIKE '%" + id + "%' and " + MOVDETESTREG + "=1";
                System.out.println(sql_Cons);
            }
            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sql_Cons);

            String[] fila = new String[datos];
            while (rs.next()) {
                fila[0] = rs.getString(IDMOVIMIENTODETALLE);
                fila[1] = rs.getString(IDMOVIMIENTOCABECERA);
                fila[2] = rs.getString(IDARTICULO);
                
                
                Statement sent2 = cn.createStatement();
                ResultSet rs2 = sent2.executeQuery("select * from ARTICULO WHERE IdArticulo='" + fila[2] + "'");
                
                if (rs2.next()) {
                    fila[3] = rs2.getString(ARTNOM);
                }
                fila[4] = rs.getString(MOVINGRESO);
                fila[5] = rs.getString(MOVDETCAN);
                fila[6] = rs.getString(MOVDETPRETOT);
                fila[7] = rs.getString(MOVDETIGV);
                fila[8] = "ACTIVO";
                model.addRow(fila);
            }
            tabla.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void llenarCajaCab(JComboBox jComboBox1) {

        try {
            jComboBox1.removeAllItems();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM MOVIMIENTOCAB WHERE "+MOVCABESTREG+"=1");
            while (rs.next()) //if(jComrs.getString("DesUniMed"))
            {
                jComboBox1.addItem(rs.getString(IDMOVIMIENTOCABECERA));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MovimientoDetalle.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
     public void llenarCajaArt(JComboBox jComboBox1) {

        try {
            jComboBox1.removeAllItems();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM ARTICULO WHERE "+ARTESTREG+"=1");
            while (rs.next()) //if(jComrs.getString("DesUniMed"))
            {
                jComboBox1.addItem(rs.getString(ARTNOM));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MovimientoDetalle.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public int consulta(String nomTabla, String nomAtrib, String buscar, String atrBuscar) throws SQLException
    {
        int id = -1;
        Statement sent2 = cn.createStatement();
        ResultSet rs2 = sent2.executeQuery("select * from "+nomTabla+" WHERE "+nomAtrib+"='" + buscar + "'");

        if (rs2.next()) {
            id = Integer.parseInt(rs2.getString(atrBuscar));
        }
        System.out.println("--> "+id);
        return id;
        
    }
    public int getIdMovimientoDetalle() {
        return IdMovimientoDetalle;
    }

    public void setIdMovimientoDetalle(int IdMovimientoDetalle) {
        this.IdMovimientoDetalle = IdMovimientoDetalle;
    }

    public int getIdMovimientoCabecera() {
        return IdMovimientoCabecera;
    }

    public void setIdMovimientoCabecera(int IdMovimientoCabecera) {
        this.IdMovimientoCabecera = IdMovimientoCabecera;
    }

    public int getIdArticulo() {
        return IdArticulo;
    }

    public void setIdArticulo(int IdArticulo) {
        this.IdArticulo = IdArticulo;
    }

    public int getMovIngreso() {
        return MovIngreso;
    }

    public void setMovIngreso(int MovIngreso) {
        this.MovIngreso = MovIngreso;
    }

    public int getMovDetCan() {
        return MovDetCan;
    }

    public void setMovDetCan(int MovDetCan) {
        this.MovDetCan = MovDetCan;
    }

    public double getMovDetPreTot() {
        return MovDetPreTot;
    }

    public void setMovDetPreTot(double MovDetPreTot) {
        this.MovDetPreTot = MovDetPreTot;
    }

    public double getMovDetIgv() {
        return MovDetIgv;
    }

    public void setMovDetIgv(double MovDetIgv) {
        this.MovDetIgv = MovDetIgv;
    }

    public int getMovDetEstReg() {
        return MovDetEstReg;
    }

    public void setMovDetEstReg(int MovDetEstReg) {
        this.MovDetEstReg = MovDetEstReg;
    }

    public void llenarCajaArtVentas(JComboBox jComboBox1) {
        try {
            jComboBox1.removeAllItems();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM ARTICULO WHERE "+ARTESTREG+"=1 and artcat=2");
            while (rs.next()) //if(jComrs.getString("DesUniMed"))
            {
                jComboBox1.addItem(rs.getString(ARTNOM));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MovimientoDetalle.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        
//To change body of generated methods, choose Tools | Templates.
    }
    
    public void llenarCajaArtTodos(JComboBox jComboBox1) {
        try {
            jComboBox1.removeAllItems();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM ARTICULO WHERE "+ARTESTREG+"=1 ");
            while (rs.next()) //if(jComrs.getString("DesUniMed"))
            {
                jComboBox1.addItem(rs.getString(ARTNOM));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MovimientoDetalle.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        
//To change body of generated methods, choose Tools | Templates.
    }
    
}
