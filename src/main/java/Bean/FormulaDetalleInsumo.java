/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import static Bean.Articulo.cn;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USUARIO
 0*/
public class FormulaDetalleInsumo {
    private int  IdFormulaDetalleInsumo;
      private int idFormulaCabecera;
    private int idArticulo;
    private double ForDetInsCan;
    private int ForDetInsEstReg;
    
    public FormulaDetalleInsumo(){
    }
    public FormulaDetalleInsumo(int id ){
    IdFormulaDetalleInsumo= id ;
        String sql_Cons="SELECT * FROM formulaDetalle where IdFormulaDetalleInsumo ="+id;
        try {
            Statement sent;
          
            sent = cn.createStatement();
            System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                setIdFormulaCabecera(rs.getInt("idFormulaCabecera"));
                setForDetInsCan(rs.getDouble("fordetcanideal"));
                  
                setForDetInsEstReg(rs.getInt("fordetestreg"));
                setIdArticulo(rs.getInt("idarticulo"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    	
    
    }
    public static  ArrayList<FormulaDetalleInsumo> getFormulaDetalleByCabecera(int id ){
        String sql_Cons="SELECT * FROM formulaDetalle where idFormulaCabecera ="+id;
     ArrayList<FormulaDetalleInsumo> ret= new ArrayList<>();
        try {
            Statement sent;
          
            sent = cn.createStatement();
            System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                FormulaDetalleInsumo newdet= new FormulaDetalleInsumo();
                newdet.setIdFormulaCabecera(rs.getInt("idFormulaCabecera"));
                newdet.setIdFormulaCabecera(rs.getInt("IdFormulaDetalleInsumo"));

                newdet.setForDetInsCan(rs.getDouble("fordetInscan"));

                newdet.setForDetInsEstReg(rs.getInt("fordetestreg"));
             newdet.   setIdArticulo(rs.getInt("idarticulo"));
             ret.add(newdet);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    	
    return ret ;
    }
    
    public FormulaDetalleInsumo(int id,boolean art ){
        String sql_Cons="SELECT * FROM formulaDetalle where idArticulo ="+id;
        try {
            Statement sent;
          
            sent = cn.createStatement();
            System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                setIdFormulaCabecera(rs.getInt("idFormulaCabecera"));
                setForDetInsCan(rs.getDouble("fordetcanideal"));
    IdFormulaDetalleInsumo= rs.getInt("IdFormulaDetalleInsumo") ;

                setForDetInsEstReg(rs.getInt("fordetestreg"));
                setIdArticulo(rs.getInt("idarticulo"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    	
    
    }
    public int insertar(){
          try {
              //INSERT INTO `empresa`(`IdEmpresa`, `IdEmpresaGrupo`, `EmpRuc`, `EmpNom`, `EmpTel`, `EmpDir`, `EmpEstReg`)
              String sentIn = "INSERT INTO FormulaDetalleInsumo (IdFormulaDetalleInsumo , idFormulaCabecera, idArticulo,ForDetInsCan,ForDetInsEstReg)";
              sentIn = sentIn + "Values ("+getIdFormulaDetalle()+"," +getIdFormulaCabecera() + "," +getIdArticulo() + ","+ getForDetInsCan()  +","+ getForDetInsEstReg()+")";
              System.out.println(sentIn);
              Statement sent = cn.createStatement();
              sent.executeUpdate(sentIn);
              System.out.println("guardado");
          } catch (SQLException ex) {
              Logger.getLogger(FormulaCabecera.class.getName()).log(Level.SEVERE, null, ex);
              return 1 ;

          }
          return 0 ;
    }
     public void modificar() throws SQLException {

        String sentIn = "update FormulaDetalleInsumo set  ";
        sentIn += "idarticulo"+ "=" + getIdArticulo()+ ",";
        sentIn += "idFormulaCabecera"+ "=" + getIdFormulaCabecera()+ ",";

        sentIn += "forDetInsCan" + "=" + getForDetInsCan()+ ",";
        sentIn += "ForDetInsEstReg" + "=" + getForDetInsEstReg()+ ""+
      " where ";
        sentIn += "idformulaDetalle" + "=" + getIdFormulaDetalle();

        System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("guardado");
    }
       public void eliminar() throws SQLException {

        String sentIn = "update FormulaDetalleInsumo set  ";
        sentIn += "ForDetInsEstReg" + "=0" +
      " where ";
        sentIn += "IdFormulaDetalle" + "=" + getIdFormulaDetalle();

        System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("guardado");
    }
    public int getIdFormulaCabecera() {
        return idFormulaCabecera;
    }

    public void setIdFormulaCabecera(int idFormulaCabecera) {
        this.idFormulaCabecera = idFormulaCabecera;
    }

    public int getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(int idArticulo) {
        this.idArticulo = idArticulo;
    }

    public double getForDetInsCan() {
        return ForDetInsCan;
    }

    public void setForDetInsCan(double FormCabDetCan) {
        this.ForDetInsCan = FormCabDetCan;
    }

    public int getForDetInsEstReg() {
        return ForDetInsEstReg;
    }

    public void setForDetInsEstReg(int FormCabEstReg) {
        this.ForDetInsEstReg = FormCabEstReg;
    }

    public int getIdFormulaDetalle() {
        return IdFormulaDetalleInsumo;
    }

    public void setIdFormulaDetalleInsumo (int IdFormulaDetalleInsumo) {
        this.IdFormulaDetalleInsumo = IdFormulaDetalleInsumo;
    }

}
