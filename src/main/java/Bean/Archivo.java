/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import static GUI.ArticuloGUI.cn;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Administrador
 */
public class Archivo {
    private int IdArchivo;
    private int IdSede;
    private String nombreArchivo;
    private String CodigoArchivo;

    public Archivo(int IdArchivo, int IdSede, String nombreArchivo, String CodigoArchivo) {
        this.IdArchivo = IdArchivo;
        this.IdSede = IdSede;
        this.nombreArchivo = nombreArchivo;
        this.CodigoArchivo = CodigoArchivo;
    }
    public Archivo()  { }

    public int getIdArchivo() {
        return IdArchivo;
    }

    public void setIdArchivo(int IdArchivo) {
        this.IdArchivo = IdArchivo;
    }

    public int getIdSede() {
        return IdSede;
    }

    public void setIdSede(int IdSede) {
        this.IdSede = IdSede;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getCodigoArchivo() {
        return CodigoArchivo;
    }

    public void setCodigoArchivo(String CodigoArchivo) {
        this.CodigoArchivo = CodigoArchivo;
    }
    
    public void insertar() throws SQLException {
        //INSERT INTO `empresa`(`IdEmpresa`, `IdEmpresaGrupo`, `EmpRuc`, `EmpNom`, `EmpTel`, `EmpDir`, `EmpEstReg`)
        String sentIn = "INSERT INTO `archivo`(`IdSede`, `NombreArchivo`, `CodigoArchivo`) ";
        sentIn = sentIn + "Values (" +getIdSede()+ ","+ getNombreArchivo()  +","+ getCodigoArchivo()+")";
        System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("guardado");
        
    }

    @Override
    public String toString() {
        return "Archivo{" + "IdArchivo=" + IdArchivo + ", IdSede=" + IdSede + ", nombreArchivo=" + nombreArchivo + ", CodigoArchivo=" + CodigoArchivo + '}';
    }
    
    
}
