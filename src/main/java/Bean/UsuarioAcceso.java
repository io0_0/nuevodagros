/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author Edward
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Conexion.Mysql;
import Maestros.*;
import java.awt.Checkbox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Administrador
 */
public class UsuarioAcceso implements Atributos {

    public static Connection cn = Mysql.getConection();

    private int idUsuario;
    private int idUsuAcces;
    private int idAcceso;
    int datos = 3;

    String[] atributos = {"Codigo", "Nombre", "Apellido Paterno", "Apellido Materno", "DNI", "Cargo", "Usuario", "Clave", "ESTADOO REGISTRO"};

    String sql_Cons = "SELECT * FROM USUARIO WHERE " + USUESTREG + "=1";
    private DefaultTableModel model = new DefaultTableModel(null, atributos);

    public UsuarioAcceso(int idUsuario, int idAcceso) {
        this.idUsuario = idUsuario;
        this.idAcceso = idAcceso;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdUsuAcces() {
        return idUsuAcces;
    }

    public void setIdUsuAcces(int idUsuAcces) {
        this.idUsuAcces = idUsuAcces;
    }

    public int getIdAcceso() {
        return idAcceso;
    }

    public void setIdAcceso(int idAcceso) {
        this.idAcceso = idAcceso;
    }

    public UsuarioAcceso() {
    }

    public void insertar(String[] datos) throws SQLException {
        int[] datEnt = new int[datos.length-1];
        System.err.println("insertar arreglo de "+datEnt.length);
        for (int i = 1; i < datos.length; i++) {
            if (datos[i].equals("true")) {
                datEnt[i - 1] = i;
            }
        }
        for (int i = 0; i < datEnt.length; i++) {
            if (datEnt[i] != 0) {
                String sentIn = "INSERT INTO UsuarioAcceso (" + IDUSUARIO + "," + IDACCESO + ")";
                sentIn = sentIn + "Values (" + getIdUsuario() + "," + datEnt[i] + ")";
                System.out.println(sentIn);//Imprimir
                Statement sent = cn.createStatement();
                sent.executeUpdate(sentIn);
                System.out.println("Guardado");

            }

        }

    }

    public void modificar(String[] datos) throws SQLException {

        int j = 1;
        for (int i = 1; i < datos.length; i++) {
            if (datos[i].equals("true")) {
                Statement st = cn.createStatement();
                ResultSet rs2 = st.executeQuery("Select * from UsuarioAcceso where "+IDUSUARIO+"="+getIdUsuario()+" and "+IDACCESO+"="+i);
                boolean existe = rs2.next();
                if(!existe){
                    String sentUp = "INSERT INTO UsuarioAcceso (" + IDUSUARIO + "," + IDACCESO + ")";
                    sentUp = sentUp + "Values (" + getIdUsuario() + "," + i + ")";
                    System.out.println(sentUp);//Imprimir
                    Statement sent = cn.createStatement();
                    sent.executeUpdate(sentUp);
                    System.out.println("Guardado");
                }
            } else {
                String sentUp = "DELETE FROM UsuarioAcceso WHERE ";
                sentUp += IDUSUARIO + "=" + getIdUsuario();
                sentUp += " and "+IDACCESO + "=" + j;
                System.out.println(sentUp);//Imprimir
                Statement sent = cn.createStatement();
                sent.executeUpdate(sentUp);
                System.out.println("Guardado");

            }
            j++;
        }
    }

}
