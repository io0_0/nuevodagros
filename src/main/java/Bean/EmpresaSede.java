package Bean;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTable;
import Conexion.Mysql;
import Maestros.*;
import static Maestros.Atributos.EMPESTREG;
import static Maestros.Atributos.EMPNOM;
import static Maestros.Atributos.IDEMPRESA;
import java.util.ArrayList;

/**
 *
 * @author Equipo EPIS
 */
public class EmpresaSede implements Atributos {

    public static Connection cn = Mysql.getConection();

    public static void llenarCajaMisEmpresas(JComboBox jComboEmpSedOrig) {
        try {
            MovimientoCab cabe= new MovimientoCab();
            String where = " ";
            where  = where + " inner join empresa on empresasede.idempresa = empresa.idempresa "
                    + "inner join empresagrupo on empresa.idempresagrupo = empresagrupo.idempresagrupo"
                    + " where empresagrupo.idempresagrupo=1 and empresasede.empsedestreg=1  ";
            cabe.llenarCaja(jComboEmpSedOrig, "EMPRESASEDE", EMPSEDNOM, where);
        } catch (SQLException ex) {
            Logger.getLogger(EmpresaSede.class.getName()).log(Level.SEVERE, null, ex);
        }
 }
    public static void llenarCajaMisEmpresascon(JComboBox jComboEmpSedOrig,String id ) {
        try {
            MovimientoCab cabe= new MovimientoCab();
            String where = " ";
            where  = where + " inner join empresa on empresasede.idempresa = empresa.idempresa "
                    + "inner join empresagrupo on empresa.idempresagrupo = empresagrupo.idempresagrupo"
                    + " where empresagrupo.idempresagrupo=1 and empresasede.empsedestreg=1  ";
            cabe.llenarCaja(jComboEmpSedOrig, "EMPRESASEDE", EMPSEDNOM, where);
        } catch (SQLException ex) {
            Logger.getLogger(EmpresaSede.class.getName()).log(Level.SEVERE, null, ex);
        }
 }
public static void llenarCajaMisEmpresasAlmacen(JComboBox jComboEmpSedOrig ,String id) {
        try {
            MovimientoCab cabe= new MovimientoCab();
            String where = " ";
            where  = where + " inner join empresa on empresasede.idempresa = empresa.idempresa "
                    + "inner join empresagrupo on empresa.idempresagrupo = empresagrupo.idempresagrupo"
                    + " where empresagrupo.idempresagrupo=1 and empresasede.empsedestreg=1 and empresasede.empsednom like '%Almacen%'  ";
            cabe.llenarCajaId(jComboEmpSedOrig, "EMPRESASEDE", EMPSEDNOM, where ,id);
        } catch (SQLException ex) {
            Logger.getLogger(EmpresaSede.class.getName()).log(Level.SEVERE, null, ex);
        }
 }
    @Override
    public String toString() {
        return EmpSedNom ;
    }

    public static ArrayList<EmpresaSede> getEmpresaSedesForDato(String col, String dato) {

       ArrayList<EmpresaSede>  retorno = new ArrayList<EmpresaSede>();

        String sql_Cons="SELECT * FROM EmpresaSede where "+col+"="+dato;
        try {
            Statement sent;
          
           // System.out.println("prubea ");
            sent = cn.createStatement();
           // System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                EmpresaSede emp = new EmpresaSede();
               emp. setEmpSedDir(rs.getString("EmpSedDir"));

               emp.   setEmpSedDin(rs.getString("EmpSedDin"));
             emp.     setEmpSedNom(rs.getString("EmpSedNom"));
               emp. setEmpSedEstReg(Integer.parseInt(rs.getString("EmpSedEstReg")));
               emp. setIdEmpresa(rs.getString("IdEmpresa"));
               emp. setIdEmpresaSede(rs.getString("IdEmpresaSede"));
             retorno.add(emp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    return retorno ; 
    
    }
    private String IdEmpresaSede;
    private String IdEmpresa;
    private String EmpSedNom;
    private String EmpSedDin;
    public static ArrayList<EmpresaSede> getEmpresasAlmacen(){
        ArrayList<EmpresaSede> retorno = new ArrayList<>();

        String where = " select * from empresasede";
            where  = where + " inner join empresa on empresasede.idempresa = empresa.idempresa "
                    + "inner join empresagrupo on empresa.idempresagrupo = empresagrupo.idempresagrupo"
                    + " where empresagrupo.idempresagrupo=1 and empresasede.empsedestreg=1 and empresasede.empsednom like '%Almacen%'  ";
            System.out.println("Bean.EmpresaSede.getEmpresasAlmacen()\n"+where);
        try {
            Statement sent;
          
           // System.out.println("prubea ");
            sent = cn.createStatement();
           // System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(where);
            
            while (rs.next()) {
                EmpresaSede emp = new EmpresaSede();
               emp. setEmpSedDir(rs.getString("EmpSedDir"));

               emp.   setEmpSedDin(rs.getString("EmpSedDin"));
             emp.     setEmpSedNom(rs.getString("EmpSedNom"));
               emp. setEmpSedEstReg(Integer.parseInt(rs.getString("EmpSedEstReg")));
               emp. setIdEmpresa(rs.getString("IdEmpresa"));
               emp. setIdEmpresaSede(rs.getString("IdEmpresaSede"));
             retorno.add(emp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return retorno ;     
    }
    
    
    public EmpresaSede(String idEmpresaSede2) {
		// TODO Auto-generated constructor stub
    	IdEmpresaSede=idEmpresaSede2;
        String sql_Cons="SELECT * FROM EmpresaSede where idEmpresaSede ="+IdEmpresaSede;
        try {
            Statement sent;
          
            sent = cn.createStatement();
           // System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                setEmpSedDir(rs.getString("EmpSedDir"));

               setEmpSedDin(rs.getString("EmpSedDin"));
               setEmpSedNom(rs.getString("EmpSedNom"));
             setEmpSedEstReg(Integer.parseInt(rs.getString("EmpSedEstReg")));
             setIdEmpresa(rs.getString("IdEmpresa"));
             setIdEmpresaSede(rs.getString("IdEmpresaSede"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
	}

    
    public String getEmpSedDir() {
        return EmpSedDir;
    }

    public void setEmpSedDir(String EmpSedDir) {
        this.EmpSedDir = EmpSedDir;
    }
    private String EmpSedDir;
    private int EmpSedEstReg = 1;

    int datos = 7;//Para la JTable

    String[] atributos = {"Codigo", "CodigoEmpresa", "NombreEmpresa", "NombreSede", "Dinero", "Direccion","Estado Registro"};

    String sql_Cons;//="SELECT * FROM EMPRESASEDE WHERE "+EMPSEDESTREG+"=1";
    private MiModel model = new MiModel(null, atributos);

    public EmpresaSede(String idEmp, String nomEmpSede, String  dinEmpSede,String dirEmpSede) {
        IdEmpresa = idEmp;
        EmpSedNom = nomEmpSede;
        EmpSedDin = dinEmpSede;
        EmpSedDir=dirEmpSede;
    }

    public EmpresaSede() {
    }
    public void insertar() throws SQLException {
        String sentIn = "INSERT INTO EmpresaSede (" + IDEMPRESA + "," + EMPSEDNOM + "," + EMPSEDDIN + "," + EMPSEDDIR + "," +EMPSEDESTREG + ")";
        sentIn = sentIn + "Values (" + getIdEmpresa() + "," + getEmpSedNom() + "," + getEmpSedDin() + "," + getEmpSedDir()+ "," +getEmpSedEstReg() + ")";
        System.out.println(sentIn);//Imprimir
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("Guardado");
    }
    public void insertarId() throws SQLException {
        String sentIn = "INSERT INTO EmpresaSede ("+IDEMPRESASEDE +","+ IDEMPRESA + "," + EMPSEDNOM + "," + EMPSEDDIN + "," + EMPSEDDIR + "," +EMPSEDESTREG + ")";
        sentIn = sentIn + "Values (" + getIdEmpresaSede()+","+ getIdEmpresa() + "," + getEmpSedNom() + "," + getEmpSedDin() + "," + getEmpSedDir()+ "," +getEmpSedEstReg() + ")";
        System.out.println(sentIn);//Imprimir
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("Guardado");
    }

    public void modificar() throws SQLException {

        String sentUp = "UPDATE EmpresaSede SET  ";
        sentUp += IDEMPRESA + "=" + getIdEmpresa() + ",";
        sentUp += EMPSEDNOM + "=" + getEmpSedNom() + ",";
        sentUp += EMPSEDDIN + "=" + getEmpSedDin() + ",";
        sentUp += EMPSEDDIR + "=" + getEmpSedDir() + ",";
        sentUp += EMPSEDESTREG + "=" + getEmpSedEstReg() + " WHERE ";
        sentUp += IDEMPRESASEDE+ "=" + getIdEmpresaSede();

        System.out.println(sentUp);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentUp);
        System.out.println("Modificado");
    }

    public void eliminar() throws SQLException {
        String sentEl = "UPDATE EmpresaSede SET  ";
        sentEl += EMPSEDESTREG + "=" + 0 + " WHERE ";
        sentEl += IDEMPRESASEDE + "=" + getIdEmpresaSede();

        System.out.println(sentEl);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentEl);
        System.out.println("Eliminado");
    }

    public void llenarTabla(JTable tabla, String id, String idEmp) {
        try {
            Statement sent;
            //if (id.length() != 0) {

                sql_Cons = "SELECT * FROM EmpresaSede WHERE " + EMPSEDNOM + " LIKE '%" + id + "%' and " + EMPSEDESTREG + "=1"
                +" and " +IDEMPRESA+"="+idEmp;
                System.out.println(sql_Cons);
                
            //}
            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sql_Cons);

            String[] fila = new String[datos];
            while (rs.next()) {
                fila[0] = rs.getString(IDEMPRESASEDE);
                fila[1] = rs.getString(IDEMPRESA);
                Statement sent2 = cn.createStatement();
                ResultSet rs2 = sent2.executeQuery("select * from EMPRESA WHERE IdEmpresa='" + fila[1] + "'");
                //DUDOSO
                if (rs2.next()) {
                    fila[2] = rs2.getString(EMPNOM);
                }
                fila[3] = rs.getString(EMPSEDNOM);
                fila[4] = rs.getString(EMPSEDDIN);
                fila[5]=rs.getString(EMPSEDDIR);
                //fila[7] = rs.getString(EMPSEDESTREG);
                fila[6] = "ACTIVO";
                model.addRow(fila);
            }
            tabla.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void llenarCaja(JComboBox jComboBox1) {

        try {
            jComboBox1.removeAllItems();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM Empresa WHERE "+EMPESTREG+"=1");
            while (rs.next()) //if(jComrs.getString("DesUniMed"))
            {
                jComboBox1.addItem(rs.getString(EMPNOM));
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmpresaSede.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public String consulta(String nomTabla, String nomAtrib, String buscar, String atrBuscar) throws SQLException
    {
        String id =""+ -1;
        Statement sent2 = cn.createStatement();
        ResultSet rs2 = sent2.executeQuery("select * from "+nomTabla+" WHERE "+nomAtrib+"='" + buscar + "'");

        if (rs2.next()) {
            id = rs2.getString(atrBuscar);
        }
        return id;
    }
    
    public String getIdEmpresaSede() {
        return IdEmpresaSede;
    }
    public void setIdEmpresaSede(String IdEmpresaSede) {
        this.IdEmpresaSede = IdEmpresaSede;
    }

    public String getIdEmpresa() {
        return IdEmpresa;
    }
    public void setIdEmpresa(String IdEmpresa) {
        this.IdEmpresa = IdEmpresa;
    }

    public String getEmpSedNom() {
        return EmpSedNom;
    }
    public void setEmpSedNom(String EmpSedNom) {
        this.EmpSedNom = EmpSedNom;
    }
    
    public String  getEmpSedDin() {
        return EmpSedDin;
    }
    public void setEmpSedDin(String EmpSedDin) {
        this.EmpSedDin = EmpSedDin;
    }
   
    public int getEmpSedEstReg() {
        return EmpSedEstReg;
    }

    public void setEmpSedEstReg(int EmpSedEstReg) {
        this.EmpSedEstReg = EmpSedEstReg;
    }

    public static void main(String [] args){
        EmpresaSede.getEmpresasAlmacen();
    
    }

}
