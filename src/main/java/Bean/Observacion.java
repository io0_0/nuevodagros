/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import Bean.MiModel;
import Conexion.Mysql;
import Maestros.Atributos;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Edwin Alex
 */
public class Observacion implements Atributos{

    public static Connection cn = Mysql.getConection();
    private int IdObservacion  ;
    private int IdMovimientoCabecera;
    private int IdUsuario ;
    private String ObservacionDescripcion="";

    public int getIdObservacion() {
        return IdObservacion;
    }

    public void setIdObservacion(int IdObservacion) {
        this.IdObservacion = IdObservacion;
    }

    public int getIdMovimientoCabecera() {
        return IdMovimientoCabecera;
    }

    public void setIdMovimientoCabecera(int IdMovimientoCabecera) {
        this.IdMovimientoCabecera = IdMovimientoCabecera;
    }

    public int getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(int IdUsuario) {
        this.IdUsuario = IdUsuario;
    }

    public String getObservacionDescripcion() {
        return ObservacionDescripcion;
    }

    public void setObservacionDescripcion(String ObservacionDescripcion) {
        this.ObservacionDescripcion = ObservacionDescripcion;
    }
 
     
    static int datos = 9;
    
    static String[] atributos = {"CodigoArticulo","Nombre Articulo", "CodigoUnidad","NombreUnidad","CodigoTipo","NombreTipo", "Costo","IGV","Estado Registro"};

    static String sql_Cons = "SELECT * FROM ARTICULO WHERE "+ARTESTREG+"=1";
    private MiModel model = new MiModel(null, atributos);

    public Observacion() {

    }

    
    public Observacion (int id ){
    	IdObservacion=id;
        String sql_Cons="SELECT * FROM observacion where idObservacion ="+id;
        try {
            Statement sent;
          
            sent = cn.createStatement();
      //      System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                setIdObservacion(rs.getInt("IdObservacion"));
                setIdMovimientoCabecera(rs.getInt("IdUsuario"));
                setIdMovimientoCabecera(rs.getInt("IdMovimientoCabecera"));
                setObservacionDescripcion(rs.getString("ObservacionDescripcion"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    	
    	
    }

    public Observacion (int id  , int idCabe){
    	IdObservacion=id;
        String sql_Cons="SELECT * FROM observacion where idMovimientoCabecera ="+idCabe;
        try {
            Statement sent;
          
            sent = cn.createStatement();
       //     System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                setIdObservacion(rs.getInt("IdObservacion"));
                setIdMovimientoCabecera(rs.getInt("IdUsuario"));
                setIdMovimientoCabecera(rs.getInt("IdMovimientoCabecera"));
                setObservacionDescripcion(rs.getString("ObservacionDescripcion"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    	
    	
    }
    
    public void insertar() throws SQLException {
        //INSERT INTO `empresa`(`IdEmpresa`, `IdEmpresaGrupo`, `EmpRuc`, `EmpNom`, `EmpTel`, `EmpDir`, `EmpEstReg`)
        String sentIn = "INSERT INTO Observacion ( `IdMovimientoCabecera`, `IdUsuario`, `ObservacionDescripcion` )";
        sentIn = sentIn + "Values ("  +getIdMovimientoCabecera()+ ","+ getIdUsuario() +",'"+ getObservacionDescripcion()+"')";
    //    System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("guardado");
    }

    public void modificar() throws SQLException {

        String sentIn = String.format("UPDATE `observacion` SET ,"
                + "`IdMovimientoCabecera`=%s,"
                + "`IdUsuario`=%s,"
                + "`ObservacionDescripcion`='%s'"
                + " WHERE idObservacion=%s", getIdMovimientoCabecera(),getIdUsuario(),getObservacionDescripcion(),getIdObservacion());
      //  System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("guardado");
    }

    
    public static String generarCodigo(JTextField a){
        try {
            Statement sent2 = cn.createStatement();
            ResultSet rs2 = sent2.executeQuery("SELECT MAX(IdObservacion) FROM Observacion");
            if(rs2.next())
                a.setText((Integer.parseInt(rs2.getString(1))+1)+"");
            return rs2.getString(1);
        } catch (SQLException ex) {
            Logger.getLogger(Observacion.class.getName()).log(Level.SEVERE, null, ex);
        }
            return "-1";

    }
}


