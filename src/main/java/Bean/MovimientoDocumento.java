package Bean;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import static Bean.Articulo.cn;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import Conexion.Mysql;
import Maestros.*;
import java.util.ArrayList;

/**
 *
 * @author Equipo EPIS
 */
public class MovimientoDocumento implements Atributos {

    public static Connection cn = Mysql.getConection();
    private int IdMovimientoDocumento;
    private String MovDocDes;
    private String MovDocAbr;
    private String MovDocSunat;

    public String getMovDocSunat() {
        return MovDocSunat;
    }

    public void setMovDocSunat(String MovDocSunat) {
        this.MovDocSunat = MovDocSunat;
    }
    private int MovDocEstReg = 1;

    public int getIdMovimientoDocumento() {
        return IdMovimientoDocumento;
    }

    public void setIdMovimientoDocumento(int IdMovimientoDocumento) {
        this.IdMovimientoDocumento = IdMovimientoDocumento;
    }

    public String getMovDocDes() {
        return MovDocDes;
    }

    public void setMovDocDes(String MovDocDes) {
        this.MovDocDes = MovDocDes;
    }

    public String getMovDocAbr() {
        return MovDocAbr;
    }

    public void setMovDocAbr(String MovDocAbr) {
        this.MovDocAbr = MovDocAbr;
    }

    public int getMovDocEstReg() {
        return MovDocEstReg;
    }

    public void setMovDocEstReg(int MovDocEstReg) {
        this.MovDocEstReg = MovDocEstReg;
    }

    
    int datos = 4;//Para la JTable

 String[] atributos = {"Codigo", "Descripcion", "Abreviatura","Estado Registro"};

 String sql_Cons = "SELECT * FROM MOVIMIENTODOCUMENTO WHERE " + MOVDOCESTREG + "=1 ORDER BY `MovDocCodSunat` ASC";
                 private MiModel model = new MiModel(null, atributos);

    public MovimientoDocumento(String desMovDoc, String abrMovDoc) {
        MovDocDes=desMovDoc;
        MovDocAbr=abrMovDoc;
    }
    public MovimientoDocumento(String id ){
        IdMovimientoDocumento=Integer.parseInt(id);
           String sql_Cons="SELECT * FROM movimientodocumento where idmovimientoDocumento ="+id+" and movdocestreg = 1";
        try {
            Statement sent;
          
            sent = cn.createStatement();
          //  System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                setMovDocAbr(rs.getString("MovDocAbr"));
                setMovDocDes(rs.getString("MovDocDes"));
                setMovDocSunat(rs.getString("MovDocCodSunat"));
                setMovDocEstReg(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    
    }
    public MovimientoDocumento() {
    }

    public void insertar() throws SQLException {
        String sentIn = "INSERT INTO MOVIMIENTODOCUMENTO (" + MOVDOCDES+ "," +MOVDOCABR+","+ MOVDOCESTREG +",MOVDOCCODSUNAT"+ ")";
       sentIn = sentIn + "Values (" + this.getMovDocDes() + ","+ this.getMovDocAbr() +"," + this.getMovDocEstReg()+ ","+this.getMovDocSunat()+")";
     //   System.out.println(sentIn);//Imprimir
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
      //  System.out.println("Guardado");
    }

    public void modificar() throws SQLException {

        String sentUp = "UPDATE MOVIMIENTODOCUMENTO SET  ";
        sentUp += MOVDOCDES + "=" + this.getMovDocDes() + ",";
        sentUp += MOVDOCABR + "=" + this.getMovDocAbr() + ",";
                sentUp += "MOVDOCCODSUNAT=" + this.getMovDocSunat()+ ",";

        sentUp += MOVDOCESTREG + "=" + this.getMovDocEstReg() + " WHERE ";
        sentUp += IDMOVIMIENTODOCUMENTO + "=" + this.getIdMovimientoDocumento();

     //   System.out.println(sentUp);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentUp);
     //   System.out.println("Modificado");
    }

    public void eliminar() throws SQLException {
        String sentEl = "UPDATE MOVIMIENTODOCUMENTO SET  ";
        sentEl += MOVDOCESTREG + "=" + 0 + " WHERE ";
        sentEl += IDMOVIMIENTODOCUMENTO + "=" + this.getIdMovimientoDocumento();

      //  System.out.println(sentEl);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentEl);
     //   System.out.println("Eliminado");
    }

    public void llenarTabla(JTable tabla, String id) {
        try {
            Statement sent;
            if (id.length() != 0) {
                sql_Cons = "SELECT * FROM MOVIMIENTODOCUMENTO WHERE " + MOVDOCDES+ " LIKE '%" + id + "%' and " + MOVDOCESTREG + "=1  ORDER BY `MovDocCodSunat` ASC";
            }
                           System.out.println(sql_Cons);

            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sql_Cons);

            String[] fila = new String[datos];
            while (rs.next()) {
                fila[0] = rs.getString("movdoccodsunat");
                fila[1] = rs.getString(MOVDOCDES);
                fila[2] = rs.getString(MOVDOCABR);
                fila[3] = "ACTIVO";
                model.addRow(fila);
            }
            tabla.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   public static ArrayList < MovimientoDocumento> getFordato(String dato , String valor ){
 ArrayList < MovimientoDocumento >  documentos= new ArrayList<>();
       
       String sql_Cons="SELECT * FROM movimientodocumento where "+dato+" ="+valor+" and movdocestreg = 1";
        try {
            Statement sent;
          
            sent = cn.createStatement();
          //  System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
               
                
                MovimientoDocumento doc= new MovimientoDocumento();
    doc.setIdMovimientoDocumento(rs.getInt("IdmovimientoDocumento"));
                doc.  setMovDocAbr(rs.getString("MovDocAbr"));
                 doc.setMovDocDes(rs.getString("MovDocDes"));
                doc. setMovDocSunat(rs.getString("MovDocCodSunat"));
             doc.    setMovDocEstReg(1);
             documentos.add(doc);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    
   
   return documentos;
   }

    @Override
    public String toString() {
        return MovDocDes ;
    }
    
   

    
}
