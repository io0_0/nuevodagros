package Bean;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import Conexion.Mysql;
import Maestros.*;

/**
 *
 * @author Equipo EPIS
 */
public class EmpresaGrupo implements Atributos {

    public static Connection cn = Mysql.getConection();
    private int IdEmpresaGrupo;
    private String EmpGruNom;
    private String EmpGruTel;
    private int EmpGruEstReg = 1;

    

    int datos = 4;//Para la JTable

    String[] atributos = {"Codigo", "Nombre", "Telefono", "Estado Registro"};

    String sql_Cons="SELECT * FROM EMPRESAGRUPO WHERE "+EMPGRUESTREG+"=1";
    private MiModel model = new MiModel(null, atributos);

    public EmpresaGrupo(String nomEmpGru, String telEmpGru) {
        EmpGruNom=nomEmpGru;
        EmpGruTel=telEmpGru;
    }

    public EmpresaGrupo() {
    }

    public void insertar() throws SQLException {
        String sentIn = "INSERT INTO EmpresaGrupo (" + EMPGRUNOM + "," + EMPGRUTEL+ "," + EMPGRUESTREG + ")";
        sentIn = sentIn + "Values (" + this.getEmpGruNom() + "," + this.getEmpGruTel() + "," + this.getEmpGruEstReg() + ")";
        System.out.println(sentIn);//Imprimir
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("Guardado");
    }

    public void modificar() throws SQLException {

        String sentUp = "UPDATE EMPRESAGRUPO SET  ";
        sentUp += EMPGRUNOM + "=" + getEmpGruNom() + ",";
        sentUp += EMPGRUTEL + "=" + getEmpGruTel() + ",";
        sentUp += EMPGRUESTREG + "=" + getEmpGruEstReg() + " WHERE ";
        sentUp += IDEMPRESAGRUPO + "=" + getIdEmpresaGrupo();

        System.out.println(sentUp);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentUp);
        System.out.println("Modificado");
    }

    public void eliminar() throws SQLException {
        String sentEl = "UPDATE EmpresaGrupo SET  ";
        sentEl += EMPGRUESTREG + "=" + 0 + " WHERE ";
        sentEl += IDEMPRESAGRUPO + "=" + this.getIdEmpresaGrupo();

        System.out.println(sentEl);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentEl);
        System.out.println("Eliminado");
    }

    public void llenarTabla(JTable tabla, String id) {
        try {
            Statement sent;
            if (id.length() != 0) {
                sql_Cons = "SELECT * FROM EMPRESAGRUPO WHERE " + EMPGRUNOM + " LIKE '%" + id + "%' and " + EMPGRUESTREG + "=1";
                System.out.println(sql_Cons);
            }
            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sql_Cons);

            String[] fila = new String[datos];
            while (rs.next()) {
                fila[0] = rs.getString(IDEMPRESAGRUPO);
                fila[1] = rs.getString(EMPGRUNOM);
                fila[2] = rs.getString(EMPGRUTEL);
                fila[3] = "ACTIVO";
                model.addRow(fila);
            }
            tabla.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public int getIdEmpresaGrupo() {
        return IdEmpresaGrupo;
    }

    public void setIdEmpresaGrupo(int IdEmpresaGrupo) {
        this.IdEmpresaGrupo = IdEmpresaGrupo;
    }

    public String getEmpGruNom() {
        return EmpGruNom;
    }

    public void setEmpGruNom(String EmpGruNom) {
        this.EmpGruNom = EmpGruNom;
    }

    public String getEmpGruTel() {
        return EmpGruTel;
    }

    public void setEmpGruTel(String EmpGruTel) {
        this.EmpGruTel = EmpGruTel;
    }

    public int getEmpGruEstReg() {
        return EmpGruEstReg;
    }

    public void setEmpGruEstReg(int EmpGruEstReg) {
        this.EmpGruEstReg = EmpGruEstReg;
    }

    
}
