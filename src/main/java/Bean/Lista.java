package Bean;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import Conexion.Mysql;
import Maestros.*;
/**
 *
 * @author Equipo EPIS
 */
public class Lista implements Atributos {

    public static Connection cn = Mysql.getConection();
    private int IdLista;
    private int IdEmpresaSede;
    private String ListDes;
    private int ListEstReg = 1;

    int datos = 5;//Para la JTable

    String[] atributos = {"Codigo", "Descripcion", "CodigoEmpresaSede", "NombreEmpresaSede", "Estado Registro"};
    String sql_Cons="SELECT * FROM LISTA WHERE "+LISTESTREG+"=1";
    
    private MiModel model = new MiModel(null, atributos);
    
    public Lista(int idEmpSede, String desLis) {
        IdEmpresaSede = idEmpSede;
        ListDes = desLis;
    }

    public Lista() {
    }
public Lista(int  id) {
    IdLista=id;
        String sql_Cons="SELECT * FROM lista where idLista ="+id;
        try {
            Statement sent;
          
            sent = cn.createStatement();
            System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                setIdEmpresaSede(rs.getInt("IdEmpresaSede"));
                setIdLista(rs.getInt("IdLista"));
                setListDes(rs.getString("ListDes"));
                setListEstReg(rs.getInt("ListEstReg"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertar() throws SQLException {
        String sentIn = "INSERT INTO LISTA (" + IDEMPRESASEDE + "," + LISTDES + "," + LISTESTREG + ")";
        sentIn = sentIn + "Values (" + this.getIdEmpresaSede()+ "," + this.getListDes() + "," +  this.getListEstReg() + ")";
        System.out.println(sentIn);//Imprimir
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("Guardado");
    }

    public void modificar() throws SQLException {
        String sentUp = "UPDATE LISTA SET  ";
        sentUp += IDEMPRESASEDE + "=" + getIdEmpresaSede() + ",";
        sentUp += LISTDES + "=" + getListDes() + ",";
        sentUp += LISTESTREG + "=" + getListEstReg() + " WHERE ";
        sentUp += IDLISTA+ "=" + getIdLista();

        System.out.println(sentUp);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentUp);
        System.out.println("Modificado");
    }

    public void eliminar() throws SQLException {
        String sentEl = "UPDATE LISTA SET  ";
        sentEl += LISTESTREG + "=" + 0 + " WHERE ";
        sentEl += IDLISTA + "=" + getIdLista();

        System.out.println(sentEl);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentEl);
        System.out.println("Eliminado");
    }

    public void llenarTabla(JTable tabla, String id) {
        try {
            Statement sent;
            if (id.length() != 0) {

                sql_Cons = "SELECT * FROM LISTA WHERE " + LISTDES + " LIKE '%" + id + "%' and " + LISTESTREG + "=1";
                System.out.println(sql_Cons);
            }
            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sql_Cons);

            String[] fila = new String[datos];
            while (rs.next()) {
                fila[0] = rs.getString(IDLISTA);
                fila[1] = rs.getString(LISTDES);
                
                
                fila[2] = rs.getString(IDEMPRESASEDE);
                Statement sent2 = cn.createStatement();
                ResultSet rs2 = sent2.executeQuery("select * from EMPRESASEDE WHERE IdEmpresaSede='" + fila[2] + "'");
                //DUDOSO
                if (rs2.next()) {
                    fila[3] = rs2.getString(EMPSEDNOM);
                }
                fila[4] = "ACTIVO";
                model.addRow(fila);
            }
            tabla.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void llenarCaja(JComboBox jComboBox1) {

        try {
            jComboBox1.removeAllItems();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM EMPRESASEDE WHERE "+EMPSEDESTREG+"=1");
            while (rs.next()) //if(jComrs.getString("DesUniMed"))
            {
                jComboBox1.addItem(rs.getString(EMPSEDNOM));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Lista.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public int consulta(String nomTabla, String nomAtrib, String buscar, String atrBuscar) throws SQLException
    {
        int id = -1;
        Statement sent2 = cn.createStatement();
        ResultSet rs2 = sent2.executeQuery("select * from "+nomTabla+" WHERE "+nomAtrib+"='" + buscar + "'");

        if (rs2.next()) {
            id = Integer.parseInt(rs2.getString(atrBuscar));
        }
        return id;
    }
    public int getIdLista() {
        return IdLista;
    }

    public void setIdLista(int IdLista) {
        this.IdLista = IdLista;
    }

    public int getIdEmpresaSede() {
        return IdEmpresaSede;
    }

    public void setIdEmpresaSede(int IdEmpresaSede) {
        this.IdEmpresaSede = IdEmpresaSede;
    }

    public String getListDes() {
        return ListDes;
    }

    public void setListDes(String ListDes) {
        this.ListDes = ListDes;
    }

    public int getListEstReg() {
        return ListEstReg;
    }

    public void setListEstReg(int ListEstReg) {
        this.ListEstReg = ListEstReg;
    }
   
}
