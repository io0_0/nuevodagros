/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import static Bean.Articulo.cn;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USUARIO
 0*/
public class FormulaDetalle {

    public static ArrayList<FormulaDetalle> getFromArticuloPrincipal(int idArticulo) {
        try {
            ArrayList <FormulaDetalle> formula = new ArrayList<>();
            String sql = "SELECT     formulaDetalleProducto.IdFormulaDetalleProducto as id ,"
                    + "formulaDetalleProducto.IdFormulaCabecera as idCabecera ,"
                    + "formulaDetalleProducto.IdArticulo as idArticulo ,"
                    + "formulaDetalleProducto.IdCategoriaProduccion as idCategoriaProduccion ,"
                    + "formulaDetalleProducto.ForDetProCan as cantidad ,"
                    + "formulaDetalleProducto.ForDetProEstReg as reg "
                    + "from formulaDetalleProducto"
                    + " INNER JOIN FormulaCabecera  ON FormulaCabecera.IdFormulaCabecera = formulaDetalleProducto.IdFormulaCabecera "
                    + " where  FormulaCabecera.IdArticulo = "+idArticulo ;
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            System.out.println(sql);
            while (rs.next()){
                FormulaDetalle formulita = new FormulaDetalle();
                formulita.setForDetProCan(rs.getInt("cantidad"));
                formulita.setForDetProEstReg(rs.getInt("reg"));
                formulita.setIdArticulo(rs.getInt("idArticulo"));
                formulita.setIdCategoriaProduccion(rs.getInt("IdCategoriaProduccion"));
                formulita.setIdFormulaCabecera(rs.getInt("idCabecera"));
                formulita.setIdFormulaDetalleProducto(rs.getInt("id"));
                formula.add(formulita);
            }
            return formula ;
        } catch (SQLException ex) {
            Logger.getLogger(FormulaDetalle.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null ; 
    }
    private int  IdFormulaDetalleProducto;
      private int idFormulaCabecera;
    private int idArticulo;
    private double ForDetProCan;
    private int ForDetProEstReg;
    private int  IdCategoriaProduccion ;

    public int getIdCategoriaProduccion() {
        return IdCategoriaProduccion;
    }

    public void setIdCategoriaProduccion(int IdCategoriaProduccion) {
        this.IdCategoriaProduccion = IdCategoriaProduccion;
    }
    
    public FormulaDetalle(){
    }
    public FormulaDetalle(int id ){
    IdFormulaDetalleProducto= id ;
        String sql_Cons="SELECT * FROM formulaDetalleProducto where IdFormulaDetalleProducto ="+id;
        try {
            Statement sent;
          
            sent = cn.createStatement();
            System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                setIdFormulaCabecera(rs.getInt("idFormulaCabecera"));
                setForDetProCan(rs.getDouble("fordetprocan"));
                  setIdCategoriaProduccion(rs.getInt("IdCategoriaProduccion"));
                setForDetProEstReg(rs.getInt("fordetproestreg"));
                setIdArticulo(rs.getInt("idarticulo"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    	
    
    }
    public static  ArrayList<FormulaDetalle> getFormulaDetalleByCabecera(int id ){
        String sql_Cons="SELECT * FROM FormulaDetalleProducto where fordetProestreg = 1 and idFormulaCabecera ="+id;
     ArrayList<FormulaDetalle> ret= new ArrayList<>();
        try {
            Statement sent;
          
            sent = cn.createStatement();
            System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                FormulaDetalle newdet= new FormulaDetalle();
                newdet.setIdFormulaCabecera(rs.getInt("idFormulaCabecera"));
                newdet.setIdFormulaCabecera(rs.getInt("IdFormulaDetalleProducto"));
                 newdet.setIdCategoriaProduccion(rs.getInt("IdCategoriaProduccion"));
                newdet.setForDetProCan(rs.getDouble("fordetProcan"));

                newdet.setForDetProEstReg(rs.getInt("fordetproestreg"));
             newdet.   setIdArticulo(rs.getInt("idarticulo"));
             ret.add(newdet);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    	
    return ret ;
    }
    
    public FormulaDetalle(int id,boolean art ){
        String sql_Cons="SELECT * FROM formulaDetalle where idArticulo ="+id;
        try {
            Statement sent;
          
            sent = cn.createStatement();
            System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                setIdFormulaCabecera(rs.getInt("idFormulaCabecera"));
                setForDetProCan(rs.getDouble("fordetcanideal"));
    IdFormulaDetalleProducto= rs.getInt("IdFormulaDetalleProducto") ;
                setIdCategoriaProduccion(rs.getInt("IdCategoriaProduccion"));
                setForDetProEstReg(rs.getInt("fordetestreg"));
                setIdArticulo(rs.getInt("idarticulo"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    	
    
    }
    public int insertar(){
          try {
              //INSERT INTO `empresa`(`IdEmpresa`, `IdEmpresaGrupo`, `EmpRuc`, `EmpNom`, `EmpTel`, `EmpDir`, `EmpEstReg`)
              String sentIn = "INSERT INTO FormulaDetalleProducto (IdFormulaDetalleProducto , idFormulaCabecera, idArticulo,IdCategoriaProduccion,ForDetProCan,ForDetProEstReg)";
              sentIn = sentIn + "Values ("+getIdFormulaDetalle()+"," +getIdFormulaCabecera() + "," +getIdArticulo() +","+getIdCategoriaProduccion()+ ","+ getForDetProCan()  +","+ getForDetProEstReg()+")";
              System.out.println(sentIn);
              Statement sent = cn.createStatement();
              sent.executeUpdate(sentIn);
              System.out.println("guardado");
          } catch (SQLException ex) {
              Logger.getLogger(FormulaCabecera.class.getName()).log(Level.SEVERE, null, ex);
              return 1 ;

          }
          return 0 ;
    }
     public void modificar() throws SQLException {

        String sentIn = "update FormulaDetalleProducto set  ";
        sentIn += "idarticulo"+ "=" + getIdArticulo()+ ",";
        sentIn += "idFormulaCabecera"+ "=" + getIdFormulaCabecera()+ ",";
        sentIn += "idcategoriaProduccion"+ "=" + getIdCategoriaProduccion()+ ",";

        sentIn += "forDetProCan" + "=" + getForDetProCan()+ ",";
        sentIn += "ForDetProEstReg" + "=" + getForDetProEstReg()+ ""+
      " where ";
        sentIn += "idformulaDetalleProducto" + "=" + getIdFormulaDetalle();

        System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("guardado");
    }
       public void eliminar() throws SQLException {

        String sentIn = "update FormulaDetalleProducto set  ";
        sentIn += "ForDetProEstReg" + "=0" +
      " where ";
        sentIn += "IdFormulaDetalleProducto" + "=" + getIdFormulaDetalle();

        System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("guardado");
    }
    public int getIdFormulaCabecera() {
        return idFormulaCabecera;
    }

    public void setIdFormulaCabecera(int idFormulaCabecera) {
        this.idFormulaCabecera = idFormulaCabecera;
    }

    public int getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(int idArticulo) {
        this.idArticulo = idArticulo;
    }

    public double getForDetProCan() {
        return ForDetProCan;
    }

    public void setForDetProCan(double FormCabDetCan) {
        this.ForDetProCan = FormCabDetCan;
    }

    public int getForDetProEstReg() {
        return ForDetProEstReg;
    }

    public void setForDetProEstReg(int FormCabEstReg) {
        this.ForDetProEstReg = FormCabEstReg;
    }

    public int getIdFormulaDetalle() {
        return IdFormulaDetalleProducto;
    }

    public void setIdFormulaDetalleProducto (int IdFormulaDetalleProducto) {
        this.IdFormulaDetalleProducto = IdFormulaDetalleProducto;
    }

}
