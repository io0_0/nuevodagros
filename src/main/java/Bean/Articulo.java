/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import Bean.MiModel;
import Conexion.Mysql;
import Maestros.Atributos;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Edwin Alex
 */
public class Articulo implements Atributos{

    public static Connection cn = Mysql.getConection();
   
    private int IdArticulo;
    private int IdUnidadMedida; 
    private String ArtNom;
    private double ArtCos;
    private double ArtIgv;
    private int ArtCat;
    private int ArtEstReg = 1;
    private int ArtCol=1;
    public int getArtCat() {
        return ArtCat;
    }

    public void setArtCat(int ArtCat) {
        this.ArtCat = ArtCat;
    }

    public Articulo(int IdArticulo, int IdUnidadMedida, String ArtNom, double ArtCos, double ArtIgv, int ArtCat) {
        this.IdArticulo = IdArticulo;
        this.IdUnidadMedida = IdUnidadMedida;
        this.ArtNom = ArtNom;
        this.ArtCos = ArtCos;
        this.ArtIgv = ArtIgv;
        this.ArtCat = ArtCat;
    }

    static int datos = 9;
    
    static String[] atributos = {"CodigoArticulo","Nombre Articulo", "CodigoUnidad","NombreUnidad","CodigoTipo","NombreTipo", "Costo","IGV","Estado Registro"};

    static String sql_Cons = "SELECT * FROM ARTICULO WHERE "+ARTESTREG+"=1";
    private MiModel model = new MiModel(null, atributos);

    public Articulo() {

    }

    
    public  static Articulo getArticuloByName(String name ){
    	String sql_Cons="SELECT * FROM articulo where artnom ='"+name+"' and artestreg=1";
        Articulo art = new Articulo();
        try {
            Statement sent;
          
            sent = cn.createStatement();
          //  System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
              art.setIdArticulo(rs.getInt("idArticulo"));
                art.setArtCos(Double.parseDouble(rs.getString("ArtCos")));
              art.setArtEstReg(Integer.parseInt(rs.getString("artEstReg")));
              art.setArtIgv(Double.parseDouble(rs.getString("artIgv")));
              art.setArtNom(rs.getString("artNom"));
              art.setIdArticulo(Integer.parseInt(rs.getString("idArticulo")));
              art.setIdUnidadMedida(Integer.parseInt(rs.getString("idUnidadMedida")));
                art.setArtCol(rs.getInt("artcol"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    	
    	return art;
    }
    public Articulo (int id ){
    	IdArticulo=id;
        String sql_Cons="SELECT * FROM articulo where idArticulo ="+id;
        try {
            Statement sent;
          
            sent = cn.createStatement();
          //  System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
              setArtCos(Double.parseDouble(rs.getString("ArtCos")));
              setArtEstReg(Integer.parseInt(rs.getString("artEstReg")));
              setArtIgv(Double.parseDouble(rs.getString("artIgv")));
              setArtNom(rs.getString("artNom"));
              setIdArticulo(Integer.parseInt(rs.getString("idArticulo")));
              setIdUnidadMedida(Integer.parseInt(rs.getString("idUnidadMedida")));
                setArtCol(rs.getInt("artcol"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    	
    	
    }

    public Articulo(int IdUnidadMedida, String ArtNom, double ArtCos, double ArtIgv) {
        this.IdUnidadMedida = IdUnidadMedida;
        this.ArtNom = ArtNom;
        this.ArtCos = ArtCos;
        this.ArtIgv = ArtIgv;
    }
    

    public int getIdArticulo() {
        return IdArticulo;
    }

    public void setIdArticulo(int IdArticulo) {
        this.IdArticulo = IdArticulo;
    }

    public int getIdUnidadMedida() {
        return IdUnidadMedida;
    }

    public void setIdUnidadMedida(int IdUnidadMedida) {
        this.IdUnidadMedida = IdUnidadMedida;
    }

    public String getArtNom() {
        return ArtNom;
    }

    public void setArtNom(String ArtNom) {
        this.ArtNom = ArtNom;
    }

    
    public int getArtEstReg() {
        return ArtEstReg;
    }

    public void setArtEstReg(int ArtEstReg) {
        this.ArtEstReg = ArtEstReg;
    }
    
    public double getArtCos() {
        return ArtCos;
    }

    public void setArtCos(double ArtCos) {
        this.ArtCos = ArtCos;
    }

    public double getArtIgv() {
        return ArtIgv;
    }

    public void setArtIgv(double ArtIgv) {
        this.ArtIgv = ArtIgv;
    }
    
    public void insertar() throws SQLException {
        //INSERT INTO `empresa`(`IdEmpresa`, `IdEmpresaGrupo`, `EmpRuc`, `EmpNom`, `EmpTel`, `EmpDir`, `EmpEstReg`)
        String sentIn = "INSERT INTO Articulo (" +IDUNIDADMEDIDA+","+ARTNOM+   ","+ARTCOS+ ","+ARTIGV+", ArtCat )";
        sentIn = sentIn + "Values (" +getIdUnidadMedida() + "," +getArtNom() + ","+ getArtCos()  +","+ getArtIgv()+","+getArtCat()+")";
     //   System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
       // System.out.println("guardado");
    }

    public void insertarConId() throws SQLException {
        //INSERT INTO `empresa`(`IdEmpresa`, `IdEmpresaGrupo`, `EmpRuc`, `EmpNom`, `EmpTel`, `EmpDir`, `EmpEstReg`)
        String sentIn = "INSERT INTO Articulo (idArticulo," +IDUNIDADMEDIDA+","+ARTNOM+   ","+ARTCOS+ ","+ARTIGV+", ArtCat )";
        sentIn = sentIn + "Values ("+getIdArticulo()+"," +getIdUnidadMedida() + "," +getArtNom() + ","+ getArtCos()  +","+ getArtIgv()+","+getArtCat()+")";
     //   System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
       // System.out.println("guardado");
    }
    public void modificar() throws SQLException {

        String sentIn = "update Articulo set  ";
        sentIn += IDUNIDADMEDIDA + "=" + getIdUnidadMedida()+ ",";
        sentIn += ARTNOM + "=" + getArtNom()+ ",";
        sentIn += ARTCOS + "=" + getArtCos()+ ",";
                sentIn += IDARTICULO + "=" + getIdArticulo()+",";

         sentIn += "ArtCat" + "=" + getArtCat()+ ",";
        sentIn += ARTIGV + "=" + getArtIgv() +  " where ";
        sentIn += IDARTICULO + "=" + getIdArticulo();

     //   System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
      //  System.out.println("guardado");
    }

    public void eliminar() throws SQLException {
        String sentIn = "update Articulo set  ";
        sentIn += ARTESTREG + "=" + 0 + " where ";
        sentIn += IDARTICULO + "=" + getIdArticulo();

    //    System.out.println(sentIn);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("guardado");
        
        
    }
    public void llenarTabla(JTable tabla, String id) {
        try {
            Statement sent;
            //if (id.length() != 0) {
                sql_Cons = "SELECT * FROM articulo WHERE "+ ARTNOM+ " LIKE '%" + id + "%' and "+ARTESTREG+" =1";
              //  System.out.println(sql_Cons);
            //}
            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sql_Cons);

            String[] fila = new String[datos];
            while (rs.next()) {
                                
                fila[0] = rs.getString(IDARTICULO);
                fila[1] = rs.getString(ARTNOM);
                fila[2] = rs.getString(IDUNIDADMEDIDA);
                
                Statement sent2 = cn.createStatement();
                ResultSet rs2 = sent2.executeQuery("SELECT * FROM `unidadmedida` WHERE "+ IDUNIDADMEDIDA+"="+fila[2] );
                
                if(rs2.next()){
                    fila[3]= rs2.getString(UNIMEDDES);
                }
                fila[4] = rs.getString("ArtCat");
                Statement sent3 = cn.createStatement();
                ResultSet rs3 = sent3.executeQuery("SELECT * FROM `CATEGORIA` WHERE "+ IDCATEGORIA+"="+fila[4] );
                
                if(rs3.next()){
                    fila[5]= rs3.getString(CATNOM);
                }
                fila[6] = rs.getString(ARTCOS);
                fila[7] = rs.getString(ARTIGV);
          
                fila[8] = "ACTIVO";
                model.addRow(fila);
            }
            tabla.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void llenarTablaWhere(JTable tabla, String where) {
        try {
            Statement sent;
            //if (id.length() != 0) {
                sql_Cons = "SELECT * FROM articulo where "+where ;
               // System.out.println(sql_Cons);
            //}
            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sql_Cons);

            String[] fila = new String[datos];
            while (rs.next()) {
                                
                fila[0] = rs.getString(IDARTICULO);
                fila[1] = rs.getString(ARTNOM);
                fila[2] = rs.getString(IDUNIDADMEDIDA);
                
                Statement sent2 = cn.createStatement();
                ResultSet rs2 = sent2.executeQuery("SELECT * FROM `unidadmedida` WHERE "+ IDUNIDADMEDIDA+"="+fila[2] );
                
                if(rs2.next()){
                    fila[3]= rs2.getString(UNIMEDDES);
                }
                fila[4] = rs.getString("ArtCat");
                Statement sent3 = cn.createStatement();
                ResultSet rs3 = sent3.executeQuery("SELECT * FROM `CATEGORIA` WHERE "+ IDCATEGORIA+"="+fila[4] );
                
                if(rs3.next()){
                    fila[5]= rs3.getString(CATNOM);
                }
                fila[6] = rs.getString(ARTCOS);
                fila[7] = rs.getString(ARTIGV);
          
                fila[8] = "ACTIVO";
                model.addRow(fila);
            }
            tabla.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void generarCodigo(JTextField a){
        try {
            Statement sent2 = cn.createStatement();
            ResultSet rs2 = sent2.executeQuery("SELECT MAX(IdArticulo) FROM ARTICULO");
            if(rs2.next())
                a.setText((Integer.parseInt(rs2.getString(1))+1)+"");
        } catch (SQLException ex) {
            Logger.getLogger(Articulo.class.getName()).log(Level.SEVERE, null, ex);
        }
            

    }
    public static void llenarCaja(JComboBox jComboBox1) {
        try {
            jComboBox1.removeAllItems();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM `unidadmedida` WHERE UniMedEstReg=1");
            while (rs.next()) //if(jComrs.getString("DesUniMed"))
            {
                jComboBox1.addItem(rs.getString(UNIMEDDES));
            }

        } catch (SQLException ex) {
            Logger.getLogger(Articulo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void llenarCategoria(JComboBox jComboBox1) {
        try {
            jComboBox1.removeAllItems();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM `categoria` WHERE CatEstReg=1");
            while (rs.next()) //if(jComrs.getString("DesUniMed"))
            {
                jComboBox1.addItem(rs.getString(CATNOM));
            }

        } catch (SQLException ex) {
            Logger.getLogger(Articulo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getArtCol() {
        return ArtCol;
    }

    public void setArtCol(int ArtCol) {
        this.ArtCol = ArtCol;
    }
     public static ArrayList<Articulo> getLike(String like) {
        
        
        
        ArrayList < Articulo>  salida = new ArrayList<>();
        
        String sql_Cons="SELECT * FROM articulo where artEstReg= 1 and artNom like '%"+like+"%'  ";
        try {
            Statement sent;
          
            sent = cn.createStatement();
           // System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);
            
            while (rs.next()) {
                Articulo art = new Articulo();
                art.setIdArticulo(rs.getInt("IdArticulo"));
              art.setArtCos(Double.parseDouble(rs.getString("ArtCos")));
             art. setArtEstReg(Integer.parseInt(rs.getString("artEstReg")));
             art. setArtIgv(Double.parseDouble(rs.getString("artIgv")));
             art. setArtNom(rs.getString("artNom"));
             art. setIdArticulo(Integer.parseInt(rs.getString("idArticulo")));
            art.  setIdUnidadMedida(Integer.parseInt(rs.getString("idUnidadMedida")));
              art.  setArtCol(rs.getInt("artcol"));
            salida.add(art);
            
            
            }
        } catch (Exception e) {
            e.printStackTrace();
        } 
    return salida ;}
}


