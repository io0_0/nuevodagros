package Bean;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import Conexion.Mysql;
import Maestros.*;

/**
 *
 * @author Equipo EPIS
 */
public class MovimientoTipo implements Atributos {

    public static Connection cn = Mysql.getConection();
    private int IdMovimientoTipo;
    private String MovTipDes;
    private int MovTipEstReg = 1;

    
    int datos = 3;//Para la JTable

    String[] atributos = {"Codigo", "Descripcion", "Estado Registro"};

    String sql_Cons="SELECT * FROM MOVIMIENTOTIPO WHERE "+MOVTIPESTREG+"=1";
    
    private MiModel model = new MiModel(null, atributos);

    public MovimientoTipo(String descMovTip) {
        MovTipDes=descMovTip;
    }

    public MovimientoTipo() {
    }

    public void insertar() throws SQLException {
        String sentIn = "INSERT INTO MOVIMIENTOTIPO (" + MOVTIPDES+ "," + MOVTIPESTREG + ")";
        sentIn = sentIn + "Values (" + this.getMovTipDes() + "," + this.getMovTipEstReg()+ ")";
        System.out.println(sentIn);//Imprimir
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentIn);
        System.out.println("Guardado");
    }

    public void modificar() throws SQLException {

        String sentUp = "UPDATE MOVIMIENTOTIPO SET  ";
        sentUp += MOVTIPDES + "=" + getMovTipDes() + ",";
        sentUp += MOVTIPESTREG + "=" + getMovTipEstReg() + " WHERE ";
        sentUp += IDMOVIMIENTOTIPO + "=" + this.getIdMovimientoTipo();

        System.out.println(sentUp);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentUp);
        System.out.println("Modificado");
    }

    public void eliminar() throws SQLException {
        String sentEl = "UPDATE MOVIMIENTOTIPO SET  ";
        sentEl += MOVTIPESTREG + "=" + 0 + " WHERE ";
        sentEl += IDMOVIMIENTOTIPO + "=" + this.getIdMovimientoTipo();

        System.out.println(sentEl);
        Statement sent = cn.createStatement();
        sent.executeUpdate(sentEl);
        System.out.println("Eliminado");
    }

    public void llenarTabla(JTable tabla, String id) {
        try {
            Statement sent;
            if (id.length() != 0) {
                sql_Cons = "SELECT * FROM MOVIMIENTOTIPO WHERE " + MOVTIPDES + " LIKE '%" + id + "%' and " + MOVTIPESTREG + "=1";
                System.out.println(sql_Cons);
            }
            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sql_Cons);

            String[] fila = new String[datos];
            while (rs.next()) {
                fila[0] = rs.getString(IDMOVIMIENTOTIPO);
                fila[1] = rs.getString(MOVTIPDES);
                fila[2] = "ACTIVO";
                model.addRow(fila);
            }
            tabla.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   
    
    public int getIdMovimientoTipo() {
        return IdMovimientoTipo;
    }

    public void setIdMovimientoTipo(int IdMovimientoTipo) {
        this.IdMovimientoTipo = IdMovimientoTipo;
    }

    public String getMovTipDes() {
        return MovTipDes;
    }

    public void setMovTipDes(String MovTipDes) {
        this.MovTipDes = MovTipDes;
    }

    public int getMovTipEstReg() {
        return MovTipEstReg;
    }

    public void setMovTipEstReg(int MovTipEstReg) {
        this.MovTipEstReg = MovTipEstReg;
    }
    

    
}
