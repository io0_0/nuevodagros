/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package COMPRASBorrego;

import Facturador.*;
import Bean.Articulo;
import Bean.Configurar;
import Maestros.GenerarCodigo;
import Maestros.Maestro;
import GUI.EmpresaGUI;
import Maestros.ArticuloM;
import static Maestros.ArticuloM.cn;
import Bean.Empresa;
import Bean.EmpresaSede;
import Bean.Generico;
import Bean.Lista;
import Bean.MovimientoCab;
import Bean.MovimientoDet;
import Bean.Observacion;
import Bean.UnidadMedida;
import Bean.Usuario;
import Bean.empresasedeserie;
import Conexion.Mysql;
import GUI.Principal;
import Maestros.Atributos;
import static Maestros.Atributos.IDMOVIMIENTOCABECERA;
import static Maestros.Atributos.MOVDETESTREG;
import Maestros.MiModel;
import Maestros.MovimientoDetalleM;
import Modificar.MODIFICARCOMPROBANTE;
import br.com.adilson.util.Extenso;
import br.com.adilson.util.PrinterMatrix;
import com.mysql.jdbc.MySQLConnection;
import com.toedter.calendar.JCalendar;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.swing.*;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Equipo EPIS
 */
public class ComprasBorre extends javax.swing.JFrame implements Atributos {

    protected factura FACTURA = new factura();
    ArrayList<Component> editables = new ArrayList<Component>();
    public static final int ANCHO_FORM_LLENAR = 972;
    public static final int ALTO_FORM_LLENAR = 375;
    public static final int ANCHO_FORM_MOSTRAR = 1400;
    public static final int ALTO_FORM_MOSTRAR = 755;
    public static final int ANCHO_FORM_BOT = 335;
    public static final int ALTO_FORM_BOT = 10;
    public static final int ANCHO_FORM = 1400;
    public static final int ALTO_FORM = 1100;
    public static int anchoLlenar;
    public static int altoLlenar;
    boolean bandera = true;
    int cantDatos = 9;
    public int indiceFila = 0;
    public static ComprasBorre actual;
    Maestro maestro = new Maestro();
    //Este objeto siempre tendra los atributos de la Tabla para que se ejecute sql en la clase EmpresaSql.
    MovimientoDetalleM ejecutor = maestro.gestionarMovimientoDetalle();
    MovimientoCab cabe = new MovimientoCab();
    private String usuario = "";
    protected String otroCamp = "";
    boolean isModificando = false;
    public panelPedido[] paneles = new panelPedido[50];
    static int anchoFinal = 0;
    static int altoFinal = 0;

    public ComprasBorre(String desu) throws SQLException {
        
        if (actual != null) {
            actual.dispose();
        }
        initComponents();

        usuario = desu;
        iniMio();
        actual = this;
    }

    public ComprasBorre(factura fac) {
        if (actual != null) {
            actual.dispose();
        }

        initComponents();

        iniMio();
        ejecutor.llenarCajaArtVentas(JComboArt);

        JTabla.setForeground(Color.red);
     //   jComboDocTipo.setEnabled(false);

        try {
            fac.obtenerDetalles();
        } catch (SQLException ex) {

            System.err.println("mal ");

            Logger.getLogger(ComprasBorre.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (MovimientoDet det : fac.getDetalles()) {

            det.setMovDetCan(det.getMovDetCan().substring(0, det.getMovDetCan().length() - 3));
        }
        isModificando = true;
        FACTURA = fac;
        FACTURA.getDetalles();

        llenarDatos();
        llenarComentario();

        actual = this;
        
    }

    public ComprasBorre() throws SQLException {
        //    FACTURA.setCabecera(new MovimientoCab());
        if (actual != null) {
            actual.dispose();
        }

        initComponents();
        iniMio();

        ejecutor.llenarCajaArtVentas(JComboArt);

        estado(1);
        paraPruebas();
        actual = this;

    }

    public void iniMio() {
        try {
            List<String> lista = PrintUtility.getPrinterServiceNameList();
            for (String a : lista) {
                System.out.println(a);
            }
            JTCodigoCab.setVisible(false);

            JTabla.setForeground(Color.red);
          //  jComboDocTipo.setEnabled(false);
            refrescar();
            JCalendar jj = new JCalendar();
            fecha1.setDate(jj.getDate());
            cabe.llenarCaja(jComboDocTipo, "DOCUMENTO", "DocDes");
            cabe.llenarCaja(jComboComprobate, "MOVIMIENTODocumento", "MOVDocDes", "  where movdocestreg = 1");

            String sql_Cons = "SELECT * FROM empresa where idempresaGrupo =2";

            // trabajr con limites de porciones 
            //el asunto limites de recursos 
            //aguadito de pollo para las parillas
            llenarEmpresaSede(); 
            
                //ejecutor.llenarCajaArt(JComboArt);
            //FACTURA.getCabecera().generarCodigo(JTCodigoCab);
            MovimientoCab movi = new MovimientoCab();
            movi.setIdEmpresaSedeDes(cabe.sacarDato("empresaSEde ", "empsednom", "'" + jComboEmpSedOrig.getSelectedItem().toString() + "'"));
            //String revisar=RevisarDocumento();
            //movi.setIdEmpresaSede(revisar);
            movi.setIdMovimientoCabecera("''");
            String idmovdoc = cabe.sacarDato("movimientodocumento", "movdocDes", "'" + jComboComprobate.getSelectedItem().toString() + "'");
            movi.setIdMovimientoDocumento(idmovdoc);
            movi.setIdUsuario(Usuario.getUsuarioActual().getIdUsuario()+"");
            movi.setIdMovimientoTipo("1");

//            movi.setMovCabEnv("0");
            movi.setMovCabEstReg("1");
            //Date da=new Date();

            movi.setMovCabFec("'" + ((JTextField) (fecha1.getDateEditor().getUiComponent())).getText() + "'");
            movi.setMovCabMovSto("1");
            movi.setMovCabNum("'" + jTextNumero.getText() + "'");
            movi.setMovCabSer("'" + jTextSerie.getText() + "'");
            FACTURA.setCabecera(movi);

            jComboComprobate.setSelectedIndex(0);
            jComboDocTipo.setSelectedIndex(1);


// TODO add your handling code here:
            empresasedeserie data;
            String idDocumento =    cabe.sacarDato("movimientodocumento", "movdocDes", "'" + jComboComprobate.getSelectedItem().toString() + "'");

            // FACTURA.getCabecera().setIdEmpresaSedeDes(jComboEmpSedOrig.getSelectedItem().toString());
            data = new empresasedeserie(FACTURA.getCabecera().sacarDato("empresasede", "empsednom", "'" + jComboEmpSedOrig.getSelectedItem().toString() + "'"), idDocumento);
            int datos2 = Integer.parseInt(data.getEmpsedsernum());
            datos2++;
            String cero = "";
            for (int i = (int) (Math.floor(Math.log10(datos2))); i < data.getEmpsedsernum().length() - 1; i++) {
                cero += 0;

            }
            jTextNumero.setText(cero + "" + datos2);
            jTextSerie.setText(data.getEmpsedserser());
        } catch (SQLException ex) {
            Logger.getLogger(ComprasBorre.class.getName()).log(Level.SEVERE, null, ex);
        }
        jComboDocTipo.setEnabled(true);
        editables.add(jComboComprobate);
        editables.add(jComboDocTipo);
        editables.add(jComboEmpSedOrig);
        editables.add(JBEliminar);
          jTextSerie.setDocument(new JTextFieldLimit(4));
        altoFinal = FormParaLlenar.getHeight() + 100;
        anchoFinal = FormParaLlenar.getWidth();

        altoLlenar = FormParaLlenar1.getHeight() + 10;
        anchoLlenar = FormParaLlenar1.getWidth();
    }

    public void seteditables() {
        for (Component editable : editables) {
            editable.setEnabled(false);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        FormParaLlenar = new javax.swing.JPanel();
        fecha1 = new com.toedter.calendar.JDateChooser();
        JTCodigoCab = new javax.swing.JTextField();
        jLabelTitulo = new javax.swing.JLabel();
        FormBotones = new javax.swing.JPanel();
        JBNuevo = new javax.swing.JButton();
        JBModificar = new javax.swing.JButton();
        JBEliminar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        JTabla = new javax.swing.JTable();
        jTextFieldTotal = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTextFieldIGV = new javax.swing.JTextField();
        JTextFieldsumatoPre = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        JButtonGuardar = new javax.swing.JButton();
        JButtonSalir = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jComboEmpSedOrig = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();
        jComboComprobate = new javax.swing.JComboBox();
        jLabel10 = new javax.swing.JLabel();
        jComboDocTipo = new javax.swing.JComboBox();
        jLabelCambiar = new javax.swing.JLabel();
        JTDocumentoDestino = new javax.swing.JTextField();
        jLabelParaNoobs = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        JTNombre = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        JTDireccion = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTextSerie = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jTextNumero = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        FormParaLlenar1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        JBGrabar1 = new javax.swing.JButton();
        JBSalir1 = new javax.swing.JButton();
        JComboArt = new javax.swing.JComboBox();
        jLabel14 = new javax.swing.JLabel();
        JTCantidad = new javax.swing.JTextField();
        jtbuscar = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        JTCOMPRAS = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        JTUnitario = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        jLabelUnidadMedida = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabelIGV = new javax.swing.JLabel();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("COMPROBANTE DE VENTA");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setForeground(new java.awt.Color(255, 255, 255));
        setResizable(false);

        FormParaLlenar.setBackground(new java.awt.Color(255, 255, 255));

        fecha1.setDateFormatString("dd-MM-yyyy");

        JTCodigoCab.setEnabled(false);
        JTCodigoCab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTCodigoCabActionPerformed(evt);
            }
        });

        jLabelTitulo.setFont(new java.awt.Font("Tahoma", 0, 22)); // NOI18N
        jLabelTitulo.setText("REGISTRO COMPRAS: BOLETA");

        FormBotones.setBackground(new java.awt.Color(255, 255, 255));
        FormBotones.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 51, 0), 3));

        JBNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/NUEVO.JPG"))); // NOI18N
        JBNuevo.setText("Agregar");
        JBNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBNuevoActionPerformed(evt);
            }
        });

        JBModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/MODIFICAR.JPG"))); // NOI18N
        JBModificar.setText("Modificar");
        JBModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBModificarActionPerformed(evt);
            }
        });

        JBEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/ELIMINAR.JPG"))); // NOI18N
        JBEliminar.setText("Eliminar");
        JBEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBEliminarActionPerformed(evt);
            }
        });

        JTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        JTabla.setGridColor(new java.awt.Color(0, 0, 0));
        jScrollPane2.setViewportView(JTabla);

        jTextFieldTotal.setEditable(false);
        jTextFieldTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldTotalActionPerformed(evt);
            }
        });

        jLabel11.setText("TOTAL");

        jTextFieldIGV.setEditable(false);
        jTextFieldIGV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldIGVActionPerformed(evt);
            }
        });

        JTextFieldsumatoPre.setEditable(false);

        jLabel18.setText("PRETOTAL");

        jLabel19.setText("TOTAL IGV");

        javax.swing.GroupLayout FormBotonesLayout = new javax.swing.GroupLayout(FormBotones);
        FormBotones.setLayout(FormBotonesLayout);
        FormBotonesLayout.setHorizontalGroup(
            FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormBotonesLayout.createSequentialGroup()
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane2)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, FormBotonesLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(JTextFieldsumatoPre, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel19)
                        .addGap(10, 10, 10)
                        .addComponent(jTextFieldIGV, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(JBNuevo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(JBModificar, javax.swing.GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE)
                    .addComponent(JBEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(232, 232, 232))
        );
        FormBotonesLayout.setVerticalGroup(
            FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormBotonesLayout.createSequentialGroup()
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(FormBotonesLayout.createSequentialGroup()
                        .addComponent(JBNuevo)
                        .addGap(18, 18, 18)
                        .addComponent(JBModificar)
                        .addGap(18, 18, 18)
                        .addComponent(JBEliminar)
                        .addGap(177, 177, 177))
                    .addGroup(FormBotonesLayout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldIGV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19)
                    .addComponent(JTextFieldsumatoPre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18))
                .addContainerGap(53, Short.MAX_VALUE))
        );

        JButtonGuardar.setText("Guardar");
        JButtonGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JButtonGuardarActionPerformed(evt);
            }
        });

        JButtonSalir.setText("Salir");
        JButtonSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JButtonSalirActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 0, 0), 3));

        jLabel4.setText("NUESTRA EMPRESA");

        jComboEmpSedOrig.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboEmpSedOrig.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboEmpSedOrigItemStateChanged(evt);
            }
        });
        jComboEmpSedOrig.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboEmpSedOrigActionPerformed(evt);
            }
        });

        jLabel9.setText("COMPROBANTE");

        jComboComprobate.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboComprobate.setName("combo"); // NOI18N
        jComboComprobate.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboComprobateItemStateChanged(evt);
            }
        });
        jComboComprobate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboComprobateActionPerformed(evt);
            }
        });

        jLabel10.setText("TIPO DOCUMENTO");

        jComboDocTipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabelCambiar.setText("NR° DOC/PROV");

        JTDocumentoDestino.setText("0");
        JTDocumentoDestino.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTDocumentoDestinoActionPerformed(evt);
            }
        });
        JTDocumentoDestino.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                JTDocumentoDestinoKeyTyped(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                JTDocumentoDestinoKeyReleased(evt);
            }
        });

        jLabelParaNoobs.setText("el ruc tiene que tener 11 digitos ");

        jLabel8.setText("NOBRE PROVEEDOR");

        JTNombre.setText("CLIENTES VARIOS");
        JTNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                JTNombreKeyTyped(evt);
            }
        });

        jLabel15.setText("DIRECCION ");

        JTDireccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTDireccionActionPerformed(evt);
            }
        });
        JTDireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                JTDireccionKeyTyped(evt);
            }
        });

        jLabel7.setText("SERIE");

        jTextSerie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextSerieActionPerformed(evt);
            }
        });
        jTextSerie.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextSerieKeyTyped(evt);
            }
        });

        jLabel17.setText("NUMERO");

        jTextNumero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextNumeroActionPerformed(evt);
            }
        });
        jTextNumero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextNumeroKeyTyped(evt);
            }
        });

        jLabel13.setText("OBSERVACION");

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jComboEmpSedOrig, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabelCambiar)
                            .addComponent(jLabel13)
                            .addComponent(jLabel7)
                            .addComponent(jLabel15)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel17)
                                    .addComponent(jLabel8))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextSerie)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(3, 3, 3)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(JTDireccion)
                                            .addComponent(jLabelParaNoobs)
                                            .addComponent(JTNombre, javax.swing.GroupLayout.DEFAULT_SIZE, 266, Short.MAX_VALUE))
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(jTextNumero)))
                            .addComponent(jLabel9)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addGap(27, 27, 27)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jComboComprobate, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jComboDocTipo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(JTDocumentoDestino, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 348, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jComboEmpSedOrig, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jComboComprobate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboDocTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelCambiar)
                    .addComponent(JTDocumentoDestino, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(jLabelParaNoobs)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15)
                    .addComponent(JTDireccion, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jTextSerie, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(jTextNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout FormParaLlenarLayout = new javax.swing.GroupLayout(FormParaLlenar);
        FormParaLlenar.setLayout(FormParaLlenarLayout);
        FormParaLlenarLayout.setHorizontalGroup(
            FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormParaLlenarLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormParaLlenarLayout.createSequentialGroup()
                        .addComponent(JTCodigoCab, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(229, 229, 229)
                        .addComponent(jLabelTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 378, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(fecha1, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FormParaLlenarLayout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(FormParaLlenarLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(FormBotones, javax.swing.GroupLayout.PREFERRED_SIZE, 675, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(FormParaLlenarLayout.createSequentialGroup()
                                .addGap(149, 149, 149)
                                .addComponent(JButtonGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(JButtonSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 267, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addGap(39, 39, 39))
        );
        FormParaLlenarLayout.setVerticalGroup(
            FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormParaLlenarLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(JTCodigoCab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabelTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(fecha1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(FormParaLlenarLayout.createSequentialGroup()
                        .addComponent(FormBotones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(JButtonGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(JButtonSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(50, Short.MAX_VALUE))
        );

        FormParaLlenar1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setText("BUSCAR:");

        jLabel5.setText("ARTICULO:");

        JBGrabar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/GUARDAR2.JPG"))); // NOI18N
        JBGrabar1.setText("Grabar");
        JBGrabar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBGrabar1ActionPerformed(evt);
            }
        });

        JBSalir1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/CANCELAR_1.JPG"))); // NOI18N
        JBSalir1.setText("Salir sin Grabar");
        JBSalir1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBSalir1ActionPerformed(evt);
            }
        });

        JComboArt.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        JComboArt.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                JComboArtItemStateChanged(evt);
            }
        });
        JComboArt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JComboArtActionPerformed(evt);
            }
        });
        JComboArt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JComboArtKeyPressed(evt);
            }
        });

        jLabel14.setText("CANTIDAD:");

        JTCantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTCantidadActionPerformed(evt);
            }
        });
        JTCantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                JTCantidadKeyTyped(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JTCantidadKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                JTCantidadKeyReleased(evt);
            }
        });

        jtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtbuscarActionPerformed(evt);
            }
        });
        jtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jtbuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtbuscarKeyReleased(evt);
            }
        });

        jLabel20.setText("PRECIO TOTAL");

        JTCOMPRAS.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                JTCOMPRASFocusGained(evt);
            }
        });
        JTCOMPRAS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTCOMPRASActionPerformed(evt);
            }
        });
        JTCOMPRAS.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                JTCOMPRASKeyTyped(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JTCOMPRASKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                JTCOMPRASKeyReleased(evt);
            }
        });

        jLabel21.setText("PRECIO UNITARIO");

        JTUnitario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTUnitarioActionPerformed(evt);
            }
        });
        JTUnitario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                JTUnitarioKeyTyped(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JTUnitarioKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                JTUnitarioKeyReleased(evt);
            }
        });

        jLabel22.setText("UNIDAD MEDIDA:");

        jLabelUnidadMedida.setText("LITROS");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(jTable1);

        jLabelIGV.setText("NO POSEE");

        javax.swing.GroupLayout FormParaLlenar1Layout = new javax.swing.GroupLayout(FormParaLlenar1);
        FormParaLlenar1.setLayout(FormParaLlenar1Layout);
        FormParaLlenar1Layout.setHorizontalGroup(
            FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormParaLlenar1Layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormParaLlenar1Layout.createSequentialGroup()
                        .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel21)
                            .addGroup(FormParaLlenar1Layout.createSequentialGroup()
                                .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, FormParaLlenar1Layout.createSequentialGroup()
                                            .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                        .addGroup(FormParaLlenar1Layout.createSequentialGroup()
                                            .addComponent(jLabel14)
                                            .addGap(71, 71, 71)))
                                    .addGroup(FormParaLlenar1Layout.createSequentialGroup()
                                        .addComponent(jLabel22)
                                        .addGap(42, 42, 42)))
                                .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(JTCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(JTUnitario, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(JComboArt, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabelUnidadMedida)
                                    .addComponent(JTCOMPRAS, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabelIGV, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(FormParaLlenar1Layout.createSequentialGroup()
                                .addGap(101, 101, 101)
                                .addComponent(JBGrabar1)
                                .addGap(57, 57, 57)
                                .addComponent(JBSalir1)))
                        .addGap(64, 64, 64))
                    .addGroup(FormParaLlenar1Layout.createSequentialGroup()
                        .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(77, Short.MAX_VALUE))
        );
        FormParaLlenar1Layout.setVerticalGroup(
            FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormParaLlenar1Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormParaLlenar1Layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(FormParaLlenar1Layout.createSequentialGroup()
                        .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(JComboArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel22)
                            .addComponent(jLabelUnidadMedida))
                        .addGap(18, 18, 18)
                        .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(JTCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel14))
                        .addGap(18, 18, 18)
                        .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(JTUnitario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormParaLlenar1Layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(FormParaLlenar1Layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(jLabel20))
                            .addGroup(FormParaLlenar1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(JTCOMPRAS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelIGV)
                        .addGap(16, 16, 16)
                        .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(JBGrabar1)
                            .addComponent(JBSalir1))
                        .addGap(32, 32, 32))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(FormParaLlenar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(67, 67, 67)
                .addComponent(FormParaLlenar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(FormParaLlenar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(FormParaLlenar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JBSalir1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBSalir1ActionPerformed
        estado(1);
        try {
            refrescar();
        } catch (SQLException ex) {
            Logger.getLogger(ComprasBorre.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_JBSalir1ActionPerformed

    private void JBGrabar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBGrabar1ActionPerformed

        grabarDetalle();


    }//GEN-LAST:event_JBGrabar1ActionPerformed

    private void JTCantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTCantidadActionPerformed

    }//GEN-LAST:event_JTCantidadActionPerformed

    private void JTCantidadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTCantidadKeyTyped

        try {
            double s = Double.parseDouble(JTCantidad.getText()) * Double.parseDouble(JTUnitario.getText());
            s = Math.round(s*1000)/1000;
            JTCOMPRAS.setText("" + s);
        } catch (Exception e) {
        }

    }//GEN-LAST:event_JTCantidadKeyTyped

    private void jTextNumeroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextNumeroKeyTyped
        char c = evt.getKeyChar();

        if (Character.isLetter(c)) {
            getToolkit().beep();
            evt.consume();

        }
    }//GEN-LAST:event_jTextNumeroKeyTyped

    private void jTextNumeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextNumeroActionPerformed

    }//GEN-LAST:event_jTextNumeroActionPerformed

    private void jTextSerieKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextSerieKeyTyped


    }//GEN-LAST:event_jTextSerieKeyTyped

    private void jTextSerieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextSerieActionPerformed

    }//GEN-LAST:event_jTextSerieActionPerformed

    private void JBEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBEliminarActionPerformed
        if (JTabla.getSelectedRow() != -1 && JTabla.getSelectedRow() < FACTURA.getDetalles().size()) {
                FACTURA.getDetalles().remove(JTabla.getSelectedRow());
        
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un item  valido para Eliminar");
        }
        estado(1);
       double totSinIGV= 0.0;
       double igv= 0.0;
        for (MovimientoDet detalle : FACTURA.getDetalles()) {
            totSinIGV += Double.parseDouble(detalle.getMovDetCosTot());
            igv +=Double.parseDouble(detalle.getMovDetIgv());
        }
        double tot = totSinIGV+igv;
        DecimalFormat df = new DecimalFormat(".##");
JTextFieldsumatoPre.setText(df.format(totSinIGV));
jTextFieldIGV.setText(df.format(igv));
jTextFieldTotal.setText(df.format(tot));
    }//GEN-LAST:event_JBEliminarActionPerformed

    private void JBModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBModificarActionPerformed
        bandera = false;
        String[] datos = new String[cantDatos];
        ejecutor.llenarCajaArtVentas(JComboArt);

        if (JTabla.getSelectedRow() != -1 && JTabla.getSelectedRow() < FACTURA.getDetalles().size()) {
            estado(2);
            indiceFila = JTabla.getSelectedRow();
            JTCodigoCab.setEditable(false);

            JTCantidad.setText(FACTURA.getDetalles().get(indiceFila).getMovDetCan());
           double ddd = Double.parseDouble(FACTURA.getDetalles().get(indiceFila).getMovDetCosTot())+ Double.parseDouble(FACTURA.getDetalles().get(indiceFila).getMovDetIgv());
           ddd = Math.round(ddd*1000)/1000.0;
            JTCOMPRAS.setText(ddd+" ");
            int j;
            Map<Integer, Articulo> art = new HashMap<Integer, Articulo>();
            for (j = 0; j < JComboArt.getItemCount(); j++) {
                String idAr = FACTURA.getDetalles().get(indiceFila).getIdArticulo();
                if (JComboArt.getItemAt(j).equals((new Articulo(Integer.parseInt(idAr))).getArtNom())) {
                    break;
                }
            }
            JComboArt.setSelectedItem(JComboArt.getItemAt(j));

            datos[8] = "1";

        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un item valido para modificar");
        }
    }//GEN-LAST:event_JBModificarActionPerformed

    private void JBNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBNuevoActionPerformed
        estado(2);
        jtbuscar.setText("");
        if (FACTURA.getDetalles().size() == 0) {
            otroCamp = ejecutor.generarCodigo();
            // System.out.println("este es el id q estas sacando "+otroCamp);

        } else {
            int valor = -1;
            for (MovimientoDet col : FACTURA.getDetalles()) {
                valor = Math.max(valor, Integer.parseInt(col.getIdMovimientoDetalle()));
            }
            valor++;
            otroCamp = "" + valor;

        }

        ejecutor.llenarCajaArtVentas(JComboArt);
        JTCantidad.setText("");
        JTCOMPRAS.setText("");
        bandera = true;
        String art = JComboArt.getSelectedItem().toString();
        llenarTabla2(art);
        //  limpiar();

    }//GEN-LAST:event_JBNuevoActionPerformed

    private void jComboEmpSedOrigActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboEmpSedOrigActionPerformed
        // TODO add your handling code here:
        // FACTURA.getCabecera().setIdEmpresaSedeDes(jComboEmpSedOrig.getSelectedItem().toString());
    }//GEN-LAST:event_jComboEmpSedOrigActionPerformed

    private void jComboEmpSedOrigItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboEmpSedOrigItemStateChanged
        /*empresasedeserie data;
        if (jComboEmpSedOrig.getSelectedItem() != null) {

            try {
                // FACTURA.getCabecera().setIdEmpresaSedeDes(jComboEmpSedOrig.getSelectedItem().toString());
                data = new empresasedeserie(MovimientoCab.sacarDato("empresasede", "empsednom", "'" + jComboEmpSedOrig.getSelectedItem().toString() + "'"));
                int datos =Integer.parseInt(data.getEmpsedsernum());
                datos++;
                jTextNumero.setText(""+datos);
                jTextSerie.setText(data.getEmpsedserser());
            } catch (SQLException ex) {
                Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }*/
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboEmpSedOrigItemStateChanged

    private void jComboComprobateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboComprobateActionPerformed

        // los nuevos registros son agregados al MODEL del JCombo HIJO

    }//GEN-LAST:event_jComboComprobateActionPerformed

    private void JTDocumentoDestinoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTDocumentoDestinoKeyTyped
        char c = evt.getKeyChar();

        if (Character.isLetter(c)) {
            getToolkit().beep();
            evt.consume();

        }
    }//GEN-LAST:event_JTDocumentoDestinoKeyTyped

    private void JTDocumentoDestinoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTDocumentoDestinoActionPerformed
        String entrada = jComboComprobate.getSelectedItem().toString();
        switch (entrada) {

        }
        // TODO add your handling code here:
    }//GEN-LAST:event_JTDocumentoDestinoActionPerformed

    private void jtbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtbuscarKeyPressed
                           if(evt.getKeyCode()==KeyEvent.VK_ENTER){
                           JComboArt.requestFocusInWindow();
                           return;
                           }
        try {
            String condicion = "Select * from articulo where ArtEstReg =1 AND ArtNom like '%" + jtbuscar.getText() + "%'";
            //   System.err.println(condicion);
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(condicion);
            JComboArt.removeAllItems();
            int a = 0;
            //     System.err.println("presionoooo¡¡¡");
            while (rs.next()) {

                JComboArt.addItem(rs.getString("ArtNom"));
                if (a == 0) {

                    UnidadMedida uni = new UnidadMedida(rs.getString("idUnidadMedida"));
                    jLabelUnidadMedida.setText(uni.getUniMedDes());

                }
                a++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ComprasBorre.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(JComboArt.getSelectedItem()!=null)
        llenarTabla2(JComboArt.getSelectedItem().toString());

    }//GEN-LAST:event_jtbuscarKeyPressed

    private void jTextFieldTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldTotalActionPerformed

    private void jTextFieldIGVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldIGVActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldIGVActionPerformed

    private void JTDocumentoDestinoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTDocumentoDestinoKeyReleased
        // TODO add your handling code here:

        String consulta = "select * from empresa where empruc ='" + JTDocumentoDestino.getText() + "'";

        try {
            Statement sent;
            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(consulta);

            if (rs.next()) {
                JTNombre.setText(rs.getString("empnom"));
                String consulta2 = "select * from empresasede where idempresa =" + rs.getString("idempresa");
                Statement sent2;
                sent2 = cn.createStatement();
                ResultSet rs2 = sent2.executeQuery(consulta2);
                if (rs2.next()) {
                    JTDireccion.setText(rs2.getString("EmpSedDir"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }//GEN-LAST:event_JTDocumentoDestinoKeyReleased

    private void jtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtbuscarKeyReleased

        try {
            String condicion = "Select * from articulo where ArtEstReg =1 AND ArtNom like '%" + jtbuscar.getText() + "%'";
            //System.err.println(condicion);
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(condicion);
            JComboArt.removeAllItems();
            while (rs.next()) {
                JComboArt.addItem(rs.getString("ArtNom"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ComprasBorre.class.getName()).log(Level.SEVERE, null, ex);
        }
        // TODO add your handling code here:
        // TODO add your handling code here:
    }//GEN-LAST:event_jtbuscarKeyReleased

    private void JComboArtItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_JComboArtItemStateChanged
        if (JComboArt.getSelectedItem() != null) {
            try {
                String id = FACTURA.getCabecera().sacarDato("articulo", "artnom", "'" + JComboArt.getSelectedItem().toString() + "'");
                Articulo art = new Articulo(Integer.parseInt(id));
                UnidadMedida uni = new UnidadMedida("" + art.getIdUnidadMedida());
                jLabelUnidadMedida.setText(uni.getUniMedDes());
                if(art.getArtIgv()>0.1){
                jLabelIGV.setText("POSEE IGV");
                }else{
                jLabelIGV.setText("NO POSEE IGV");
                }
                llenarTabla2(JComboArt.getSelectedItem().toString());
// TODO add your handling code here:
            } catch (SQLException ex) {
                Logger.getLogger(ComprasBorre.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_JComboArtItemStateChanged

    private void jComboComprobateItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboComprobateItemStateChanged
        // TODO add your handling code here:


    }//GEN-LAST:event_jComboComprobateItemStateChanged

    private void JTNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTNombreKeyTyped
        char c = evt.getKeyChar();
        if (Character.isDigit(c)) {
            evt.consume();
        } else {
            evt.setKeyChar(Character.toUpperCase(c));
        }
    }//GEN-LAST:event_JTNombreKeyTyped

    private void JTDireccionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTDireccionKeyTyped

    }//GEN-LAST:event_JTDireccionKeyTyped

    private void JTCodigoCabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTCodigoCabActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JTCodigoCabActionPerformed

    private void JTDireccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTDireccionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JTDireccionActionPerformed

    private void JTCOMPRASActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTCOMPRASActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JTCOMPRASActionPerformed

    private void JTCOMPRASKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTCOMPRASKeyTyped
  
calcularUnitario();      // TODO add your handling code here:
    }//GEN-LAST:event_JTCOMPRASKeyTyped

    private void JTUnitarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTUnitarioActionPerformed
//jtbuscar.requestFocusInWindow();        // TODO add your handling code here:
    }//GEN-LAST:event_JTUnitarioActionPerformed

    private void JTUnitarioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTUnitarioKeyTyped
        try {
            calcularPrecio();
        } catch (Exception e) {
        }
        // TODO add your handling code here:

    }//GEN-LAST:event_JTUnitarioKeyTyped

    private void JTCOMPRASKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTCOMPRASKeyPressed
calcularUnitario();        // TODO add your handling code here:

    }//GEN-LAST:event_JTCOMPRASKeyPressed

    private void JTCOMPRASKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTCOMPRASKeyReleased
        // TODO add your handling code here:
        
        
calcularUnitario();
        // JTCOMPRAS.setText(""+Integer.parseInt(JTCantidad.getText())*Integer.parseInt(JTUnitario.getText()));
    }//GEN-LAST:event_JTCOMPRASKeyReleased

    private void JTCOMPRASFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTCOMPRASFocusGained
        // TODO add your handling code here:
  
    }//GEN-LAST:event_JTCOMPRASFocusGained

    private void JTCantidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTCantidadKeyReleased
        try {
      calcularPrecio();
        } catch (Exception e) {
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_JTCantidadKeyReleased

    private void JTUnitarioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTUnitarioKeyReleased
        try {
            calcularPrecio();  } catch (Exception e) {
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_JTUnitarioKeyReleased

    private void JTUnitarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTUnitarioKeyPressed
    if(evt.getKeyCode()==KeyEvent.VK_ENTER){
                            jtbuscar.requestFocusInWindow();
      return ;                     
                           }
        calcularPrecio();
        // TODO add your handling code here:
    }//GEN-LAST:event_JTUnitarioKeyPressed

    private void JTCantidadKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTCantidadKeyPressed

            if(evt.getKeyCode()==KeyEvent.VK_ENTER){
                           JTUnitario.requestFocusInWindow();
                           return ;
                           }
        calcularPrecio();

      
// TODO add your handling code here:
    }//GEN-LAST:event_JTCantidadKeyPressed

    private void JButtonSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JButtonSalirActionPerformed

        this.dispose();        // TODO add your handling code here:
    }//GEN-LAST:event_JButtonSalirActionPerformed

    private void JButtonGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JButtonGuardarActionPerformed
        guardar();        // TODO add your handling code here:
    }//GEN-LAST:event_JButtonGuardarActionPerformed

    private void jtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtbuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtbuscarActionPerformed

    private void JComboArtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JComboArtActionPerformed

        // TODO add your handling code here:
    }//GEN-LAST:event_JComboArtActionPerformed

    private void JComboArtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JComboArtKeyPressed
    if(evt.getKeyCode()==KeyEvent.VK_ENTER){
                           JTCantidad.requestFocusInWindow();
                           return;
                           }        // TODO add your handling code here:
    }//GEN-LAST:event_JComboArtKeyPressed

    public void estado(int p) {
        ///*estado para buscar en la grilla*/ 
        if (p == 1) {
            try {
                this.setTitle("COMPROBANTE DE VENTA");
                //limpiarFormLlenar();
                //      System.out.println("this " + this.getWidth() + " " + this.getHeight());
                //    System.out.println("form para llenar" + FormParaLlenar.getWidth() + "  " + FormParaLlenar.getHeight());
                //  System.out.println("form botones " + FormBotones.getWidth() + "--" + FormBotones.getHeight());

                FormParaLlenar.setVisible(true);
                FormBotones.setVisible(true);
                FormParaLlenar1.setVisible(false);
                this.setSize(anchoFinal, altoFinal);
                //      cambiarTamaño(0.435F, 0.74F);
                centrarPantalla();
                refrescar();
            } catch (SQLException ex) {
                Logger.getLogger(ComprasBorre.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (p == 2)/*estado para ingresar un dato*/ {

            this.setTitle("INSERTAR PEDIDO ");
            this.setSize(anchoLlenar, altoLlenar);
            centrarPantalla();
            FormParaLlenar.setVisible(false);
            FormBotones.setVisible(false);
            FormParaLlenar1.setVisible(true);

        }
        if (p == 3) {

        }
    }

    private double getPrecioUni(MovimientoDet detallito) throws SQLException {
        Lista a = new Lista();
        String sql_Cons = "SELECT * FROM lista where idEmpresaSede =" + FACTURA.getCabecera().getIdEmpresaSedeDes() + " and ListEstReg=1";
        String listilla = "";
        try {
            Statement sent;

            sent = cn.createStatement();
            //  System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);

            while (rs.next()) {
                listilla = rs.getString("idlista");
                System.out.println(listilla);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Statement sent;

        sent = cn.createStatement();
        sql_Cons = "Select * From articuloprecio where  idLista= '" + listilla + "' AND idArticulo='" + detallito.getIdArticulo() + "'";
        System.out.println(sql_Cons);

        ResultSet rs = sent.executeQuery(sql_Cons);
        double resp = 0;
        if (rs.next()) {
            resp = Double.parseDouble(rs.getString("ArtPreDes"));

        }
        return resp;
    }

    private void centrarPantalla() {
        Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension ventana = this.getSize();
        this.setLocation((int) (pantalla.width - ventana.width) / 2, (int) (pantalla.height - ventana.height) / 2);

    }

    public void refrescar() throws SQLException {
        llenarTablaFactura(JTabla, "sadasdsadadwqeqsxqweqwsdxcascqw", FACTURA.getDetalles(), FACTURA.getCabecera());

        // limpiar();
        //  JTabla.setEnabled(false);
    }

    public void llenarTablaFactura(JTable tabla, String id, ArrayList<MovimientoDet> detalles, MovimientoCab cabe) {
        try {
            String[] atri = {"N°", "CANTIDAD", "DESCRIPCION", "PRECIO UNITARIO", "TOTAL"};
            int filas = 20;
            MiModel model1 = new MiModel(null, atri);
            String sql_Cons = "";
            Statement sent;
            if (id.length() != 0) {
                sql_Cons = "SELECT * FROM MOVIMIENTODET WHERE " + IDMOVIMIENTOCABECERA + " LIKE '%" + id + "%' and " + MOVDETESTREG + "=1";
                System.out.println(sql_Cons);
            }
            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sql_Cons);
            String[] fila = new String[5];
            int i = 0;
            while (rs.next()) {
                i++;
                fila[0] = "" + i;
                fila[1] = rs.getString("MovDetCan");
                fila[2] = MovimientoCab.sacarDato("articulo", "idarticulo", "" + rs.getInt("idarticulo"), "artnom");
                double venta = rs.getDouble("movdetpretot");
                double compra = rs.getDouble("movdetcostot")+rs.getDouble("movdetigv");
                double valorUnitario = 0;
                if (venta > compra) {
                    valorUnitario = venta / rs.getDouble("movdetcan");

                } else {
                    valorUnitario = compra / rs.getDouble("movdetcan");

                }
                double tot = valorUnitario * Double.parseDouble(fila[1]);

                valorUnitario = Math.round(valorUnitario*1000)/1000.0;
                fila[3] = "" + valorUnitario; 
                tot = Math.round(tot*1000)/1000.0;
                fila[4] = "" + tot;
                model1.addRow(fila);

            }
            for (MovimientoDet de : detalles) {
                i++;
                fila[0] = "" + i;
                fila[1] = de.getMovDetCan();
                fila[2] = MovimientoCab.sacarDato("articulo", "idarticulo", de.getIdArticulo(), "artnom");
                double venta = Double.parseDouble(de.getMovDetPreTot());
                double compra = Double.parseDouble(de.getMovDetCosTot())+Double.parseDouble(de.getMovDetIgv());
                double valorUnitario = 0;
                if (venta > compra) {
                    valorUnitario = venta / Double.parseDouble(de.getMovDetCan());

                } else {
                    valorUnitario = compra / Double.parseDouble(de.getMovDetCan());

                }

                double tot = valorUnitario * Double.parseDouble(fila[1]);

                valorUnitario = Math.round(valorUnitario*1000)/1000.0;

                fila[3] = "" + valorUnitario;

                tot = Math.round(tot*1000)/1000.0;
                fila[4] = "" + tot;
                model1.addRow(fila);

            }
            for (; i < filas; i++) {
                fila = new String[5];
                fila[0] = " ";
                fila[1] = " ";
                fila[2] = " ";
                fila[3] = " ";
                fila[4] = " ";

                model1.addRow(fila);
            }
            tabla.setModel(model1);

            tabla.getColumnModel().getColumn(0).setPreferredWidth(40);
            tabla.getColumnModel().getColumn(1).setPreferredWidth(100);
            tabla.getColumnModel().getColumn(2).setPreferredWidth(350);
            tabla.getColumnModel().getColumn(3).setPreferredWidth(250);
            tabla.getColumnModel().getColumn(4).setPreferredWidth(250);
            tabla.setBackground(Color.white);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void limpiar() {
        JTCantidad.setText("");
        JTDocumentoDestino.setText("");
        JTNombre.setText("");

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ComprasBorre a = new ComprasBorre();
                    a.setVisible(true);
                    a.estado(1);
                } catch (SQLException ex) {
                    Logger.getLogger(ComprasBorre.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel FormBotones;
    private javax.swing.JPanel FormParaLlenar;
    private javax.swing.JPanel FormParaLlenar1;
    private javax.swing.JButton JBEliminar;
    private javax.swing.JButton JBGrabar1;
    private javax.swing.JButton JBModificar;
    private javax.swing.JButton JBNuevo;
    private javax.swing.JButton JBSalir1;
    private javax.swing.JButton JButtonGuardar;
    private javax.swing.JButton JButtonSalir;
    protected javax.swing.JComboBox JComboArt;
    protected javax.swing.JTextField JTCOMPRAS;
    protected javax.swing.JTextField JTCantidad;
    private javax.swing.JTextField JTCodigoCab;
    private javax.swing.JTextField JTDireccion;
    private javax.swing.JTextField JTDocumentoDestino;
    private javax.swing.JTextField JTNombre;
    protected javax.swing.JTextField JTUnitario;
    private javax.swing.JTable JTabla;
    private javax.swing.JTextField JTextFieldsumatoPre;
    private static com.toedter.calendar.JDateChooser fecha1;
    protected javax.swing.JComboBox jComboComprobate;
    protected javax.swing.JComboBox jComboDocTipo;
    protected javax.swing.JComboBox jComboEmpSedOrig;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelCambiar;
    private javax.swing.JLabel jLabelIGV;
    private javax.swing.JLabel jLabelParaNoobs;
    private javax.swing.JLabel jLabelTitulo;
    private javax.swing.JLabel jLabelUnidadMedida;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextFieldIGV;
    private javax.swing.JTextField jTextFieldTotal;
    private javax.swing.JTextField jTextNumero;
    private javax.swing.JTextField jTextSerie;
    private javax.swing.JTextField jtbuscar;
    // End of variables declaration//GEN-END:variables

    private String RevisarDocumento() throws SQLException {
        ArrayList<String> desu = new ArrayList<String>();
        Statement sent2 = cn.createStatement();
        String consulta = "select * from empresa where ";
        if (consulta.length() != 0) {
            consulta += "  empRUC= " + JTDocumentoDestino.getText();
        }
        //  System.out.println("el chato es 2k confirmed  y si no la saco yo tambien " + consulta);

        ResultSet rs2 = sent2.executeQuery(consulta);
        String retorno = "";
        if (rs2.next()) {
            Statement sent3 = cn.createStatement();
            String idempresa = rs2.getString("idempresa");
            Empresa empresaModi = new Empresa(idempresa);
            String consulta1 = "select * from empresaSede where idEmpresa = " + idempresa;
            ResultSet rs3 = sent3.executeQuery(consulta1);
            if (rs3.next()) {
                retorno = rs3.getString("idempresasede");
                EmpresaSede empresaSedeModi = new EmpresaSede(rs3.getString("idempresasede"));
                empresaSedeModi.setEmpSedDir("'" + JTDireccion.getText() + "'");
                empresaSedeModi.setEmpSedNom("'" + empresaSedeModi.getEmpSedNom() + "'");
                empresaSedeModi.modificar();
            } else {
                EmpresaSede ahora = new EmpresaSede();
                ahora.setEmpSedDin("'0'");
                ahora.setEmpSedDir("'" + JTDireccion.getText() + "'");
                ahora.setEmpSedEstReg(1);
                ahora.setEmpSedNom("'" + JTNombre.getText() + "'");
                ahora.setIdEmpresa(idempresa);
                GenerarCodigo cd = new GenerarCodigo();
                String sede = cd.generarID("empresaSEde", "idempresaSEde");
                ahora.setIdEmpresaSede(sede);
                ahora.insertar();
                retorno = ahora.getIdEmpresaSede();
            }

        } else {
            Empresa empre = new Empresa();
            empre.setEmpEstReg(1);
            empre.setEmpNom("'" + JTNombre.getText() + "'");
            empre.setEmpPer("1");

            if (jComboComprobate.getSelectedItem().toString().equals("factura")) {
                empre.setEmpPer("0");

            }
            if (jComboComprobate.getSelectedItem().toString().equals("boleta")) {
                empre.setEmpPer("1");

            }

            empre.setEmpRUC(JTDocumentoDestino.getText());
            empre.setEmpTel(" '[]' ");
            String doctipe = FACTURA.getCabecera().sacarDato("documento", "docDes", "'" + jComboDocTipo.getSelectedItem().toString() + "'");
            empre.setIdDocumento(doctipe);
            //empre.setEmpTipDoc(jComboDocTipo.getSelectedItem().toString());
            empre.setIdEmpGrup("2");
            GenerarCodigo cd = new GenerarCodigo();

            String sede = cd.generarID("empresa", "idempresa");
            empre.setIdEmpresa(sede);
            empre.insertarId();
            EmpresaSede ahora = new EmpresaSede();
            ahora.setEmpSedDin("0");
            ahora.setEmpSedDir("'" + JTDireccion.getText() + "'");
            ahora.setEmpSedEstReg(1);
            ahora.setEmpSedNom("'" + JTNombre.getText() + "'");
            ahora.setIdEmpresa(sede);
            GenerarCodigo cd2 = new GenerarCodigo();
            String sede1 = cd2.generarID("empresaSEde", "idempresaSEde");
            ahora.setIdEmpresaSede(sede1);
            ahora.insertar();
            retorno = ahora.getIdEmpresaSede();
        }
        //actualizando datos  
        String consulta4 = "select * from empresa where empruc =" + JTDocumentoDestino.getText();

        return retorno;
    }

    public void paraPruebas() {
        /*
         JTNombre.setText("Elvis Tellez Mendoza ");
         JTDocumentoDestino.setText("72943030");
         jTextSerie.setText("00001");
         jTextNumero.setText("00001");
         MovimientoDet de = new MovimientoDet("NULL",JTCodigoCab.getText(), "1", "1", "100", "2000", "900", "0.18", "1");
         FACTURA.getDetalles().add(de);
         */
    }

    private boolean camposBienLlenados() {
        boolean val = true;
        String err = "";
        boolean a = jTextNumero.getText().length() == 8;
        a = true;
        if (a) {
            val = val && true;
        } else {
            val = val && false;
            err += "LONGITUD NO VALIDA PARA EL NUMERO \n ";
        }
        if (jComboDocTipo.getSelectedItem().equals("DNI") && false) {
            val = val && false;
            err += "EL DNI NO CUMPLE CON LOS DIGITOS NESESARIOS \n ";
        }
        if (fecha1.getDate() == null) {
            val = val && false;
            err += "FECHA NO VALIDA \n";
        }
        if (FACTURA.getDetalles().isEmpty()) {
            val = val && false;
            err += "INGRESE ALGUN DETELLE \n";

        }
        if (JTNombre.getText().length() <= 2) {
            val = val && false;
            err += "NOMBRE NO VALIDO \n";
        }

        if (jComboDocTipo.getSelectedItem().equals("RUC") && JTDocumentoDestino.getText().length() != 11) {
            val = val && false;
            err += "EL RUC NO CUMPLE CON LOS CARACTERES DEFINIDOS \n";
        }
        if (jComboDocTipo.getSelectedItem().equals("RUC") && JTDireccion.getText().length() <= 1) {
            val = val && false;
            err += "LA DIRECCION NO ES VALIDA \n";
        }

        if (!val) {
            //   System.out.println(err);
            JOptionPane.showMessageDialog(null, err);

        }
        return val;
    }

    public static boolean entero(String str) {
        boolean a = true;
        for (int i = 0; i < str.length(); i++) {
            if (!(str.charAt(i) >= 0 && str.charAt(i) <= 9)) {
                a = false;
            }
        }
        return a;
    }

    private void llenarDatos() {

        try {
            JTCodigoCab.setText(FACTURA.getCabecera().getIdMovimientoCabecera());
            jTextNumero.setText(FACTURA.getCabecera().getMovCabNum());
            jTextSerie.setText(FACTURA.getCabecera().getMovCabSer());
            String destino = MovimientoCab.sacarDato("empresasede", "idempresasede", FACTURA.getCabecera().getIdEmpresaSedeDes(), "idempresa");

            String nombre = FACTURA.getCabecera().sacarDato("empresa", "idempresa", destino, "empNom");
            JTNombre.setText(nombre);
            destino = MovimientoCab.sacarDato("empresa", "idempresa", destino, "empruc");
            JTDocumentoDestino.setText(destino);
            String comprobante = FACTURA.getCabecera().sacarDato("movimientodocumento", "idmovimientodocumento", FACTURA.getCabecera().getIdMovimientoDocumento(), "movdocdes");
            String doc = FACTURA.getCabecera().sacarDato("empresasede", "idempresasede", FACTURA.getCabecera().getIdEmpresaSede(), "idempresa");
            doc = FACTURA.getCabecera().sacarDato("empresa", "idempresa", doc, "iddocumento");
            doc = FACTURA.getCabecera().sacarDato("documento", "iddocumento", doc, "docdes");

    
            System.err.println("el doc es "+doc +" "+comprobante);
            jComboComprobate.setSelectedItem(comprobante);
            jComboDocTipo.setSelectedItem(doc);
            String desde = FACTURA.getCabecera().getMovCabFec();
            // System.out.println(desde);
            refrescar();
            llenarEmpresaSede(new EmpresaSede(FACTURA.getCabecera().getIdEmpresaSede()).getEmpSedNom());
            EmpresaSede de = new EmpresaSede(FACTURA.getCabecera().getIdEmpresaSedeDes());
            String direccion = de.getEmpSedDir();
            JTDireccion.setText(direccion);
            DecimalFormat df = new DecimalFormat("#.00");
            double sumaTotal = 0;
            double sumaSinIGV = 0;
            double sumaIGV = 0;

            for (MovimientoDet det : FACTURA.getDetalles()) {
                double IgvDet = Double.parseDouble(det.getMovDetIgv());
                sumaSinIGV += Double.parseDouble(det.getMovDetCosTot());
                sumaIGV += Double.parseDouble(det.getMovDetIgv()) ;

            }
            sumaTotal = sumaIGV + sumaSinIGV;
            String sumaSinIGVString = df.format(sumaSinIGV);
            String sumaIGVString = df.format(sumaIGV);
            jTextFieldIGV.setText(sumaIGVString);
            JTextFieldsumatoPre.setText(sumaSinIGVString);
            sumaTotal = Math.round(sumaTotal*100)/100.0;
                     
            jTextFieldTotal.setText("" + sumaTotal);
            try {
             java.util.Date date1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(desde);  
    
    
            fecha1.setDate(date1);
                
            } catch (Exception e) {
             java.util.Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(desde);  
    
    
            fecha1.setDate(date1);
            
            
            }
            

            /* dia = fecha.substring(0, fecha.indexOf("/"));
                String mes = fecha.substring(fecha.indexOf("/") + 1, fecha.lastIndexOf("/"));
                String año = fecha.substring(fecha.lastIndexOf("/") + 1);
             */
        } catch (SQLException ex) {
            Logger.getLogger(ComprasBorre.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ComprasBorre.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    // dagros 

    void imprimirfactura() {
        PrinterMatrix printer = new PrinterMatrix();

        Extenso e = new Extenso();

        e.setNumber(FACTURA.getDetalles().size() + 10);
        // nombre 
        // ruc origen 
        //nombre destino
        // fecha
        // serie  y numero 
        String nombre = jComboEmpSedOrig.getSelectedItem().toString();
        EmpresaSede empsed = new EmpresaSede(FACTURA.getCabecera().getIdEmpresaSedeDes());
        Empresa emp = new Empresa(empsed.getIdEmpresa());
        String titulo = "";
        if (!jComboComprobate.getSelectedItem().toString().equalsIgnoreCase("FACTURA")) {
            titulo = "\t" + jComboComprobate.getSelectedItem().toString() + " ELECTRONICA" + "N°:B" + jTextSerie.getText() + "-" + jTextNumero.getText();;
        } else {

            titulo = "\t" + jComboComprobate.getSelectedItem().toString() + " ELECTRONICA" + "N°:F" + jTextSerie.getText() + "-" + jTextNumero.getText();;
        }
        String ruc = "RUC: " + emp.getEmpRUC();
        String destino = "CLIENTE: " + JTNombre.getText();
        String fecha = "FECHA: " + ((JTextField) (fecha1.getDateEditor().getUiComponent())).getText();
        //String  serie_numero ="NRO de DOC: "+jTextSerie.getText()+"-"+jTextNumero.getText();
        String docu = "DNI /RUC:" + JTDocumentoDestino.getText();
        String direccion = "" + JTDireccion.getText();
        String direccionNuestra = "" + empsed.getEmpSedDir();
        int constante = 42;
        int lineasAimprimir = 27 + FACTURA.getDetalles().size();
        String cabecera = "|         PRODUCTO      |CANTIDAD|IMPORTE|";
        String linea = "";
        for (int i = 0; i < 41; i++) {
            linea += "-";

        }
        String[] arr = {titulo, nombre, ruc, direccionNuestra, destino, docu, direccion, fecha, " ", cabecera, linea};
        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(lineasAimprimir, constante);
        int i = 2;
        for (; i < arr.length + 2; i++) {
            String arr1 = arr[i - 2];
            printer.printTextWrap(i, i, 1, constante, arr1);

        }
        i++;
        int a = 0;
        for (a = i; a < FACTURA.getDetalles().size() + i; a++) {
            String descripcion = JTabla.getValueAt(a - i, 2).toString();

            String cantidad = JTabla.getValueAt(a - i, 1).toString();
            String importe = JTabla.getValueAt(a - i, 4).toString();
            String splitS = cabecera.substring(1, cabecera.length() - 1);
            String split[] = {"       PRODUCTO      ", "CANTIDAD", "IMPORTE"};
            while (descripcion.length() < split[0].length()) {
                if (descripcion.length() > split[0].length()) {
                    break;
                }
                descripcion = " " + descripcion;

                if (descripcion.length() > split[0].length()) {
                    break;
                }
                descripcion = descripcion + " ";

            }
            while (cantidad.length() < split[1].length()) {
                if (cantidad.length() > split[1].length()) {
                    break;
                }
                cantidad = " " + cantidad;

                if (cantidad.length() > split[1].length()) {
                    break;
                }
                cantidad = cantidad + " ";

            }
            while (importe.length() < split[2].length()) {
                if (importe.length() > split[2].length()) {
                    break;
                }
                importe = " " + importe;

                if (importe.length() > split[2].length()) {
                    break;
                }
                importe = importe + " ";

            }
            String datos = "|" + descripcion + "|" + cantidad + "|" + importe + "|";
            //  System.err.println(datos );
            printer.printTextWrap(a, a, 1, constante, datos);

        }
        printer.printTextWrap(a, a, 1, constante, linea);
        a++;
        printer.printTextWrap(a, a, 1, constante, "");
        a++;
        if (jComboComprobate.getSelectedItem().toString().equalsIgnoreCase("FACTURA")) {
            printer.printTextWrap(a, a, 1, constante, "MONTO:" + JTextFieldsumatoPre.getText() + "\tTIGV:" + jTextFieldIGV.getText() + "   TOTAL:" + jTextFieldTotal.getText());
            a++;
        } else {
            printer.printTextWrap(a, a, 1, constante, "\t\t\t TOTAL:" + jTextFieldTotal.getText());
            a++;

        }
        printer.printTextWrap(a, a, 1, constante, "");
        a++;
        printer.printTextWrap(a, a, 1, constante, "Representacion impresa de la factura  ");
        a++;
        printer.printTextWrap(a, a, 1, constante, "electrónica generada desde el sistema");
        a++;
        printer.printTextWrap(a, a, 1, constante, "facturador SUNAT. Puede verificarla");
        a++;
        printer.printTextWrap(a, a, 1, constante, "utilizando su clave SOL ");
        a++;
        //Imprimir * de la 2da linea a 25 en la columna 1;
        // printer.printCharAtLin(2, 25, 1, "*");
        //Imprimir * 1ra linea de la columa de 1 a 80
        // printer.printCharAtCol(1, 1, constante, "=");
        //Imprimir Encabezado nombre del La EMpresa
        // printer.printTextWrap(1, 2, 1, constante, "FACTURA DE VENTA");
        //printer.printTextWrap(linI, linE, colI, colE, null);t
        //printer.printTextWrap(2, 3, 1, 22, "Num. Boleta : " );
        //    printer.printTextWrap(3, 3, 1, 22, "Num. Boleta : " );
        // printer.printTextWrap(4, 4, 1, 22, "Num. Boleta : " );
        //  printer.printTextWrap(4, 4, 1, 22, "Num. Boleta : " );
        // printer.printTextWrap(4, 4, 1, 22, "Num. Boleta : " );

        // la primer linea es la fila donde se imprime 
        /*
        for (int i = 0; i &lt; filas; i++) { printer.printTextWrap(9 + i, 10, 1, 80, tblVentas.getValueAt(i,0).toString()+"|"+tblVentas.getValueAt(i,1).toString()+"| "+tblVentas.getValueAt(i,2).toString()+"| "+tblVentas.getValueAt(i,3).toString()+"|"+ tblVentas.getValueAt(i,4).toString()); } if(filas &gt; 15){
        printer.printCharAtCol(filas + 1, 1, 80, "=");
        printer.printTextWrap(filas + 1, filas + 2, 1, 80, "TOTAL A PAGAR " + txtVentaTotal.getText());
        printer.printCharAtCol(filas + 2, 1, 80, "=");
        printer.printTextWrap(filas + 2, filas + 3, 1, 80, "Esta boleta no tiene valor fiscal, solo para uso interno.: + Descripciones........");
        }else{
        printer.printCharAtCol(25, 1, 80, "=");
        printer.printTextWrap(26, 26, 1, 80, "TOTAL A PAGAR " + txtVentaTotal.getText());
        printer.printCharAtCol(27, 1, 80, "=");
        printer.printTextWrap(27, 28, 1, 80, "Esta boleta no tiene valor fiscal, solo para uso interno.: + Descripciones........");
 
        }*/
        printer.toFile("impresion.txt");

        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream("impresion.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();

        if (defaultPrintService != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);
                byte[] bytes = {27, 105, 3};
                DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
                DocPrintJob job = PrintServiceLookup.lookupDefaultPrintService().createPrintJob();
                Doc doc = new SimpleDoc(bytes, flavor, null);
                //  Thread.sleep(3000);
                job.print(doc, null);
                System.err.println("paso esta linea");

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            //  System.err.println("No existen impresoras instaladas");
        }

        //inputStream.close();
    }

    void imprimirfactura2() {
        if (jComboComprobate.getSelectedIndex() == 1) {
            imprimirBoletaMatricial();
        } else {
            imprimirFacturaMatricial();
        }
    }

    void imprimirBoletaMatricial() {
        PrinterMatrix printer = new PrinterMatrix();

        // nombre 
        // ruc origen 
        //nombre destino
        // fecha
        // serie  y numero 
        String nombre = jComboEmpSedOrig.getSelectedItem().toString();
        EmpresaSede empsed = new EmpresaSede(FACTURA.getCabecera().getIdEmpresaSedeDes());
        Empresa emp = new Empresa(empsed.getIdEmpresa());
        String titulo = "";
        if (!jComboComprobate.getSelectedItem().toString().equalsIgnoreCase("FACTURA")) {
            titulo = "\t" + jComboComprobate.getSelectedItem().toString() + " ELECTRONICA" + "N°:B" + jTextSerie.getText() + "-" + jTextNumero.getText();;
        } else {

            titulo = "\t" + jComboComprobate.getSelectedItem().toString() + " ELECTRONICA" + "N°:F" + jTextSerie.getText() + "-" + jTextNumero.getText();;
        }
        String ruc = "RUC: " + emp.getEmpRUC();
        String destino = "               " + JTNombre.getText();
        String fecha = "                                                  " + ((JTextField) (fecha1.getDateEditor().getUiComponent())).getText();
        //String  serie_numero ="NRO de DOC: "+jTextSerie.getText()+"-"+jTextNumero.getText();
        String docu = "DNI /RUC:" + JTDocumentoDestino.getText();
        String direccion = "" + JTDireccion.getText();
        fecha = fecha.replaceAll("-", "     ");
        String direccionNuestra = "" + empsed.getEmpSedDir();
        int constante = 81;
        int lineasAimprimir = 24;
        String cabecera = "|         PRODUCTO      |CANTIDAD|IMPORTE|";
        String linea = "";
        for (int i = 0; i < 80; i++) {
            linea += "-";

        }
        String[] arr = {" ", " ", " ", fecha, "", destino, " "};
        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(lineasAimprimir, constante);
        int i = 0;
        for (; i < arr.length; i++) {
            String arr1 = arr[i];
            printer.printTextWrap(i, i, 1, constante, arr1);

        }
        i++;
        int a = 0;
        for (a = i; a < FACTURA.getDetalles().size() + i; a++) {
            String descripcion = JTabla.getValueAt(a - i, 2).toString();

            String cantidad = JTabla.getValueAt(a - i, 1).toString();
            String costoUnitario = JTabla.getValueAt(a - i, 3).toString();
            String importe = JTabla.getValueAt(a - i, 4).toString();
            String espacioCantidad = getEspacios(cantidad, 8);
            String espacioDescripcion = getEspacios(descripcion, 37);
            String espacioCostoUnitario = getEspacios(costoUnitario, 11);
            String datos = "   " + cantidad + espacioCantidad + descripcion + espacioDescripcion
                    + costoUnitario + espacioCostoUnitario + importe;
            //  System.err.println(datos );
            printer.printTextWrap(a, a, 1, constante, datos);

        }
        for (int j = 0; j < 11 - FACTURA.getDetalles().size(); j++) {
            printer.printTextWrap(a, a, 1, constante, "");
            a++;

        }

        if (jComboComprobate.getSelectedItem().toString().equalsIgnoreCase("FACTURA")) {
            printer.printTextWrap(a, a, 1, constante, "MONTO:" + JTextFieldsumatoPre.getText() + "\tTIGV:" + jTextFieldIGV.getText() + "   TOTAL:" + jTextFieldTotal.getText());
            a++;
        } else {
            printer.printTextWrap(a, a, 1, constante, "\t\t\t\t\t\t\t\t" + jTextFieldTotal.getText());
            a++;

        }

        printer.toFile("impresion.txt");

        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream("impresion.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintUtility.findPrintService("E3");

        if (defaultPrintService != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);
                System.err.println("paso esta linea");

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            //  System.err.println("No existen impresoras instaladas");
        }

        //inputStream.close();
    }

    void imprimirFacturaMatricial() {
        PrinterMatrix printer = new PrinterMatrix();

        // nombre 
        // ruc origen 
        //nombre destino
        // fecha
        // serie  y numero 
        String nombre = jComboEmpSedOrig.getSelectedItem().toString();
        EmpresaSede empsed = new EmpresaSede(FACTURA.getCabecera().getIdEmpresaSedeDes());
        Empresa emp = new Empresa(empsed.getIdEmpresa());
        String titulo = "";
        String blancosIzq = "               ";
        String ruc = "RUC: " + emp.getEmpRUC();
        String destino = blancosIzq + JTNombre.getText();
        String fecha = blancosIzq + ((JTextField) (fecha1.getDateEditor().getUiComponent())).getText();
        //String  serie_numero ="NRO de DOC: "+jTextSerie.getText()+"-"+jTextNumero.getText();
        String docu = blancosIzq + JTDocumentoDestino.getText();
        String direccion = blancosIzq + JTDireccion.getText();
        fecha = fecha.replaceAll("-", "     ");
        String direccionNuestra = "" + empsed.getEmpSedDir();
        int constante = 137;
        int lineasAimprimir = 40;
        String cabecera = "|         PRODUCTO      |CANTIDAD|IMPORTE|";
        String linea = "";
        for (int i = 0; i < 160; i++) {
            linea += "-";

        }
        String[] arr = {"", "", "", "", "", "", destino, direccion, docu, fecha, " ", " "};
        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(lineasAimprimir, constante);
        int i = 0;
        for (; i < arr.length; i++) {
            String arr1 = arr[i];
            printer.printTextWrap(i, i, 1, constante, arr1);

        }
        i++;
        int a = 0;
        for (a = i; a < FACTURA.getDetalles().size() + i; a++) {
            String descripcion = JTabla.getValueAt(a - i, 2).toString();

            String cantidad = JTabla.getValueAt(a - i, 1).toString();
            String costoUnitario = JTabla.getValueAt(a - i, 3).toString();
            String importe = JTabla.getValueAt(a - i, 4).toString();
            String espacioCantidad = getEspacios(cantidad, 8);
            String espacioDescripcion = getEspacios(descripcion, 110);
            String espacioCostoUnitario = getEspacios(costoUnitario, 11);
            String datos = "   " + cantidad + espacioCantidad + descripcion + espacioDescripcion
                    + costoUnitario + espacioCostoUnitario + importe;
            //  System.err.println(datos );
            printer.printTextWrap(a, a, 1, constante, datos);

        }
        for (int j = 0; j < 20 - FACTURA.getDetalles().size(); j++) {
            printer.printTextWrap(a, a, 1, constante, "");
            a++;

        }

        printer.printTextWrap(a, a, 1, constante, "\t\t\t\t\t\t\t\t\t\t\t\t\t" + JTextFieldsumatoPre.getText() + "\t\t" + jTextFieldIGV.getText() + "\t\t" + jTextFieldTotal.getText());
        a++;

        printer.toFile("impresion.txt");

        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream("impresion.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();

        if (defaultPrintService != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);
                System.err.println("paso esta linea");

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            //  System.err.println("No existen impresoras instaladas");
        }

        //inputStream.close();
    }

    public boolean guardar() {
      
        String idMovimientoCabecera = FACTURA.getCabecera().getIdMovimientoCabecera();
        if (camposBienLlenados()) {

            try {
                double dinero = 0;
                //ESTE VALE

                FACTURA.ajustar();

                for (MovimientoDet det : FACTURA.getDetalles()) {
                    dinero -= Double.parseDouble(det.getMovDetCosTot());

                }

                EmpresaSede emp = new EmpresaSede(FACTURA.getCabecera().getIdEmpresaSedeDes());

                dinero += Double.parseDouble(emp.getEmpSedDin());
                emp.setEmpSedDin("'" + dinero + "'");
                emp.setEmpSedDir("'" + emp.getEmpSedDir() + "'");
                emp.setEmpSedNom("'" + emp.getEmpSedNom() + "'");
                emp.modificar();

                String identificador = RevisarDocumento();
                // System.out.println(identificador);
                MovimientoCab movi = new MovimientoCab();
                movi.setIdEmpresaSede(cabe.sacarDato("empresaSEde ", "empsednom", "'" + jComboEmpSedOrig.getSelectedItem().toString() + "'"));
                //String revisar=RevisarDocumento();
                //movi.setIdEmpresaSede(revisar);
                String idmovdoc = cabe.sacarDato("movimientodocumento", "movdocDes", "'" + jComboComprobate.getSelectedItem().toString() + "'");
                movi.setIdMovimientoDocumento(idmovdoc);
                String a = Principal.jLUsuarioGeneral != null ? Principal.jLUsuarioGeneral.getText() : "1";
                movi.setIdUsuario("'" + a + "'");
                movi.setIdMovimientoTipo("1");

//                movi.setMovCabEnv("0");
                movi.setMovCabEstReg("1");

                /* dia = fecha.substring(0, fecha.indexOf("/"));
                String mes = fecha.substring(fecha.indexOf("/") + 1, fecha.lastIndexOf("/"));
                String año = fecha.substring(fecha.lastIndexOf("/") + 1);
                 */
                //  String fechaF = año + "-" + mes + "-" + dia;
                //    System.err.println(fechaF);
                java.util.Date d = fecha1.getDate();
                SimpleDateFormat da = new SimpleDateFormat("yyyy/MM/dd");
                           java.util.Date datey = new java.util.Date ();
//Caso 1: obtener la hora y salida por pantalla con formato:

       DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
       String dateSalida = hourdateFormat.format(datey);
     
                movi.setMovCabFec("'" + da.format(d) +" "+dateSalida+ "'");



                movi.setMovCabMovSto("1");
                movi.setMovCabNum("'" + jTextNumero.getText() + "'");
                movi.setMovCabSer("'" + jTextSerie.getText() + "'");
                movi.setIdEmpresaSedeDes(identificador);
                //FACTURA.getCabecera().setIdEmpresaSede(identificador);
                FACTURA.setCabecera(movi);

                if (!isModificando) {
                    movi.setMovCabNum("'" + jTextNumero.getText() + "'");
                    movi.setMovCabSer("'" + jTextSerie.getText() + "'");
                    FACTURA.getCabecera().generarCodigo(JTCodigoCab);
                    movi.setIdMovimientoCabecera( JTCodigoCab.getText() );
                    movi.setMovCabProBoo(0);
                    FACTURA.setCabecera(movi);

                    FACTURA.getCabecera().insertar();
                    for (MovimientoDet det : FACTURA.getDetalles()) {
                        
                        det.setIdMovimientoCabecera(JTCodigoCab.getText());
                        det.insertar();
                    }

                } else {
                    FACTURA.getCabecera().setMovCabSer(FACTURA.getCabecera().getMovCabSer().substring(1, FACTURA.getCabecera().getMovCabSer().length() - 1));
                    FACTURA.getCabecera().setMovCabNum(FACTURA.getCabecera().getMovCabNum().substring(1, FACTURA.getCabecera().getMovCabNum().length() - 1));
                    factura fac = new factura();
                    
                    fac.setCabecera(new MovimientoCab());
                    fac.getCabecera().setIdMovimientoCabecera(JTCodigoCab.getText());
                    FACTURA.getCabecera().setIdMovimientoCabecera(JTCodigoCab.getText());
                    fac.obtenerDetalles();
                    FACTURA.getCabecera().modificar();
                    GenerarCodigo gc = new GenerarCodigo();
                    String cod = gc.generarID("MovimientoDet", "IdMovimientoDetalle");
                    int numCod = Integer.parseInt(cod);
                    for (MovimientoDet det : FACTURA.getDetalles()) {
                        det.setIdMovimientoDetalle("" + numCod);
                        det.insertar();
                        numCod++;
                    }
                    for (MovimientoDet det : fac.getDetalles()) {
                        det.eliminar();
                    }
                }
                JOptionPane.showMessageDialog(null, "GRABADO HECHO EXITOSAMENTE");

            } catch (SQLException ex) {
                Logger.getLogger(ComprasBorre.class.getName()).log(Level.SEVERE, null, ex);
            }

            guardarComentarios();
            this.dispose();
    MODIFICARCOMPROBANTE a;

        a = new MODIFICARCOMPROBANTE(MODIFICARCOMPROBANTE.COMPRA);
        a.setVisible(true);
            
            return true;
        }
        return false;
    }

    protected void grabarDetalle() {

        DecimalFormat df = new DecimalFormat("#.000");

        //Insertar segun bandera
        Pattern expresionRegular = Pattern.compile("[0-9]*");
        Matcher ma = expresionRegular.matcher(JTCantidad.getText());

        if (false) {
            JOptionPane.showMessageDialog(null, "Por favor ingrese todos los campos");
        } else {
            MovimientoDet actual;
            if (bandera) {
                FACTURA.getDetalles().add(new MovimientoDet());
                actual = FACTURA.getDetalles().get(FACTURA.getDetalles().size() - 1);

            } else {
                //modificar

                actual = FACTURA.getDetalles().get(indiceFila);

            }
            String nombre = JComboArt.getSelectedItem().toString();

            Articulo art = Articulo.getArticuloByName(nombre);
            String dato =""+ art.getIdArticulo();
            actual.setIdArticulo(dato);
            actual.setIdMovimientoCabecera(JTCodigoCab.getText());
            actual.setIdMovimientoDetalle(otroCamp);
            actual.setMovDetCan(JTCantidad.getText());
            double  valorReal =0 ;
               valorReal=Double.parseDouble( JTCOMPRAS.getText());
               valorReal=valorReal / (1+art.getArtIgv());
           //valorReal = valorReal/(1+art.getArtIgv());
            actual.setMovDetCosTot(""+(double)Math.round(valorReal * 1000d) / 1000d);
            double valorIgv = valorReal *(art.getArtIgv());
            actual.setMovDetIgv(""+(double)Math.round(valorIgv * 1000d) / 1000d);
            
            actual.setMovDetPreTot("0");
            actual.setMovIngreso("" + 1);
            actual.setMovDetEstReg("" + 1);
            if (bandera) {
                for (int w = 0; w < FACTURA.getDetalles().size() - 1; w++) {
                    MovimientoDet det = FACTURA.getDetalles().get(w);
                    boolean articuloyadigitado = det.getIdArticulo().equals(actual.getIdArticulo());
                    if (articuloyadigitado) {
                        double cantidad = Double.parseDouble(actual.getMovDetCan());
                        cantidad += Double.parseDouble(det.getMovDetCan());
                        double precioNuevo = Double.parseDouble(actual.getMovDetCosTot());
                        precioNuevo += Double.parseDouble(det.getMovDetCosTot());
                        det.setMovDetCan("" + cantidad);
                        det.setMovDetCosTot("" + precioNuevo);
                      valorIgv = precioNuevo *(art.getArtIgv());
              det.setMovDetIgv(""+(double)Math.round(valorIgv * 1000d) / 1000d);
                        
                        FACTURA.getDetalles().remove(actual);

                    }
                }
            }
            double sumaTotal = 0;
            double sumaSinIGV = 0;
            double sumaIGV = 0;

            for (MovimientoDet det : FACTURA.getDetalles()) {
                try {
        sumaIGV += Double.parseDouble(det.getMovDetIgv());
            
                    sumaSinIGV += Double.parseDouble(det.getMovDetCosTot());
                    
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "Ingrese un  valor  de compra adecuado");
                    return;
                }
            }
             sumaTotal =sumaSinIGV+sumaIGV ;
                   sumaTotal = Math.round(sumaTotal*1000)/1000.0;
            String sumaSinIGVString = df.format(sumaSinIGV);
            String sumaIGVString = df.format(sumaIGV);
            jTextFieldIGV.setText(sumaIGVString);
            JTextFieldsumatoPre.setText(sumaSinIGVString);
            jTextFieldTotal.setText("" + sumaTotal);
            estado(1);

        }
    }

    private String getEspacios(String cantidad, int i) {
        String res = "";
        for (int j = 0; j < i - cantidad.length(); j++) {
            res = res + " ";
        }
        return res;
    }

    private void imprimirTiket() {
        PrinterMatrix printer = new PrinterMatrix();

        Extenso e = new Extenso();

        e.setNumber(FACTURA.getDetalles().size() + 10);
        // nombre 
        // ruc origen 
        //nombre destino
        // fecha
        // serie  y numero 
        String nombre = jComboEmpSedOrig.getSelectedItem().toString();
        EmpresaSede empsed = new EmpresaSede(FACTURA.getCabecera().getIdEmpresaSedeDes());
        Empresa emp = new Empresa(empsed.getIdEmpresa());
        String titulo = "";
        if (!jComboComprobate.getSelectedItem().toString().equalsIgnoreCase("FACTURA")) {
            titulo = "TICKET " + "N°:B" + jTextSerie.getText() + "-" + jTextNumero.getText();;
        } else {

            titulo = "TICKET " + "N°:F" + jTextSerie.getText() + "-" + jTextNumero.getText();;
        }
        String ruc = "RUC: " + emp.getEmpRUC();
        String destino = "CLIENTE: " + JTNombre.getText();
        long timeInMillis = System.currentTimeMillis();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(timeInMillis);
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd/MM/yyyy hh:mm:ss a");
        String dateforrow = dateFormat.format(cal1.getTime());
        String fecha = "FECHA: " + dateforrow;
        //String  serie_numero ="NRO de DOC: "+jTextSerie.getText()+"-"+jTextNumero.getText();
        String docu = "DNI /RUC:" + JTDocumentoDestino.getText();
        String direccion = "" + JTDireccion.getText();
        String direccionNuestra = "" + empsed.getEmpSedDir();
        int constante = 40;
        int lineasAimprimir = 18 + FACTURA.getDetalles().size();
        String cabecera = "|       PRODUCTO      |CANTIDAD|IMPORTE|";
        String linea = "";
        for (int i = 0; i < 39; i++) {
            linea += "-";

        }
        String[] arr = {titulo, fecha, " ", cabecera, linea};
        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(lineasAimprimir, constante);
        int i = 0;
        for (; i < arr.length; i++) {
            String arr1 = arr[i];
            printer.printTextWrap(i, i, 1, constante, arr1);

        }
        i++;
        int a = 0;
        for (a = i; a < FACTURA.getDetalles().size() + i; a++) {
            String descripcion = JTabla.getValueAt(a - i, 2).toString();

            String cantidad = JTabla.getValueAt(a - i, 1).toString();
            String importe = JTabla.getValueAt(a - i, 4).toString();
            String splitS = cabecera.substring(1, cabecera.length() - 1);
            String split[] = {"     PRODUCTO      ", "CANTIDAD", "IMPORTE"};
            while (descripcion.length() < split[0].length()) {
                if (descripcion.length() > split[0].length()) {
                    break;
                }
                descripcion = " " + descripcion;

                if (descripcion.length() > split[0].length()) {
                    break;
                }
                descripcion = descripcion + " ";

            }
            while (cantidad.length() < split[1].length()) {
                if (cantidad.length() > split[1].length()) {
                    break;
                }
                cantidad = " " + cantidad;

                if (cantidad.length() > split[1].length()) {
                    break;
                }
                cantidad = cantidad + " ";

            }
            while (importe.length() < split[2].length()) {
                if (importe.length() > split[2].length()) {
                    break;
                }
                importe = " " + importe;

                if (importe.length() > split[2].length()) {
                    break;
                }
                importe = importe + " ";

            }
            String datos = "|" + descripcion + "|" + cantidad + "|" + importe + "|";
            //  System.err.println(datos );
            printer.printTextWrap(a, a, 1, constante, datos);

        }
        printer.printTextWrap(a, a, 1, constante, linea);
        a++;
        printer.printTextWrap(a, a, 1, constante, "");
        a++;
        if (jComboComprobate.getSelectedItem().toString().equalsIgnoreCase("FACTURA")) {
            printer.printTextWrap(a, a, 1, constante, "MONTO:" + JTextFieldsumatoPre.getText() + "\tTIGV:" + jTextFieldIGV.getText() + "   TOTAL:" + jTextFieldTotal.getText());
            a++;
        } else {
            printer.printTextWrap(a, a, 1, constante, "\t\t\t TOTAL:" + jTextFieldTotal.getText());
            a++;

        }
        //Imprimir * de la 2da linea a 25 en la columna 1;
        // printer.printCharAtLin(2, 25, 1, "*");
        //Imprimir * 1ra linea de la columa de 1 a 80
        // printer.printCharAtCol(1, 1, constante, "=");
        //Imprimir Encabezado nombre del La EMpresa
        // printer.printTextWrap(1, 2, 1, constante, "FACTURA DE VENTA");
        //printer.printTextWrap(linI, linE, colI, colE, null);t
        //printer.printTextWrap(2, 3, 1, 22, "Num. Boleta : " );
        //    printer.printTextWrap(3, 3, 1, 22, "Num. Boleta : " );
        // printer.printTextWrap(4, 4, 1, 22, "Num. Boleta : " );
        //  printer.printTextWrap(4, 4, 1, 22, "Num. Boleta : " );
        // printer.printTextWrap(4, 4, 1, 22, "Num. Boleta : " );

        // la primer linea es la fila donde se imprime 
        /*
        for (int i = 0; i &lt; filas; i++) { printer.printTextWrap(9 + i, 10, 1, 80, tblVentas.getValueAt(i,0).toString()+"|"+tblVentas.getValueAt(i,1).toString()+"| "+tblVentas.getValueAt(i,2).toString()+"| "+tblVentas.getValueAt(i,3).toString()+"|"+ tblVentas.getValueAt(i,4).toString()); } if(filas &gt; 15){
        printer.printCharAtCol(filas + 1, 1, 80, "=");
        printer.printTextWrap(filas + 1, filas + 2, 1, 80, "TOTAL A PAGAR " + txtVentaTotal.getText());
        printer.printCharAtCol(filas + 2, 1, 80, "=");
        printer.printTextWrap(filas + 2, filas + 3, 1, 80, "Esta boleta no tiene valor fiscal, solo para uso interno.: + Descripciones........");
        }else{
        printer.printCharAtCol(25, 1, 80, "=");
        printer.printTextWrap(26, 26, 1, 80, "TOTAL A PAGAR " + txtVentaTotal.getText());
        printer.printCharAtCol(27, 1, 80, "=");
        printer.printTextWrap(27, 28, 1, 80, "Esta boleta no tiene valor fiscal, solo para uso interno.: + Descripciones........");
 
        }*/
        printer.toFile("impresion.txt");
        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream("impresion.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();
        PrintService defaultPrintService = PrintUtility.findPrintService("E1");

        if (defaultPrintService != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();

            try {
                printJob.print(document, attributeSet);

                //  Thread.sleep(3000);
                System.err.println("paso esta linea");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            //  System.err.println("No existen impresoras instaladas");
        }
        try {
            inputStream.close();
        } catch (IOException ex) {
            Logger.getLogger(ComprasBorre.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void guardarComentarios() {
        try {
            Observacion de = new Observacion();
            String cabecera = FACTURA.getCabecera().getIdMovimientoCabecera();
            de.setIdMovimientoCabecera(Integer.parseInt(cabecera));
            String usu = FACTURA.getCabecera().getIdUsuario();
            de.setIdUsuario(Integer.parseInt(usu.substring(1, usu.length() - 1)));
            de.setObservacionDescripcion(jTextArea1.getText());
            de.insertar();
        } catch (SQLException ex) {
            Logger.getLogger(ComprasBorre.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void llenarComentario() {
        Observacion obs = new Observacion(0, Integer.parseInt(FACTURA.getCabecera().getIdMovimientoCabecera()));
        jTextArea1.setText(obs.getObservacionDescripcion());
    }

    private void llenarTabla2(String articulo) {
        Inventario.Inventario.llenarTabla(jTable1, articulo);

    }

    private void calcularPrecio() {
         try {
     double s = Double.parseDouble(JTCantidad.getText()) * Double.parseDouble(JTUnitario.getText());
            s = Math.round(s*1000)/1000.0;
            JTCOMPRAS.setText("" + s);     
        } catch (Exception e) {
        }
    }
    
    private void calcularUnitario() {
         try {
     double s = Double.parseDouble(JTCOMPRAS.getText()) / Double.parseDouble(JTCantidad.getText());
            s = Math.round(s*1000)/1000.0;
            JTUnitario.setText("" + s);     
        } catch (Exception e) {
        }
    }
    

    public  void llenarEmpresaSede() {
            EmpresaSede.llenarCajaMisEmpresasAlmacen(jComboEmpSedOrig,"0");
    }

    public  void llenarEmpresaSede(String empSedNom) {
       EmpresaSede.llenarCajaMisEmpresasAlmacen(jComboEmpSedOrig,empSedNom);
         }

}
