/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Kardex;
 
/**
 *
 * @author Gutierrez
 */
public class Atributos_Kardex {
   
    private String fec;
    private String codmov;
    private String movdes;
    
    private String ing;
    private String sali;
    private String sal;
    private String costUni;
    private String ingmv;
    private String salimv;
    private String salmv;

    public Atributos_Kardex(String fec, String codmov, String movdes, String ing, String sali, String sal, String costUni, String ingmv, String salimv, String salmv) {
        this.fec = fec;
        this.codmov = codmov;
        this.movdes = movdes;
        this.ing = ing;
        this.sali = sali;
        this.sal = sal;
        this.costUni = costUni;
        this.ingmv = ingmv;
        this.salimv = salimv;
        this.salmv = salmv;
    }

    public String getFec() {
        return fec;
    }

    public void setFec(String fec) {
        this.fec = fec;
    }

    public String getCodmov() {
        return codmov;
    }

    public void setCodmov(String codmov) {
        this.codmov = codmov;
    }

    public String getMovdes() {
        return movdes;
    }

    public void setMovdes(String movdes) {
        this.movdes = movdes;
    }

    public String getIng() {
        return ing;
    }

    public void setIng(String ing) {
        this.ing = ing;
    }

    public String getSali() {
        return sali;
    }

    public void setSali(String sali) {
        this.sali = sali;
    }

    public String getSal() {
        return sal;
    }

    public void setSal(String sal) {
        this.sal = sal;
    }

    public String getCostUni() {
        return costUni;
    }

    public void setCostUni(String costUni) {
        this.costUni = costUni;
    }

    public String getIngmv() {
        return ingmv;
    }

    public void setIngmv(String ingmv) {
        this.ingmv = ingmv;
    }

    public String getSalimv() {
        return salimv;
    }

    public void setSalimv(String salimv) {
        this.salimv = salimv;
    }

    public String getSalmv() {
        return salmv;
    }

    public void setSalmv(String salmv) {
        this.salmv = salmv;
    }
    
}