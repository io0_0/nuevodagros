/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Kardex;

import Maestros.Atributos;
import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 *
 * @author Gutierrez
 */
public class pasarDatos implements JRDataSource {

    private List <Atributos_Kardex> atributos = new ArrayList<>();
    private int indiceParticipantes=-1;

    pasarDatos() {
       
    }
    
    @Override
    public boolean next() throws JRException {
    return ++indiceParticipantes<atributos.size();
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
        
        Object valor=null;
        
        if("fec".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getFec();
            
        }else if("codmov".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getCodmov();
        
        }else if("movdes".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getMovdes();
       
        }else if("ing".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getIng();
           
        }else if("sali".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getSali();
        
        }else if("sal".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getSal();
       
        }else if("costUni".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getCostUni();
        
        }else if("ingmv".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getIngmv();
                   
        }else if("salimv".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getSalimv();
        
        }else if("salmv".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getSalmv();
        }
        return valor;
    }
    
    public void addDatos(Atributos_Kardex atributo ){
        this.atributos.add(atributo);
    }
    
}
