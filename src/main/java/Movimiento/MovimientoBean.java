package      Movimiento;

import Facturador.*;
import Bean.Articulo;
import Bean.Empresa;
import Bean.UnidadMedida;
import Bean.MovimientoDet;
import Bean.MovimientoCab;
import Bean.EmpresaSede;
import Maestros.ArticuloM;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import      Conexion.Mysql;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MovimientoBean {
	public static final String BOLETA="3";
	public static final String FACTURA="1";

	private MovimientoCab  cabecera= new MovimientoCab();
	static Connection cn=Mysql.getConection();
	static int datos =11;
	private EmpresaSede empresaSedeOri;
	private Empresa empresaOri;
	private EmpresaSede empresaSedeDes;
	private Empresa empresaDes;
	private ArrayList <MovimientoDet> detalles ;
	public MovimientoBean(){
		detalles= new ArrayList<MovimientoDet>();
		
	}
	public MovimientoCab getCabecera() {
		return cabecera;
	}
	public void setCabecera(MovimientoCab cabecera1)  {
		this.cabecera = cabecera1;
		 //empresaSedeOri = new EmpresaSede("'"+de+"'");
		//empresaOri = new Empresa(empresaSedeOri.getIdEmpresa()) ;
		// empresaSedeDes = new EmpresaSede(cabecera1.getIdEmpresaSedeDes());
	//	empresaDes = new Empresa(empresaSedeDes.getIdEmpresa()) ;
		//ajustar();
	}
	public ArrayList<MovimientoDet> getDetalles() {
		return detalles;
	}
	public void setDetalles(ArrayList<MovimientoDet> detalles) {
		this.detalles = detalles;
	}
	public void ajustar(){
		ArrayList <MovimientoDet> detallesFinal = new ArrayList<MovimientoDet>();
		boolean  [] visitado = new boolean[detalles.size()];
		for (int i = 0; i < visitado.length; i++) {
			visitado [i]=false;

		}
		for (int i = 0; i < detalles.size(); i++) {
			MovimientoDet acDet= detalles.get(i);
			String  articulo = acDet.getIdArticulo();
			int   cantidadActual = Integer.parseInt(acDet.getMovDetCan());
			if(!visitado[i])detallesFinal.add(acDet);
			for (int j = i+1; j < detalles.size(); j++) {
				if(visitado[j])continue;
				MovimientoDet temp= detalles.get(j);
				if(temp.getIdArticulo().equals(articulo)){
					visitado[j]=true;
					cantidadActual+= Integer.parseInt(temp.getMovDetCan());
				}
			}

			acDet.setMovDetCan(""+cantidadActual);
		}
		detalles=detallesFinal;


	}
	public void obtenerDetalles() throws SQLException{
		Statement sent;
		String sql_Cons = "Select * from MovimientoDet  where idMovimientoCabecera= "+getCabecera().getIdMovimientoCabecera() + " and MovDetEstReg = 1";
		System.out.println(sql_Cons);
		sent = cn.createStatement();
		ResultSet rs = sent.executeQuery(sql_Cons);
		ArrayList<MovimientoDet> det = new ArrayList<MovimientoDet>();
		while (rs.next()) {
			MovimientoDet temo = new MovimientoDet();
			temo.setIdMovimientoDetalle(rs.getString("IdMovimientoDetalle"));
			temo.setIdMovimientoCabecera(rs.getString("IdMovimientoCabecera"));
			temo.setIdArticulo(rs.getString("IdArticulo"));
			temo.setMovIngreso(rs.getString("MovIngreso"));
			temo.setMovDetCan(rs.getString("MovDetCan"));
			temo.setMovDetPreTot(rs.getString("MovDetPreTot"));
                        temo.setMovDetCosTot(rs.getString("MovDetCosTot"));
			temo.setMovDetIgv(rs.getString("MovDetIgv"));
			temo.setMovDetEstReg(rs.getString("MovDetEstReg"));
			System.out.println(temo);

			if(temo.getMovDetEstReg().equals("1")){
				det.add(temo);
			}

		}
		setDetalles(det);
		System.out.println(detalles.size());



	}

	
	//01|2016-11-10|000|6|20100282721|JUAN LENG DELGADO SAC|PEN|0.00|0.00|0.00|927.96|0.00|0.00|167.04|0.00|0.00|1095.00
	
	public static void main(String[] args) throws SQLException, IOException {
//		enviarFacturaSunat();
	
		
	}
}
