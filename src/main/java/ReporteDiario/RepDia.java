/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReporteDiario;

/**
 *
 * @author Gutierrez
 */
class RepDia {
    String artCod;
    String artDes;
    String artCant;
    String artPreTot;

    public RepDia(String artCod, String artDes, String artCant, String artPreTot) {
        this.artCod = artCod;
        this.artDes = artDes;
        this.artCant = artCant;
        this.artPreTot = artPreTot;
    }

    public String getArtCod() {
        return artCod;
    }

    public void setArtCod(String artCod) {
        this.artCod = artCod;
    }

    public String getArtDes() {
        return artDes;
    }

    public void setArtDes(String artDes) {
        this.artDes = artDes;
    }

    public String getArtCant() {
        return artCant;
    }

    public void setArtCant(String artCant) {
        this.artCant = artCant;
    }

    public String getArtPreTot() {
        return artPreTot;
    }

    public void setArtPreTot(String artPreTot) {
        this.artPreTot = artPreTot;
    }

    
}