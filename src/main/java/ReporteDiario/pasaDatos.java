/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReporteDiario;


import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 *
 * @author Gutierrez
 */
public class pasaDatos implements JRDataSource {

    private List <RepDia> atributos = new ArrayList<>();
    private int indiceParticipantes=-1;

    pasaDatos() {
       
    }
    
    @Override
    public boolean next() throws JRException {
    return ++indiceParticipantes<atributos.size();
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
        
        Object valor=null;
        
        if("artCod".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getArtCod();
            
        }else if("artDes".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getArtDes();
        
        }else if("artCant".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getArtCant();
       
        }
        else if("artPreTot".equals(jrf.getName())){
            valor=atributos.get(indiceParticipantes).getArtPreTot();
       
        }
        return valor;
    }
    
    public void addDatos(RepDia atributo ){
        this.atributos.add(atributo);
    }
    
}
