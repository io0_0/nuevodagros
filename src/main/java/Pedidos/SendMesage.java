/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pedidos;

import Bean.*;
import Conexion.Mysql;
import GUI.Principal;
import static GUI.Principal.jLUsuarioGeneral;
import Maestros.Atributos;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Writer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.util.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author USUARIO
 */
public class SendMesage {

    public static int numeroEmpresa = 0;
    public static int numeroEmpresaSede = 0;
    public static int numeroCabecera = 0;
    public static int numeroDetalle = 0;

    static Connection cn = Mysql.getConection();
    static String correoSede = "elgalpon.pedidos@gmail.com";
    static String contraseniaSede = "pedidos123";
    // static String correoCentral = "elvis.tellez.1234@gmail.com";
    static String correoCentral = "elvis.tellez.1234@gmail.com";

 
    private static String getFecha() {
        java.util.Date dia = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(dia);
    }
    String nombre = "";
    static int proximo_id = 0;
 

    private static int getNuevoId(String tabla) {
        try {
            Connection cnMetadata = Mysql.getConection();

            String sqlIdAuto = "SELECT MAX(`Id" + tabla + "`) from ";

            if (tabla.equals("MovimientoCabecera")) {
                tabla = "MovimientoCab";
            }
            if (tabla.equals("MovimientoDetalle")) {
                tabla = "MovimientoDet";
            }
            sqlIdAuto += ("`" + tabla + "`");
            Statement sentId = cnMetadata.createStatement();
            ResultSet rsId = sentId.executeQuery(sqlIdAuto);
            System.out.println(sqlIdAuto);
            int id;
            if (rsId.next()) {
                id = rsId.getInt(1) + 1;
            } else {
                id = 0;
            }
            cnMetadata.close();
            System.out.println(id);
            return id;

        } catch (SQLException ex) {
            Logger.getLogger(SendMesage.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }

    }
    public static void SendMail(String archivo) throws MessagingException {
        String polleria   = "dagros";
        // se obtiene el objeto Session. La configuraciÃ³n es para
        // una cuenta de gmail.
        System.out.println( archivo);
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        //props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.smtp.port", "587");
        props.setProperty("mail.smtp.user", correoSede);
        props.setProperty("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props, null);
        //session.setDebug(true);

        // Se compone la parte del texto
        BodyPart texto = new MimeBodyPart();
        texto.setText("Texto del mensaje");

        // Se compone el adjunto con la imagen
        BodyPart adjunto = new MimeBodyPart();
        adjunto.setDataHandler(new DataHandler(new FileDataSource(archivo)));

        //adjunto.setDataHandler(new DataHandler(new FileDataSource("C:/Users/Edwin Alex/Documents/lista.txt")));
        adjunto.setFileName("requerimientosPolleria-"+polleria+getFecha()+".json");

        // Una MultiParte para agrupar texto e imagen.
        MimeMultipart multiParte = new MimeMultipart();
        multiParte.addBodyPart(texto);
        multiParte.addBodyPart(adjunto);

        // Se compone el correo, dando to, from, subject y el
        // contenido.
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(correoSede));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(correoCentral));
        message.setSubject("Envio Archivos ");
        message.setContent(multiParte);

        // Se envia el correo.
        Transport t = session.getTransport("smtp");
        t.connect(correoSede, contraseniaSede);
        t.sendMessage(message, message.getAllRecipients());
        // t.sendMessage(message, message.getAllRecipients());
        t.close();

    }


    static String sha1(String input) throws NoSuchAlgorithmException {
        MessageDigest mDigest = MessageDigest.getInstance("SHA1");
        byte[] result = mDigest.digest(input.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

    public static String getFechaActual() {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
        return formateador.format(ahora);
    }

    protected String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

    public static void main(String[] args) {
       }
}
