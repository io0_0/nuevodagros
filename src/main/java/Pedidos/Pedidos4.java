/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pedidos;

import Bean.MovimientoCab;
import Bean.MovimientoDet;
import Facturador.factura;
import GUI.Principal;
import static Pedidos.Pedidos2.actual;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author io
 */
public class Pedidos4  extends Pedidos2{
    
    public Pedidos4(factura get) {
           super(get);
    }    
    @Override
 public boolean guardar() {
        jDialog1.setVisible(true);
        jDialog1.setLocationRelativeTo(this);
        if (camposBienLlenados()) {

            try {
                double dinero = 0;
                //ESTE VALE

                FACTURA.ajustar();

                int wwwww = jComboEmpSedOrig.getSelectedIndex() + 1;

                // System.out.println(identificador);
                MovimientoCab movi = new MovimientoCab();
                movi.setIdEmpresaSedeDes(cabe.sacarDato("empresaSEde ", "empsednom", "'" + jComboEmpSedDes.getSelectedItem().toString() + "'"));
                movi.setIdEmpresaSede(cabe.sacarDato("empresaSEde ", "empsednom", "'" + jComboEmpSedOrig.getSelectedItem().toString() + "'"));

//String revisar=RevisarDocumento();
                //movi.setIdEmpresaSedeDes(revisar);
                int auxLLL = Integer.parseInt(cabe.sacarDato("MovimientoDocumento", "MovDocDes", "'" + jComboComprobate.getSelectedItem().toString() + "'"));
                String idmovdoc = auxLLL + "";
                movi.setIdMovimientoDocumento(idmovdoc);
                String a = Principal.jLUsuarioGeneral != null ? Principal.jLUsuarioGeneral.getText() : "1";
                movi.setIdUsuario("'" + a + "'");
                movi.setIdMovimientoTipo("2");

//                movi.setMovCabEnv("0");
                movi.setMovCabEstReg("1");

                java.util.Date d = fecha1.getDate();
                SimpleDateFormat da = new SimpleDateFormat("yyyy/MM/dd");

                movi.setMovCabFec("'" + da.format(d) + "'");

                movi.setMovCabMovSto("0");
                movi.setMovCabNum(jTextNumero.getText());
                movi.setMovCabSer(jTextSerie.getText());
                
//FACTURA.getCabecera().setIdEmpresaSedeDes(identificador);
                System.out.println(FACTURA.getCabecera().getIdMovimientoCabecera()+"  " +movi.getIdEmpresaSede()+"  "+movi.getIdEmpresaSedeDes());
movi.setIdMovimientoCabecera(FACTURA.getCabecera().getIdMovimientoCabecera());
                movi.setMovCabProBoo(0);
               movi.setIdMovimientoCabeceraAsociado(movi.getIdMovimientoCabecera());
                FACTURA.setCabecera(movi);

                FACTURA.getCabecera().modificar();
                for (String eliminaEstosDetalle : eliminaEstosDetalles) {
                    MovimientoDet s = new MovimientoDet();
                    s.setIdMovimientoDetalle(eliminaEstosDetalle);
                    s.eliminar();;
                }
                for (MovimientoDet det : FACTURA.getDetalles()) {
                    det.setIdMovimientoDetalle(MovimientoDet.generarCodigo());
                    det.setIdMovimientoCabecera(FACTURA.getCabecera().getIdMovimientoCabecera());
                    double getPrecioPromedio =MovimientoDet.getPrecioPromedio(det.getIdArticulo(),movi.getIdEmpresaSede() ); 
                    getPrecioPromedio = getPrecioPromedio*Double.parseDouble(det.getMovDetCan());
                    det.setMovDetPreTot(""+getPrecioPromedio);
                    det.insertar();
                }
                FACTURA.getCabecera().setIdMovimientoTipo(""+1);
                String destino=FACTURA.getCabecera().getIdEmpresaSedeDes();
                FACTURA.getCabecera().setIdEmpresaSedeDes(FACTURA.getCabecera().getIdEmpresaSede());
                FACTURA.getCabecera().setIdEmpresaSede(destino);
               
                FACTURA.insertarWithNewId(Integer.parseInt(FACTURA.getCabecera().getIdMovimientoCabeceraAsociado()));
//PedidoObj ped = new PedidoObj(FACTURA);
               // String ruta = ped.generarArchivoPlano();
//enviarMensaje(ruta);
                jDialog1.dispose();
                JOptionPane.showMessageDialog(null, "GRABADO HECHO EXITOSAMENTE");

  
                    ModificarPedido pantalla;

            pantalla = new ModificarPedido();
       
            pantalla.setVisible(true);
   
                    System.gc();
                    this.dispose();

            } catch (SQLException ex) {
                Logger.getLogger(Pedidos3.class.getName()).log(Level.SEVERE, null, ex);
            }

            return true;
        }
        return false;
    }
    
    
}
