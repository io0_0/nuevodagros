/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pedidos;

import Bean.MovimientoCab;
import Bean.MovimientoDet;
import Facturador.factura;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author USUARIO
 */
public class PedidoObj implements Serializable{
    private MovimientoCab cabecera ;
    private ArrayList<MovimientoDet> detalles;
   public  PedidoObj(factura FACTURA) {
        cabecera= new MovimientoCab(FACTURA.getCabecera());
        detalles = FACTURA.getDetalles();
    }
    public PedidoObj(){}
    public String generarArchivoPlano() throws IOException{
   String fileName="D:\\empleado.json";
        ObjectMapper mapper = new ObjectMapper();
  mapper.setVisibility(JsonMethod.FIELD, JsonAutoDetect.Visibility.ANY);
String json = mapper.writerWithDefaultPrettyPrinter()
                    .writeValueAsString(this);
mapper.writerWithDefaultPrettyPrinter()
      .writeValue(new File(fileName), this);
System.out.println("SALIDA JSON: \n" + json);
    return fileName;
    }
    public static  PedidoObj leerJSON(String ruta) throws IOException{
        ObjectMapper mapper = new ObjectMapper();

PedidoObj emp = mapper.readValue(
 new File(ruta), PedidoObj.class);

System.out.println("OBJETO EMPLEADO: \n" + emp.cabecera);
    return emp;}

    public MovimientoCab getCabecera() {
        return cabecera;
    }

    public void setCabecera(MovimientoCab cabecera) {
        this.cabecera = cabecera;
    }

    public ArrayList<MovimientoDet> getDetalles() {
        return detalles;
    }

    public void setDetalles(ArrayList<MovimientoDet> detalles) {
        this.detalles = detalles;
    }
    
}
