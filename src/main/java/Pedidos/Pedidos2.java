/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package Pedidos; 
import Bean.Articulo;
import Bean.Configurar;
import Maestros.Maestro;
import GUI.EmpresaGUI;
import static Maestros.ArticuloM.cn;
import Bean.Empresa;
import Bean.EmpresaSede;
import Bean.Lista;
import Bean.MovimientoCab;
import Bean.MovimientoDet;
import Bean.MovimientoDetalle;
import Bean.MovimientoDocumento;
import Bean.UnidadMedida;
import Bean.empresasedeserie;
import Conexion.Mysql;
import Enviar2.ArchivosGenerdor;
import Enviar2.EnviarArchivosPlanos;
import Facturador.PrintUtility;
import Facturador.factura;
import Facturador.panelPedido;
import GUI.Principal;
import Maestros.Atributos;
import static Maestros.Atributos.IDMOVIMIENTOCABECERA;
import static Maestros.Atributos.MOVDETESTREG;
import Maestros.MiModel;
import Maestros.MovimientoDetalleM;
import br.com.adilson.util.Extenso;
import br.com.adilson.util.PrinterMatrix;
import com.mysql.jdbc.MySQLConnection;
import com.toedter.calendar.JCalendar;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.MessagingException;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.swing.*;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Equipo EPIS
 */
public class Pedidos2 extends javax.swing.JFrame implements Atributos {

    protected factura FACTURA = new factura();
    ArrayList<Component> editables = new ArrayList<Component>();
    public static final int ANCHO_FORM_LLENAR = 972;
    public static final int ALTO_FORM_LLENAR = 375;
    public static final int ANCHO_FORM_MOSTRAR = 1400;
    public static final int ALTO_FORM_MOSTRAR = 755;
    public static final int ANCHO_FORM_BOT = 335;
    public static final int ALTO_FORM_BOT = 10;
    public static final int ANCHO_FORM = 1400;
    public static final int ALTO_FORM = 1100;
    boolean bandera = true;
    int cantDatos = 9;
    public int indiceFila = 0;
    public static Pedidos2 actual;
    Maestro maestro = new Maestro();
    //Este objeto siempre tendra los atributos de la Tabla para que se ejecute sql en la clase EmpresaSql.
    MovimientoDetalleM ejecutor = maestro.gestionarMovimientoDetalle();
    MovimientoCab cabe = new MovimientoCab();
    private String usuario = "";
    protected String otroCamp = "";
    boolean isModificando = false;
    public panelPedido[] paneles = new panelPedido[50];
    static int anchoFinal = 0;
    static int altoFinal = 0;
    static int anchoLlenar = 0 ; 
    static int altoLlenar = 0 ; 
    public Pedidos2() throws SQLException {
        //    FACTURA.setCabecera(new MovimientoCab());
        if (actual != null) {
            actual.dispose();
        }

        
        iniMio();

        actual = this;

    }
    ArrayList<String> eliminaEstosDetalles = new ArrayList<>();

    Pedidos2(factura get) {
  if (actual != null) {
            actual.dispose();
        }
        FACTURA.setCabecera(new MovimientoCab(get.getCabecera().getIdMovimientoCabecera()));
        
        try {
            FACTURA.obtenerDetalles();
        } catch (SQLException ex) {
            Logger.getLogger(Pedidos3.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (MovimientoDet detalle : FACTURA.getDetalles()) {
            eliminaEstosDetalles.add(detalle.getIdMovimientoDetalle());
        }
        
       
        iniMio();
        try {
            iniCabe();
        } catch (ParseException ex) {
            Logger.getLogger(Pedidos2.class.getName()).log(Level.SEVERE, null, ex);
        }
        actual = this;        
    }
public void iniCabe() throws ParseException{
        EmpresaSede orig = new EmpresaSede(FACTURA.getCabecera().getIdEmpresaSede());

        EmpresaSede des = new EmpresaSede(FACTURA.getCabecera().getIdEmpresaSedeDes());
        System.out.println(jComboEmpSedDes.getSelectedItem().toString());
        
        jComboEmpSedOrig.setSelectedItem(orig.getEmpSedNom());
        jComboEmpSedDes.setSelectedItem(des.getEmpSedNom());
        jTextNumero.setText(FACTURA.getCabecera().getMovCabNum());
        jTextSerie.setText(FACTURA.getCabecera().getMovCabSer());
        MovimientoDocumento s = new MovimientoDocumento(FACTURA.getCabecera().getIdMovimientoDocumento());
         String sDate1=FACTURA.getCabecera().getMovCabFec();  
           java.util.Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);  
            fecha1.setDate(date1);
    jComboComprobate.setSelectedItem(s.getMovDocDes());
        jTextNumero.setText(FACTURA.getCabecera().getMovCabNum());
        jTextSerie.setText(FACTURA.getCabecera().getMovCabSer());
      
}
    public void iniMio() {
        try {
           initComponents();
            JTabla.setForeground(Color.red);
            refrescar();
            JCalendar jj = new JCalendar();
            fecha1.setDate(jj.getDate());
            cabe.llenarCaja(jComboComprobate, "MOVIMIENTODocumento", "MOVDocDes"," where movdocestreg=1");
            EmpresaSede.llenarCajaMisEmpresas(jComboEmpSedOrig);
            EmpresaSede.llenarCajaMisEmpresas(jComboEmpSedDes);
            //ejecutor.llenarCajaArt(JComboArt);
            jComboEmpSedDes.setSelectedIndex(3);
         //   jTextNumero.setEditable(false);
          //  jTextSerie.setEditable(false);
            jComboComprobate.setSelectedIndex(2);
            jComboComprobate.setEditable(false);

// TODO add your handling code here:
            empresasedeserie data;
            String idDocumento = "4";

// FACTURA.getCabecera().setIdEmpresaSede(jComboEmpSedOrig.getSelectedItem().toString());
            data = new empresasedeserie(FACTURA.getCabecera().sacarDato("empresasede", "empsednom", "'" + jComboEmpSedDes.getSelectedItem().toString() + "'"), idDocumento);
            int datos2 = Integer.parseInt(data.getEmpsedsernum());
            datos2++;
            String cero = "";
            for (int i = (int) (Math.floor(Math.log10(datos2))); i < data.getEmpsedsernum().length() - 1; i++) {
                cero += 0;

            }
            jTextNumero.setText(cero + "" + datos2);
            jTextSerie.setText(data.getEmpsedserser());
        } catch (SQLException ex) {
            Logger.getLogger(Pedidos2.class.getName()).log(Level.SEVERE, null, ex);
        }
        editables.add(jComboComprobate);
        editables.add(jComboEmpSedDes);
        editables.add(JBEliminar);
        editables.add(JBGrabar);
        llenarArticulos();
        JTCodigoCab.setVisible(false);
        anchoFinal = FormParaLlenar.getWidth();
        altoFinal = FormParaLlenar.getHeight();
        anchoLlenar = FormParaLlenar1.getWidth()+50;
        altoLlenar = FormParaLlenar1.getHeight()+10;
        estado(1);
        jComboComprobate.setSelectedIndex(jComboComprobate.getItemCount()-1);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jLabel1 = new javax.swing.JLabel();
        jMenuItem1 = new javax.swing.JMenuItem();
        FormParaLlenar = new javax.swing.JPanel();
        fecha1 = new com.toedter.calendar.JDateChooser();
        JTCodigoCab = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jComboComprobate = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();
        jComboEmpSedDes = new javax.swing.JComboBox();
        jLabelTitulo = new javax.swing.JLabel();
        FormBotones = new javax.swing.JPanel();
        JBNuevo = new javax.swing.JButton();
        JBModificar = new javax.swing.JButton();
        JBEliminar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        JTabla = new javax.swing.JTable();
        JBGrabar = new javax.swing.JButton();
        JBSalir = new javax.swing.JButton();
        jTextSerie = new javax.swing.JTextField();
        jTextNumero = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jComboEmpSedOrig = new javax.swing.JComboBox();
        FormParaLlenar1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        JBGrabar1 = new javax.swing.JButton();
        JBSalir1 = new javax.swing.JButton();
        JComboArt = new javax.swing.JComboBox();
        jLabel14 = new javax.swing.JLabel();
        JTCantidad = new javax.swing.JTextField();
        jtbuscar = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        jLabelUnidades = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();

        jDialog1.setTitle("ENVIANDO");
        jDialog1.setMinimumSize(new java.awt.Dimension(315, 110));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("ENVIANDO POR FAVOR ESPERE ");

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1Layout.createSequentialGroup()
                .addContainerGap(32, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );

        jMenuItem1.setText("jMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("COMPROBANTE DE VENTA");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setForeground(new java.awt.Color(255, 255, 255));
        setResizable(false);

        FormParaLlenar.setBackground(new java.awt.Color(255, 255, 255));
        FormParaLlenar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                FormParaLlenarKeyPressed(evt);
            }
        });

        fecha1.setDateFormatString("dd-MM-yyyy");

        JTCodigoCab.setEnabled(false);
        JTCodigoCab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTCodigoCabActionPerformed(evt);
            }
        });

        jLabel3.setText("FECHA:");

        jLabel4.setText("LOCAL LLEGADA");

        jComboComprobate.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboComprobate.setName("combo"); // NOI18N
        jComboComprobate.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboComprobateItemStateChanged(evt);
            }
        });
        jComboComprobate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboComprobateActionPerformed(evt);
            }
        });

        jLabel9.setText("COMPROBANTE");

        jComboEmpSedDes.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboEmpSedDes.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboEmpSedDesItemStateChanged(evt);
            }
        });
        jComboEmpSedDes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboEmpSedDesActionPerformed(evt);
            }
        });

        jLabelTitulo.setFont(new java.awt.Font("Tahoma", 0, 22)); // NOI18N
        jLabelTitulo.setText("Pedido de insumos ");

        FormBotones.setBackground(new java.awt.Color(255, 255, 255));

        JBNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/NUEVO.JPG"))); // NOI18N
        JBNuevo.setText("Agregar");
        JBNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBNuevoActionPerformed(evt);
            }
        });

        JBModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/MODIFICAR.JPG"))); // NOI18N
        JBModificar.setText("Modificar");
        JBModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBModificarActionPerformed(evt);
            }
        });

        JBEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/ELIMINAR.JPG"))); // NOI18N
        JBEliminar.setText("Eliminar");
        JBEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBEliminarActionPerformed(evt);
            }
        });

        JTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        JTabla.setGridColor(new java.awt.Color(0, 0, 0));
        jScrollPane2.setViewportView(JTabla);

        JBGrabar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/GUARDAR2.JPG"))); // NOI18N
        JBGrabar.setText("Grabar");
        JBGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBGrabarActionPerformed(evt);
            }
        });

        JBSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/CANCELAR.JPG"))); // NOI18N
        JBSalir.setText("Salir sin Grabar");
        JBSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FormBotonesLayout = new javax.swing.GroupLayout(FormBotones);
        FormBotones.setLayout(FormBotonesLayout);
        FormBotonesLayout.setHorizontalGroup(
            FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormBotonesLayout.createSequentialGroup()
                .addContainerGap(30, Short.MAX_VALUE)
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormBotonesLayout.createSequentialGroup()
                        .addComponent(JBGrabar, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(62, 62, 62)
                        .addComponent(JBSalir))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(JBNuevo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(JBEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(JBModificar, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE))
                .addGap(23, 23, 23))
        );
        FormBotonesLayout.setVerticalGroup(
            FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormBotonesLayout.createSequentialGroup()
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormBotonesLayout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(JBNuevo)
                        .addGap(18, 18, 18)
                        .addComponent(JBModificar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JBEliminar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 131, Short.MAX_VALUE))
                    .addGroup(FormBotonesLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGap(18, 18, 18)))
                .addGroup(FormBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JBGrabar, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JBSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(110, 110, 110))
        );

        jTextSerie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextSerieActionPerformed(evt);
            }
        });
        jTextSerie.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextSerieKeyTyped(evt);
            }
        });

        jTextNumero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextNumeroActionPerformed(evt);
            }
        });
        jTextNumero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextNumeroKeyTyped(evt);
            }
        });

        jLabel7.setText("SERIE");

        jLabel17.setText("NUMERO");

        jLabel6.setText("LOCAL ORIGEN");

        jComboEmpSedOrig.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboEmpSedOrig.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboEmpSedOrigItemStateChanged(evt);
            }
        });
        jComboEmpSedOrig.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboEmpSedOrigActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FormParaLlenarLayout = new javax.swing.GroupLayout(FormParaLlenar);
        FormParaLlenar.setLayout(FormParaLlenarLayout);
        FormParaLlenarLayout.setHorizontalGroup(
            FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormParaLlenarLayout.createSequentialGroup()
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormParaLlenarLayout.createSequentialGroup()
                            .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel4)
                                .addComponent(jLabel9)
                                .addComponent(jLabel7)
                                .addComponent(jLabel6))
                            .addGap(35, 35, 35)
                            .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jComboEmpSedOrig, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jComboEmpSedDes, javax.swing.GroupLayout.PREFERRED_SIZE, 405, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(43, 43, 43))
                        .addGroup(FormParaLlenarLayout.createSequentialGroup()
                            .addGap(147, 147, 147)
                            .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jComboComprobate, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTextSerie, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(62, 62, 62)
                            .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(FormParaLlenarLayout.createSequentialGroup()
                                    .addComponent(jLabel17)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jTextNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(FormParaLlenarLayout.createSequentialGroup()
                                    .addComponent(jLabel3)
                                    .addGap(18, 18, 18)
                                    .addComponent(fecha1, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(FormParaLlenarLayout.createSequentialGroup()
                        .addComponent(jLabelTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(82, 82, 82)
                        .addComponent(JTCodigoCab, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(FormBotones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(36, Short.MAX_VALUE))
        );
        FormParaLlenarLayout.setVerticalGroup(
            FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormParaLlenarLayout.createSequentialGroup()
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormParaLlenarLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabelTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(JTCodigoCab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jComboEmpSedOrig, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jComboEmpSedDes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(jComboComprobate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9))
                    .addComponent(fecha1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextSerie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(FormParaLlenarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel17)
                        .addComponent(jLabel7)))
                .addGap(15, 15, 15)
                .addComponent(FormBotones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52))
        );

        FormParaLlenar1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setText("BUSCAR:");

        jLabel5.setText("ARTICULO:");

        JBGrabar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/GUARDAR2.JPG"))); // NOI18N
        JBGrabar1.setText("Grabar");
        JBGrabar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBGrabar1ActionPerformed(evt);
            }
        });

        JBSalir1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/CANCELAR_1.JPG"))); // NOI18N
        JBSalir1.setText("Salir sin Grabar");
        JBSalir1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBSalir1ActionPerformed(evt);
            }
        });

        JComboArt.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        JComboArt.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                JComboArtItemStateChanged(evt);
            }
        });
        JComboArt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JComboArtKeyPressed(evt);
            }
        });

        jLabel14.setText("CANTIDAD:");

        JTCantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTCantidadActionPerformed(evt);
            }
        });
        JTCantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                JTCantidadKeyTyped(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JTCantidadKeyPressed(evt);
            }
        });

        jtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtbuscarActionPerformed(evt);
            }
        });
        jtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jtbuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtbuscarKeyReleased(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jLabel8.setText("UNIDADES");

        jLabelUnidades.setText("KILOGRAMOS");

        javax.swing.GroupLayout FormParaLlenar1Layout = new javax.swing.GroupLayout(FormParaLlenar1);
        FormParaLlenar1.setLayout(FormParaLlenar1Layout);
        FormParaLlenar1Layout.setHorizontalGroup(
            FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormParaLlenar1Layout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5)
                    .addComponent(jLabel14)
                    .addComponent(jLabel8))
                .addGap(63, 63, 63)
                .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelUnidades)
                    .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(FormParaLlenar1Layout.createSequentialGroup()
                            .addComponent(JBGrabar1)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
                            .addComponent(JBSalir1))
                        .addComponent(JComboArt, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(JTCantidad)
                        .addComponent(jtbuscar)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        FormParaLlenar1Layout.setVerticalGroup(
            FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormParaLlenar1Layout.createSequentialGroup()
                .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, FormParaLlenar1Layout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(55, 55, 55)
                        .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(JComboArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(jLabelUnidades))
                        .addGap(29, 29, 29)
                        .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(JTCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(FormParaLlenar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(JBGrabar1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(JBSalir1, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, FormParaLlenar1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(160, Short.MAX_VALUE))
        );

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        jMenu3.setText("jMenu3");

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Nuevo Item");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem2);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setText("Regresar");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem3);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(FormParaLlenar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FormParaLlenar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(51, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(FormParaLlenar, javax.swing.GroupLayout.PREFERRED_SIZE, 620, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(FormParaLlenar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JBSalir1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBSalir1ActionPerformed
salirDeAgregadoItem();
    }//GEN-LAST:event_JBSalir1ActionPerformed
   public void salirDeAgregadoItem(){
   
           estado(1);
        try {
            refrescar();
        } catch (SQLException ex) {
            Logger.getLogger(Pedidos2.class.getName()).log(Level.SEVERE, null, ex);
        }
   }
    private void JBGrabar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBGrabar1ActionPerformed

        grabarDetalle();

    }//GEN-LAST:event_JBGrabar1ActionPerformed

    private void JTCantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTCantidadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JTCantidadActionPerformed

    private void JTCantidadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTCantidadKeyTyped
   if(evt.getKeyCode()==KeyEvent.VK_ENTER){
                           jtbuscar.requestFocusInWindow();
                           return;
                           }     

    }//GEN-LAST:event_JTCantidadKeyTyped

    private void jTextNumeroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextNumeroKeyTyped
      
    }//GEN-LAST:event_jTextNumeroKeyTyped

    private void jTextNumeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextNumeroActionPerformed

    }//GEN-LAST:event_jTextNumeroActionPerformed

    private void jTextSerieKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextSerieKeyTyped
     
    }//GEN-LAST:event_jTextSerieKeyTyped

    private void jTextSerieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextSerieActionPerformed

    }//GEN-LAST:event_jTextSerieActionPerformed

    private void JBEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBEliminarActionPerformed
        if (JTabla.getSelectedRow() != -1 && JTabla.getSelectedRow() < FACTURA.getDetalles().size()) {
            try {
                MovimientoDet de = FACTURA.getDetalles().get(JTabla.getSelectedRow());

                Articulo articulo = new Articulo(Integer.parseInt(de.getIdArticulo()));
                double a = getPrecioUni(de);

                FACTURA.getDetalles().remove(JTabla.getSelectedRow());
            } catch (SQLException ex) {
                Logger.getLogger(Pedidos2.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un item  valido para Eliminar");
        }
        estado(1);
    }//GEN-LAST:event_JBEliminarActionPerformed

    private void JBModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBModificarActionPerformed
        bandera = false;
        String[] datos = new String[cantDatos];

        if (JTabla.getSelectedRow() != -1 && JTabla.getSelectedRow() < FACTURA.getDetalles().size()) {
            estado(2);
            indiceFila = JTabla.getSelectedRow();

            JTCodigoCab.setText(FACTURA.getDetalles().get(indiceFila).getIdMovimientoDetalle());
            JTCodigoCab.setEditable(false);

            JTCantidad.setText(FACTURA.getDetalles().get(indiceFila).getMovDetCan());

            int j;
            for (j = 0; j < JComboArt.getItemCount(); j++) {
                String idAr = FACTURA.getDetalles().get(indiceFila).getIdArticulo();
                if (JComboArt.getItemAt(j).equals((new Articulo(Integer.parseInt(idAr))).getArtNom())) {
                    break;
                }
            }
            JComboArt.setSelectedItem(JComboArt.getItemAt(j));

            datos[8] = "1";

        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un item valido para modificar");
        }
    }//GEN-LAST:event_JBModificarActionPerformed

    private void JBNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBNuevoActionPerformed
agregarItem();
    }//GEN-LAST:event_JBNuevoActionPerformed
  public void agregarItem(){
          estado(2);
        jtbuscar.setText("");
        if (FACTURA.getDetalles().size() == 0) {
            otroCamp = ejecutor.generarCodigo();
            // System.out.println("este es el id q estas sacando "+otroCamp);

        } else {
            int valor = -1;
            for (MovimientoDet col : FACTURA.getDetalles()) {
                valor = Math.max(valor, Integer.parseInt(col.getIdMovimientoDetalle()));
            }
            valor++;
            otroCamp = "" + valor;

        }

        JTCantidad.setText("");
        bandera = true;
        llenarTabla2(JComboArt.getSelectedItem().toString());
        //  limpiar();

  }
    private void jComboEmpSedDesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboEmpSedDesActionPerformed
        // TODO add your handling code here:
        // FACTURA.getCabecera().setIdEmpresaSede(jComboEmpSedOrig.getSelectedItem().toString());
    }//GEN-LAST:event_jComboEmpSedDesActionPerformed

    private void jComboEmpSedDesItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboEmpSedDesItemStateChanged
        /*empresasedeserie data;
        if (jComboEmpSedOrig.getSelectedItem() != null) {
        
        try {
        // FACTURA.getCabecera().setIdEmpresaSede(jComboEmpSedOrig.getSelectedItem().toString());
        data = new empresasedeserie(MovimientoCab.sacarDato("empresasede", "empsednom", "'" + jComboEmpSedOrig.getSelectedItem().toString() + "'"));
        int datos =Integer.parseInt(data.getEmpsedsernum());
        datos++;
        jTextNumero.setText(""+datos);
        jTextSerie.setText(data.getEmpsedserser());
        } catch (SQLException ex) {
        Logger.getLogger(facturadorGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        }*/
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboEmpSedDesItemStateChanged

    private void jComboComprobateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboComprobateActionPerformed

        // los nuevos registros son agregados al MODEL del JCombo HIJO

    }//GEN-LAST:event_jComboComprobateActionPerformed

    private void JBSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBSalirActionPerformed

        this.dispose();

    }//GEN-LAST:event_JBSalirActionPerformed

    private void JBGrabarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBGrabarActionPerformed

        guardar();
    }//GEN-LAST:event_JBGrabarActionPerformed

    private void jtbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtbuscarKeyPressed
   if(evt.getKeyCode()==KeyEvent.VK_ENTER){
                           JComboArt.requestFocusInWindow();
                           return;
                           }        
        llenarArticulos();
        if(JComboArt!=null && JComboArt.getSelectedItem()!=null)
        llenarTabla2(JComboArt.getSelectedItem().toString());

    }//GEN-LAST:event_jtbuscarKeyPressed

    private void jtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtbuscarKeyReleased
   if(evt.getKeyCode()==KeyEvent.VK_ENTER){
                           JComboArt.requestFocusInWindow();
                           return;
                           }        
        llenarArticulos();    
        if(JComboArt!=null && JComboArt.getSelectedItem()!=null)
        llenarTabla2(JComboArt.getSelectedItem().toString());// TODO add your handling code here:
    }//GEN-LAST:event_jtbuscarKeyReleased

    private void JComboArtItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_JComboArtItemStateChanged
        if (JComboArt.getSelectedItem() != null) {
            try {
                String id = FACTURA.getCabecera().sacarDato("articulo", "artnom", "'" + JComboArt.getSelectedItem().toString() + "'");
                Articulo art = new Articulo(Integer.parseInt(id));
                
        llenarTabla2(JComboArt.getSelectedItem().toString());
        UnidadMedida uni = new UnidadMedida(art.getIdUnidadMedida()+"");
        jLabelUnidades.setText(uni.getUniMedDes());
// TODO add your handling code here:
            } catch (SQLException ex) {
                Logger.getLogger(Pedidos2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_JComboArtItemStateChanged

    private void jComboComprobateItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboComprobateItemStateChanged
        try {
            // TODO add your handling code here:
            empresasedeserie data = new empresasedeserie();
            if (jComboComprobate == null || jComboComprobate.getSelectedItem() == null) {
                return;
            }
            String idDocumento = cabe.sacarDato("MovimientoDocumento", "movDocDes", "'" + jComboComprobate.getSelectedItem().toString() + "'");
            if (isModificando == true || jComboComprobate == null || jComboComprobate.getSelectedItem() == null) {
                return;
            }

            try {
                // FACTURA.getCabecera().setIdEmpresaSede(jComboEmpSedOrig.getSelectedItem().toString());
                String num = FACTURA.getCabecera().sacarDato("empresasede", "empsednom", "'" + jComboEmpSedDes.getSelectedItem().toString() + "'");
                if (num.equals("0")) {
                    num = "1";
                }
                data = new empresasedeserie(num, idDocumento);
                int datos2 = Integer.parseInt(data.getEmpsedsernum());
                datos2++;
                String cero = "";
                for (int i = (int) (Math.floor(Math.log10(datos2))); i < data.getEmpsedsernum().length() - 1; i++) {
                    cero += 0;

                }
                jTextNumero.setText(cero + "" + datos2);
                jTextSerie.setText(data.getEmpsedserser());

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "A ocurrido un Error Verifique si existe una serie y numero para el documento solicitado");
                Logger.getLogger(Pedidos2.class.getName()).log(Level.SEVERE, null, ex);
            }

            Object item = jComboComprobate.getSelectedItem();

        } catch (SQLException ex) {
            Logger.getLogger(Pedidos2.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_jComboComprobateItemStateChanged

    private void JTCodigoCabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTCodigoCabActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JTCodigoCabActionPerformed

    private void jComboEmpSedOrigItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboEmpSedOrigItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboEmpSedOrigItemStateChanged

    private void jComboEmpSedOrigActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboEmpSedOrigActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboEmpSedOrigActionPerformed

    private void jtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtbuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtbuscarActionPerformed

    private void JComboArtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JComboArtKeyPressed
   if(evt.getKeyCode()==KeyEvent.VK_ENTER){
                           JTCantidad.requestFocusInWindow();
                           return;
                           }        // TODO add your handling code here:
    }//GEN-LAST:event_JComboArtKeyPressed

    private void JTCantidadKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTCantidadKeyPressed
     
         if(evt.getKeyCode()==KeyEvent.VK_ENTER){
                           jtbuscar.requestFocusInWindow();
                           return;
                           } 

// TODO add your handling code here:
    }//GEN-LAST:event_JTCantidadKeyPressed

    private void FormParaLlenarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_FormParaLlenarKeyPressed

     if(evt.getKeyCode()==KeyEvent.VK_A){
     
         System.out.println("Pedidos.Pedidos2.FormParaLlenarKeyPressed() aAAAAAAA");
     }
     


        // TODO add your handling code here:
    }//GEN-LAST:event_FormParaLlenarKeyPressed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
agregarItem();        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
salirDeAgregadoItem();        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    public void estado(int p) {
        ///*estado para buscar en la grilla*/
        if (p == 1) {
            try {
                this.setTitle("PEDIDO DE INSUMOS");
                //limpiarFormLlenar();
                //      System.out.println("this " + this.getWidth() + " " + this.getHeight());
                //    System.out.println("form para llenar" + FormParaLlenar.getWidth() + "  " + FormParaLlenar.getHeight());
                //  System.out.println("form botones " + FormBotones.getWidth() + "--" + FormBotones.getHeight());

                FormParaLlenar.setVisible(true);
                FormBotones.setVisible(true);
                FormParaLlenar1.setVisible(false);
                this.setSize(anchoFinal, altoFinal);
                //      cambiarTamaño(0.435F, 0.74F);
    //            centrarPantalla();
                refrescar();
            } catch (SQLException ex) {
                Logger.getLogger(Pedidos2.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (p == 2)/*estado para ingresar un dato*/ {
            this.setTitle("INSERTAR PEDIDO ");
            this.setSize(anchoLlenar, altoLlenar);
      //      centrarPantalla();
            FormParaLlenar.setVisible(false);
            FormBotones.setVisible(false);
            FormParaLlenar1.setVisible(true);
           jtbuscar.requestFocusInWindow();
            if (p == 3) {

            }
        }
    }

    private double getPrecioUni(MovimientoDet detallito) throws SQLException {
        Lista a = new Lista();
        String sql_Cons = "SELECT * FROM lista where idEmpresaSede =" + FACTURA.getCabecera().getIdEmpresaSede() + " and ListEstReg=1";
        String listilla = "";
        try {
            Statement sent;

            sent = cn.createStatement();
            //  System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);

            while (rs.next()) {
                listilla = rs.getString("idlista");
                System.out.println(listilla);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Statement sent;

        sent = cn.createStatement();
        sql_Cons = "Select * From articuloprecio where  idLista= '" + listilla + "' AND idArticulo='" + detallito.getIdArticulo() + "'";
    //    System.out.println(sql_Cons);

        ResultSet rs = sent.executeQuery(sql_Cons);
        double resp = 0;
        if (rs.next()) {
            resp = Double.parseDouble(rs.getString("ArtPreDes"));

        }
        return resp;
    }

    private void centrarPantalla() {
        Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension ventana = this.getSize();
        this.setLocation((int) (pantalla.width - ventana.width) / 2, (int) (pantalla.height - ventana.height) / 2);

    }

    public void refrescar() throws SQLException {
        llenarTablaSunat(JTabla, "sadasdsadadwqeqsxqweqwsdxcascqw", FACTURA.getDetalles(), FACTURA.getCabecera());

        // limpiar();
        //  JTabla.setEnabled(false);
    }

    public void llenarTablaSunat(JTable tabla, String id, ArrayList<MovimientoDet> detalles, MovimientoCab cabe) {
        try {
            String[] atri = {"N°", "CANTIDAD", "UNIDADEs","DESCRIPCION"};
            int filas = 10;
            MiModel model1 = new MiModel(null, atri);
            String[] fila = new String[4];
            int i = 0;
            for (MovimientoDet de : detalles) {
                i++;
                fila[0] = "" + i;
                fila[1] = de.getMovDetCan();
               Articulo s  = new Articulo(Integer.parseInt(de.getIdArticulo()));
                fila[2] = new UnidadMedida(s.getIdUnidadMedida()+"").getUniMedDes();
                fila[3] = s.getArtNom();

                model1.addRow(fila);

            }
            for (; i < filas; i++) {
                fila = new String[4];
                fila[0] = " ";
                fila[1] = " ";
                fila[2] = " ";

                fila[3] = " ";
                model1.addRow(fila);
            }
            tabla.setModel(model1);

            tabla.getColumnModel().getColumn(0).setPreferredWidth(40);
            tabla.getColumnModel().getColumn(1).setPreferredWidth(200);
            tabla.getColumnModel().getColumn(2).setPreferredWidth(200);
            
            tabla.getColumnModel().getColumn(3).setPreferredWidth(750);
            tabla.setBackground(Color.white);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void limpiar() {
        JTCantidad.setText("");

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
        * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EmpresaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Pedidos2 a = new Pedidos2();
                    a.setVisible(true);
                    a.estado(1);
                } catch (SQLException ex) {
                    Logger.getLogger(Pedidos2.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel FormBotones;
    protected javax.swing.JPanel FormParaLlenar;
    private javax.swing.JPanel FormParaLlenar1;
    private javax.swing.JButton JBEliminar;
    private javax.swing.JButton JBGrabar;
    private javax.swing.JButton JBGrabar1;
    private javax.swing.JButton JBModificar;
    private javax.swing.JButton JBNuevo;
    private javax.swing.JButton JBSalir;
    private javax.swing.JButton JBSalir1;
    protected javax.swing.JComboBox JComboArt;
    protected javax.swing.JTextField JTCantidad;
    private javax.swing.JTextField JTCodigoCab;
    private javax.swing.JTable JTabla;
    protected static com.toedter.calendar.JDateChooser fecha1;
    protected javax.swing.JComboBox jComboComprobate;
    protected javax.swing.JComboBox jComboEmpSedDes;
    protected javax.swing.JComboBox jComboEmpSedOrig;
    protected javax.swing.JDialog jDialog1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelTitulo;
    private javax.swing.JLabel jLabelUnidades;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    protected javax.swing.JTextField jTextNumero;
    protected javax.swing.JTextField jTextSerie;
    private javax.swing.JTextField jtbuscar;
    // End of variables declaration//GEN-END:variables

    public void paraPruebas() {
        /*
        JTNombre.setText("Elvis Tellez Mendoza ");
        JTDocumentoDestino.setText("72943030");
        jTextSerie.setText("00001");
        jTextNumero.setText("00001");
        MovimientoDet de = new MovimientoDet("NULL",JTCodigoCab.getText(), "1", "1", "100", "2000", "900", "0.18", "1");
        FACTURA.getDetalles().add(de);
         */
    }

    public boolean camposBienLlenados() {
        boolean val = true;
        String err = "";
        boolean a = jTextNumero.getText().length() == 8;
        a = true;
        if (a) {
            val = val && true;
        } else {
            val = val && false;
            err += "LONGITUD NO VALIDA PARA EL NUMERO \n ";
        }

        if (fecha1.getDate() == null) {
            val = val && false;
            err += "FECHA NO VALIDA \n";
        }
        if (FACTURA.getDetalles().isEmpty()) {
            val = val && false;
            err += "INGRESE ALGUN DETELLE \n";

        }

        if (!val) {
            //   System.out.println(err);
            JOptionPane.showMessageDialog(null, err);

        }
        return val;
    }

    public static boolean entero(String str) {
        boolean a = true;
        for (int i = 0; i < str.length(); i++) {
            if (!(str.charAt(i) >= 0 && str.charAt(i) <= 9)) {
                a = false;
            }
        }
        return a;
    }

    private void llenarDatos() {

        try {
            JTCodigoCab.setText(FACTURA.getCabecera().getIdMovimientoCabecera());
            jTextNumero.setText(FACTURA.getCabecera().getMovCabNum());
            jTextSerie.setText(FACTURA.getCabecera().getMovCabSer());
            String destino = MovimientoCab.sacarDato("empresasede", "idempresasede", FACTURA.getCabecera().getIdEmpresaSedeDes(), "idempresa");

            String nombre = FACTURA.getCabecera().sacarDato("empresa", "idempresa", destino, "empNom");
            destino = MovimientoCab.sacarDato("empresa", "idempresa", destino, "empruc");
            String comprobante = FACTURA.getCabecera().sacarDato("movimientodocumento", "idmovimientodocumento", FACTURA.getCabecera().getIdMovimientoDocumento(), "movdocdes");
            String doc = FACTURA.getCabecera().sacarDato("empresasede", "idempresasede", FACTURA.getCabecera().getIdEmpresaSedeDes(), "idempresa");
            doc = FACTURA.getCabecera().sacarDato("empresa", "idempresa", doc, "iddocumento");
            doc = FACTURA.getCabecera().sacarDato("documento", "iddocumento", doc, "docdes");

            String empSedeString = FACTURA.getCabecera().sacarDato("empresasede", "idempresasede", FACTURA.getCabecera().getIdEmpresaSede(), "empsednom");

            //   System.err.println("el doc es "+doc +" "+comprobante);
            jComboComprobate.setSelectedItem(comprobante);
            jComboEmpSedDes.setSelectedItem(empSedeString);
            String desde = FACTURA.getCabecera().getMovCabFec();
            // System.out.println(desde);
            refrescar();
            double sumaTotal = 0.0;
            for (MovimientoDet det : FACTURA.getDetalles()) {
                sumaTotal += Double.parseDouble(det.getMovDetPreTot());
            }
            EmpresaSede de = new EmpresaSede(FACTURA.getCabecera().getIdEmpresaSedeDes());
            String direccion = de.getEmpSedDir();
            DecimalFormat df = new DecimalFormat("#.00");
            double sumaSinIGV = sumaTotal * (1 - 0.18 / 1.18);
            double sumaIGV = sumaTotal / 1.18 * 0.18;
            String sumaSinIGVString = df.format(sumaSinIGV);
            String sumaIGVString = df.format(sumaIGV);

            if (desde.substring(0, 1).equals("'")) {
                desde = desde.substring(1, desde.length() - 1);
            }
            int dia = Integer.parseInt(desde.substring(8));
            int mes = Integer.parseInt(desde.substring(5, 7));
            int año = Integer.parseInt(desde.substring(0, 4));
            String fechaF = dia + "-" + mes + "-" + año;
            Date d = new Date(año - 1900, mes - 1, dia);
            fecha1.setDate(d);

            /* dia = fecha.substring(0, fecha.indexOf("/"));
            String mes = fecha.substring(fecha.indexOf("/") + 1, fecha.lastIndexOf("/"));
            String año = fecha.substring(fecha.lastIndexOf("/") + 1);
             */
        } catch (SQLException ex) {
            Logger.getLogger(Pedidos2.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    // dagros
    void imprimirPedido() {
        PrinterMatrix printer = new PrinterMatrix();

        Extenso e = new Extenso();

        e.setNumber(FACTURA.getDetalles().size() + 10);
        // nombre
        // ruc origen
        //nombre destino
        // fecha
        // serie  y numero
        String nombre = jComboEmpSedDes.getSelectedItem().toString();
        EmpresaSede empsed = new EmpresaSede(FACTURA.getCabecera().getIdEmpresaSede());
        Empresa emp = new Empresa(empsed.getIdEmpresa());
        String titulo = "";
        titulo = "\t" + jComboComprobate.getSelectedItem().toString() + " " + "N°:B" + jTextSerie.getText() + "-" + jTextNumero.getText();;

        String fecha = "FECHA: " + ((JTextField) (fecha1.getDateEditor().getUiComponent())).getText();
        //String  serie_numero ="NRO de DOC: "+jTextSerie.getText()+"-"+jTextNumero.getText();
        int constante = 40;
        int lineasAimprimir = 27 + FACTURA.getDetalles().size();
        String cabecera = "|         PRODUCTO      |CANTIDAD|";
        String linea = "";
        for (int i = 0; i < 41; i++) {
            linea += "-";

        }

        String[] arr = {titulo, nombre, fecha, " ", cabecera, linea};
        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(lineasAimprimir, constante);
        int i = 2;
        for (; i < arr.length + 2; i++) {
            String arr1 = arr[i - 2];
            printer.printTextWrap(i, i, 1, constante, arr1);

        }
        i++;
        int a = 0;
        for (a = i; a < FACTURA.getDetalles().size() + i; a++) {
            String descripcion = JTabla.getValueAt(a - i, 2).toString();

            String cantidad = JTabla.getValueAt(a - i, 1).toString();
            String splitS = cabecera.substring(1, cabecera.length() - 1);
            String split[] = {"       PRODUCTO      ", "CANTIDAD"};
            while (descripcion.length() < split[0].length()) {
                if (descripcion.length() > split[0].length()) {
                    break;
                }
                descripcion = " " + descripcion;

                if (descripcion.length() > split[0].length()) {
                    break;
                }
                descripcion = descripcion + " ";

            }
            while (cantidad.length() < split[1].length()) {
                if (cantidad.length() > split[1].length()) {
                    break;
                }
                cantidad = " " + cantidad;

                if (cantidad.length() > split[1].length()) {
                    break;
                }
                cantidad = cantidad + " ";

            }
            String datos = "|" + descripcion + "|" + cantidad + "|";
            //  System.err.println(datos );
            printer.printTextWrap(a, a, 1, constante, datos);

        }
        printer.printTextWrap(a, a, 1, constante, linea);
        a++;
        printer.printTextWrap(a, a, 1, constante, "");
        a++;

        printer.printTextWrap(a, a, 1, constante, "");
        a++;

        printer.printTextWrap(a, a, 1, constante, "");
        a++;

        printer.printTextWrap(a, a, 1, constante, "__________________________");
        a++;
        printer.printTextWrap(a, a, 1, constante, "Firma-DNI  EnvioArtPlanta ");
        a++;
        printer.printTextWrap(a, a, 1, constante, "");
        a++;

        printer.printTextWrap(a, a, 1, constante, "");
        a++;

        printer.printTextWrap(a, a, 1, constante, "_________________________");
        a++;
        printer.printTextWrap(a, a, 1, constante, "Firma-Recepcion Conforme ");
        a++;
        //Imprimir * de la 2da linea a 25 en la columna 1;
        // printer.printCharAtLin(2, 25, 1, "*");
        //Imprimir * 1ra linea de la columa de 1 a 80
        // printer.printCharAtCol(1, 1, constante, "=");
        //Imprimir Encabezado nombre del La EMpresa
        // printer.printTextWrap(1, 2, 1, constante, "FACTURA DE VENTA");
        //printer.printTextWrap(linI, linE, colI, colE, null);t
        //printer.printTextWrap(2, 3, 1, 22, "Num. Boleta : " );
        //    printer.printTextWrap(3, 3, 1, 22, "Num. Boleta : " );
        // printer.printTextWrap(4, 4, 1, 22, "Num. Boleta : " );
        //  printer.printTextWrap(4, 4, 1, 22, "Num. Boleta : " );
        // printer.printTextWrap(4, 4, 1, 22, "Num. Boleta : " );

        // la primer linea es la fila donde se imprime
        /*
        for (int i = 0; i &lt; filas; i++) { printer.printTextWrap(9 + i, 10, 1, 80, tblVentas.getValueAt(i,0).toString()+"|"+tblVentas.getValueAt(i,1).toString()+"| "+tblVentas.getValueAt(i,2).toString()+"| "+tblVentas.getValueAt(i,3).toString()+"|"+ tblVentas.getValueAt(i,4).toString()); } if(filas &gt; 15){
        printer.printCharAtCol(filas + 1, 1, 80, "=");
        printer.printTextWrap(filas + 1, filas + 2, 1, 80, "TOTAL A PAGAR " + txtVentaTotal.getText());
        printer.printCharAtCol(filas + 2, 1, 80, "=");
        printer.printTextWrap(filas + 2, filas + 3, 1, 80, "Esta boleta no tiene valor fiscal, solo para uso interno.: + Descripciones........");
        }else{
        printer.printCharAtCol(25, 1, 80, "=");
        printer.printTextWrap(26, 26, 1, 80, "TOTAL A PAGAR " + txtVentaTotal.getText());
        printer.printCharAtCol(27, 1, 80, "=");
        printer.printTextWrap(27, 28, 1, 80, "Esta boleta no tiene valor fiscal, solo para uso interno.: + Descripciones........");
        
        }*/
        printer.toFile("impresion.txt");

        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream("impresion.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();

        if (defaultPrintService != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            //  System.err.println("No existen impresoras instaladas");
        }

        //inputStream.close();
    }

    public boolean guardar() {
        jDialog1.setVisible(true);
        jDialog1.setLocationRelativeTo(this);
        if (camposBienLlenados()) {

            try {
                double dinero = 0;
                //ESTE VALE

                FACTURA.ajustar();

                int wwwww = jComboEmpSedOrig.getSelectedIndex() + 1;

                // System.out.println(identificador);
                MovimientoCab movi = new MovimientoCab();
                movi.setIdEmpresaSedeDes(cabe.sacarDato("empresaSEde ", "empsednom", "'" + jComboEmpSedDes.getSelectedItem().toString() + "'"));
                movi.setIdEmpresaSede(cabe.sacarDato("empresaSEde ", "empsednom", "'" + jComboEmpSedOrig.getSelectedItem().toString() + "'"));

//String revisar=RevisarDocumento();
                //movi.setIdEmpresaSedeDes(revisar);
                int auxLLL = Integer.parseInt(cabe.sacarDato("MovimientoDocumento", "MovDocDes", "'" + jComboComprobate.getSelectedItem().toString() + "'"));
                String idmovdoc = auxLLL + "";
                movi.setIdMovimientoDocumento(idmovdoc);
                String a = Principal.jLUsuarioGeneral != null ? Principal.jLUsuarioGeneral.getText() : "1";
                movi.setIdUsuario("'" + a + "'");
                movi.setIdMovimientoTipo("2");

//                movi.setMovCabEnv("0");
                movi.setMovCabEstReg("1");

                java.util.Date d = fecha1.getDate();
                SimpleDateFormat da = new SimpleDateFormat("yyyy/MM/dd");

                movi.setMovCabFec("'" + da.format(d) + "'");

                movi.setMovCabMovSto("0");
                movi.setMovCabNum("'" + jTextNumero.getText() + "'");
                movi.setMovCabSer("'" + jTextSerie.getText() + "'");

//FACTURA.getCabecera().setIdEmpresaSedeDes(identificador);
                FACTURA.getCabecera().generarCodigo(JTCodigoCab);
                movi.setIdMovimientoCabecera("'" + JTCodigoCab.getText() + "'");
                movi.setMovCabProBoo(0);
               movi.setIdMovimientoCabeceraAsociado(movi.getIdMovimientoCabecera());
                FACTURA.setCabecera(movi);

                FACTURA.getCabecera().insertar();
                for (MovimientoDet det : FACTURA.getDetalles()) {
                    det.setIdMovimientoCabecera(JTCodigoCab.getText());
                    double getPrecioPromedio =MovimientoDet.getPrecioPromedio(det.getIdArticulo(),movi.getIdEmpresaSede() ); 
                    getPrecioPromedio = getPrecioPromedio*Double.parseDouble(det.getMovDetCan());
                    det.setMovDetPreTot(""+getPrecioPromedio);
                    det.insertarID();
                }
                FACTURA.getCabecera().setIdMovimientoTipo(""+1);
                String destino=FACTURA.getCabecera().getIdEmpresaSedeDes();
                FACTURA.getCabecera().setIdEmpresaSedeDes(FACTURA.getCabecera().getIdEmpresaSede());
                FACTURA.getCabecera().setIdEmpresaSede(destino);
               
                FACTURA.insertarWithNewId();
//PedidoObj ped = new PedidoObj(FACTURA);
               // String ruta = ped.generarArchivoPlano();
//enviarMensaje(ruta);
                empresasedeserie serie = new empresasedeserie(FACTURA.getCabecera().getIdEmpresaSedeDes(), ""+5);
                int numero = Integer.parseInt(serie.getEmpsedsernum()) + 1;
                String ceros = "";
                for (int i = (int) (Math.floor(Math.log10(numero))); i < serie.getEmpsedsernum().length() - 1; i++) {
                    ceros += "0";
                }
                serie.setEmpsedsernum(ceros + "" + numero);
                serie.modificar();
                jDialog1.dispose();
                JOptionPane.showMessageDialog(null, "GRABADO HECHO EXITOSAMENTE");

                    System.gc();
                ModificarPedido pantalla;

            pantalla = new ModificarPedido();
       
            pantalla.setVisible(true);
    this.dispose();
        
                
            } catch (SQLException ex) {
                Logger.getLogger(Pedidos2.class.getName()).log(Level.SEVERE, null, ex);
            }

            return true;
        }
        return false;
    }

    public void enviarMensaje(String archivo) {

        Calendar c = Calendar.getInstance();
		String dia = Integer.toString(c.get(Calendar.DATE));
		String mes = Integer.toString(c.get(Calendar.MONTH));
		String annio = Integer.toString(c.get(Calendar.YEAR));
		String hora = Integer.toString(c.get(Calendar.HOUR));
		String minuto = Integer.toString(c.get(Calendar.MINUTE));

    }

    protected void grabarDetalle() {

        DecimalFormat df = new DecimalFormat("#.00");
        try{
        	Double.parseDouble(JTCantidad.getText());
        	
        }
        catch(Exception  e){
            JOptionPane.showMessageDialog(null, "Por favor ingrese todos los campos");
            return ;
        }
        //Insertar segun bandera
       
        if (false) {
            JOptionPane.showMessageDialog(null, "Por favor ingrese todos los campos");
        } else {
            MovimientoDet actual;
            if (bandera) {
                FACTURA.getDetalles().add(new MovimientoDet());
                actual = FACTURA.getDetalles().get(FACTURA.getDetalles().size() - 1);

            } else {
                //modificar

                actual = FACTURA.getDetalles().get(indiceFila);

            }
            String nombre = JComboArt.getSelectedItem().toString();

            String dato = " ";
            try {
                dato = actual.sacarDato("articulo", "artnom", nombre);
            } catch (SQLException ex) {
                Logger.getLogger(Pedidos2.class.getName()).log(Level.SEVERE, null, ex);
            }
            actual.setIdArticulo(dato);
            actual.setIdMovimientoCabecera(JTCodigoCab.getText());
            actual.setIdMovimientoDetalle(otroCamp);
            actual.setMovDetCan(JTCantidad.getText());
            actual.setMovDetCosTot("0");
            Articulo art = new Articulo(Integer.parseInt(actual.getIdArticulo()));
            actual.setMovIngreso("" + 1);
            actual.setMovDetEstReg("" + 1);
            if (bandera) {
                for (int w = 0; w < FACTURA.getDetalles().size() - 1; w++) {
                    MovimientoDet det = FACTURA.getDetalles().get(w);
                    boolean articuloyadigitado = det.getIdArticulo().equals(actual.getIdArticulo());
                    if (articuloyadigitado) {
                        double cantidad = Double.parseDouble(actual.getMovDetCan());
                        cantidad += Double.parseDouble(det.getMovDetCan());
                       
                        det.setMovDetCan("" + cantidad);
                        FACTURA.getDetalles().remove(actual);

                    }
                }
            }

            estado(1);

        }
    }

    void llenarArticulos() {
        try {
            String condicion = "Select * from articulo where ArtEstReg =1 AND ArtNom like '%" + jtbuscar.getText() + "%'";
            //   System.err.println(condicion);
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(condicion);
            JComboArt.removeAllItems();
            //     System.err.println("presionoooo¡¡¡");
            while (rs.next()) {
                JComboArt.addItem(rs.getString("ArtNom"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Pedidos2.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
private void llenarTabla2(String articulo) {
        Inventario.Inventario.llenarTabla(jTable1,articulo);
    
    }

}
