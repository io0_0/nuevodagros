/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IngresoAlmacen;

import Conexion.Mysql;
import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author cesar
 */
public class GuardarCompra {
    String sql;
    Connection reg;
    //Datos a utilizar para Movimiento Cabecera
    String IdMovCab;    //Id de movimiento
    String IdMovDoc;    //Id de movimiento documento
    String IdUsu;   //Id del usuario que hace la orden
    String IdEmpSed;    //Id de EMpresa sede
    String IdEmpSedDes;    //Id de EMpresa sede Detino 
    String IdMovTip;    //Id de tipo de movimiento
    String MovCabSto;   //Stock
    String MovCabFec;   //Fecha
    String MovCabSer;   //Serie
    String MovCabNum;   //Numero
    String MovCabEnv;   //Envio
    String MovCabEstReg;    //Estado de Registro
    
    //Datos a utilizar para Movimiento Detalle
    String IdMovDet;    //Id de movimiento detalle 
    String IdMovCabe;    //Id de movimiento cabecera
    String IdArt;   //Id del articulo
    String MovIng;    //Movimiento Ingreso
    String MovDetCan;    //Cantidad
    String MovDetPreTot;   //Precio Total
    String MovDetCosTot;   //Costo Total
    String MovDetIgv;   // IGV
    String MovDetEstReg;   //Estado de Registro
    
    public String guardarCabecera(String[] datos) {
        /*
        Guardamos datos
        */
        IdMovCab = datos[0];    //Id de movimiento
        IdMovDoc = datos[1];    //Id de movimiento documento
        IdUsu = datos[2];   //Id del usuario que hace la orden
        IdEmpSed = datos[3];    //Id de EMpresa sede
        IdEmpSedDes = datos[4];    //Id de EMpresa sede destino
        IdMovTip = datos[5];    //Id de tipo de movimiento
        MovCabSto = datos[6];   //Stock
        MovCabFec = datos[7];   //Fecha
        MovCabSer = datos[8];   //Serie
        MovCabNum = datos[9];   //Numero
        MovCabEnv = datos[10];   //Si se envio (produccion=0) no se envia
        MovCabEstReg = datos[11];
        
        /*
        Insertamos en la base de datos
        */
        try {
//           Mysql con = new Mysql();
            reg = Mysql.getConection();

            sql = "INSERT INTO movimientocab (IdMovimientoCabecera, IdMovimientoDocumento, IdUsuario, IdEmpresaSede, IdEmpresaSedeDes, IdMovimientoTipo, MovCabMovSto, MovCabFec, MovCabSer, MovCabNum, MovCabEnv, MovCabEstReg)VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            PreparedStatement pst = reg.prepareStatement(sql);
            pst.setInt(1, Integer.parseInt(IdMovCab));
            pst.setInt(2, Integer.parseInt(IdMovDoc));
            pst.setInt(3, Integer.parseInt(IdUsu));
            pst.setInt(4, Integer.parseInt(IdEmpSed));
            pst.setInt(5, Integer.parseInt(IdEmpSedDes));
            pst.setInt(6, Integer.parseInt(IdMovTip));
            pst.setBoolean(7, Boolean.parseBoolean(MovCabSto));
            SimpleDateFormat format = new SimpleDateFormat("dddd/MM/yy");
            java.util.Date myDate;
            myDate = format.parse(MovCabFec);
            java.sql.Date sqlDate = new java.sql.Date(myDate.getTime());
            pst.setDate(8, sqlDate);
            pst.setString(9, MovCabSer);
            pst.setString(10, MovCabNum);
            pst.setBoolean(11, Boolean.parseBoolean(MovCabEnv));
            pst.setInt(12, Integer.parseInt(MovCabEstReg));
            
            System.out.println("datos: --> "+ IdMovCab +"  "+IdMovDoc +"  "+IdUsu +"  "+IdEmpSed +"  "+IdEmpSedDes +"  "+IdMovTip +"  "+
                    MovCabSto +"  "+MovCabFec+"  "+MovCabSer+"  "+MovCabNum+"  "+MovCabEnv+"  "+MovCabEstReg);
            int n = pst.executeUpdate();
            if (n > 0) {
                System.out.println("Registrado con exito -> CompraCabecera");
            } else {
                JOptionPane.showMessageDialog(null, "Error al registrar (faltan campos)");
            }
            
            /*
            Actualizar en La tabla de EMpresaSerie
            */ 
            sql = ("UPDATE empresasedeserie SET EmpSedSerNum = EmpSedSerNum + '" + 1 + "' WHERE IdEmpresaSede='" + IdEmpSedDes + "' and idmovimientodocumento = '"+IdMovDoc+"'"); 
            System.err.println(sql);
            Statement st = reg.createStatement(); 
            st.execute(sql); 
            System.out.println("Modificado con exito la Serie y el Número");
            reg.close();
            
        } catch (HeadlessException | NumberFormatException | SQLException | ParseException e) {
            Logger.getLogger(GuardarCompra.class.getName()).log(Level.SEVERE, null, e);
        }

        return IdMovCab;
    }
    
    public String guardarDetalle(String[] datos){
        /*
        Guardamos datos
        */
        IdMovDet = datos[0];    //Id de movimiento detalle
        IdMovCabe = datos[1];   //Id de movimiento cabecera
        IdArt = datos[2];    //Id articulos
        MovIng = datos[3];   // Movimiento de Ingreso (insumos=0 producto=1 merma=2)
        MovDetCan = datos[4];    //Cantidad
        MovDetPreTot = datos[5];    //Precio total
        MovDetCosTot = datos[6];   //Costo total
        MovDetIgv = datos[7];   //IGV
        MovDetEstReg = datos[8];   //EStado de Registro
        
        /*
        Insertamos en la base de datos
        */
        try {
//          Mysql con = new Mysql();
            reg = Mysql.getConection();

            sql = "INSERT INTO movimientodet (IdMovimientoCabecera, IdArticulo, "
                    + "MovIngreso, MovDetCan, MovDetCosTot, MovDetIgv, MovDetEstReg)VALUES (?, ?, ?, ?, ?, ?, ?)";

            PreparedStatement pst = reg.prepareStatement(sql);
            // BORREGO 
            
            pst.setInt(1, Integer.parseInt(IdMovCabe));
            pst.setInt(2, Integer.parseInt(IdArt));
            pst.setInt(3, Integer.parseInt(MovIng));
            pst.setInt(4, Integer.parseInt(MovDetCan));
            pst.setDouble(5, Double.parseDouble(MovDetCosTot));
            pst.setDouble(6, Double.parseDouble(MovDetIgv));
            pst.setInt(7, Integer.parseInt(MovDetEstReg));
            System.out.println("datos: --> "+ IdMovCabe +"  "+IdArt +"  "+MovIng +"  "+MovDetCan +"  "+MovDetCosTot +"  "+
                    MovDetIgv +"  "+MovDetEstReg);

            int n = pst.executeUpdate();
            if (n > 0) {
                System.out.println("Registrado con exito -> CompraDetalle");
            } else {
                JOptionPane.showMessageDialog(null, "Error al registrar (faltan campos)");
            }
            reg.close();
        } catch (HeadlessException | NumberFormatException | SQLException e) {
            System.out.println(e.getMessage());
            Logger.getLogger(GuardarCompra.class.getName()).log(Level.SEVERE, null, e);
        }
        return IdMovDet;
    }
    
    
     public String guardarGasto(double gastoTotal, String idEmpresa) throws SQLException{
         //Guardamos gasto en empresaSede
         reg = Mysql.getConection();
         sql = ("UPDATE empresasede SET EmpSedDin = EmpSedDin - " + gastoTotal + " WHERE IdEmpresaSede='" + idEmpresa + "'");
         Statement st = reg.createStatement();
         st.execute(sql);
//         (null, "Modificado con exito");
         reg.close();
         return idEmpresa;
     }

    void actualizarCosto(String idArt, String costoNuevo, String cantidad) {
        try {
            //Guardamos gasto en empresaSede
            reg = Mysql.getConection();
            double cant = Double.parseDouble(cantidad);
            double costoT = Double.parseDouble(costoNuevo);
            ResultSet rs;
            Statement st;
            sql = "SELECT * FROM articulo WHERE IdArticulo = "+idArt;
            st = reg.createStatement();
            rs = st.executeQuery(sql);
            double igv = 0.18; 
            while(rs.next()){
                igv = rs.getDouble("ArtIgv");
            }
            
            DecimalFormat decimal = new DecimalFormat("#.##");
            double igvSobre=igv+1;
            double denominador = Double.parseDouble(decimal.format(igvSobre).replace(',', '.'));
            double nuevoCosto = (costoT/cant)/(denominador);
            
            nuevoCosto = Double.parseDouble(decimal.format(nuevoCosto).replace(',', '.'));
            
            sql = ("UPDATE articulo SET ArtCos = '" + nuevoCosto + "' WHERE IdArticulo='" + idArt + "'");
            System.out.println("Guardar gasto -> "+sql);
            Statement st2 = reg.createStatement();
            st2.execute(sql);
            System.out.println("Modificado con exito -> Actualizar Costo");
            reg.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(GuardarCompra.class.getName()).log(Level.SEVERE, null, ex);
        }
         
    }
}
