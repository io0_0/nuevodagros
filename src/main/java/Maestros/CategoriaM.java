/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Maestros;

import Bean.Categoria;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author Equipo EPIS
 */
public class CategoriaM implements Gestionable {

    Categoria empresaClase;

    public String insertar(String[] datos) {
        try {
            String nombre= "'" + datos[0] + "'";
            
          
            empresaClase = new Categoria(nombre);
            empresaClase.insertar();
          
        } catch (SQLException ex) {
            Logger.getLogger(CategoriaM.class.getName()).log(Level.SEVERE, null, ex);
             
        }
        return empresaClase.getIdCategoria()+"";
    }

    public String modificar(String[] datos) {
       
        try {
            String nombre = "'" + datos[1] + "'";
            
            empresaClase = new Categoria(nombre);
            empresaClase.setIdCategoria(Integer.parseInt(datos[0]));
            empresaClase.modificar();

        } catch (SQLException ex) {
            Logger.getLogger(CategoriaM.class.getName()).log(Level.SEVERE, null, ex);
        }

        return empresaClase.getIdCategoria()+"";
    }

    public String eliminar(String[] datos) {

        try {
            int idMovDoc=Integer.parseInt(datos[0]);
            Categoria e=new Categoria("");
            e.setIdCategoria(idMovDoc);
           
            e.eliminar();
                        
        } catch (SQLException ex) {
            Logger.getLogger(MovimientoTipoM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos[0];
    }
    public void llenarTabla(JTable tabla, String id) {
        empresaClase=new Categoria();
        empresaClase.llenarTabla(tabla, id);
    }
    
    public void generarCodigo(JTextField a)
    {
       GenerarCodigo gc = new GenerarCodigo();
       a.setText(gc.generarID("Categoria", "IdCategoria"));
    }
}
