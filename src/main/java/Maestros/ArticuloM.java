/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Maestros;

import Bean.Articulo;
import Bean.Articulo;
import Conexion.Mysql;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import Maestros.*;

/**
 *
 * @author Lab01-14
 */
public class ArticuloM implements Gestionable, Atributos {
    public static Connection cn = Mysql.getConection();
    Articulo ejecutor;

    
    @Override
    public String insertar(String[] datos) {
        try {
            int idunidadmedida = -1;
            Statement sent2 = cn.createStatement();
            ResultSet rs2 = sent2.executeQuery("select * from unidadmedida WHERE "+UNIMEDDES+"='" + datos[0] + "'");

            if (rs2.next()) {
                idunidadmedida = Integer.parseInt(rs2.getString(IDUNIDADMEDIDA));
            }

            String artnom = "'" + datos[1] + "'";
            double artcos = Double.parseDouble(datos[2]);
            double artigv = Double.parseDouble(datos[3]);
            
            ejecutor = new Articulo(idunidadmedida, artnom,artcos,artigv);
            
            ejecutor.insertar();
            

        } catch (SQLException ex) {
            Logger.getLogger(ArticuloM.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ejecutor.getIdArticulo()+"";

    }

    @Override
    public String modificar(String[] datos) {
        /*int idEmp=Integer.parseInt(datos[0].toString());
        String rucEmp=datos[1];
        String nomEmp=datos[2];
        String telEmp=datos[3];
        String dirEmp=datos[4];
        int idEmpGrup=Integer.parseInt(datos[5].toString());
        Empresa e=new Empresa(idEmp,rucEmp,nomEmp,telEmp,dirEmp,idEmpGrup);
        e.modificar();
        System.out.print("Modificado Empresa: ");
        return datos[0];*/
        try {

            int idunidadmedida = -1;
            Statement sent2 = cn.createStatement();
            ResultSet rs2 = sent2.executeQuery("select * from unidadmedida WHERE "+UNIMEDDES+"='" + datos[1] + "'");

            if (rs2.next()) {
                idunidadmedida = Integer.parseInt(rs2.getString(IDUNIDADMEDIDA));
            }

            String artnom = "'" + datos[2] + "'";
            double artcos=Double.parseDouble(datos[3]);
            double artigv=Double.parseDouble(datos[4]);
            //String artcan = datos[2];
            
            ejecutor = new Articulo(idunidadmedida, artnom, artcos, artigv);
            ejecutor.setIdArticulo(Integer.parseInt(datos[0]));
            ejecutor.modificar();
            
            
        } catch (SQLException ex) {
            Logger.getLogger(ArticuloM.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ejecutor.getIdArticulo()+"";

    }
    @Override
    public String eliminar(String[] datos) {

        try {
            int idEmp=Integer.parseInt(datos[0]);
            ejecutor=new Articulo();
            ejecutor.setIdArticulo(idEmp);
           
            ejecutor.eliminar();
                        
        } catch (SQLException ex) {
            Logger.getLogger(ArticuloM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos[0];
    }
    /*
    public static void main(args[])
    {
        EmpresaM e=new EmpresaM();
        datos[]={};
        e.insertar(datos)
    }*/

    
}
