/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Maestros;

/**
 *
 * @author Propietario
 */
public interface Atributos {
    
    //TABLA CATEGORIA
    final static String IDCATEGORIA= "IdCategoria";
    final static String CATNOM = "CatNom";
    final static String CATESTREG = "CatEstReg";
    
    
    
    //TABLA UNIDADMEDIDA
    final static String IDUNIDADMEDIDA = "IdUnidadMedida";
    final static String UNIMEDDES = "UniMedDes";
    final static String UNIMEDABR = "UniMedAbr";
    final static String UNIMEDESTREG = "UniMedEstReg";
    
    //TABLA ARTICULO
    final static String IDARTICULO="IdArticulo";
    final static String ARTNOM="ArtNom";
    final static String ARTCOS="ArtCos";
    final static String ARTIGV="ArtIgv";
    
    final static String ARTESTREG="ArtEstReg";
    
    //TABLA EMPRESA
    final static String IDEMPRESA="IdEmpresa";
    final static String EMPRUC="EmpRuc";
    final static String EMPNOM="EmpNom";
    final static String EMPTEL="EmpTel";
     final static String EMPPER="EmpPer";
    final static String EMPESTREG="EmpEstReg";
    
    //TABLA EMPRESAGRUPO
    final static String IDEMPRESAGRUPO="IdEmpresaGrupo";
    final static String EMPGRUNOM="EmpGruNom";
    final static String EMPGRUTEL="EmpGruTel";
    final static String EMPGRUESTREG="EmpGruEstReg";
    
    //TABLA EMPRESASEDE
    final static String IDEMPRESASEDE="IdEmpresaSede";
    final static String EMPSEDNOM="EmpSedNom";
    final static String EMPSEDDIN="EmpSedDin";
    final static String EMPSEDDIR="EmpSedDir";
    final static String EMPSEDESTREG="EmpSedEstReg";
    
    //TABLA LISTA
    final static String IDLISTA="IdLista";
    final static String LISTDES="ListDes";
    final static String LISTESTREG="ListEstReg";
    //TABLA MOVIMIENTOTIPO
    final static String IDMOVIMIENTOTIPO="IdMovimientoTipo";
    final static String MOVTIPDES="MovTipDes";
    final static String MOVTIPESTREG="MovTipEstReg";
    
    //TABLA ARTICULOPRECIO
    final static String IDARTICULOPRECIO="IdArticuloPrecio";
    final static String ARTPREDES="ArtPreDes";
    final static String ARTPREESTREG="ArtPreEstReg";
    
    //TABLA MOVIMIENTODOCUMENTO
    final static String IDMOVIMIENTODOCUMENTO="IdMovimientoDocumento";
    final static String MOVDOCDES="MovDocDes";
    final static String MOVDOCABR="MovDocAbr";
    final static String MOVDOCESTREG="MovDocEstReg";
    
    //TABLA MOVIMIENTODET
    final static String IDMOVIMIENTODETALLE="IdMovimientoDetalle";
    final static String IDMOVIMIENTOCABECERA="IdMovimientoCabecera";
    final static String MOVINGRESO="MovIngreso";
    final static String MOVDETCAN="MovDetCan";
    final static String MOVDETPRETOT="MovDetPreTot";
    final static String MOVDETIGV="MovDetIgv";
    final static String MOVDETESTREG="MovDetEstReg";
    //TABLA USUARIO
    final static String IDUSUARIO="IdUsuario";
    final static String USUNOM="UsuNom";
    final static String USUDNI="UsuDni";
    final static String USUCAR="UsuCar";
    final static String USUAPEPAT="UsuApePat";
    final static String USUAPEMAT="UsuApeMat";
    final static String USUNOMCOR="UsuNomCor";
    final static String USUCLA="UsuCla";
    final static String USUESTREG="UsuEstReg";

    //TABLA USUARIOACCCESO
    final static String IDUSUACCES="IdUsuAcces";
    final static String IDACCESO="IdAcceso";
    
    
//    
     //TABLA MOVIMIENTOCAB
    //final static String IDMOVIMIENTOCABECERA="IdMovimientoDetalle";
     //TABLA MOVIMIENTODETALLE
    final static String MOVCABESTREG="MovCabEstReg";
    
     //TABLA DOCUMENTO
    final static String IDDOCUMENTO="IdDocumento";
    final static String DOCDES="DocDes";
    final static String DOCNOM="DocNom";
    final static String DOCESTREG="DocEstReg";
    
    
    
    
}
