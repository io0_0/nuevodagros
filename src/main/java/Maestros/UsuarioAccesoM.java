/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Maestros;

import Bean.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author Equipo EPIS
 */
public class UsuarioAccesoM implements Gestionable {

    UsuarioAcceso usuario;

    public String insertar(String[] datos) {
        try {
            int idUsuario = Integer.parseInt(datos[0]) ;
            usuario = new UsuarioAcceso();
            usuario.setIdUsuario(idUsuario);
            usuario.insertar(datos);
          
        } catch (SQLException ex) {
            Logger.getLogger(UnidadMedidaM.class.getName()).log(Level.SEVERE, null, ex);
             
        }
        return usuario.getIdUsuAcces()+"";
    }

    public String modificar(String[] datos) {
       
        try {
            int idUsuario = Integer.parseInt(datos[0]);
            usuario = new UsuarioAcceso();
            usuario.setIdUsuario(idUsuario);
            usuario.modificar(datos);

        } catch (SQLException ex) {
            Logger.getLogger(UnidadMedidaM.class.getName()).log(Level.SEVERE, null, ex);
        }

        return usuario.getIdUsuAcces()+"";
    }

    @Override
    public String eliminar(String[] datos) {
        return null;
    }

   
    
  
}
