/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Maestros;

import Bean.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author Equipo EPIS
 */
public class UsuarioM implements Gestionable,Atributos {
    
    String sql_Cons = "SELECT * FROM usuario WHERE "+USUESTREG+" =1";
    Usuario usuarioClase;
    int datos =6;
   

    public String insertar(String[] datos) {
        try {
            datos[6]=Usuario.encryptar(datos[6]);
            for (int i = 0; i < datos.length; i++) {
                System.out.println(datos[i]);
            }
            usuarioClase = new Usuario("'"+datos[0]+"'","'"+datos[1]+"'","'"+ datos[2]+"'","'"+datos[3]+"'","'"+datos[4]+"'","'"+datos[5]+"'","'"+datos[6]+"'");
            usuarioClase.insertar();
          
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioM.class.getName()).log(Level.SEVERE, null, ex);
             
        }
        return usuarioClase.getIdUsuario()+"";
    }

    public String modificar(String[] datos) {
       
        try {
            datos[7]=Usuario.encryptar(datos[7]);
            usuarioClase = new Usuario("'"+datos[1]+"'","'"+datos[2]+"'","'"+ datos[3]+"'","'"+datos[4]+"'","'"+datos[5]+"'","'"+datos[6]+"'","'"+datos[7]+"'");
            usuarioClase.setIdUsuario(Integer.parseInt(datos[0]));
            usuarioClase.modificar();

        } catch (SQLException ex) {
            Logger.getLogger(UsuarioM.class.getName()).log(Level.SEVERE, null, ex);
        }

        return usuarioClase.getIdUsuario()+"";
    }

    public String eliminar(String[] datos) {

        try {
            int idUsuario=Integer.parseInt(datos[0]);
            Usuario e=new Usuario();
            e.setIdUsuario(idUsuario);
            e.eliminar();
                        
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos[0];
    }
    public void llenarTabla(JTable tabla, String id) {
        usuarioClase=new Usuario();
        usuarioClase.llenarTabla(tabla, id);
    }
    public void generarCodigo(JTextField a)
    {
       GenerarCodigo gc = new GenerarCodigo();
       a.setText(gc.generarID("Usuario", IDUSUARIO));
    }

    public void llenarTablaPestaña(JTable Pestania) {
        usuarioClase=new Usuario();
        usuarioClase.llenarTablaPestaña(Pestania);
    }
    public void llenarTablaPestaña(JTable Pestania,String id) {
        usuarioClase=new Usuario();
        usuarioClase.llenarTablaPestaña(Pestania,id);
    }

   
  
}
