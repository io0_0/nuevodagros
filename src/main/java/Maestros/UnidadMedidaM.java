/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Maestros;

import Bean.UnidadMedida;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author Equipo EPIS
 */
public class UnidadMedidaM implements Gestionable {

    UnidadMedida empresaClase;

    public String insertar(String[] datos) {
        try {
            String desUniMed = "'" + datos[0] + "'";
            String abrUniMed = "'" + datos[1] + "'";
            
            empresaClase = new UnidadMedida(desUniMed,abrUniMed);
            empresaClase.insertar();
          
        } catch (SQLException ex) {
            Logger.getLogger(UnidadMedidaM.class.getName()).log(Level.SEVERE, null, ex);
             
        }
        return empresaClase.getIdUnidadMedida()+"";
    }

    public String modificar(String[] datos) {
       
        try {
            String desUniMed = "'" + datos[1] + "'";
            String abrUniMed = "'" + datos[2] + "'";

            empresaClase = new UnidadMedida(desUniMed,abrUniMed);
            empresaClase.setIdUnidadMedida(Integer.parseInt(datos[0]));
            empresaClase.modificar();

        } catch (SQLException ex) {
            Logger.getLogger(UnidadMedidaM.class.getName()).log(Level.SEVERE, null, ex);
        }

        return empresaClase.getIdUnidadMedida()+"";
    }

    public String eliminar(String[] datos) {

        try {
            int idUniMed=Integer.parseInt(datos[0]);
            UnidadMedida e=new UnidadMedida("","");
            e.setIdUnidadMedida(idUniMed);
            e.eliminar();
                        
        } catch (SQLException ex) {
            Logger.getLogger(UnidadMedidaM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos[0];
    }
    public void llenarTabla(JTable tabla, String id) {
        empresaClase=new UnidadMedida();
        empresaClase.llenarTabla(tabla, id);
    }
    
    public void generarCodigo(JTextField a)
    {
       GenerarCodigo gc = new GenerarCodigo();
       a.setText(gc.generarID("UNIDADMEDIDA", "IdUnidadMedida"));
    }
    
  
}
