/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Maestros;

import Bean.*;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Edwin Alex
 */
public class MiModel extends DefaultTableModel {
    public MiModel(Object[][] data, String[] columnNames) {
        super(data, columnNames);
        
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        if(column==5)return true;
        return false;
    }
    
            Class[] types = new Class [] {
                String.class, String.class, String.class, String.class, String.class, java.lang.Boolean.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        
}
