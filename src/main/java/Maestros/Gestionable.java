/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Maestros;

/**
 *
 * @author Equipo EPIS
 */
public interface Gestionable{
    public String insertar(String[] datos);
    public String modificar(String[] datos); //id incluido en el array posicion 0
    public String eliminar(String[] datos);
}
