/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Maestros;

import Bean.Articulo;
import Bean.MovimientoCab;
import Bean.MovimientoDet;
import Bean.MovimientoDetalle;
import static Bean.Usuario.cn;
import static Maestros.Atributos.IDMOVIMIENTOCABECERA;
import static Maestros.Atributos.MOVDETESTREG;
import java.awt.Color;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author Equipo EPIS
 */
public class MovimientoDetalleM implements Gestionable {
    MovimientoDetalle empresaClase;

    public String insertar(String[] datos) {
        try {

            int idCab=Integer.parseInt(datos[0]);
            int idArt = empresaClase.consulta("ARTICULO","ArtNom",datos[1],"IdArticulo");
            System.out.println("-------->"+idArt);
            int movIng = Integer.parseInt(datos[2]);
            int movDetCan = Integer.parseInt(datos[3]);
            double movDetPreTot = Double.parseDouble(datos[4]);
            double movDetIgv = Double.parseDouble(datos[5]);
            //System.out.println(idArt);
            empresaClase = new MovimientoDetalle(idCab,idArt,movIng,movDetCan,movDetPreTot,movDetIgv);
            empresaClase.insertar();
          
        } catch (SQLException ex) {
            Logger.getLogger(MovimientoDetalleM.class.getName()).log(Level.SEVERE, null, ex);
             
        }
        return empresaClase.getIdMovimientoDetalle()+"";
    }

    public String modificar(String[] datos) {
       
        try {
            int idCab=Integer.parseInt(datos[1]);
            int idArt = empresaClase.consulta("ARTICULO","ArtNom",datos[2],"IdArticulo");
            int movIng = Integer.parseInt(datos[3]);
            int movDetCan = Integer.parseInt(datos[4]);
            double movDetPreTot = Double.parseDouble(datos[5]);
            double movDetIgv = Double.parseDouble(datos[6]);
            
             empresaClase = new MovimientoDetalle(idCab,idArt,movIng,movDetCan,movDetPreTot,movDetIgv);
            empresaClase.setIdMovimientoDetalle(Integer.parseInt(datos[0]));
            empresaClase.modificar();

        } catch (SQLException ex) {
            Logger.getLogger(MovimientoDetalleM.class.getName()).log(Level.SEVERE, null, ex);
        }

        return empresaClase.getIdMovimientoDetalle()+"";
    }

    public String eliminar(String[] datos) {

        try {
            int idDet=Integer.parseInt(datos[0]);
            MovimientoDetalle e=new MovimientoDetalle();
            e.setIdMovimientoDetalle(idDet);
            e.eliminar();
                        
        } catch (SQLException ex) {
            Logger.getLogger(MovimientoDetalleM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos[0];
    }
    public void llenarTabla(JTable tabla, String id) {
        empresaClase=new MovimientoDetalle();
        empresaClase.llenarTabla(tabla, id);
    }
    public void llenarCajaCab(JComboBox jComboBox1) {
        empresaClase=new MovimientoDetalle();
        empresaClase.llenarCajaCab(jComboBox1);
    }
     public void llenarCajaArt(JComboBox jComboBox1) {
        empresaClase=new MovimientoDetalle();
        empresaClase.llenarCajaArt(jComboBox1);
    }
    public void generarCodigo(JTextField a)
    {
       GenerarCodigo gc = new GenerarCodigo();
       a.setText(gc.generarID("movimientodet", "IdMovimientoDetalle"));
    }
    
     public String generarCodigo()
    {
       GenerarCodigo gc = new GenerarCodigo();
       return gc.generarID("movimientodet", "IdMovimientoDetalle");
    }
      

    public void llenarTablaFactura(JTable JTabla, String sad, ArrayList<MovimientoDet> detalles,MovimientoCab cab) {
      empresaClase=new MovimientoDetalle();
        llenarTablaSunat(JTabla, sad,detalles,cab); 
    }

    public void llenarTablaSunat(JTable tabla, String id, ArrayList<MovimientoDet> detalles,MovimientoCab cabe) {
         try {
                DecimalFormat df  = new DecimalFormat("#.00");
             String [] atri = {"N°","CANTIDAD", "DESCRIPCION","PRECIO UNITARIO","TOTAL","ParaLlevar"};
             int filas=10;
               MiModel model1 = new MiModel(null, atri);
               String sql_Cons="";
            Statement sent;
            if (id.length() != 0) {
                sql_Cons = "SELECT * FROM MOVIMIENTODET WHERE " + IDMOVIMIENTOCABECERA + " LIKE '%" + id + "%' and " + MOVDETESTREG + "=1";
                System.out.println(sql_Cons);
            }
            sent = cn.createStatement();
            ResultSet rs = sent.executeQuery(sql_Cons);
            Object[] fila = new Object[6];
            int i =0;
            while (rs.next()) {
                 i++;
                   fila[0]=""+i;
                  fila[1] = rs.getString("getMovDetCan");
                fila[2] = MovimientoCab.sacarDato("articulo", "idarticulo",""+rs.getInt("idarticulo"),"artnom");
                double venta=rs.getDouble("movdetpretot");
                double compra=rs.getDouble("movdetcostot");
                double valorUnitario= 0;
                if (venta>compra){
                    valorUnitario= venta/rs.getDouble("movdetcan");
                
                }
                else{
                                    valorUnitario= compra/rs.getDouble("movdetcan");

                
                }
                    fila[3]= ""+valorUnitario;
                double tot= Double.parseDouble(fila[3].toString())*Double.parseDouble(fila[1].toString());
               
                fila[3]= ""+String.format("%,.2f", valorUnitario); ;
                fila[4]=""+String.format("%,.2f", tot);
                model1.addRow(fila);
                fila[5] = false;

            }
            for (MovimientoDet de : detalles) {
                    i++;
                   fila[0]=""+i;
                  fila[1] = de.getMovDetCan();
                Articulo a = new Articulo(Integer.parseInt(de.getIdArticulo()));
                  fila[2] = a.getArtNom();
                double venta=Double.parseDouble(de.getMovDetPreTot());
                double compra=Double.parseDouble(de.getMovDetCosTot());
                double valorUnitario= 0;
                if (venta>compra){
                    valorUnitario= venta/Double.parseDouble(de.getMovDetCan());
                
                }
                else{
                    valorUnitario= compra/Double.parseDouble(de.getMovDetCan());
                }
                    fila[3]= ""+valorUnitario;
                double tot= Double.parseDouble(fila[3].toString())*Double.parseDouble(fila[1].toString());
                fila[3]= ""+String.format("%,.2f", valorUnitario); 
                fila[4]=""+String.format("%,.2f", tot);;
                model1.addRow(fila);
                fila[5]= false;
                
                
            }
            for(;i<filas;i++){
                fila = new String[5];
                fila[0]=" ";
                                fila[1]=" ";
                fila[2]=" ";
                fila[3]=" ";
                fila[4]=" ";

                  
                model1.addRow(fila);
            }
           tabla.setModel(model1);
           
            tabla.getColumnModel().getColumn(0).setPreferredWidth(40);
           tabla.getColumnModel().getColumn(1).setPreferredWidth(100);
        tabla.getColumnModel().getColumn(2).setPreferredWidth(750);
           tabla.getColumnModel().getColumn(3).setPreferredWidth(200);
           tabla.getColumnModel().getColumn(4).setPreferredWidth(100);
           tabla.setBackground(Color.white);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }	
        private double getPrecioUni(MovimientoDet detallito,MovimientoCab cab) throws SQLException {

        String sql_Cons = "SELECT * FROM lista where idEmpresaSede =" + cab.getIdEmpresaSede();
        String listilla = "";
        try {
            Statement sent;

            sent = cn.createStatement();
            System.out.println(sql_Cons);
            ResultSet rs = sent.executeQuery(sql_Cons);

            while (rs.next()) {
                listilla = rs.getString("idlista");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Statement sent;

        sent = cn.createStatement();
        sql_Cons = "Select * From articuloprecio where  idLista= " + listilla + " AND idArticulo=" + detallito.getIdArticulo();
        System.out.println(sql_Cons);

        ResultSet rs = sent.executeQuery(sql_Cons);
        double resp = 0;
        if (rs.next()) {
            resp = Double.parseDouble(rs.getString("ArtPreDes"));

        }
        return resp;
    }

    public void llenarCajaArtVentas(JComboBox JComboArt) {
       //To change body of generated methods, choose Tools | Templates.
       empresaClase=new MovimientoDetalle();
        empresaClase.llenarCajaArtTodos(JComboArt);
    }
  
}
