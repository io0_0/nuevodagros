/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Maestros;

import Bean.MovimientoDocumento;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author Equipo EPIS
 */
public class MovimientoDocumentoM implements Gestionable {

    MovimientoDocumento empresaClase;

    public String insertar(String[] datos) {
        try {
            String desMovDoc = "'" + datos[0] + "'";
            String abrMovDoc = "'" + datos[1] + "'";
          
            empresaClase = new MovimientoDocumento(desMovDoc,abrMovDoc);
            empresaClase.insertar();
          
        } catch (SQLException ex) {
            Logger.getLogger(MovimientoDocumentoM.class.getName()).log(Level.SEVERE, null, ex);
             
        }
        return empresaClase.getIdMovimientoDocumento()+"";
    }

    public String modificar(String[] datos) {
       
        try {
            String desMovDoc = "'" + datos[1] + "'";
            String abrMovDoc = "'" + datos[2] + "'";

            empresaClase = new MovimientoDocumento(desMovDoc,abrMovDoc);
            empresaClase.setIdMovimientoDocumento(Integer.parseInt(datos[0]));
            empresaClase.modificar();

        } catch (SQLException ex) {
            Logger.getLogger(MovimientoDocumentoM.class.getName()).log(Level.SEVERE, null, ex);
        }

        return empresaClase.getIdMovimientoDocumento()+"";
    }

    public String eliminar(String[] datos) {

        try {
            int idMovDoc=Integer.parseInt(datos[0]);
            MovimientoDocumento e=new MovimientoDocumento("","");
            e.setIdMovimientoDocumento(idMovDoc);
           
            e.eliminar();
                        
        } catch (SQLException ex) {
            Logger.getLogger(MovimientoTipoM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos[0];
    }
    public void llenarTabla(JTable tabla, String id) {
        empresaClase=new MovimientoDocumento();
        empresaClase.llenarTabla(tabla, id);
    }
    
    public void generarCodigo(JTextField a)
    {
       GenerarCodigo gc = new GenerarCodigo();
       a.setText(gc.generarID("MovimientoDocumento", "IdMovimientoDocumento"));
    }
}
