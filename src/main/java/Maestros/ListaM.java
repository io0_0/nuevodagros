/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Maestros;

import Bean.Lista;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author Equipo EPIS
 */
public class ListaM implements Gestionable {

    
    Lista empresaClase;

    public String insertar(String[] datos) {
        try {

            int idEmpSed = -1;
            /*Statement sent2 = cn.createStatement();
            ResultSet rs2 = sent2.executeQuery("select * from empresagrupo WHERE EmpGruNom='" + datos[0] + "'");

            if (rs2.next()) {
                gruEmp = Integer.parseInt(rs2.getString("IdEmpresaGrupo"));
            }*/
            idEmpSed = empresaClase.consulta("empresasede","EmpSedNom",datos[0],"IdEmpresaSede");

            String desLis = "'" + datos[1] + "'";
            

            empresaClase = new Lista(idEmpSed, desLis);
            empresaClase.insertar();
          
        } catch (SQLException ex) {
            Logger.getLogger(ListaM.class.getName()).log(Level.SEVERE, null, ex);
             
        }
        return empresaClase.getIdLista()+"";
    }

    public String modificar(String[] datos) {
       
        try {
            int idEmpSed = empresaClase.consulta("empresasede","EmpSedNom",datos[1],"IdEmpresaSede");
            /*Statement sent2 = cn.createStatement();
            ResultSet rs2 = sent2.executeQuery("select * from empresagrupo WHERE EmpGruNom='" + datos[1] + "'");

            if (rs2.next()) {
                gruEmp = Integer.parseInt(rs2.getString("IdEmpresaGrupo"));
            }*/
            String desLis = "'" + datos[2] + "'";

            empresaClase = new Lista(idEmpSed, desLis);
            empresaClase.setIdLista(Integer.parseInt(datos[0]));
           
            empresaClase.modificar();

        } catch (SQLException ex) {
            Logger.getLogger(ListaM.class.getName()).log(Level.SEVERE, null, ex);
        }

        return empresaClase.getIdLista()+"";
    }

    public String eliminar(String[] datos) {

        try {
            int idLis=Integer.parseInt(datos[0]);
            Lista e=new Lista(0,"");
            e.setIdLista(idLis);
            e.eliminar();
                        
        } catch (SQLException ex) {
            Logger.getLogger(ListaM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos[0];
    }
    public void llenarTabla(JTable tabla, String id) {
        empresaClase=new Lista();
        empresaClase.llenarTabla(tabla, id);
    }
    public void llenarCaja(JComboBox jComboBox1) {
        empresaClase=new Lista();
        empresaClase.llenarCaja(jComboBox1);
    }
    public void generarCodigo(JTextField a)
    {
       GenerarCodigo gc = new GenerarCodigo();
       a.setText(gc.generarID("Lista", "IdLista"));
    }
    
  
}
