/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Maestros;

import static Bean.Empresa.cn;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Anonimo
 */
public class GenerarCodigo {

    public GenerarCodigo() {
    }

    public String generarID(String nomTabla, String idTabla) {

        int n = 0;
        try {

            String id = "";
            Statement sent = cn.createStatement();
            ResultSet rs2 = sent.executeQuery("SELECT MAX( " + idTabla + " ) AS " + idTabla + " FROM " + nomTabla);

            if (rs2.next()) {
                id = rs2.getString(1);
            }
            if (id == null)//Cuando no hay ningun registro en la tabla
            {
                n = 1;//Problemas como hago el autoincrement cuando este sea 3,4 o mas
            } else//Sigue incrementando
            {
                n = Integer.parseInt(id);
                n += 1;
            }

        } catch (SQLException ex) {
            Logger.getLogger(GenerarCodigo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n + "";
    }
    //Si o si debe ser estado registro=1
    public String devuelveResultado(String nomTabla, String atributo,String buscar, String mostrar) {
        String re="";
        try {
            Statement sent2 = cn.createStatement();
            ResultSet rs2 = sent2.executeQuery("select * from "+nomTabla+" WHERE " + atributo + "='" + buscar + "'");
            
            if (rs2.next()) {
                re=rs2.getString(mostrar);
            }
        } catch (SQLException ex) {
            Logger.getLogger(GenerarCodigo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return re;
    }
}
