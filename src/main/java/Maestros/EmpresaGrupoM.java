/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Maestros;

import Bean.EmpresaGrupo;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author Equipo EPIS
 */
public class EmpresaGrupoM implements Gestionable {

    //String idEmp="IdEmpresa";
    /*String idEmpGrup = "IdEmpresaGrupo";
    String empRuc = "EmpRuc";
    String empNom = "EmpNom";
    String empTel = "EmpTel";
    String empDir = "EmpDir";
    String empEst = "EmpEstReg";*/
    EmpresaGrupo empresaClase;

    public String insertar(String[] datos) {
        try {
            String nomEmpGru = "'" + datos[0] + "'";
            String telEmpGru = "'" + datos[1] + "'";
          
            empresaClase = new EmpresaGrupo(nomEmpGru,telEmpGru);
            empresaClase.insertar();
          
        } catch (SQLException ex) {
            Logger.getLogger(EmpresaGrupo.class.getName()).log(Level.SEVERE, null, ex);
             
        }
        return empresaClase.getIdEmpresaGrupo()+"";
    }

    public String modificar(String[] datos) {
       
        try {
            
            String nomEmpGru = "'" + datos[1] + "'";
            String telEmpGru = "'" + datos[2] + "'";
            

            empresaClase = new EmpresaGrupo(nomEmpGru,telEmpGru);
            empresaClase.setIdEmpresaGrupo(Integer.parseInt(datos[0]));
            empresaClase.modificar();

        } catch (SQLException ex) {
            Logger.getLogger(EmpresaGrupoM.class.getName()).log(Level.SEVERE, null, ex);
        }

        return empresaClase.getIdEmpresaGrupo()+"";
    }

    public String eliminar(String[] datos) {

        try {
            int idEmpGru=Integer.parseInt(datos[0]);
            EmpresaGrupo e=new EmpresaGrupo("","");
            e.setIdEmpresaGrupo(idEmpGru);
           
            e.eliminar();
                        
        } catch (SQLException ex) {
            Logger.getLogger(EmpresaGrupoM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos[0];
    }
    public void llenarTabla(JTable tabla, String id) {
        empresaClase=new EmpresaGrupo();
        empresaClase.llenarTabla(tabla, id);
    }
    
    public void generarCodigo(JTextField a)
    {
       GenerarCodigo gc = new GenerarCodigo();
       a.setText(gc.generarID("EmpresaGrupo", "IdEmpresaGrupo"));
    }
    
  
}
