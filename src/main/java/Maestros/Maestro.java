/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Maestros;

import Bean.*;
/**
 *
 * @author Cesar
 */
public class Maestro {
    
    public MovimientoDocumentoM gestionarMovimientoDocumento(){
        return new MovimientoDocumentoM();
    }
    public EmpresaM gestionarEmpresa(){
        return new EmpresaM();
    }
    //public MovimientoTip gestionarMovimientoTip(){
    //    return new MovimientoTip();
    //}
    public UnidadMedidaM gestionarUnidadMedida() {
        return new UnidadMedidaM();
    }
    
    public ArticuloM gestionarArticulo(){
        return new ArticuloM();
    }
    public EmpresaSedeM gestionarEmpresaSede() {
         return new EmpresaSedeM ();
    }
    public ListaM gestionarLista() {
         return new ListaM ();
    }
    public EmpresaGrupoM gestionarEmpresaGrupo() {
         return new EmpresaGrupoM ();
    }
    public MovimientoTipoM gestionarMovimientoTipo() {
         return new MovimientoTipoM ();
    }
    public ArticuloPrecioM gestionarArticuloPrecio() {
         return new ArticuloPrecioM ();
    } 
     public MovimientoDetalleM gestionarMovimientoDetalle() {
         return new MovimientoDetalleM ();
    } 

    public UsuarioM gestionarUsuario() {
       return new UsuarioM();
    }

    public UsuarioAccesoM gestionarUsuarioAcceso() {
        return new UsuarioAccesoM();
    }

    public CategoriaM gestionarCategoria() {
       return new CategoriaM();
    }
}
