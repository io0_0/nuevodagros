/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Maestros;

import Bean.EmpresaSede;
import Bean.MovimientoCab;
import Bean.MovimientoDet;
import static Bean.Usuario.cn;
import static Maestros.Atributos.*;
import java.awt.Color;
import java.sql.SQLException;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author Equipo EPIS
 */
public class EmpresaSedeM implements Gestionable {

    //String idEmp="IdEmpresa";
    /*String idEmpGrup = "IdEmpresaGrupo";
    String empRuc = "EmpRuc";
    String empNom = "EmpNom";
    String empTel = "EmpTel";
    String empDir = "EmpDir";
    String empEst = "EmpEstReg";*/
    EmpresaSede empresaClase;

    public String insertar(String[] datos) {
        try {

          String idEmp = ""+-1;
            /*Statement sent2 = cn.createStatement();
            ResultSet rs2 = sent2.executeQuery("select * from empresagrupo WHERE EmpGruNom='" + datos[0] + "'");

            if (rs2.next()) {
                gruEmp = Integer.parseInt(rs2.getString("IdEmpresaGrupo"));
            }*/
            idEmp = empresaClase.consulta("empresa","EmpNom",datos[0],"IdEmpresa");

            String nomEmpSed = "'" + datos[1] + "'";
            String dinEmpSed = "'"+datos[2]+"'";
            String dirEmpSed = "'" + datos[3] + "'";
            

            empresaClase = new EmpresaSede(idEmp, nomEmpSed, dinEmpSed,dirEmpSed);
            empresaClase.insertar();
          
        } catch (SQLException ex) {
            Logger.getLogger(EmpresaSedeM.class.getName()).log(Level.SEVERE, null, ex);
             
        }
        return empresaClase.getIdEmpresaSede()+"";
    }

    public String modificar(String[] datos) {
       
        try {
            String idEmp = empresaClase.consulta("EMPRESA","EmpNom",datos[1],"IdEmpresa");
            /*Statement sent2 = cn.createStatement();
            ResultSet rs2 = sent2.executeQuery("select * from empresagrupo WHERE EmpGruNom='" + datos[1] + "'");

            if (rs2.next()) {
                gruEmp = Integer.parseInt(rs2.getString("IdEmpresaGrupo"));
            }*/
            String nomEmpSed = "'" + datos[2] + "'";
            String dinEmpSed = "'"+datos[3]+"'";
            String dirEmpSed = "'" + datos[4] + "'";
            
            empresaClase = new EmpresaSede(idEmp, nomEmpSed, dinEmpSed,dirEmpSed);
            empresaClase.setIdEmpresaSede(datos[0]);
            empresaClase.modificar();

        } catch (SQLException ex) {
            Logger.getLogger(EmpresaSedeM.class.getName()).log(Level.SEVERE, null, ex);
        }

        return empresaClase.getIdEmpresaSede()+"";
    }

    public String eliminar(String[] datos) {

        try {
            String idEmp=datos[0];
            EmpresaSede e=new EmpresaSede(""+0,"",""+0,"");
            e.setIdEmpresaSede(idEmp);
            e.eliminar();
                        
        } catch (SQLException ex) {
            Logger.getLogger(EmpresaSedeM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos[0];
    }
    public void llenarTabla(JTable tabla, String id, String idEmp) {
        empresaClase=new EmpresaSede();
        empresaClase.llenarTabla(tabla, id, idEmp);
    }
    public void llenarCaja(JComboBox jComboBox1) {
        empresaClase=new EmpresaSede();
        empresaClase.llenarCaja(jComboBox1);
    }
    public void generarCodigo(JTextField a)
    {
       GenerarCodigo gc = new GenerarCodigo();
       a.setText(gc.generarID("EmpresaSede", "IdEmpresaSede"));
    }
      public String generarCodigo()
    {
       GenerarCodigo gc = new GenerarCodigo();
       return gc.generarID("EmpresaSede", "IdEmpresaSede");
    }
    public String devuelveResultado(String buscar)
    {
       GenerarCodigo gc = new GenerarCodigo();
       return gc.devuelveResultado("Empresa", "IdEmpresa", buscar, "EmpNom");
       
    }
    

}



  

