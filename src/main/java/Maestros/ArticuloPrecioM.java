/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Maestros;

import Bean.ArticuloPrecio;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author Equipo EPIS
 */
public class ArticuloPrecioM implements Gestionable {
    ArticuloPrecio empresaClase;

    public String insertar(String[] datos) {
        try {

            int idArt = -1;
            int idList=-1;
            
            idArt = empresaClase.consulta("ARTICULO","ArtNom",datos[0],"IdArticulo");
            idList = empresaClase.consulta("LISTA","ListDes",datos[1],"IdLista");
            double desArtPre = Double.parseDouble(datos[2]);
            //System.out.println(idArt);
            empresaClase = new ArticuloPrecio(idArt, idList, desArtPre);
            empresaClase.insertar();
          
        } catch (SQLException ex) {
            Logger.getLogger(ArticuloPrecioM.class.getName()).log(Level.SEVERE, null, ex);
             
        }
        return empresaClase.getIdArticuloPrecio()+"";
    }

    public String modificar(String[] datos) {
       
        try {
            int idArt = empresaClase.consulta("ARTICULO","ArtNom",datos[1],"IdArticulo");
            int idList = empresaClase.consulta("LISTA","ListDes",datos[2],"IdLista");
            double desArtPre = Double.parseDouble(datos[3]);
            empresaClase = new ArticuloPrecio(idArt, idList, desArtPre);
            empresaClase.setIdArticuloPrecio(Integer.parseInt(datos[0]));
            empresaClase.modificar();

        } catch (SQLException ex) {
            Logger.getLogger(ArticuloPrecioM.class.getName()).log(Level.SEVERE, null, ex);
        }

        return empresaClase.getIdArticuloPrecio()+"";
    }

    public String eliminar(String[] datos) {

        try {
            int idArt=Integer.parseInt(datos[0]);
            ArticuloPrecio e=new ArticuloPrecio(0,0,0);
            e.setIdArticuloPrecio(idArt);
            e.eliminar();
                        
        } catch (SQLException ex) {
            Logger.getLogger(ArticuloPrecioM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos[0];
    }
    public void llenarTabla(JTable tabla, String id) {
        empresaClase=new ArticuloPrecio();
        empresaClase.llenarTabla(tabla, id);
    }
    public void llenarCajaArt(JComboBox jComboBox1) {
        empresaClase=new ArticuloPrecio();
        empresaClase.llenarCajaArt(jComboBox1);
    }
     public void llenarCajaList(JComboBox jComboBox1) {
        empresaClase=new ArticuloPrecio();
        empresaClase.llenarCajaList(jComboBox1);
    }
    public void generarCodigo(JTextField a)
    {
       GenerarCodigo gc = new GenerarCodigo();
       a.setText(gc.generarID("ArticuloPrecio", "IdArticuloPrecio"));
    }
    
  
}
