/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Maestros;

import Bean.Empresa;
import static Bean.Empresa.cn;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author Equipo EPIS
 */
public class EmpresaM implements Gestionable {

    //String idEmp="IdEmpresa";
    /*String idEmpGrup = "IdEmpresaGrupo";
    String empRuc = "EmpRuc";
    String empNom = "EmpNom";
    String empTel = "EmpTel";
    String empDir = "EmpDir";
    String empEst = "EmpEstReg";*/
    Empresa empresaClase;

    public String insertar(String[] datos) {
        try {

        String gruEmp =""+ -1;
            /*Statement sent2 = cn.createStatement();
            ResultSet rs2 = sent2.executeQuery("select * from empresagrupo WHERE EmpGruNom='" + datos[0] + "'");

            if (rs2.next()) {
                gruEmp = Integer.parseInt(rs2.getString("IdEmpresaGrupo"));
            }*/
            gruEmp = empresaClase.consulta("empresagrupo","EmpGruNom",datos[0],"IdEmpresaGrupo");

            String rucEmp = "'" + datos[1] + "'";
            String nomEmp = "'" + datos[2] + "'";
            String telEmp = "'" + datos[3] + "'";
            String perEmp = datos[4];
            
            String idDoc = empresaClase.consulta("documento","DocDes",datos[5],"IdDocumento");
            //String idDoc = "'" + datos[5] + "'";

            empresaClase = new Empresa(gruEmp, rucEmp, nomEmp, telEmp, perEmp, idDoc);
            empresaClase.insertar();
          
        } catch (SQLException ex) {
            Logger.getLogger(EmpresaM.class.getName()).log(Level.SEVERE, null, ex);
             
        }
        return empresaClase.getIdEmpresa()+"";
    }

    public String modificar(String[] datos) {
       
        try {
            String gruEmp = empresaClase.consulta("empresagrupo","EmpGruNom",datos[1],"IdEmpresaGrupo");
            /*Statement sent2 = cn.createStatement();
            ResultSet rs2 = sent2.executeQuery("select * from empresagrupo WHERE EmpGruNom='" + datos[1] + "'");

            if (rs2.next()) {
                gruEmp = Integer.parseInt(rs2.getString("IdEmpresaGrupo"));
            }*/
            
            String rucEmp = "'" + datos[2] + "'";
            String nomEmp = "'" + datos[3] + "'";
            String telEmp = "'" + datos[4] + "'";
            String perEmp = datos[5];
            
            String idDoc = empresaClase.consulta("documento","DocDes",datos[6],"IdDocumento");
            //String idDoc = "'" + datos[6] + "'";

            empresaClase = new Empresa(gruEmp, rucEmp, nomEmp, telEmp, perEmp, idDoc);
          
            empresaClase.setIdEmpresa(datos[0]);
           
            empresaClase.modificar();

        } catch (SQLException ex) {
            Logger.getLogger(EmpresaM.class.getName()).log(Level.SEVERE, null, ex);
        }

        return empresaClase.getIdEmpresa()+"";
    }

    public String eliminar(String[] datos) {

        try {
            String idEmp=datos[0];
            Empresa e=new Empresa(""+0,"","","",""+0,""+0);
            e.setIdEmpresa(idEmp);
           
            e.eliminar();
                        
        } catch (SQLException ex) {
            Logger.getLogger(EmpresaM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos[0];
    }
    public void llenarTabla(JTable tabla, String id, String  idg) {
        empresaClase=new Empresa();
        empresaClase.llenarTabla(tabla, id,idg);
    }
    public void llenarCaja(JComboBox jComboBox1) {
        empresaClase=new Empresa();
        empresaClase.llenarCaja(jComboBox1);
    }
     public void llenarCajaDocumento(JComboBox jComboBox1) {
        empresaClase=new Empresa();
        empresaClase.llenarCajaDocumento(jComboBox1);
    }
    public void generarCodigo(JTextField a)
    {
       GenerarCodigo gc = new GenerarCodigo();
       a.setText(gc.generarID("Empresa", "IdEmpresa"));
    }
    public String devuelveResultado(String buscar)
    {
       GenerarCodigo gc = new GenerarCodigo();
       return gc.devuelveResultado("EmpresaGrupo", "IdEmpresaGrupo", buscar, "EmpGruNom");
       
    }
    
  public void llenarCajaMismo(JComboBox jComboBox1){
        empresaClase=new Empresa();
        empresaClase.llenarCajaMismo(jComboBox1);
    }
}
