/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Maestros;

import Bean.MovimientoTipo;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author Equipo EPIS
 */
public class MovimientoTipoM implements Gestionable {

    MovimientoTipo empresaClase;

    public String insertar(String[] datos) {
        try {
            String descMovTipo = "'" + datos[0] + "'";
          
            empresaClase = new MovimientoTipo(descMovTipo);
            empresaClase.insertar();
          
        } catch (SQLException ex) {
            Logger.getLogger(MovimientoTipoM.class.getName()).log(Level.SEVERE, null, ex);
             
        }
        return empresaClase.getIdMovimientoTipo()+"";
    }

    public String modificar(String[] datos) {
       
        try {
            String descMovTipo = "'" + datos[1] + "'";
            

            empresaClase = new MovimientoTipo(descMovTipo);
            empresaClase.setIdMovimientoTipo(Integer.parseInt(datos[0]));
            empresaClase.modificar();

        } catch (SQLException ex) {
            Logger.getLogger(MovimientoTipoM.class.getName()).log(Level.SEVERE, null, ex);
        }

        return empresaClase.getIdMovimientoTipo()+"";
    }

    public String eliminar(String[] datos) {

        try {
            int idMovTipo=Integer.parseInt(datos[0]);
            MovimientoTipo e=new MovimientoTipo("");
            e.setIdMovimientoTipo(idMovTipo);
           
            e.eliminar();
                        
        } catch (SQLException ex) {
            Logger.getLogger(MovimientoTipoM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos[0];
    }
    public void llenarTabla(JTable tabla, String id) {
        empresaClase=new MovimientoTipo();
        empresaClase.llenarTabla(tabla, id);
    }
    
    public void generarCodigo(JTextField a)
    {
       GenerarCodigo gc = new GenerarCodigo();
       a.setText(gc.generarID("MovimientoTipo", "IdMovimientoTipo"));
    }
    
  
}
