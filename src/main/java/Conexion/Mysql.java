/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

/**
 *
 * @author Equipo EPIS
 */
public class Mysql {

    /*private static String db="polleria1";
     private static String user="tefa2";
     private static String passw="tefa";    
     private static String url="jdbc:mysql://192.168.1.47:3306/"+db;
     private static Connection c;*/
    /*public   static String db = "polleria2";
    private static String user = "root";
    private static String passw = "";
    //private final static String  urlFinal = "jdbc:mysql://201.240.245.76:3306/" ;
       private final static String  urlFinal = "jdbc:mysql://localhost:3306/" ;
*/
       
         public   static String db = "polleria2";
    private static String user = "root";
    private static String passw = "";
    //private final static String  urlFinal = "jdbc:mysql://201.240.245.76:3306/" ;
       //private final static String  urlFinal = "jdbc:mysql://192.168.1.225:3306/" ;
 private static final String urlFinal = "jdbc:mysql://localhost:3306/";
        private static String url = urlFinal+ db;
    
    private static Connection c;
//19
    public static Connection getConection() {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;

      try {
         // Apertura del fichero y creacion de BufferedReader para poder
         // hacer una lectura comoda (disponer del metodo readLine()).
         archivo = new File ("archivo.txt");
         fr = new FileReader (archivo);
         br = new BufferedReader(fr);

         // Lectura del fichero
         String linea;
         while((linea=br.readLine())!=null){
            //System.out.println(linea);
            db=linea;
         url = urlFinal+ db;
         }
      }
      catch(Exception e){
       //  e.printStackTrace();
      }finally{
         // En el finally cerramos el fichero, para asegurarnos
         // que se cierra tanto si todo va bien como si salta 
         // una excepcion.
         try{                    
            if( null != fr ){   
               fr.close();     
            }                  
         }catch (Exception e2){ 
            e2.printStackTrace();
         }
      }
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            c = DriverManager.getConnection(url, user, passw);
            //System.out.println("LERO LERO");
        } catch (Exception e) { 
            JOptionPane.showMessageDialog(null, "Error" + e.getMessage());
        }
        return c;
    }

    public static Connection getConection(String schema) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url1 = urlFinal + schema;

            c = DriverManager.getConnection(url1, user, passw);
           // System.out.println("LERO LERO");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error" + e.getMessage());
        }
        return c;
    }
    
    public static void main(String[] args) {
        getConection();
    }
}
